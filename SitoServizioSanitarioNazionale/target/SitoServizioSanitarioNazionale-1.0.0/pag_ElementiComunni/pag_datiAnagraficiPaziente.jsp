<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div class="row container-fluid ">
    <div class="col-md-5">
        <h3><b>Foto profilo attuale:</b></h3>
        <c:choose>
            <c:when test="${s_FotoAttivaPaziente ne null}">
                <img class="img-thumbnail rounded w-100" 
                     alt="Profilo"
                     src="${pageContext.request.contextPath}/DB_FOTO${s_FotoAttivaPaziente.path}${s_FotoAttivaPaziente.nome}"
                     onerror ="imgError(this);" >
            </c:when>
            <c:when test="${b_FotoAttivaPaziente ne null}">
                <img class="img-thumbnail rounded w-100" 
                     alt="Profilo"
                     src="${pageContext.request.contextPath}/DB_FOTO${b_FotoAttivaPaziente.path}${b_FotoAttivaPaziente.nome}"
                     onerror ="imgError(this);" >
            </c:when>
            <c:otherwise>
                <img class="img-thumbnail rounded w-100"
                     src="sito_IMG/blank-profile-picture.png"
                     alt="Profilo"
                     onerror="this.src='${pageContext.request.contextPath}\\sito_IMG\\blank-profile-picture.png';"/>
            </c:otherwise>
        </c:choose>
    </div>
    <div class="col-md-7">
        <div class="row container-fluid">
            <div class="col-md-4">
                <h5>
                    <b>
                    Nome:
                    </b>
                </h5>
            </div>
            <div class="col-md-8">
                <h5>
                    <c:choose>
                        <c:when test="${s_Paziente ne null}">
                            <c:out value="${s_Paziente.nome}"/> 
                        </c:when>
                        <c:when test="${b_Paziente ne null}">
                            <c:out value="${b_Paziente.nome}"/> 
                        </c:when>
                    </c:choose>
                </h5>
            </div>
        </div>
        <div class="row container-fluid">
            <div class="col-md-4">
                <h5>
                    <b>
                    Cognome: 
                    </b>
                </h5>
            </div>
            <div class="col-md-8">
                <h5>
                    <c:choose>
                        <c:when test="${s_Paziente ne null}">
                            <c:out value="${s_Paziente.cognome}"/>  
                        </c:when>
                        <c:when test="${b_Paziente ne null}">
                            <c:out value="${b_Paziente.cognome}"/>  
                        </c:when>
                    </c:choose>
                </h5>
            </div>
        </div>
        <div class="row container-fluid">
            <div class="col-md-4">
                <h5>
                    <b>
                    Nato il:
                    </b>
                </h5>
            </div>
            <div class="col-md-8">
                <h5>
                    <c:choose>
                        <c:when test="${s_Paziente ne null}">
                            <c:out value="${s_Paziente.nascita}"/> 
                        </c:when>
                        <c:when test="${b_Paziente ne null}">
                            <c:out value="${b_Paziente.nascita}"/> 
                        </c:when>
                    </c:choose>
                </h5>
            </div>
        </div>
        <div class="row container-fluid">
            <div class="col-md-4">
                <h5>
                    <b>
                    Sesso:
                    </b>
                </h5>
            </div>
            <div class="col-md-8">
                <h5>
                    <c:choose>
                        <c:when test="${s_Paziente ne null}">
                            <c:choose>
                                <c:when test="${s_Paziente.sesso}">
                                    Maschio
                                </c:when>
                                <c:otherwise>
                                    Femmina
                                </c:otherwise>
                            </c:choose>
                        </c:when>
                        <c:when test="${b_Paziente ne null}">
                            <c:choose>
                                <c:when test="${b_Paziente.sesso}">
                                    Maschio
                                </c:when>
                                <c:otherwise>
                                    Femmina
                                </c:otherwise>
                            </c:choose>
                        </c:when>
                    </c:choose>
                </h5>
            </div>
        </div>
        <div class="row container-fluid">
            <div class="col-md-4">
                <h5>
                    <b>
                    Codice fiscale: 
                    </b>
                </h5>
            </div>
            <div class="col-md-8">
                <h5>
                    <c:choose>
                        <c:when test="${s_Paziente ne null}">
                            <c:out value="${s_Paziente.codF}"/> 
                        </c:when>
                        <c:when test="${b_Paziente ne null}">
                            <c:out value="${b_Paziente.codF}"/> 
                        </c:when>
                    </c:choose>
                </h5>
            </div>
        </div>
        <div class="row container-fluid">
            <div class="col-md-4">
                <h5>
                    <b>
                    Residente a:
                    </b>
                </h5>
            </div>
            <div class="col-md-8">
                <h5>
                    <c:choose>
                        <c:when test="${s_Paziente ne null}">
                            <c:out value="${s_Paziente.citta}"/> 
                        </c:when>
                        <c:when test="${b_Paziente ne null}">
                            <c:out value="${b_Paziente.citta}"/> 
                        </c:when>
                    </c:choose>
                </h5>
            </div>
        </div>
        <div class="row container-fluid">
            <div class="col-md-4">
                <h5>
                    <b>
                    Email: 
                    </b>
                </h5>
            </div>
            <div class="col-md-8">
                <h5>
                    <c:choose>
                        <c:when test="${s_Paziente ne null}">
                            <c:out value="${s_Paziente.email}"/>
                        </c:when>
                        <c:when test="${b_Paziente ne null}">
                            <c:out value="${b_Paziente.email}"/>
                        </c:when>
                    </c:choose>
                </h5>
            </div>
        </div>
        <div class="row container-fluid">
            <div class="col-md-4">
                <h5>
                    <b>
                    Abita in provincia di: 
                    </b>
                </h5>
            </div>
            <div class="col-md-8">
                <h5>
                    <c:choose>
                        <c:when test="${s_Paziente ne null}">
                            <c:out value="${s_PazienteViveProvincia.nome}"/> 
                        </c:when>
                        <c:when test="${b_Paziente ne null}">
                            <c:out value="${b_PazienteViveProvincia.nome}"/> 
                        </c:when>
                    </c:choose>
                </h5>
            </div>
        </div>
    </div> 
</div>