CREATE TABLE Farmaco( 
	id 				INTEGER NOT NULL GENERATED ALWAYS AS IDENTITY,
	nome			VARCHAR(30),
	descrizione		VARCHAR(250),
	CONSTRAINT Farmaco_PK PRIMARY KEY (id)
	);

CREATE TABLE Farmacia( 
	pIVA			VARCHAR(11) NOT NULL,
	nome			VARCHAR(30),
	email			VARCHAR(50) NOT NULL,
	password		VARCHAR(65) NOT NULL,
	salt			VARCHAR(65) NOT NULL,
	CONSTRAINT Farmacia_PK PRIMARY KEY (pIVA),
	CONSTRAINT Farmacia_email_unique UNIQUE (EMAIL)
	);

CREATE TABLE Esame(
	id				INTEGER NOT NULL GENERATED ALWAYS AS IDENTITY,
	nome			VARCHAR(40),
	CONSTRAINT Esame_PK PRIMARY KEY (id)
	);

CREATE TABLE SSProvinciale(
	id				INTEGER NOT NULL GENERATED ALWAYS AS IDENTITY,
	nome			VARCHAR(30), 
	password		VARCHAR(65) NOT NULL,
	salt			VARCHAR(65) NOT NULL,
	CONSTRAINT SSProvinciale_PK PRIMARY KEY (id),
	CONSTRAINT SSProvinciale_nome_unique UNIQUE (nome)
	);

CREATE TABLE ErogaESSSP(
	id_SSP			INTEGER NOT NULL,
	id_Esame		INTEGER NOT NULL,
	CONSTRAINT ErogaESSSP_PK PRIMARY KEY (id_SSP,id_Esame),
	
	CONSTRAINT ErogaESSSP_idSSP_FK FOREIGN KEY(id_SSP)
		REFERENCES SSProvinciale (id) ON DELETE CASCADE,
	CONSTRAINT Esame_idSSP_FK FOREIGN KEY(id_Esame)
		REFERENCES Esame (id) ON DELETE CASCADE
	);
	
CREATE TABLE Persona(
	codF			VARCHAR(16) NOT NULL,
	nome 			VARCHAR(30),
	cognome			VARCHAR(30),
	password		VARCHAR(65) NOT NULL,
	salt			VARCHAR(65) NOT NULL,
	nascita			DATE,
	citta			VARCHAR(50),
	sesso			BOOLEAN,
	email			VARCHAR(50) NOT NULL,
	vive_id_SSP		INTEGER NOT NULL,
	CONSTRAINT Persona_PK PRIMARY KEY (codF),
	
	CONSTRAINT Persona_vive_id_SSP_FK FOREIGN KEY(vive_id_SSP)
		REFERENCES SSProvinciale (id) ON UPDATE RESTRICT
	);

CREATE TABLE MedicoSpec(
	codF			VARCHAR(16) NOT NULL,
	CONSTRAINT MedicoSpec_PK PRIMARY KEY (codF),
	
	CONSTRAINT MedicoSpec_codF_FK FOREIGN KEY(codF)
		REFERENCES Persona (codF) ON UPDATE RESTRICT
	);
	
CREATE TABLE MedicoBase(
	codF			VARCHAR(16) NOT NULL,
	lavora_id_SSP	INTEGER NOT NULL,
	CONSTRAINT MedicoBase_PK PRIMARY KEY (codF),
	
	CONSTRAINT MedicoBase_codF_FK FOREIGN KEY(codF)
		REFERENCES Persona (codF) ON UPDATE RESTRICT,
	CONSTRAINT MedicoBase_lavoraSSP_FK FOREIGN KEY(lavora_id_SSP)
		REFERENCES SSProvinciale (id) ON UPDATE RESTRICT
	);
	
CREATE TABLE Foto(
	id				INTEGER NOT NULL GENERATED ALWAYS AS IDENTITY,
	id_Persona		VARCHAR(16) NOT NULL,
	data			DATE,
	path			VARCHAR(100),
	nome 			VARCHAR(50),
	scelta			BOOLEAN,
	CONSTRAINT Foto_PK PRIMARY KEY (id,id_Persona),
	
	CONSTRAINT Foto_idPE_FK FOREIGN KEY(id_Persona)
		REFERENCES Persona(codF) ON UPDATE RESTRICT
	);

CREATE TABLE CartellaClinicaPerMB(
	id				INTEGER NOT NULL GENERATED ALWAYS AS IDENTITY,
	dataInizio		DATE NOT NULL,
	dataFine		DATE ,
	id_MedicoBase	VARCHAR(16) NOT NULL,
	id_Persona		VARCHAR(16) NOT NULL,
	vistoMedicoBase	BOOLEAN NOT NULL,
	CONSTRAINT CartellaClinica_PK PRIMARY KEY (id),
	
	CHECK(dataInizio <= dataFine),
	
	CONSTRAINT CartellaClinica_idMB_FK FOREIGN KEY(id_MedicoBase)
		REFERENCES MedicoBase(codF) ON UPDATE RESTRICT,
	CONSTRAINT CartellaClinica_idPA_FK FOREIGN KEY(id_Persona)
		REFERENCES Persona(codF) ON UPDATE RESTRICT 
	);
	
CREATE TABLE Ricetta(
	id					INTEGER NOT NULL GENERATED ALWAYS AS IDENTITY,
	scadenza			DATE,
	dataPrescrizione	DATE,
	dataEvalsa			DATE,
	prezzo				REAL,
	quantita			INTEGER,
	id_CartellaClinica	INTEGER NOT NULL,
	id_Farmaco			INTEGER NOT NULL,
	id_Farmacia			VARCHAR(11),
	vistoPaziente		BOOLEAN NOT NULL,
	CONSTRAINT Ricetta_PK PRIMARY KEY (id),
	
	CHECK(dataPrescrizione< scadenza),
	CHECK(( dataPrescrizione<= dataEvalsa) OR (dataEvalsa IS NULL)),
	CHECK((dataEvalsa<= scadenza) OR (dataEvalsa IS NULL)),
	
	CONSTRAINT Ricetta_idCU_FK FOREIGN KEY(id_CartellaClinica)
		REFERENCES CartellaClinicaPerMB(id) ON UPDATE RESTRICT,
	CONSTRAINT Ricetta_idFO_FK FOREIGN KEY(id_Farmaco)
		REFERENCES Farmaco(id) ON UPDATE RESTRICT,
	CONSTRAINT Ricetta_idFA_FK FOREIGN KEY(id_Farmacia)
		REFERENCES Farmacia(pIVA) ON UPDATE RESTRICT
	);

CREATE TABLE VisitaMB(
	id					INTEGER NOT NULL GENERATED ALWAYS AS IDENTITY,
	prescrizione		VARCHAR(250),
	riassuntoVisita		VARCHAR(250),
	data				DATE,
	dataPrescrizione 	DATE,
	id_Persona			VARCHAR(16) NOT NULL,
	id_MedicoBase		VARCHAR(16)	NOT NULL,
	vistoPaziente		BOOLEAN NOT NULL,
	vistoReportPaziente	BOOLEAN,
	CONSTRAINT VisitaMB_PK PRIMARY KEY (id),
	
	CHECK(data>= dataPrescrizione),
	
	CONSTRAINT VisitaMB_idPE_FK FOREIGN KEY(id_Persona)
		REFERENCES Persona(codF) ON UPDATE RESTRICT,
	CONSTRAINT VisitaMB_idMB_FK FOREIGN KEY(id_MedicoBase)
		REFERENCES MedicoBase(codF) ON UPDATE RESTRICT
	);
	
CREATE TABLE VisitaMSEReport(
	id						INTEGER NOT NULL GENERATED ALWAYS AS IDENTITY,
	prescrizione 			VARCHAR(250),
	riassuntoVisita			VARCHAR(250),
	data					DATE,
	dataPrescrizione 		DATE,
	tichet					REAL,
	report					VARCHAR(250),
	pagato					BOOLEAN,
	id_Persona				VARCHAR(16) NOT NULL,
	id_MedicoSpec			VARCHAR(16)	NOT NULL,
	vistoPaziente			BOOLEAN NOT NULL,
	vistoReportPaziente		BOOLEAN,
	vistoReportMedicoBase 	BOOLEAN,
	CONSTRAINT VisitaMSEReport_PK PRIMARY KEY (id),
	
	CHECK(data>= dataPrescrizione),
	
	CONSTRAINT VisitaMSEReport_idPE_FK FOREIGN KEY(id_Persona)
		REFERENCES Persona(codF) ON UPDATE RESTRICT,
	CONSTRAINT VisitaMSEReport_idMS_FK FOREIGN KEY(id_MedicoSpec)
		REFERENCES MedicoSpec(codF) ON UPDATE RESTRICT
	);
	
CREATE TABLE PrescrizioneEsameEReferto(
	id					INTEGER NOT NULL GENERATED ALWAYS AS IDENTITY,
	data				DATE,
	dataPrescrizione 	DATE,
	risultato			VARCHAR(250),
	tichet 				REAL,
	pagato				BOOLEAN,
	cura				VARCHAR(250),
	richiamo			BOOLEAN,
	id_CartellaClinica 	INTEGER NOT NULL,
	id_MedicoSpec				VARCHAR(16) ,
	id_Esame				INTEGER NOT NULL,
	vistoPaziente			BOOLEAN NOT NULL,
	vistoReportPaziente		BOOLEAN,
	vistoReportoMedicoBase 	BOOLEAN,
	CONSTRAINT Prescrizione_PK PRIMARY KEY (id),
	
	CHECK(data>= dataPrescrizione),
	
	CONSTRAINT Prescrizione_idCU_FK FOREIGN KEY(id_CartellaClinica)
		REFERENCES CartellaClinicaPerMB(id) ON UPDATE RESTRICT,
	CONSTRAINT Prescrizione_idMS_FK FOREIGN KEY(id_MedicoSpec)
		REFERENCES MedicoSpec(codF) ON UPDATE RESTRICT,
	CONSTRAINT Prescrizione_idES_FK FOREIGN KEY(id_Esame)
		REFERENCES Esame(id) ON UPDATE RESTRICT
	);
