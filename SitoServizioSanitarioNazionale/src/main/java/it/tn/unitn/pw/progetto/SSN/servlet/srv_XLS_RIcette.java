/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.tn.unitn.pw.progetto.SSN.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import it.tn.unitn.pw.progetto.SSN.DAO.common.exception.DAOException;
import it.tn.unitn.pw.progetto.SSN.DAO.common.exception.DAOFactoryException;
import it.tn.unitn.pw.progetto.SSN.DAO.common.factory.DAOFactory;
import it.tn.unitn.pw.progetto.SSN.DAO.persistence.*;
import it.tn.unitn.pw.progetto.SSN.persistence.entities.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Calendar;
import java.sql.Date;//sql
import org.apache.poi.ss.usermodel.Workbook;

import it.tn.unitn.pw.progetto.SSN.Genera.XLS.*;



/**
 *
 * @author Brian
 */
public class srv_XLS_RIcette extends HttpServlet {

     //Questi sono i tipi di "schemi" di DAO che uso nel body
    private Ricetta_DAO ricette_DAO;
    private Persona_DAO persona_DAO;
    private CartellaClinicaPerMB_DAO cartella_DAO;
    private MedicoBase_DAO mb_DAO;
    private Farmacia_DAO farmacia_DAO;
    private Farmaco_DAO farmaco_DAO;
    //Serve per usare i metodi del DAO e inizializzare (grazie al cazzo, si chiama init) le cose del DAO
    @Override
    public void init() throws ServletException{
        DAOFactory daoFactory = (DAOFactory) super.getServletContext().getAttribute("daoFactory");
        if(daoFactory==null){
            throw new ServletException("Impossible to get daoFactory");
        }
        try{
            //Qui ci vanno tutte le classi del DAO che vanno importate.
            ricette_DAO = daoFactory.getDAO(Ricetta_DAO.class);
            persona_DAO = daoFactory.getDAO(Persona_DAO.class);
            cartella_DAO = daoFactory.getDAO(CartellaClinicaPerMB_DAO.class);
            mb_DAO = daoFactory.getDAO(MedicoBase_DAO.class);
            farmacia_DAO = daoFactory.getDAO(Farmacia_DAO.class);
            farmaco_DAO = daoFactory.getDAO(Farmaco_DAO.class);
        }catch(DAOFactoryException ex){
            throw new ServletException("Impossible to get daoFactory", ex);
        }
    }
    
     protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
   
        //Si prende il cittadino partendo dall'id della provincia
        SSProvinciale ssProvincia = (SSProvinciale) request.getSession(false).getAttribute("p_ssProvinciale");
        try{

            LocalDate oggi = LocalDate.now();
            //Lista dei cittadini della provincia
            List<Persona> persone = persona_DAO.getBySSProvincialeId_Vive(ssProvincia.getId());
            ArrayList<Ricette> coseXLS = new ArrayList<>(); 
            for(Persona p : persone){
                //Bisogna iterare le persone per prendere le ricette di ogni persona
                List<Ricetta> ricette = ricette_DAO.getByPazienteId_Ririrate(p.getCodF()); //Qua ci va il codice fiscale delle persone

                for(Ricetta r : ricette){
                    //Bisogna mettere un if per retrivare tutte le ricette erogate oggi
                    //setto le due ore uguali così compara solo il giorno in pratica ;)

                    //LocalDate dataRicetta=(r.getDataPrescrizione().toLocalDate()); //--->>RITIRATE E PRESCRITTE OGGI
                    LocalDate dataRicetta=(r.getDataEvalsa().toLocalDate());         //--->>RITIRATE OGGI
                    if( dataRicetta.isEqual(oggi)){

                        //In ogni ricetta c'è l'id della cartella clinica e in essa c'è il codice fiscale del medico di base
                        //e con quello si usa il metodo
                        CartellaClinicaPerMB cartella = cartella_DAO.getByPrimaryKey(r.getId_CartellaClinica());//qui ci va l'id della cartella clinica
                        MedicoBase medico = mb_DAO.getByPrimaryKey(cartella.getId_MedicoBase());//qui ci va l'id del medico di base preso dalla cartella clinica

                        Farmaco farmaco = farmaco_DAO.getByPrimaryKey(r.getId_Farmaco());
                        Farmacia farmacia = farmacia_DAO.getByPrimaryKey(r.getId_Farmacia());
                        //ora che ho tutte le info di una ricetta di oggi, devo solo metterle nell'xls.

                        //Creo l'oggetto con la ricetta corrente
                        Ricette corrente = new Ricette(r.getId(), r.getDataPrescrizione(), farmaco.getNome(), farmacia.getNome(), medico.getNome()+" "+medico.getCognome(), p.getNome()+" "+p.getCognome(), r.getPrezzoFarmacia());
                        //Aggiungo la ricetta corrente all'array List
                        coseXLS.add(corrente);
                    }   
                }
            }
            //Qua creo l'XLS con tutte le ricette
            Xls.Xls(coseXLS, response);


        }catch(DAOException e){
            System.out.println("Error servlet XLS: "+e);
            response.sendRedirect("SSP.jsp");
        }

    }
    

     /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
