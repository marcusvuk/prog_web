/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.tn.unitn.pw.progetto.SSN.DAO.persistence;

import java.util.List;
import java.sql.Date;
        
import it.tn.unitn.pw.progetto.SSN.persistence.entities.VisitaMB;
import it.tn.unitn.pw.progetto.SSN.DAO.common.DAO;
import it.tn.unitn.pw.progetto.SSN.DAO.common.exception.DAOException;
/**
 *
 * @author Brian
 */
public interface VisitaMB_DAO extends DAO<VisitaMB, Integer>{
/*============================================================================*/    
/*                         METODO INSERT                                      */
/*============================================================================*/
    public Integer insert(VisitaMB visita)throws DAOException;
/*============================================================================*/    
/*============================================================================*/    
/*                         METODO UPDATE                                      */
/*============================================================================*/
    public void update(VisitaMB visita)throws DAOException;
    public void update_EsequzioneVisita(Integer id,Date data)throws DAOException;
    public void update_VistoPaziente(Integer id,Boolean visto)throws DAOException;
    public void update_VistoRepotDaPaziente(Integer id,Boolean visto)throws DAOException;
/*============================================================================*/
/*============================================================================*/    
/*                         METODO GET Da ID                                   */
/*============================================================================*/
    public List<VisitaMB> getByPazienteId(String id)throws DAOException;
    public List<VisitaMB> getByMedicoBaseId(String id)throws DAOException; 
/*============================================================================*/ 
/*============================================================================*/    
/*                         METODO GET Da ID fatte/da fare                     */
/*============================================================================*/
    public List<VisitaMB> getByPazienteId_Fatte(String id)throws DAOException;
    public List<VisitaMB> getByPazienteId_DaFare(String id)throws DAOException;
/*============================================================================*/   

}
