/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.tn.unitn.pw.progetto.SSN.DAO.persistence.jdbc;

import java.sql.Date;
import java.text.ParseException;
import java.util.Calendar;

import java.sql.Date;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import it.tn.unitn.pw.progetto.SSN.DAO.common.persistence.dao.jdbc.JDBCDAO;
import it.tn.unitn.pw.progetto.SSN.DAO.common.exception.*;
import it.tn.unitn.pw.progetto.SSN.DAO.persistence.*;
import it.tn.unitn.pw.progetto.SSN.persistence.entities.*;
/**
 *
 * @author Brian
 */
public class JDBC_Ricetta_DAO extends JDBCDAO<Ricetta, Integer> implements Ricetta_DAO {
/*============================================================================*/    
/*                                      METODI COSTRUTTORI                    */
/*============================================================================*/
    public JDBC_Ricetta_DAO(Connection con) {
        super(con);
    }
/*============================================================================*/
    
/*============================================================================*/    
/*                      METODI GETTER DAO IMPLEMENTATION                      */
/*============================================================================*/
    @Override
    public Long getCount() throws DAOException {
        try (Statement stmt = CON.createStatement()) {
            ResultSet counter = stmt.executeQuery( 
                     "  SELECT COUNT(*) "               +
                     "  FROM Ricetta "     );
            if (counter.next()) {
                return counter.getLong(1);
            }
        } catch (SQLException ex) {
            throw new DAOException( " Immpossibile contare gli elementi di Ricetta  " , ex);
        }
        return 0L;
    }
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    @Override
    public Ricetta getByPrimaryKey(Integer primaryKey) throws DAOException {
        if (primaryKey == null) {
            throw new DAOException( " primaryKey è null " );
        }
        try (PreparedStatement stm = CON.prepareStatement(
                                     "  SELECT *  "         +
                                     "  FROM Ricetta  "     +
                                     "  WHERE id = ? "      )) {
            // imposto i valori
            stm.setInt(1, primaryKey);
            
            try (ResultSet rs = stm.executeQuery()) {
                rs.next();
                Ricetta ricetta = new Ricetta();
                ricetta.setId                   (rs.getInt      ( "id" ));
                ricetta.setScadenza             (rs.getDate     ( "scadenza" ));
                ricetta.setDataPrescrizione     (rs.getDate     ( "dataPrescrizione" ));
                ricetta.setDataEvalsa           (rs.getDate     ( "dataEvalsa" ));
                ricetta.setQuantita             (rs.getInt      ( "quantita" ));
                ricetta.setPrezzoFarmacia       (rs.getDouble   ( "prezzo" ));
                ricetta.setId_CartellaClinica   (rs.getInt      ( "id_CartellaClinica" ));
                ricetta.setId_Farmaco           (rs.getInt      ( "id_Farmaco" ));
                ricetta.setId_Farmacia          (rs.getString   ( "id_Farmacia" ));
                ricetta.setVistoPaziente        (rs.getBoolean  ( "vistoPaziente" ));
                
                return ricetta;
            }
        } catch (SQLException ex) {
            throw new DAOException( " Impossibile perendere la Ricetta indicato dalla primary key " , ex);
        }
    }
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    @Override
    public List<Ricetta> getAll() throws DAOException {
        List<Ricetta> listaRicetta = new ArrayList<>();

        try (Statement stm = CON.createStatement()) {
            try (ResultSet rs = stm.executeQuery(
                                 "  SELECT *  "         +
                                 "  FROM Ricetta  "     )){
                
                while (rs.next()) {
                    Ricetta ricetta = new Ricetta();
                    
                    ricetta.setId                   (rs.getInt      ( "id" ));
                    ricetta.setScadenza             (rs.getDate     ( "scadenza" ));
                    ricetta.setDataPrescrizione     (rs.getDate     ( "dataPrescrizione" ));
                    ricetta.setDataEvalsa           (rs.getDate     ( "dataEvalsa" ));
                    ricetta.setQuantita             (rs.getInt      ( "quantita" ));
                    ricetta.setPrezzoFarmacia       (rs.getDouble   ( "prezzo" ));
                    ricetta.setId_CartellaClinica   (rs.getInt      ( "id_CartellaClinica" ));
                    ricetta.setId_Farmaco           (rs.getInt      ( "id_Farmaco" ));
                    ricetta.setId_Farmacia          (rs.getString   ( "id_Farmacia" ));
                    ricetta.setVistoPaziente        (rs.getBoolean  ( "vistoPaziente" ));

                    listaRicetta.add(ricetta);
                }
            }
        } catch (SQLException ex) {
            throw new DAOException( " Imposibile estrarre tutta la lista di Ricetta  " , ex);
        }

        return listaRicetta;
    }
/*============================================================================*/
   
/*============================================================================*/    
/*                      METODI QUERY                                          */
/*============================================================================*/
//------------------------------------------------------------------------------
//                      INSERT
//------------------------------------------------------------------------------
    @Override
    public Integer insert(Ricetta ricetta) throws DAOException {
        if (ricetta == null) {
            throw new DAOException( " La ricetta è null " );
        }
        try (PreparedStatement ps = CON.prepareStatement(
                     " INSERT INTO Ricetta "  +
                                 "( scadenza  "             +    "  ,  "    +
                                 "  dataPrescrizione  "     +    "  ,  "    +
                                 "  dataEvalsa  "           +    "  ,  "    +
                                 "  quantita  "             +    "  ,  "    +
                                 "  prezzo  "               +    "  ,  "    +
                                 "  id_CartellaClinica  "   +    "  ,  "    +
                                 "  id_Farmaco  "           +    "  ,  "    +
                                 "  id_Farmacia  "          +    "  ,  "    +
                                 "  vistoPaziente  "        +    "  )  "    +  
                     " VALUES (?,?,?,?,?,?,?,?,?) " , Statement.RETURN_GENERATED_KEYS)) {
            
            // imposto i valori
            ps.setDate      (1 , ricetta.getScadenza());
            ps.setDate      (2 , ricetta.getDataPrescrizione());
            ps.setDate      (3 , ricetta.getDataEvalsa());
            ps.setInt       (4 , ricetta.getQuantita());
            ps.setDouble    (5 , ricetta.getPrezzoFarmacia());
            ps.setInt       (6 , ricetta.getId_CartellaClinica());
            ps.setInt       (7 , ricetta.getId_Farmaco());
            ps.setString    (8 , ricetta.getId_Farmacia());
            ps.setBoolean   (9 , ricetta.getVistoPaziente());
            
            ps.executeUpdate();
            
            ResultSet rs = ps.getGeneratedKeys();
            if (rs.next()) {
                ricetta.setId(rs.getInt(1));
            }
            
            return ricetta.getId();
        } catch (SQLException ex) {
            throw new DAOException( " Impossibile aggiungere un nuovo record di Ricetta " , ex);
        }
    }
//------------------------------------------------------------------------------
//                      UPDATE
//------------------------------------------------------------------------------
    @Override
    public void update(Ricetta ricetta) throws DAOException {
        if (ricetta == null) {
            throw new DAOException( " La ricetta è null " );
        }
        try (PreparedStatement ps = CON.prepareStatement(
                    "  UPDATE Ricetta  "  +
                    "  SET   scadenza = ?  "             +     "  ,  "    +
                          "  dataPrescrizione = ?  "     +     "  ,  "    +
                          "  dataEvalsa = ?  "           +     "  ,  "    +
                          "  quantita = ?  "             +     "  ,  "    +
                          "  prezzo = ?  "               +     "  ,  "    +
                          "  id_CartellaClinica = ?  "   +     "  ,  "    +
                          "  id_Farmaco = ?  "           +     "  ,  "    +
                          "  id_Farmacia = ?  "          +     "  ,  "    +
                          "  vistoPaziente = ?  "        +     
                    "  WHERE id = ?  ")) {
            
            // imposto i valori
            
            ps.setDate      (1 , ricetta.getScadenza());
            ps.setDate      (2 , ricetta.getDataPrescrizione());
            ps.setDate      (3 , ricetta.getDataEvalsa());
            ps.setInt       (4 , ricetta.getQuantita());
            ps.setDouble    (5 , ricetta.getPrezzoFarmacia());
            ps.setInt       (6 , ricetta.getId_CartellaClinica());
            ps.setInt       (7 , ricetta.getId_Farmaco());
            ps.setString    (8 , ricetta.getId_Farmacia());
            ps.setBoolean   (9, ricetta.getVistoPaziente());
            //WHERE
            ps.setInt       (10, ricetta.getId());
            
            ps.executeUpdate();
            
            
        } catch (SQLException ex) {
            throw new DAOException( " Impossibile aggiornare il record di Ricetta " , ex);
        }
    }
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    public void update_VistoPaziente(Integer id,Boolean visto)throws DAOException{
        if ((id == null) || (visto==null)) {
            throw new DAOException( " L'id della ricetta o lo stato visto è null " );
        }
        try (PreparedStatement ps = CON.prepareStatement(
                    "  UPDATE Ricetta  "  +
                    "  SET   vistoPaziente = ?  "        +     
                    "  WHERE id = ?  ")) {
            
            // imposto i valori
            ps.setBoolean   (1, visto);
            //WHERE
            ps.setInt       (2,id);
            
            ps.executeUpdate();
            
            
        } catch (SQLException ex) {
            throw new DAOException( " Impossibile aggiornare lo stato di visto paziente del record Ricetta " , ex);
        }
    }
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    @Override
    public void update_Evalsa(Integer id, Date dataEvalsa, Double prezzo, String id_Farmacia) throws DAOException {
        if (id == null || dataEvalsa == null || prezzo == null  || id_Farmacia == null) {
            throw new DAOException( " L'id,la data,il prezzo o id della Farmacia è null " );
        }
        try (PreparedStatement ps = CON.prepareStatement(
                     "  UPDATE Ricetta  "  +
                     "  SET  dataEvalsa = ?  "           +     "  ,  "    +
                          "  prezzo = ?  "               +     "  ,  "    +
                          "  id_Farmacia = ?  "          +     
                     "  WHERE id = ? AND      "          + 
                     "        id_Farmacia IS NULL " , Statement.RETURN_GENERATED_KEYS)) {
            
            // imposto i valori
            ps.setDate      (1, dataEvalsa);
            ps.setDouble    (2, prezzo);
            ps.setString    (3, id_Farmacia);
            //WHERE
            ps.setInt       (4, id);
            
            ps.executeUpdate();
            
        } catch (SQLException ex) {
            throw new DAOException( " Impossibile aggiornare il record di Ricetta " , ex);
        }
    }
//------------------------------------------------------------------------------
//                      SELECT
//------------------------------------------------------------------------------
    @Override
    public List<Ricetta> getByPazienteId(String id) throws DAOException {
        if (id == null ) {
            throw new DAOException( " id  è null " );
        }
        List<Ricetta> listaRicetta = new ArrayList<>();

        try (PreparedStatement stm = CON.prepareStatement(
                    "  SELECT   RI.id AS id ,  "                                +
                             "  RI.scadenza AS scadenza ,  "                    +
                             "  RI.dataPrescrizione AS dataPrescrizione ,  "    +
                             "  RI.dataEvalsa AS dataEvalsa ,  "                +
                             "  RI.quantita AS quantita ,  "                    +
                             "  RI.prezzo AS prezzo ,  "                        +
                             "  RI.id_CartellaClinica AS id_CartellaClinica ,  "+
                             "  RI.id_Farmaco AS id_Farmaco ,  "                +
                             "  RI.id_Farmacia AS id_Farmacia ,  "              +
                             "  RI.vistoPaziente AS vistoPaziente  "            +
                    "  FROM Ricetta AS RI,  "                                   +
                         "  CartellaClinicaPerMB AS CC "                        +                                       
                    "  WHERE  RI.id_CartellaClinica = CC.id  AND  "             +
                            " CC.id_Persona = ?  " )) {
            stm.setString(1, id);
            try (ResultSet rs = stm.executeQuery()){

                while (rs.next()) {
                    Ricetta ricetta = new Ricetta();
                    
                    ricetta.setId                   (rs.getInt      ( "id" ));
                    ricetta.setScadenza             (rs.getDate     ( "scadenza" ));
                    ricetta.setDataPrescrizione     (rs.getDate     ( "dataPrescrizione" ));
                    ricetta.setDataEvalsa           (rs.getDate     ( "dataEvalsa" ));
                    ricetta.setQuantita             (rs.getInt      ( "quantita" ));
                    ricetta.setPrezzoFarmacia       (rs.getDouble   ( "prezzo" ));
                    ricetta.setId_CartellaClinica   (rs.getInt      ( "id_CartellaClinica" ));
                    ricetta.setId_Farmaco           (rs.getInt      ( "id_Farmaco" ));
                    ricetta.setId_Farmacia          (rs.getString   ( "id_Farmacia" ));
                    ricetta.setVistoPaziente        (rs.getBoolean  ( "vistoPaziente" ));
                    
                    listaRicetta.add(ricetta);
                }
            }
        } catch (SQLException ex) {
            throw new DAOException( " Imposibile estrarre la lista di Ricetta da primary key Persona  " , ex);
        }

        return listaRicetta;
    }
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    @Override
    public List<Ricetta> getByMedicoBaseId(String id) throws DAOException {
        if (id == null ) {
            throw new DAOException( " id  è null " );
        }
        List<Ricetta> listaRicetta = new ArrayList<>();

        try (PreparedStatement stm = CON.prepareStatement(
                    "  SELECT  RI.id AS id ,  "                                 +
                            "  RI.scadenza AS scadenza ,  "                     +
                            "  RI.dataPrescrizione AS dataPrescrizione ,  "     +
                            "  RI.dataEvalsa AS dataEvalsa ,  "                 +
                            "  RI.quantita AS quantita ,  "                     +
                            "  RI.prezzo AS prezzo ,  "                         +
                            "  RI.id_CartellaClinica AS id_CartellaClinica ,  " +
                            "  RI.id_Farmaco AS id_Farmaco ,  "                 +
                            "  RI.id_Farmacia AS id_Farmacia ,  "               +
                            "  RI.vistoPaziente AS vistoPaziente  "             +
                    "  FROM Ricetta AS RI,  "                                   +
                    "  CartellaClinicaPerMB AS CC "                             +                                       
                    "  WHERE  RI.id_CartellaClinica = CC.id  AND  "             +
                             " CC.id_MedicoBase = ? "  )) {
            stm.setString(1, id);
            try (ResultSet rs = stm.executeQuery()){
               

                while (rs.next()) {
                    Ricetta ricetta = new Ricetta();
                    
                    ricetta.setId                   (rs.getInt      ( "id" ));
                    ricetta.setScadenza             (rs.getDate     ( "scadenza" ));
                    ricetta.setDataPrescrizione     (rs.getDate     ( "dataPrescrizione" ));
                    ricetta.setDataEvalsa           (rs.getDate     ( "dataEvalsa" ));
                    ricetta.setQuantita             (rs.getInt      ( "quantita" ));
                    ricetta.setPrezzoFarmacia       (rs.getDouble   ( "prezzo" ));
                    ricetta.setId_CartellaClinica   (rs.getInt      ( "id_CartellaClinica" ));
                    ricetta.setId_Farmaco           (rs.getInt      ( "id_Farmaco" ));
                    ricetta.setId_Farmacia          (rs.getString   ( "id_Farmacia" ));
                    ricetta.setVistoPaziente        (rs.getBoolean  ( "vistoPaziente" ));
                    
                    listaRicetta.add(ricetta);
                }
            }
        } catch (SQLException ex) {
            throw new DAOException( " Imposibile estrarre la lista di Ricetta da primary key MedicoBase  " , ex);
        }

        return listaRicetta;
    }
/*============================================================================*/ 
/*============================================================================*/    
/*                         METODO GET Da ID solo Attivi non conclusi          */
/*============================================================================*/
    @Override
    public List<Ricetta> getByPazienteId_Ririrate(String id) throws DAOException {
        if (id == null ) {
            throw new DAOException( " id  è null " );
        }
        List<Ricetta> listaRicetta = new ArrayList<>();

        try (PreparedStatement stm = CON.prepareStatement(
                "  SELECT   RI.id AS id ,  "                                    +
                         "  RI.scadenza AS scadenza ,  "                        +
                         "  RI.dataPrescrizione AS dataPrescrizione ,  "        +
                         "  RI.dataEvalsa AS dataEvalsa ,  "                    +
                         "  RI.quantita AS quantita ,  "                        + 
                         "  RI.prezzo AS prezzo ,  "                            +
                         "  RI.id_CartellaClinica AS id_CartellaClinica ,  "    +
                         "  RI.id_Farmaco AS id_Farmaco ,  "                    +
                         "  RI.id_Farmacia AS id_Farmacia ,  "                  +
                         "  RI.vistoPaziente AS vistoPaziente  "                +
                "  FROM Ricetta AS RI,  "                                       +
                     "  CartellaClinicaPerMB AS CC "                            +                                       
                "  WHERE  RI.id_CartellaClinica = CC.id  AND  "                 +
                       "  RI.dataEvalsa IS NOT NULL  AND  "                     +
                       "  RI.id_Farmacia IS NOT NULL  AND  "                    +
                       "  RI.prezzo IS NOT NULL  AND  "                         +
                       "  CC.id_Persona =  ?  "  )) {
            stm.setString(1, id);
            try (ResultSet rs = stm.executeQuery()){

                while (rs.next()) {
                    Ricetta ricetta = new Ricetta();
                    
                    ricetta.setId                   (rs.getInt      ( "id" ));
                    ricetta.setScadenza             (rs.getDate     ( "scadenza" ));
                    ricetta.setDataPrescrizione     (rs.getDate     ( "dataPrescrizione" ));
                    ricetta.setDataEvalsa           (rs.getDate     ( "dataEvalsa" ));
                    ricetta.setQuantita             (rs.getInt      ( "quantita" ));
                    ricetta.setPrezzoFarmacia       (rs.getDouble   ( "prezzo" ));
                    ricetta.setId_CartellaClinica   (rs.getInt      ( "id_CartellaClinica" ));
                    ricetta.setId_Farmaco           (rs.getInt      ( "id_Farmaco" ));
                    ricetta.setId_Farmacia          (rs.getString   ( "id_Farmacia" ));
                    ricetta.setVistoPaziente        (rs.getBoolean  ( "vistoPaziente" ));
                    
                    listaRicetta.add(ricetta);
                }
            }
        } catch (SQLException ex) {
            throw new DAOException( " Imposibile estrarre la lista di Ricetta da primary key Persona  " , ex);
        }

        return listaRicetta;
    }
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    @Override
    public List<Ricetta> getByPazienteId_DaRirirate(String id) throws DAOException {
        if (id == null ) {
            throw new DAOException( " id  è null " );
        }
        List<Ricetta> listaRicetta = new ArrayList<>();

        try (PreparedStatement stm = CON.prepareStatement(
                    "  SELECT  RI.id AS id ,  "                                 +
                            "  RI.scadenza AS scadenza ,  "                     +
                            "  RI.dataPrescrizione AS dataPrescrizione ,  "     +
                            "  RI.dataEvalsa AS dataEvalsa ,  "                 +
                            "  RI.quantita AS quantita ,  "                     +
                            "  RI.prezzo AS prezzo ,  "                         +
                            "  RI.id_CartellaClinica AS id_CartellaClinica ,  " +
                            "  RI.id_Farmaco AS id_Farmaco ,  "                 +
                            "  RI.id_Farmacia AS id_Farmacia ,  "               +
                            "  RI.vistoPaziente AS vistoPaziente  "             +
                    "  FROM Ricetta AS RI,  "                                   +
                         "  CartellaClinicaPerMB AS CC "                        +                                       
                    "  WHERE  RI.id_CartellaClinica = CC.id  AND  "             +
                           "  RI.dataEvalsa IS  NULL  AND  "                    +
                           "  RI.id_Farmacia IS  NULL  AND  "                   +
                           "  CC.id_Persona =  ? " )) {
            
            stm.setString(1, id);
            try (ResultSet rs = stm.executeQuery()){

                while (rs.next()) {
                    Ricetta ricetta = new Ricetta();
                    
                    ricetta.setId                   (rs.getInt      ( "id" ));
                    ricetta.setScadenza             (rs.getDate     ( "scadenza" ));
                    ricetta.setDataPrescrizione     (rs.getDate     ( "dataPrescrizione" ));
                    ricetta.setDataEvalsa           (rs.getDate     ( "dataEvalsa" ));
                    ricetta.setQuantita             (rs.getInt      ( "quantita" ));
                    ricetta.setPrezzoFarmacia       (rs.getDouble   ( "prezzo" ));
                    ricetta.setId_CartellaClinica   (rs.getInt      ( "id_CartellaClinica" ));
                    ricetta.setId_Farmaco           (rs.getInt      ( "id_Farmaco" ));
                    ricetta.setId_Farmacia          (rs.getString   ( "id_Farmacia" ));
                    ricetta.setVistoPaziente        (rs.getBoolean  ( "vistoPaziente" ));
                    
                    listaRicetta.add(ricetta);
                }
            }
        } catch (SQLException ex) {
            throw new DAOException( " Imposibile estrarre la lista di Ricetta da primary key Persona  " , ex);
        }

        return listaRicetta;
    }
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    @Override//<<<----ERRORE CONVERSIONE DATA DI OGGI
    public List<Ricetta> getByPazienteId_Scadute(String id) throws DAOException {
        if (id == null ) {
            throw new DAOException( " id  è null " );
        }
        List<Ricetta> listaRicetta = new ArrayList<>();;
        
        try (PreparedStatement stm = CON.prepareStatement(
                "  SELECT   RI.id AS id ,  "                                    +
                         "  RI.scadenza AS scadenza ,  "                        +
                         "  RI.dataPrescrizione AS dataPrescrizione ,  "        +
                         "  RI.dataEvalsa AS dataEvalsa ,  "                    +
                         "  RI.quantita AS quantita ,  "                        +
                         "  RI.prezzo AS prezzo ,  "                            +
                         "  RI.id_CartellaClinica AS id_CartellaClinica ,  "    +
                         "  RI.id_Farmaco AS id_Farmaco ,  "                    +
                         "  RI.id_Farmacia AS id_Farmacia ,  "                  +
                         "  RI.vistoPaziente AS vistoPaziente  "                +
                "  FROM Ricetta AS RI,  "                                       +
                     "  CartellaClinicaPerMB AS CC "                            +                                       
                "  WHERE  RI.id_CartellaClinica = CC.id  AND  "                 +
                       "  RI.dataEvalsa < ?  "                                  +
                       "  CC.id_Persona =  ?  ")) {
            //<<<----ERRORE CONVERSIONE DATA DI OGGI
            Calendar currenttime = Calendar.getInstance();
            Date oggi = new Date((currenttime.getTime()).getTime());
            //<<<----ERRORE CONVERSIONE DATA DI OGGI
            
            stm.setDate(1, oggi);
            stm.setString(2, id);
            try (ResultSet rs = stm.executeQuery()){

                while (rs.next()) {
                    Ricetta ricetta = new Ricetta();
                    
                    ricetta.setId                   (rs.getInt      ( "id" ));
                    ricetta.setScadenza             (rs.getDate     ( "scadenza" ));
                    ricetta.setDataPrescrizione     (rs.getDate     ( "dataPrescrizione" ));
                    ricetta.setDataEvalsa           (rs.getDate     ( "dataEvalsa" ));
                    ricetta.setQuantita             (rs.getInt      ( "quantita" ));
                    ricetta.setPrezzoFarmacia       (rs.getDouble   ( "prezzo" ));
                    ricetta.setId_CartellaClinica   (rs.getInt      ( "id_CartellaClinica" ));
                    ricetta.setId_Farmaco           (rs.getInt      ( "id_Farmaco" ));
                    ricetta.setId_Farmacia          (rs.getString   ( "id_Farmacia" ));
                    ricetta.setVistoPaziente        (rs.getBoolean  ( "vistoPaziente" ));
                    
                    listaRicetta.add(ricetta);
                }
            }
        } catch (SQLException ex1) {
            throw new DAOException( " Imposibile estrarre la lista di Ricetta da primary key Persona  " , ex1);
        } catch (Exception ex2) {
            throw new DAOException( " Errore calcolo data odierna.  " , ex2);//<<<----ERRORE CONVERSIONE DATA DI OGGI
        }

        return listaRicetta;
    }
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    @Override
    public List<Ricetta> getByMedicoBaseId_Ririrate(String id) throws DAOException {
        if (id == null ) {
            throw new DAOException( " id  è null " );
        }
        List<Ricetta> listaRicetta = new ArrayList<>();

        try (PreparedStatement stm = CON.prepareStatement(
                    "  SELECT   RI.id AS id ,  "                                +
                             "  RI.scadenza AS scadenza ,  "                    +
                             "  RI.dataPrescrizione AS dataPrescrizione ,  "    +
                             "  RI.dataEvalsa AS dataEvalsa ,  "                +
                             "  RI.quantita AS quantita ,  "                    +
                             "  RI.prezzo AS prezzo ,  "                        +
                             "  RI.id_CartellaClinica AS id_CartellaClinica ,  "+
                             "  RI.id_Farmaco AS id_Farmaco ,  "                +
                             "  RI.id_Farmacia AS id_Farmacia ,  "              +
                             "  RI.vistoPaziente AS vistoPaziente  "            +
                    "  FROM Ricetta AS RI,  "                                   +
                         "  CartellaClinicaPerMB AS CC "                        +                                       
                    "  WHERE  RI.id_CartellaClinica = CC.id  AND  "             +
                           "  RI.dataEvalsa IS NOT NULL  AND  "                 +
                           "  RI.id_Farmacia IS NOT NULL  AND  "                +
                           "  RI.prezzo IS NOT NULL  AND  "                     +
                           "  CC.id_MedicoBase = ? "  )) {
            
            stm.setString(1, id);
            try (ResultSet rs = stm.executeQuery()){

                while (rs.next()) {
                    Ricetta ricetta = new Ricetta();
                    
                    ricetta.setId                   (rs.getInt      ( "id" ));
                    ricetta.setScadenza             (rs.getDate     ( "scadenza" ));
                    ricetta.setDataPrescrizione     (rs.getDate     ( "dataPrescrizione" ));
                    ricetta.setDataEvalsa           (rs.getDate     ( "dataEvalsa" ));
                    ricetta.setQuantita             (rs.getInt      ( "quantita" ));
                    ricetta.setPrezzoFarmacia       (rs.getDouble   ( "prezzo" ));
                    ricetta.setId_CartellaClinica   (rs.getInt      ( "id_CartellaClinica" ));
                    ricetta.setId_Farmaco           (rs.getInt      ( "id_Farmaco" ));
                    ricetta.setId_Farmacia          (rs.getString   ( "id_Farmacia" ));
                    ricetta.setVistoPaziente        (rs.getBoolean  ( "vistoPaziente" ));
                    
                    listaRicetta.add(ricetta);
                }
            }
        } catch (SQLException ex) {
            throw new DAOException( " Imposibile estrarre la lista di Ricetta da primary key Persona  " , ex);
        }

        return listaRicetta;
    }
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    @Override
    public List<Ricetta> getByMedicoBaseId_DaRirirate(String id) throws DAOException {
        if (id == null ) {
            throw new DAOException( " id  è null " );
        }
        List<Ricetta> listaRicetta = new ArrayList<>();

        try (PreparedStatement stm = CON.prepareStatement(
                "  SELECT   RI.id AS id ,  "                                    +
                         "  RI.scadenza AS scadenza ,  "                        +
                         "  RI.dataPrescrizione AS dataPrescrizione ,  "        +
                         "  RI.dataEvalsa AS dataEvalsa ,  "                    +
                         "  RI.quantita AS quantita ,  "                        +
                         "  RI.prezzo AS prezzo ,  "                            +   
                         "  RI.id_CartellaClinica AS id_CartellaClinica ,  "    +
                         "  RI.id_Farmaco AS id_Farmaco ,  "                    +
                         "  RI.id_Farmacia AS id_Farmacia ,  "                  +
                         "  RI.vistoPaziente AS vistoPaziente  "                +
                "  FROM Ricetta AS RI,  "                                       +
                     "  CartellaClinicaPerMB AS CC "                            +                                       
                "  WHERE  RI.id_CartellaClinica = CC.id  AND  "                 +
                       "  RI.dataEvalsa IS  NULL  AND  "                        +
                       "  RI.id_Farmacia IS  NULL  AND  "                       +
                       "  CC.id_MedicoBase = ? "  )) {
            
            stm.setString(1, id);
            try (ResultSet rs = stm.executeQuery()){
            
                while (rs.next()) {
                    Ricetta ricetta = new Ricetta();
                    
                    ricetta.setId                   (rs.getInt      ( "id" ));
                    ricetta.setScadenza             (rs.getDate     ( "scadenza" ));
                    ricetta.setDataPrescrizione     (rs.getDate     ( "dataPrescrizione" ));
                    ricetta.setDataEvalsa           (rs.getDate     ( "dataEvalsa" ));
                    ricetta.setQuantita             (rs.getInt      ( "quantita" ));
                    ricetta.setPrezzoFarmacia       (rs.getDouble   ( "prezzo" ));
                    ricetta.setId_CartellaClinica   (rs.getInt      ( "id_CartellaClinica" ));
                    ricetta.setId_Farmaco           (rs.getInt      ( "id_Farmaco" ));
                    ricetta.setId_Farmacia          (rs.getString   ( "id_Farmacia" ));
                    ricetta.setVistoPaziente        (rs.getBoolean  ( "vistoPaziente" ));
                    
                    listaRicetta.add(ricetta);
                }
            }
        } catch (SQLException ex) {
            throw new DAOException( " Imposibile estrarre la lista di Ricetta da primary key Persona  " , ex);
        }

        return listaRicetta;
    }
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    @Override
    public List<Ricetta> getByMedicoBaseId_Scadute(String id) throws DAOException {
        if (id == null ) {
            throw new DAOException( " id  è null " );
        }
        List<Ricetta> listaRicetta = new ArrayList<>();;
        
        try (PreparedStatement stm = CON.prepareStatement(
                 "  SELECT  "   +  "  RI.id AS id ,  "                              +
                                   "  RI.scadenza AS scadenza ,  "                  +
                                   "  RI.dataPrescrizione AS dataPrescrizione ,  "  +
                                   "  RI.dataEvalsa AS dataEvalsa ,  "              +
                                   "  RI.quantita AS quantita ,  "                  +
                                   "  RI.prezzo AS prezzo ,  "                      +
                                   "  RI.id_CartellaClinica AS id_CartellaClinica , " +
                                   "  RI.id_Farmaco AS id_Farmaco ,  "              +
                                   "  RI.id_Farmacia AS id_Farmacia ,  "            +
                                   "  RI.vistoPaziente AS vistoPaziente  "          +
                 "  FROM Ricetta AS RI,  "                                          +
                      "  CartellaClinicaPerMB AS CC "                               +                                       
                 "  WHERE  RI.id_CartellaClinica = CC.id  AND  "                    +
                        "  RI.dataEvalsa < ?  "                                     +
                        "  CC.id_MedicoBase = ?  "   )) {
            
            //<<<----ERRORE CONVERSIONE DATA DI OGGI
            Calendar currenttime = Calendar.getInstance();
            Date oggi = new Date((currenttime.getTime()).getTime());
            //<<<----ERRORE CONVERSIONE DATA DI OGGI
            
            stm.setDate(1, oggi);
            stm.setString(2, id);
            try (ResultSet rs = stm.executeQuery()){
                
                while (rs.next()) {
                    Ricetta ricetta = new Ricetta();
                    
                    ricetta.setId                   (rs.getInt      ( "id" ));
                    ricetta.setScadenza             (rs.getDate     ( "scadenza" ));
                    ricetta.setDataPrescrizione     (rs.getDate     ( "dataPrescrizione" ));
                    ricetta.setDataEvalsa           (rs.getDate     ( "dataEvalsa" ));
                    ricetta.setQuantita             (rs.getInt      ( "quantita" ));
                    ricetta.setPrezzoFarmacia       (rs.getDouble   ( "prezzo" ));
                    ricetta.setId_CartellaClinica   (rs.getInt      ( "id_CartellaClinica" ));
                    ricetta.setId_Farmaco           (rs.getInt      ( "id_Farmaco" ));
                    ricetta.setId_Farmacia          (rs.getString   ( "id_Farmacia" ));
                    ricetta.setVistoPaziente        (rs.getBoolean  ( "vistoPaziente" ));
                    
                    listaRicetta.add(ricetta);
                }
            }
        } catch (SQLException ex1) {
            throw new DAOException( " Imposibile estrarre la lista di Ricetta da primary key Persona  " , ex1);
        } catch (Exception ex2) {
            throw new DAOException( " Errore calcolo data odierna.  " , ex2);//<<<----ERRORE CONVERSIONE DATA DI OGGI
        }

        return listaRicetta;
    }
/*============================================================================*/
}
