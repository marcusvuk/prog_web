
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%@page import="java.util.List"%>
<%@page import="it.tn.unitn.pw.progetto.SSN.DAO.common.exception.*"%>
<%@page import="it.tn.unitn.pw.progetto.SSN.DAO.common.factories.jdbc.*"%>
<%@page import="it.tn.unitn.pw.progetto.SSN.DAO.common.factory.*"%>
<%@page import="it.tn.unitn.pw.progetto.SSN.DAO.common.persistence.dao.jdbc.*"%>
<%@page import="it.tn.unitn.pw.progetto.SSN.DAO.persistence.*"%>
<%@page import="it.tn.unitn.pw.progetto.SSN.persistence.entities.*"%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
        <title>
            Visita
        </title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" crossorigin="anonymous">
    
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/css/select2.min.css">
  
        <!-- Cittadino -->
        <link rel="stylesheet" href="${pageContext.request.contextPath}/modal/modal_Login/css/floating-labels.css">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/modal/modal_Login/css/forms.css">
        <!-- Cittadino -->
        <link rel="icon" href="${pageContext.request.contextPath}/sito_IMG/icona.ico" type="image/x-icon" />
        <link href="${pageContext.request.contextPath}/sito_CSS/style.css" rel="stylesheet" />
        <link href="${pageContext.request.contextPath}/sito_CSS/style_MenuUtente.css" rel="stylesheet" />
    </head>
    <body onload="showEsitoModale()">
        <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/js/select2.min.js"></script>
        
        
    <!-- JAVASCRIP -->
        <%@include file="/pag_ElementiComunni/pag_MenuPrincipale.jsp" %>
        
        <%@include file="/modal/modal_ConcludiVisita/modal_ConcludiVisita.jsp" %> 
        <%@include file="/modal/modal_PagamentoVisita/modal_PagamentoVisita.jsp" %> 
        <div class="container">
            <div>
                <div class="row">
                    <div class="col-md-10">
                        <h2>
                            Visita di
                            <c:choose>
                                <c:when test="${c_Persona ne null}">
                                    <b> <c:out value="${c_Persona.cognome} ${c_Persona.nome}"/> </b>
                                </c:when>
                                <c:when test="${b_Paziente ne null}">
                                    <b> <c:out value="${b_Paziente.cognome} ${b_Paziente.nome}"/> </b>
                                </c:when>
                                <c:when test="${s_Paziente ne null}">
                                    <b> <c:out value="${s_Paziente.cognome} ${s_Paziente.nome}"/> </b>
                                </c:when>
                            </c:choose>
                        </h2>
                    </div>
                    <c:if test="${(s_Paziente ne null) && (c_b_s_Visita.data  eq null) &&
                                  (c_b_s_MedicoSpec  ne null) &&
                                   (c_b_s_MedicoSpec.codF eq s_Persona.codF )}">
                        <div class="col-md-2">
                            <a href="#"
                               class="btn btn-primary pull-right float-right"
                               data-toggle="modal" 
                               data-target="#ConcludiVisitaModal">
                                <i class="fas fa-pencil-alt"></i> Compila Report
                            </a>
                        </div>
                    </c:if>
                    <c:if test="${(b_Paziente ne null) && (c_b_s_Visita.data  eq null) && 
                                  (c_b_s_MedicoBase ne null) &&
                                  (c_b_s_MedicoBase.codF eq b_Persona.codF )}">
                        <div class="col-md-2">
                            <a href="#"
                               class="btn btn-primary pull-right float-right"
                               data-toggle="modal" 
                               data-target="#ConcludiVisitaModal">
                                <i class="fas fa-pencil-alt"></i> Effetua Visita
                            </a>
                        </div>
                    </c:if>
                    <c:if test="${(s_Paziente ne null)        && (c_b_s_Visita.data    ne null ) &&
                                  (c_b_s_MedicoSpec  ne null) && (c_b_s_Visita.pagato  eq false) &&
                                  (c_b_s_MedicoSpec.codF eq s_Persona.codF )}">
                        <div class="col-md-2">
                            <a href="#"
                               class="btn btn-primary pull-right float-right"
                               data-toggle="modal" 
                               data-target="#PagamentoVisitaModal">
                                <i class="fas fa-money-bill-wave"></i> Paga Visita
                            </a>
                        </div>
                    </c:if>
                </div>
                
                
                
                
                <div class="row">
                    <div class="col-md-5">
                        <h5>
                            <b>
                            Prescritto da:
                            </b>
                        </h5>
                    </div>
                    <div class="col-md-7">
                        <h5>
                            Dr.
                            <c:choose>
                                <c:when test="${c_b_s_MedicoBase ne null}">
                                    <c:out value="${c_b_s_MedicoBase.cognome}"/> <c:out value="${c_b_s_MedicoBase.nome}"/>
                                </c:when>
                                <c:when test="${c_b_s_MedicoSpec ne null}">
                                    <c:out value="${c_b_s_MedicoSpec.cognome}"/> <c:out value="${c_b_s_MedicoSpec.nome}"/>
                                </c:when>
                            </c:choose>
                        </h5>
                    </div>
                </div>
                        
                <div class="row">
                    <div class="col-md-5">
                        <h5>
                            <b>
                            Pescritto il giorno:
                            </b>
                        </h5>
                    </div>
                    <div class="col-md-7">
                        <h5>
                            <c:out value="${c_b_s_Visita.dataPrescrizione}"/> 
                        </h5>
                    </div>
                </div>     
                <div class="row">
                    <div class="col-md-5">
                        <h5>
                            <b>
                            Prescrizione:
                            </b>
                        </h5>
                    </div>
                    <div class="col-md-7">
                        <h5>
                            <c:out value="${c_b_s_Visita.prescrizione}"/> 
                        </h5>
                    </div>
                </div> 
                <hr>
                <c:if test="${c_b_s_Visita.data ne null}">
                    <div class="row">
                        <div class="col-md-5">
                            <h5>
                                <b>
                                Effetuato il giorno:
                                </b>
                            </h5>
                        </div>
                        <div class="col-md-7">
                            <h5>
                                <c:out value="${c_b_s_Visita.data}"/> 
                            </h5>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-5">
                            <h5>
                                <b>
                                Riassunto visita:
                                </b>
                            </h5>
                        </div>
                        <div class="col-md-7">
                            <h5>
                                <c:out value="${c_b_s_Visita.riassuntoVisita}"/> 
                            </h5>
                        </div>
                    </div>
                    <c:if test="${c_b_s_MedicoSpec ne null}">
                        <div class="row">
                            <div class="col-md-5">
                                <h5>
                                    <b>
                                    Report:
                                    </b>
                                </h5>
                            </div>
                            <div class="col-md-7">
                                <h5>
                                   <c:out value="${c_b_s_Visita.report}"/> 
                                </h5>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-5">
                                <h5>
                                    <b>
                                    Tichet:
                                    </b>
                                </h5>
                            </div>
                            <div class="col-md-7">
                                <h5>
                                    <c:out value="${c_b_s_Visita.tichet}"/> &euro;
                                </h5>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-5">
                                <h5>
                                    <b>
                                    Stato pagamento
                                    </b>
                                </h5>
                            </div>
                            <div class="col-md-7">
                                <h5>
                                    <c:choose>
                                        <c:when test="${c_b_s_Visita.pagato eq true}">
                                            Pagato
                                        </c:when>
                                        <c:otherwise>
                                            <b class="text-danger">Non pagato</b>
                                        </c:otherwise>
                                    </c:choose>
                                </h5>
                            </div>
                        </div>
                    </c:if>       
                </c:if>
            </div>
        </div>
        <%@include file="pag_ElementiComunni/pag_Footer.jsp" %>
    </body>
</html>
