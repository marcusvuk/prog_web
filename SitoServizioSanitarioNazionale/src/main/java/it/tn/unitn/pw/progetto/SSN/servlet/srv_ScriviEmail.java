/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.tn.unitn.pw.progetto.SSN.servlet;

import it.tn.unitn.pw.progetto.SSN.Genera.Email.Mail;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Brian
 */
public class srv_ScriviEmail extends HttpServlet {


    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        String contextPath = getServletContext().getContextPath();
        if (!contextPath.endsWith("/")) {
            contextPath += "/";
        }
        
        String mittente      = request.getParameter("mittente");
        String oggetto       = request.getParameter("oggetto");
        String messaggio     = request.getParameter("messaggio");
        
        if((mittente!=null)&&(oggetto!=null)&&(messaggio!=null)&&
                (!(mittente.equals(""))) &&(!(messaggio.equals("")))&&(!(oggetto.equals("")))){
            try{
                Mail.receiveEmail(messaggio,mittente, oggetto);
                response.sendRedirect(response.encodeRedirectURL(contextPath + "Index"));
            }catch(Exception e){
                response.sendRedirect("scriviEmail.jsp");
            }
        }else{
            response.sendRedirect("scriviEmail.jsp");
        }
        
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
