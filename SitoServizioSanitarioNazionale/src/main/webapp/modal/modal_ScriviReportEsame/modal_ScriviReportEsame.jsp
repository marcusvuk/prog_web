
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div name="ScriviReportEsameModal" id="ScriviReportEsameModal"  class="modal fade" >
    <div class="modal-dialog modal-login modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Scrivi report</h4>
                <button type="button"
                        class="close"
                        data-dismiss="modal"
                        aria-hidden="true">
                    &times;
                </button>    
            </div> 

            <form action="Esami.handler" method="POST">
                <input  type="hidden" id="TipoInserimetoEsame" name="TipoInserimetoEsame" value="InserimetoReport">
                
                
            <div class="modal-body">
                <div class="container-fluid justify-content-center ">  
                    <div class="container">
                        <div class="clearfix">
                            <label><h5><b>Risultato</b></h5></label>
                        </div>
                        <textarea class="form-control" id="risultatoEsame" name="risultatoEsame" required="required" rows="5"></textarea>
                    </div>
                </div>
                <hr>
                <div class="container-fluid justify-content-center ">  
                    <div class="container">
                        <div class="clearfix">
                            <label><h5><b>Cura</b></h5></label>
                        </div>
                        <textarea class="form-control" id="curaEsame" name="curaEsame" required="required" rows="5"></textarea>
                    </div>
                </div>
            </div>
            <div class=" container-fluid modal-footer">
                <c:if test="${c_b_s_Esame.richiamo ne true}">
                    <h5>
                        <b>
                        <input class="pull-right"
                               type="checkbox" 
                               id="pagatoEsame"
                               name="pagatoEsame"/>
                               <label class="form-check-label" for="pagatoEsame">Pagato</label>  
                        </b>
                    </h5>
                </c:if>
                <input  type="submit"
                        class="btn btn-primary pull-right"
                        accept=""value="Conferma"/>
            </div>
            </form>
        </div>
    </div>
</div>