/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.tn.unitn.pw.progetto.SSN.DAO.persistence;

import java.util.List;

import it.tn.unitn.pw.progetto.SSN.persistence.entities.Esame;
import it.tn.unitn.pw.progetto.SSN.DAO.common.DAO;
import it.tn.unitn.pw.progetto.SSN.DAO.common.exception.DAOException;
/**
 *
 * @author Brian
 */
public interface Esame_DAO extends DAO<Esame, Integer>{
/*============================================================================*/    
/*                         METODO INSERT                                      */
/*============================================================================*/
    public Integer insert(Esame esame)throws DAOException;
/*============================================================================*/ 
/*============================================================================*/    
/*                         METODO ADD                                      */
/*============================================================================*/
    public void addLinkEsameSSP(Integer idEsame,Integer idSSProvinciale)throws DAOException;
/*============================================================================*/
/*============================================================================*/    
/*                         METODO GET Da ID                                   */
/*============================================================================*/
    public List<Esame> getBySSProvincialeId(Integer id)throws DAOException;
/*============================================================================*/
}
