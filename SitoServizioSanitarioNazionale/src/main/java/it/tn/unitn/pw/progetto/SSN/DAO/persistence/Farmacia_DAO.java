/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.tn.unitn.pw.progetto.SSN.DAO.persistence;

import java.util.List;

import it.tn.unitn.pw.progetto.SSN.persistence.entities.Farmacia;
import it.tn.unitn.pw.progetto.SSN.DAO.common.DAO;
import it.tn.unitn.pw.progetto.SSN.DAO.common.exception.DAOException;
/**
 *
 * @author Brian
 */
public interface Farmacia_DAO extends DAO<Farmacia, String>{
/*============================================================================*/    
/*                         METODO INSERT                                      */
/*============================================================================*/
    public String insert(Farmacia farmacia)throws DAOException;
/*============================================================================*/ 
/*============================================================================*/    
/*                         METODO UPDATE                                      */
/*============================================================================*/
    public void update(Farmacia farmacia)throws DAOException;
    public void update_Password(String id,String Password )throws DAOException;
/*============================================================================*/
/*============================================================================*/    
/*                         METODO GET LOGIN                                  */
/*============================================================================*/
    public Farmacia getByFarmaciaEmailAndPassword(String email, String password)throws DAOException;
    public String getSaltFarmaciaByEmail(String email)throws DAOException;
    public Farmacia getByFarmaciaEmail(String email)throws DAOException;
/*============================================================================*/

}
