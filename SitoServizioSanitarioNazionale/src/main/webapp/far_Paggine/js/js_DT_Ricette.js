$(function () {
    $("#RicetteTable").dataTable( {
        ajax: {
            url: '/SitoServizioSanitarioNazionale/services/ricettaPaziente/'+$('#IdCliente').attr('value')+
                    '/Farmcaia/'+$('#MyIdFarmacia').attr('value'),
            dataSrc: 'results'
        },
        columns: [
            { results: 0 },
            { results: 1 },
            { results: 2 },
            { results: 3 },
            { results: 4 },
            { results: 5,//results: Corrispnde a data
              targets: -1, 
              render: function ( data, type, row, meta ) {
                    if(type === 'display'){
                        data = '<a href="Ricette?idRicetta=' + encodeURIComponent(data) + '">'+
                               '<i class="fas fa-prescription-bottle-alt" style="font-size:36px" ></i>'+
                               '</a>';
                    }
                    return data;
                }
            },
        ]
    });

});