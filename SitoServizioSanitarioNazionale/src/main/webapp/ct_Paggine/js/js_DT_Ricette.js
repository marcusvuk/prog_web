$(function () {
    $("#RicetteTable").dataTable( {
        ajax: {
            url: '/SitoServizioSanitarioNazionale/services/ricettaPaziente/'+$('#MyIdCittadino').attr('value'),
            dataSrc: 'results'
        },
        columns: [
            { results: 0,//results: Corrispnde a data
              targets: 0, 
              render: function ( data, type, row, meta ) {
                    if(type === 'display'){
                        if(data === 'true'){
                            //Vechio utente
                            data ='<i class="far fa-eye" style="font-size:36px"></i>';
                        }else{
                            //Nuovo utente
                            data ='<i class="fas fa-eye-slash  text-primary" style="font-size:36px"></i>';
                        }
                    }
                    return data;
                }
            },
            { results: 1,//results: Corrispnde a data
              targets: 1, 
              render: function ( data, type, row, meta ) {
                    if(type === 'display'){
                        if(data === "DaRitirare" ){
                            //Non ancora ritirato
                            data ='<i class="fas fa-hourglass-half" style="font-size:36px"></i>';
                        }else if(data === "Scaduto" ){
                            //Non ancora ritirato
                            data ='<i class="fas fa-hourglass-end text-danger"  text-primary" style="font-size:36px"></i>';
                        }
                    }
                    return data;
                }
            },
            { results: 2 },
            { results: 3 },
            { results: 4 },
            { results: 5 },
            { results: 6 },
            { results: 7,//results: Corrispnde a data
              targets: -1, 
              render: function ( data, type, row, meta ) {
                    if(type === 'display'){
                        data = '<a href="Ricette?idRicetta=' + encodeURIComponent(data) + '">'+
                               '<i class="fas fa-prescription-bottle-alt" style="font-size:36px" ></i>'+
                               '</a>';
                    }
                    return data;
                }
            },
        ]
    });

});