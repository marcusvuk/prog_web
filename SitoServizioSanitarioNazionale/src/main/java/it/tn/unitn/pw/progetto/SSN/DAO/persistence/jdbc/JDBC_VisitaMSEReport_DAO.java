/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.tn.unitn.pw.progetto.SSN.DAO.persistence.jdbc;

import java.sql.Date;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import it.tn.unitn.pw.progetto.SSN.DAO.common.persistence.dao.jdbc.JDBCDAO;
import it.tn.unitn.pw.progetto.SSN.DAO.common.exception.*;
import it.tn.unitn.pw.progetto.SSN.DAO.persistence.*;
import it.tn.unitn.pw.progetto.SSN.persistence.entities.*;
/**
 *
 * @author Brian
 */
public class JDBC_VisitaMSEReport_DAO extends JDBCDAO<VisitaMSEReport, Integer> implements VisitaMSEReport_DAO {
/*============================================================================*/    
/*                                      METODI COSTRUTTORI                    */
/*============================================================================*/
    public JDBC_VisitaMSEReport_DAO(Connection con) {
        super(con);
    }
/*============================================================================*/
    
/*============================================================================*/    
/*                      METODI GETTER DAO IMPLEMENTATION                      */
/*============================================================================*/
    @Override
    public Long getCount() throws DAOException {
        try (Statement stmt = CON.createStatement()) {
            ResultSet counter = stmt.executeQuery( 
                     "  SELECT COUNT(*) "               +
                     "  FROM VisitaMSEReport "     );
            if (counter.next()) {
                return counter.getLong(1);
            }
        } catch (SQLException ex) {
            throw new DAOException( " Immpossibile contare gli elementi di VisitaMSEReport  " , ex);
        }
        return 0L;
    }
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    @Override
    public VisitaMSEReport getByPrimaryKey(Integer primaryKey) throws DAOException {
        if (primaryKey == null) {
            throw new DAOException( " primaryKey è null " );
        }
        try (PreparedStatement stm = CON.prepareStatement(
                                     "  SELECT *  "                      +
                                     "  FROM VisitaMSEReport  "     +
                                     "  WHERE id = ? "                   )) {
            // imposto i valori
            stm.setInt(1, primaryKey);
            
            try (ResultSet rs = stm.executeQuery()) {
                rs.next();
                VisitaMSEReport visitaMSEReport = new VisitaMSEReport();
                
                visitaMSEReport.setId                   (rs.getInt      ( "id" ));
                visitaMSEReport.setRiassuntoVisita      (rs.getString   ( "riassuntoVisita" ));
                visitaMSEReport.setPrescrizione         (rs.getString   ( "prescrizione" ));
                visitaMSEReport.setData                 (rs.getDate     ( "data" ));
                visitaMSEReport.setDataPrescrizione     (rs.getDate     ( "dataPrescrizione" ));
                visitaMSEReport.setTichet               (rs.getDouble   ( "tichet" ));
                visitaMSEReport.setPagato               (rs.getBoolean  ( "pagato" ));
                visitaMSEReport.setReport               (rs.getString   ( "report" ));
                visitaMSEReport.setId_Persona           (rs.getString   ( "id_Persona" ));
                visitaMSEReport.setId_MedicoSpec        (rs.getString   ( "id_MedicoSpec" ));
                visitaMSEReport.setVistoPaziente        (rs.getBoolean  ( "vistoPaziente" ));
                visitaMSEReport.setVistoReportPaziente  (rs.getBoolean  ( "vistoReportPaziente" ));
                visitaMSEReport.setVistoReportMedicoBase(rs.getBoolean  ( "vistoReportMedicoBase" ));
                
                return visitaMSEReport;
            }
        } catch (SQLException ex) {
            throw new DAOException( " Impossibile perendere la VisitaMSEReport indicato dalla primary key " , ex);
        }
    }
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    @Override
    public List<VisitaMSEReport> getAll() throws DAOException {
        List<VisitaMSEReport> listaVisitaMSEReport = new ArrayList<>();

        try (Statement stm = CON.createStatement()) {
            try (ResultSet rs = stm.executeQuery(
                                 " SELECT *  "                      +
                                 " FROM VisitaMSEReport  "          )){
                
                while (rs.next()) {
                    VisitaMSEReport visitaMSEReport = new VisitaMSEReport();
                
                    visitaMSEReport.setId                   (rs.getInt      ( "id" ));
                    visitaMSEReport.setRiassuntoVisita      (rs.getString   ( "riassuntoVisita" ));
                    visitaMSEReport.setPrescrizione         (rs.getString   ( "prescrizione" ));
                    visitaMSEReport.setData                 (rs.getDate     ( "data" ));
                    visitaMSEReport.setDataPrescrizione     (rs.getDate     ( "dataPrescrizione" ));
                    visitaMSEReport.setTichet               (rs.getDouble   ( "tichet" ));
                    visitaMSEReport.setPagato               (rs.getBoolean  ( "pagato" ));
                    visitaMSEReport.setReport               (rs.getString   ( "report" ));
                    visitaMSEReport.setId_Persona           (rs.getString   ( "id_Persona" ));
                    visitaMSEReport.setId_MedicoSpec        (rs.getString   ( "id_MedicoSpec" ));
                    visitaMSEReport.setVistoPaziente        (rs.getBoolean  ( "vistoPaziente" ));
                    visitaMSEReport.setVistoReportPaziente  (rs.getBoolean  ( "vistoReportPaziente" ));
                    visitaMSEReport.setVistoReportMedicoBase(rs.getBoolean  ( "vistoReportMedicoBase" ));

                    listaVisitaMSEReport.add(visitaMSEReport);
                }
            }
        } catch (SQLException ex) {
            throw new DAOException( " Imposibile estrarre tutta la lista di VisitaMSEReport  " , ex);
        }

        return listaVisitaMSEReport;
    }
/*============================================================================*/
   
/*============================================================================*/    
/*                      METODI QUERY                                          */
/*============================================================================*/
//------------------------------------------------------------------------------
//                      INSERT
//------------------------------------------------------------------------------
    @Override
    public Integer insert(VisitaMSEReport visita) throws DAOException {
        if (visita == null) {
            throw new DAOException( " La visitaMS è null " );
        }
        try (PreparedStatement ps = CON.prepareStatement(
                     " INSERT INTO VisitaMSEReport "  +
                                     " ( riassuntoVisita  "         +    "  ,  "    +
                                     "  prescrizione  "             +    "  ,  "    +
                                     "  data  "                     +    "  ,  "    +
                                     "  dataPrescrizione  "         +    "  ,  "    +
                                     "  tichet  "                   +    "  ,  "    +
                                     "  pagato  "                   +    "  ,  "    +
                                     "  report  "                   +    "  ,  "    +
                                     "  id_Persona  "               +    "  ,  "    +
                                     "  id_MedicoSpec  "            +    "  ,  "    +
                                     "  vistoPaziente  "            +    "  ,  "    +
                                     "  vistoReportPaziente  "      +    "  ,  "    +
                                     "  vistoReportMedicoBase "     +    "  )  "    +  
                     " VALUES (?,?,?,?,?,?,?,?,?,?,?,?) " , Statement.RETURN_GENERATED_KEYS)) {
            
            // imposto i valori
            ps.setString    (1 , visita.getRiassuntoVisita());
            ps.setString    (2 , visita.getPrescrizione());
            ps.setDate      (3 , visita.getData());
            ps.setDate      (4 , visita.getDataPrescrizione());
            ps.setDouble    (5 , visita.getTichet());
            ps.setBoolean   (6 , visita.getPagato());
            ps.setString    (7 , visita.getReport());
            ps.setString    (8 , visita.getId_Persona());
            ps.setString    (9 , visita.getId_MedicoSpec());
            ps.setBoolean   (10, visita.getVistoPaziente());
            ps.setBoolean   (11, visita.getVistoReportPaziente());
            ps.setBoolean   (12, visita.getVistoReportMedicoBase());
            
            ps.executeUpdate();
            
            ResultSet rs = ps.getGeneratedKeys();
            if (rs.next()) {
                visita.setId(rs.getInt(1));
            }
            
            return visita.getId();
        } catch (SQLException ex) {
            throw new DAOException( " Impossibile aggiungere un nuovo record di VisitaMSEReport " , ex);
        }
    }
//------------------------------------------------------------------------------
//                      UPDATE
//------------------------------------------------------------------------------
    @Override
    public void update(VisitaMSEReport visita) throws DAOException {
        if (visita == null) {
            throw new DAOException( " La visitaMS è null " );
        }
        try (PreparedStatement ps = CON.prepareStatement(
                     "  UPDATE VisitaMSEReport  "  +
                     "  SET         riassuntoVisita = ?  "          +    "  ,  "    +
                                 "  prescrizione = ?  "             +    "  ,  "    +
                                 "  data = ?  "                     +    "  ,  "    +
                                 "  dataPrescrizione = ?  "         +    "  ,  "    +
                                 "  tichet = ?  "                   +    "  ,  "    +
                                 "  pagato = ?  "                   +    "  ,  "    +
                                 "  report = ?  "                   +    "  ,  "    +
                                 "  id_Persona = ?  "               +    "  ,  "    +
                                 "  id_MedicoSpec = ?  "            +    "  ,  "    +
                                 "  vistoPaziente = ?  "            +    "  ,  "    +
                                 "  vistoReportPaziente = ?  "      +    "  ,  "    +
                                 "  vistoReportMedicoBase = ?  "     +         
                     "  WHERE id = ?  ")) {
            
            // imposto i valori
            ps.setString    (1 , visita.getRiassuntoVisita());
            ps.setString    (2 , visita.getPrescrizione());
            ps.setDate      (3 , visita.getData());
            ps.setDate      (4 , visita.getDataPrescrizione());
            ps.setDouble    (5 , visita.getTichet());
            ps.setBoolean   (6 , visita.getPagato());
            ps.setString    (7 , visita.getReport());
            ps.setString    (8 , visita.getId_Persona());
            ps.setString    (9, visita.getId_MedicoSpec());
            ps.setBoolean   (10, visita.getVistoPaziente());
            ps.setBoolean   (11, visita.getVistoReportPaziente());
            ps.setBoolean   (12, visita.getVistoReportMedicoBase());
            //WHERE
            ps.setInt       (13, visita.getId());
            
            ps.executeUpdate();
            
            
        } catch (SQLException ex) {
            throw new DAOException( " Impossibile aggiornare il record di VisitaMSEReport " , ex);
        }
    }
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    @Override
    public void update_EsequzioneVisita(Integer id, Date data) throws DAOException {
        if (id == null || data == null) {
            throw new DAOException( " id o data è null " );
        }
        try (PreparedStatement ps = CON.prepareStatement(
                     " UPDATE VisitaMSEReport "  +
                     " SET   data = ?  "   +
                     " WHERE id = ?  " , Statement.RETURN_GENERATED_KEYS)) {
            
            // imposto i valori
            ps.setDate  (1, data);
            //WHERE
            ps.setInt   (2, id);
            
            ps.executeUpdate();
            
        } catch (SQLException ex) {
            throw new DAOException( " Impossibile aggiornare il record di VisitaMSEReport " , ex);
        }
    }
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    @Override
    public void update_VistoPaziente(Integer id, Boolean visto) throws DAOException {
        if (id == null || visto == null) {
            throw new DAOException( " id o visto è null " );
        }
        try (PreparedStatement ps = CON.prepareStatement(
                     " UPDATE VisitaMSEReport "        +
                     " SET   vistoPaziente = ?  "  +
                     " WHERE id = ?  " , Statement.RETURN_GENERATED_KEYS)) {
            
            // imposto i valori
            ps.setBoolean   (1, visto);
            //WHERE
            ps.setInt       (2, id);
            
            ps.executeUpdate();
            
        } catch (SQLException ex) {
            throw new DAOException( " Impossibile aggiornare il record di VisitaMSEReport " , ex);
        }
    }
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    @Override
    public void update_VistoRepotDaPaziente(Integer id, Boolean visto) throws DAOException {
        if (id == null || visto == null) {
            throw new DAOException( " id o visto è null " );
        }
        try (PreparedStatement ps = CON.prepareStatement(
                     " UPDATE VisitaMSEReport "        +
                     " SET   vistoReportPaziente = ?  "  +
                     " WHERE id = ?  " , Statement.RETURN_GENERATED_KEYS)) {
            
            // imposto i valori
            ps.setBoolean   (1, visto);
            //WHERE
            ps.setInt       (2, id);
            
            ps.executeUpdate();
            
        } catch (SQLException ex) {
            throw new DAOException( " Impossibile aggiornare il record di VisitaMSEReport " , ex);
        }
    }
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    @Override
    public void update_VistoRepotDaMedicoBase(Integer id, Boolean visto) throws DAOException {
        if (id == null || visto == null) {
            throw new DAOException( " id o visto è null " );
        }
        try (PreparedStatement ps = CON.prepareStatement(
                     " UPDATE VisitaMSEReport "        +
                     " SET    vistoReportMedicoBase = ?  "  +
                     " WHERE id = ?  " , Statement.RETURN_GENERATED_KEYS)) {
            
            // imposto i valori
            ps.setBoolean   (1, visto);
            //WHERE
            ps.setInt       (2, id);
            
            ps.executeUpdate();
            
        } catch (SQLException ex) {
            throw new DAOException( " Impossibile aggiornare il record di VisitaMSEReport " , ex);
        }
    }
//------------------------------------------------------------------------------
//                      SELECT
//------------------------------------------------------------------------------
    @Override
    public List<VisitaMSEReport> getByPazienteId(String id) throws DAOException {
        if (id == null ) {
            throw new DAOException( " id  è null " );
        }
        List<VisitaMSEReport> listaVisitaMSEReport = new ArrayList<>();

        try (PreparedStatement stm = CON.prepareStatement(
                                "  SELECT *  "                      +
                                "  FROM VisitaMSEReport  "                 +
                                "  WHERE id_Persona = ?  ")) {
            stm.setString(1, id);
            try (ResultSet rs = stm.executeQuery()){
                
                while (rs.next()) {
                    VisitaMSEReport visitaMSEReport = new VisitaMSEReport();
                    
                    visitaMSEReport.setId                   (rs.getInt      ( "id" ));
                    visitaMSEReport.setRiassuntoVisita      (rs.getString   ( "riassuntoVisita" ));
                    visitaMSEReport.setPrescrizione         (rs.getString   ( "prescrizione" ));
                    visitaMSEReport.setData                 (rs.getDate     ( "data" ));
                    visitaMSEReport.setDataPrescrizione     (rs.getDate     ( "dataPrescrizione" ));
                    visitaMSEReport.setTichet               (rs.getDouble   ( "tichet" ));
                    visitaMSEReport.setPagato               (rs.getBoolean  ( "pagato" ));
                    visitaMSEReport.setReport               (rs.getString   ( "report" ));
                    visitaMSEReport.setId_Persona           (rs.getString   ( "id_Persona" ));
                    visitaMSEReport.setId_MedicoSpec        (rs.getString   ( "id_MedicoSpec" ));
                    visitaMSEReport.setVistoPaziente        (rs.getBoolean  ( "vistoPaziente" ));
                    visitaMSEReport.setVistoReportPaziente  (rs.getBoolean  ( "vistoReportPaziente" ));
                    visitaMSEReport.setVistoReportMedicoBase(rs.getBoolean  ( "vistoReportMedicoBase" ));

                    listaVisitaMSEReport.add(visitaMSEReport);
                }
            }
        } catch (SQLException ex) {
            throw new DAOException( " Imposibile estrarre la lista di VisitaMSEReport con chiave esterna Persona  " , ex);
        }

        return listaVisitaMSEReport;
    }
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    @Override
    public List<VisitaMSEReport> getByMedicoSpecId(String id) throws DAOException {
        if (id == null ) {
            throw new DAOException( " id  è null " );
        }
        List<VisitaMSEReport> listaVisitaMSEReport = new ArrayList<>();

        try (PreparedStatement stm = CON.prepareStatement(
                                "  SELECT *  "                      +
                                "  FROM VisitaMSEReport  "          +
                                "  WHERE id_MedicoSpec = ? " )) {
            stm.setString(1, id);
            try (ResultSet rs = stm.executeQuery()){
                
                while (rs.next()) {
                    VisitaMSEReport visitaMSEReport = new VisitaMSEReport();
                    
                    visitaMSEReport.setId                   (rs.getInt      ( "id" ));
                    visitaMSEReport.setRiassuntoVisita      (rs.getString   ( "riassuntoVisita" ));
                    visitaMSEReport.setPrescrizione         (rs.getString   ( "prescrizione" ));
                    visitaMSEReport.setData                 (rs.getDate     ( "data" ));
                    visitaMSEReport.setDataPrescrizione     (rs.getDate     ( "dataPrescrizione" ));
                    visitaMSEReport.setTichet               (rs.getDouble   ( "tichet" ));
                    visitaMSEReport.setPagato               (rs.getBoolean  ( "pagato" ));
                    visitaMSEReport.setReport               (rs.getString   ( "report" ));
                    visitaMSEReport.setId_Persona           (rs.getString   ( "id_Persona" ));
                    visitaMSEReport.setId_MedicoSpec        (rs.getString   ( "id_MedicoSpec" ));
                    visitaMSEReport.setVistoPaziente        (rs.getBoolean  ( "vistoPaziente" ));
                    visitaMSEReport.setVistoReportPaziente  (rs.getBoolean  ( "vistoReportPaziente" ));
                    visitaMSEReport.setVistoReportMedicoBase(rs.getBoolean  ( "vistoReportMedicoBase" ));

                    listaVisitaMSEReport.add(visitaMSEReport);
                }
            }
        } catch (SQLException ex) {
            throw new DAOException( " Imposibile estrarre la lista di VisitaMSEReport con chiave esterna VisitaMSEReport " , ex);
        }

        return listaVisitaMSEReport;
    }
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    @Override
    public List<VisitaMSEReport> getByPazienteId_Fatte(String id) throws DAOException {
        if (id == null ) {
            throw new DAOException( " id  è null " );
        }
        List<VisitaMSEReport> listaVisitaMSEReport = new ArrayList<>();

        try (PreparedStatement stm = CON.prepareStatement(
                                "  SELECT *  "                                 +
                                "  FROM VisitaMSEReport  "                     +
                                "  WHERE  data IS NOT NULL  AND  "             +
                                        "  id_MedicoSpec IS NOT NULL  AND  "   +
                                        " id_Persona = ? "     )) {
            stm.setString(1, id);
            try (ResultSet rs = stm.executeQuery()){
                
                while (rs.next()) {
                    VisitaMSEReport visitaMSEReport = new VisitaMSEReport();
                    
                    visitaMSEReport.setId                   (rs.getInt      ( "id" ));
                    visitaMSEReport.setRiassuntoVisita      (rs.getString   ( "riassuntoVisita" ));
                    visitaMSEReport.setPrescrizione         (rs.getString   ( "prescrizione" ));
                    visitaMSEReport.setData                 (rs.getDate     ( "data" ));
                    visitaMSEReport.setDataPrescrizione     (rs.getDate     ( "dataPrescrizione" ));
                    visitaMSEReport.setTichet               (rs.getDouble   ( "tichet" ));
                    visitaMSEReport.setPagato               (rs.getBoolean  ( "pagato" ));
                    visitaMSEReport.setReport               (rs.getString   ( "report" ));
                    visitaMSEReport.setId_Persona           (rs.getString   ( "id_Persona" ));
                    visitaMSEReport.setId_MedicoSpec        (rs.getString   ( "id_MedicoSpec" ));
                    visitaMSEReport.setVistoPaziente        (rs.getBoolean  ( "vistoPaziente" ));
                    visitaMSEReport.setVistoReportPaziente  (rs.getBoolean  ( "vistoReportPaziente" ));
                    visitaMSEReport.setVistoReportMedicoBase(rs.getBoolean  ( "vistoReportMedicoBase" ));

                    listaVisitaMSEReport.add(visitaMSEReport);
                }
            }
        } catch (SQLException ex) {
            throw new DAOException( " Imposibile estrarre la lista di VisitaMSEReport con chiave esterna Persona fatte  " , ex);
        }

        return listaVisitaMSEReport;
    }
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    @Override
    public List<VisitaMSEReport> getByPazienteId_DaFare(String id) throws DAOException {
        if (id == null ) {
            throw new DAOException( " id  è null " );
        }
        List<VisitaMSEReport> listaVisitaMSEReport = new ArrayList<>();

        try (PreparedStatement stm = CON.prepareStatement(
                                "  SELECT *  "                                 +
                                "  FROM VisitaMSEReport  "                     +
                                "  WHERE  data IS NULL  AND  "                 +
                                        "  id_Persona = ?  " )) {
            stm.setString(1, id);
            try (ResultSet rs = stm.executeQuery()){
                
                while (rs.next()) {
                    VisitaMSEReport visitaMSEReport = new VisitaMSEReport();
                    
                    visitaMSEReport.setId                   (rs.getInt      ( "id" ));
                    visitaMSEReport.setRiassuntoVisita      (rs.getString   ( "riassuntoVisita" ));
                    visitaMSEReport.setPrescrizione         (rs.getString   ( "prescrizione" ));
                    visitaMSEReport.setData                 (rs.getDate     ( "data" ));
                    visitaMSEReport.setDataPrescrizione     (rs.getDate     ( "dataPrescrizione" ));
                    visitaMSEReport.setTichet               (rs.getDouble   ( "tichet" ));
                    visitaMSEReport.setPagato               (rs.getBoolean  ( "pagato" ));
                    visitaMSEReport.setReport               (rs.getString   ( "report" ));
                    visitaMSEReport.setId_Persona           (rs.getString   ( "id_Persona" ));
                    visitaMSEReport.setId_MedicoSpec        (rs.getString   ( "id_MedicoSpec" ));
                    visitaMSEReport.setVistoPaziente        (rs.getBoolean  ( "vistoPaziente" ));
                    visitaMSEReport.setVistoReportPaziente  (rs.getBoolean  ( "vistoReportPaziente" ));
                    visitaMSEReport.setVistoReportMedicoBase(rs.getBoolean  ( "vistoReportMedicoBase" ));

                    listaVisitaMSEReport.add(visitaMSEReport);
                }
            }
        } catch (SQLException ex) {
            throw new DAOException( " Imposibile estrarre la lista di VisitaMSEReport con chiave esterna Persona da fare  " , ex);
        }

        return listaVisitaMSEReport;
    }
/*============================================================================*/
}
