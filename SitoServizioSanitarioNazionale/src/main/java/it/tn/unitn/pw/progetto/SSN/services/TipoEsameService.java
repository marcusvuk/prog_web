/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.tn.unitn.pw.progetto.SSN.services;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Produces;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;
import javax.ws.rs.core.MediaType;

import com.google.gson.Gson;
import it.tn.unitn.pw.progetto.SSN.DAO.common.exception.DAOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;

import it.tn.unitn.pw.progetto.SSN.DAO.persistence.*;
import it.tn.unitn.pw.progetto.SSN.DAO.common.exception.DAOFactoryException;
import it.tn.unitn.pw.progetto.SSN.DAO.common.factory.DAOFactory;
import it.tn.unitn.pw.progetto.SSN.persistence.entities.*;
import java.sql.SQLException;
import java.util.Iterator;
import java.util.logging.Logger;
import sun.tools.tree.ThisExpression;
/**
 * REST Web Service
 *
 * @author Brian
 */
@Path("tipoEsame")
public class TipoEsameService {
    
    private static MedicoBase_DAO       medicoBase_DAO;
    private static SSProvinciale_DAO    ssProvinciale_DAO;
    private static Esame_DAO            esame_DAO;
    
    private static DAOFactory          daoFactory;

    static public void init(DAOFactory daoFactory1) {
        daoFactory =  daoFactory1;
    }
    
    
    @Context
    private UriInfo context;

    /**
     * Creates a new instance of TipoEsameService
     */
    public TipoEsameService() {
    }
//==============================================================================
    @GET
    @Path("MedicoBase/{medicoBaseCodF}/{term}")
    public String getTipoEsami(@PathParam("medicoBaseCodF") String medicoBaseCodF,@PathParam("term") String term){
        List<TipoEsame> listaTipoEsameForJson = new ArrayList<>();
        
        List<TipoEsame> results = new ArrayList<>();
 
        try {
            esame_DAO               = daoFactory.getDAO(Esame_DAO.class);
            medicoBase_DAO          = daoFactory.getDAO(MedicoBase_DAO.class);
            
            if(medicoBase_DAO.getByPrimaryKey(medicoBaseCodF)!= null){
            
                List<Esame> listaTipoEsame = esame_DAO.getAll();
                Iterator<Esame> iterTipoEsame= listaTipoEsame.iterator();
                while(iterTipoEsame.hasNext()){
                    Esame tipoEsame= iterTipoEsame.next();

                    listaTipoEsameForJson.add(
                            new TipoEsame( tipoEsame.getId(),tipoEsame.getNome() )
                                        );

                }
            }
        } catch (DAOFactoryException ex) {
            Logger.getLogger(getClass().getName()).severe(ex.toString());
        } catch (DAOException ex) {
            Logger.getLogger(getClass().getName()).severe(ex.toString());
        }catch(NumberFormatException ex){
             Logger.getLogger(getClass().getName()).severe(ex.toString());
        }
        
        
        if ((term == null) || "undefined".equals(term)) {
            results.addAll(listaTipoEsameForJson);
        } else {
            Iterator<TipoEsame> tipoEsameIterator = listaTipoEsameForJson.iterator();
            while (tipoEsameIterator.hasNext()) {
                TipoEsame element = tipoEsameIterator.next();
                if(element.getText().toLowerCase().contains(term.toLowerCase())) {
                    results.add(element);
                }
            }
        }
        Gson gson = new Gson();
        return gson.toJson(new Results(results.toArray(new TipoEsame[0])));
    }
//==============================================================================
    @GET
    @Path("SSP/{String_idSSP}/{term}")
    public String getTipoEsamiForSSP(@PathParam("String_idSSP") String String_idSSP,@PathParam("term") String term){
        List<TipoEsame> listaTipoEsameForJson = new ArrayList<>();
        
        List<TipoEsame> results = new ArrayList<>();
 
        try {
            Integer idSSP = Integer.parseInt(String_idSSP);
            esame_DAO               = daoFactory.getDAO(Esame_DAO.class);
            medicoBase_DAO          = daoFactory.getDAO(MedicoBase_DAO.class);
            ssProvinciale_DAO       = daoFactory.getDAO(SSProvinciale_DAO.class);
            
            if(ssProvinciale_DAO.getByPrimaryKey(idSSP)!= null){
            
                List<Esame> listaTipoEsame = esame_DAO.getAll();
                Iterator<Esame> iterTipoEsame= listaTipoEsame.iterator();
                while(iterTipoEsame.hasNext()){
                    Esame tipoEsame= iterTipoEsame.next();

                    listaTipoEsameForJson.add(
                            new TipoEsame( tipoEsame.getId(),tipoEsame.getNome() )
                                        );

                }
            }
        } catch (DAOFactoryException ex) {
            Logger.getLogger(getClass().getName()).severe(ex.toString());
        } catch (DAOException ex) {
            Logger.getLogger(getClass().getName()).severe(ex.toString());
        }catch(NumberFormatException ex){
             Logger.getLogger(getClass().getName()).severe(ex.toString());
        }
        
        
        if ((term == null) || "undefined".equals(term)) {
            results.addAll(listaTipoEsameForJson);
        } else {
            Iterator<TipoEsame> tipoEsameIterator = listaTipoEsameForJson.iterator();
            while (tipoEsameIterator.hasNext()) {
                TipoEsame element = tipoEsameIterator.next();
                if(element.getText().toLowerCase().contains(term.toLowerCase())) {
                    results.add(element);
                }
            }
        }
        Gson gson = new Gson();
        return gson.toJson(new Results(results.toArray(new TipoEsame[0])));
    }
//##############################################################################
    public static class TipoEsame implements Serializable {

        private Integer id;
        private String text;

        public TipoEsame(Integer id, String text) {
            this.id = id;
            this.text = text;
        }

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getText() {
            return text;
        }

        public void setText(String text) {
            this.text = text;
        }
    }
//##############################################################################
    public static class Results implements Serializable {

        private TipoEsame[] results;

        public Results(TipoEsame[] results) {
            this.results = results;
        }

        public TipoEsame[] getResults() {
            return results;
        }

        public void setResults(TipoEsame[] results) {
            this.results = results;
        }
    }
}
