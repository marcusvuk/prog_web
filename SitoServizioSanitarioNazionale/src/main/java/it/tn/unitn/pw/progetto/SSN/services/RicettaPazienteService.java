/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.tn.unitn.pw.progetto.SSN.services;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Produces;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;
import javax.ws.rs.core.MediaType;

import com.google.gson.Gson;
import it.tn.unitn.pw.progetto.SSN.DAO.common.exception.DAOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;

import it.tn.unitn.pw.progetto.SSN.DAO.persistence.*;
import it.tn.unitn.pw.progetto.SSN.DAO.common.exception.DAOFactoryException;
import it.tn.unitn.pw.progetto.SSN.DAO.common.factory.DAOFactory;
import it.tn.unitn.pw.progetto.SSN.persistence.entities.*;
import java.sql.SQLException;
import java.sql.Date;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.HashMap;
import java.util.logging.Logger;
import sun.tools.tree.ThisExpression;

/**
 * REST Web Service
 *
 * @author Brian
 */
@Path("ricettaPaziente")
public class RicettaPazienteService {
     
    
    private static DAOFactory       daoFactory;

    private static Ricetta_DAO                  ricetta_DAO;
    private static CartellaClinicaPerMB_DAO     cartellaClinica_DAO;
    private static MedicoBase_DAO               medicoBase_DAO;
    private static Farmaco_DAO                  farmaco_DAO;
    private static Farmacia_DAO                 farmacia_DAO;
    
    static public void init(DAOFactory daoFactory1) {
        daoFactory =  daoFactory1;
    }
    
    
    @Context
    private UriInfo context;

    /**
     * Creates a new instance of RicettaPazienteService
     */
    public RicettaPazienteService() {
    }
//==============================================================================
//                                 PAZIENTI
//==============================================================================
//------------------------------------------------------------------------------
//                                 PER PAZIENTE  (fatti)
//------------------------------------------------------------------------------
    @GET
    @Path("{pazienteCodF}")
    public String getPazientiFatti(@PathParam("pazienteCodF") String pazienteCodF){
        List<String[]> results = new ArrayList();
        
        List<String[]> listaRicetteDaFiltrare = new ArrayList();

        try {
            medicoBase_DAO        = daoFactory.getDAO(MedicoBase_DAO.class);
            cartellaClinica_DAO   = daoFactory.getDAO(CartellaClinicaPerMB_DAO.class);
            ricetta_DAO           = daoFactory.getDAO(Ricetta_DAO.class);
            farmaco_DAO           = daoFactory.getDAO(Farmaco_DAO.class);
            
            List<Ricetta> listaRicette = ricetta_DAO.getByPazienteId(pazienteCodF);
            
            HashMap<Integer,MedicoBase> mediciGiaCercati = new HashMap<>();
            HashMap<Integer,Farmaco> farmacoGiaCercati = new HashMap<>();
            
            Iterator<Ricetta> esameIterator = listaRicette.iterator();
            while (esameIterator.hasNext()) {
                String[] rigaRis= new String[8];
                
                Ricetta element = esameIterator.next();
                
                MedicoBase medicoBase = mediciGiaCercati.get(element.getId_CartellaClinica());
                if(medicoBase  == null ){
                    CartellaClinicaPerMB cartellaClinica = cartellaClinica_DAO.getByPrimaryKey( element.getId_CartellaClinica() );
                    medicoBase = medicoBase_DAO.getByPrimaryKey(cartellaClinica.getId_MedicoBase());
                    mediciGiaCercati.put(element.getId_CartellaClinica(), medicoBase);
                }
                
                Farmaco farmaco = farmacoGiaCercati.get(element.getId_Farmaco());
                if(farmaco  == null ){
                    farmaco = farmaco_DAO.getByPrimaryKey(element.getId_Farmaco());
                    farmacoGiaCercati.put(element.getId_Farmaco(), farmaco);
                }
                
                //Aggunta Ricetta vista o nuova in posizione 0
                rigaRis[0] = element.getVistoPaziente().toString();
                //Aggunta eventuale data di ritiro della ricetta posizione 1
                if(element.getDataEvalsa()!=null){
                    rigaRis[1] = element.getDataEvalsa().toString();
                }else{
                    GregorianCalendar data= new GregorianCalendar();
                    Date oggi = new Date(data.getTimeInMillis());
                    if( oggi.before(element.getScadenza()) ){
                        rigaRis[1] = "DaRitirare";
                    }else{
                        rigaRis[1] = "Scaduto";
                    }
                }
                //Aggunta Data di scadenza  in posizione 2
                rigaRis[2] = element.getScadenza().toString();
                //Aggunta Nome farmaco in posizione 3
                rigaRis[3] = farmaco.getNome();
                //Aggunta Tipo di Esame  in posizione 4
                rigaRis[4] = element.getQuantita().toString();
                //Aggunta Tipo di Esame  in posizione 5
                rigaRis[5] = medicoBase.getNome();
                //Aggunta Cognome medico base in posizione 6
                rigaRis[6] = medicoBase.getCognome();
                //Aggunta Tipo di Esame  in posizione 7
                rigaRis[7] = element.getId().toString();
                
                
                listaRicetteDaFiltrare.add(rigaRis);
            }
              
        } catch (DAOFactoryException ex) {
            Logger.getLogger(getClass().getName()).severe(ex.toString());
        } catch (DAOException ex) {
            Logger.getLogger(getClass().getName()).severe(ex.toString());
        }catch(NumberFormatException ex){
             Logger.getLogger(getClass().getName()).severe(ex.toString());
        }

        results.addAll(listaRicetteDaFiltrare);

        Gson gson = new Gson();
        return gson.toJson(new Results(results.toArray(new String[0][8] ) ) );
    }
//------------------------------------------------------------------------------
//==============================================================================
//                                 FARMACIA
//==============================================================================    
//------------------------------------------------------------------------------
//                                 FARMACIA  ( da ritirare)
//------------------------------------------------------------------------------
    @GET
    @Path("{clienteCodF}/Farmcaia/{farmaciaPIVA}")
    public String getPazientiFattiPerMedicoBase(@PathParam("clienteCodF") String clienteCodF,@PathParam("farmaciaPIVA") String farmaciaPIVA){
        List<String[]> results = new ArrayList();
        
        List<String[]> listaRicetteDaFiltrare = new ArrayList();

        try {
            medicoBase_DAO        = daoFactory.getDAO(MedicoBase_DAO.class);
            cartellaClinica_DAO   = daoFactory.getDAO(CartellaClinicaPerMB_DAO.class);
            ricetta_DAO           = daoFactory.getDAO(Ricetta_DAO.class);
            farmaco_DAO           = daoFactory.getDAO(Farmaco_DAO.class);
            farmacia_DAO          = daoFactory.getDAO(Farmacia_DAO.class);
            
            
            
            if(farmacia_DAO.getByPrimaryKey(farmaciaPIVA)!=null){
                
                List<Ricetta> listaRicette = ricetta_DAO.getByPazienteId_DaRirirate(clienteCodF);

                HashMap<Integer,MedicoBase> mediciGiaCercati = new HashMap<>();
                HashMap<Integer,Farmaco> farmacoGiaCercati = new HashMap<>();

                Iterator<Ricetta> esameIterator = listaRicette.iterator();
                while (esameIterator.hasNext()) {
                    String[] rigaRis= new String[6];

                    Ricetta element = esameIterator.next();
                    
                    MedicoBase medicoBase = mediciGiaCercati.get(element.getId_CartellaClinica());
                    if(medicoBase  == null ){
                        CartellaClinicaPerMB cartellaClinica = cartellaClinica_DAO.getByPrimaryKey( element.getId_CartellaClinica() );
                        medicoBase = medicoBase_DAO.getByPrimaryKey(cartellaClinica.getId_MedicoBase());
                        mediciGiaCercati.put(element.getId_CartellaClinica(), medicoBase);
                    }

                    Farmaco farmaco = farmacoGiaCercati.get(element.getId_Farmaco());
                    if(farmaco  == null ){
                        farmaco = farmaco_DAO.getByPrimaryKey(element.getId_Farmaco());
                        farmacoGiaCercati.put(element.getId_Farmaco(), farmaco);
                    }

                    
                    //Aggunta Data di scadenza  in posizione 0
                    rigaRis[0] = element.getScadenza().toString();
                    //Aggunta Nome farmaco in posizione 1
                    rigaRis[1] = farmaco.getNome();
                    //Aggunta Tipo di Esame  in posizione 2
                    rigaRis[2] = element.getQuantita().toString();
                    //Aggunta Tipo di Esame  in posizione 3
                    rigaRis[3] = medicoBase.getNome();
                    //Aggunta Cognome medico base in posizione 4
                    rigaRis[4] = medicoBase.getCognome();
                    //Aggunta Tipo di Esame  in posizione 5
                    rigaRis[5] = element.getId().toString();


                    listaRicetteDaFiltrare.add(rigaRis);
                }
            }
        } catch (DAOFactoryException ex) {
            Logger.getLogger(getClass().getName()).severe(ex.toString());
        } catch (DAOException ex) {
            Logger.getLogger(getClass().getName()).severe(ex.toString());
        }catch(NumberFormatException ex){
             Logger.getLogger(getClass().getName()).severe(ex.toString());
        }

        results.addAll(listaRicetteDaFiltrare);

        Gson gson = new Gson();
        return gson.toJson(new Results(results.toArray(new String[0][6] ) ) );
    }
//##############################################################################
    public static class Results implements Serializable {

        private String[][] results;

        public Results(String[][] results) {
            this.results = results;
        }

        public String[][] getResults() {
            return results;
        }

        public void setResults(String[][] results) {
            this.results = results;
        }
    }
}
