/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.tn.unitn.pw.progetto.SSN.filter;

import it.tn.unitn.pw.progetto.SSN.DAO.common.exception.DAOException;
import it.tn.unitn.pw.progetto.SSN.DAO.common.exception.DAOFactoryException;
import it.tn.unitn.pw.progetto.SSN.DAO.common.factory.DAOFactory;
import it.tn.unitn.pw.progetto.SSN.DAO.persistence.*;
import it.tn.unitn.pw.progetto.SSN.persistence.entities.*;
import java.io.IOException;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.List;
import java.util.logging.Logger;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
/**
 *
 * @author Brian
 */
public class Esame_Filter implements Filter {
    
    private static final boolean debug = true;

    // The filter configuration object we are associated with.  If
    // this value is null, this filter instance is not currently
    // configured. 
    private FilterConfig filterConfig = null;
//############################################################################## 
    private static DAOFactory daoFactory;
    static public void myInit(DAOFactory daoFactory1) {
        daoFactory =  daoFactory1;
    }
//##############################################################################  
    
    public Esame_Filter() {
    }    
    
//------------------------------------------------------------------------------------------QUI IL FILTRO 
    private void doBeforeProcessing(ServletRequest request, ServletResponse response)
            throws IOException, ServletException {
        if (debug) {
            log("Esami_Filter:DoBeforeProcessing");
        }
        // Write code here to process the request and/or response before
        // the rest of the filter chain is invoked.
        // For example, a logging filter might log items on the request object,
        // such as the parameters.
        if (request instanceof HttpServletRequest) {
            ServletContext servletContext = ((HttpServletRequest) request).getServletContext();
            HttpSession session = ((HttpServletRequest) request).getSession(false);
            PrescrizioneEsameEReferto   prescrizioneEsame     = null;
            Esame                       tipoEsame             = null;
            MedicoBase                  pescrittoMB           = null;
            MedicoSpec                  reportFattoMS         = null;
            
            if (session != null) {
                prescrizioneEsame       = (PrescrizioneEsameEReferto)  session.getAttribute("c_b_s_Esame");
            }
            if (prescrizioneEsame == null) {
                String contextPath = servletContext.getContextPath();
                if (!contextPath.endsWith("/")) {
                    contextPath += "/";
                }
                
                Persona cittadino  = (Persona) ((HttpServletRequest) request).getSession(false).getAttribute("c_Persona");
                Persona pazienteMB = (Persona) ((HttpServletRequest) request).getSession(false).getAttribute("b_Paziente");
                Persona pazienteMS = (Persona) ((HttpServletRequest) request).getSession(false).getAttribute("s_Paziente");

                if( (cittadino != null) || (pazienteMB != null) || (pazienteMS == null) ){
                    ((HttpServletResponse) response).sendRedirect(((HttpServletResponse) response).encodeRedirectURL(contextPath + "Cittadino/Esami"));

                }else if( (cittadino == null) && (pazienteMB != null) && (pazienteMS == null) ){
                    ((HttpServletResponse) response).sendRedirect(((HttpServletResponse) response).encodeRedirectURL(contextPath + "MedicoBase/PazienteMB/Esami"));

                }else if( (cittadino == null) && (pazienteMB == null) && (pazienteMS != null) ){
                    ((HttpServletResponse) response).sendRedirect(((HttpServletResponse) response).encodeRedirectURL(contextPath + "MedicoSpec/PazienteMS/Esami"));
                }else {
                    //ERRORE--------------------------------------------------------------------------------------------
                    ((HttpServletResponse) response).sendRedirect(((HttpServletResponse) response).encodeRedirectURL(contextPath + "Index"));
                }
                
                
            }else{

                
                
                tipoEsame = getTipoEsame(prescrizioneEsame.getId_Esame());
                if(tipoEsame !=  null){
                    ((HttpServletRequest) request).getSession().setAttribute("c_b_s_TipoEsame", tipoEsame);
                }else{
                    ((HttpServletRequest) request).getSession().setAttribute("c_b_s_TipoEsame", null);   
                }
                
                pescrittoMB = getMedicoBaseFattoPrescrizione(prescrizioneEsame.getId_CartellaClinica());
                if(pescrittoMB !=  null){
                    ((HttpServletRequest) request).getSession().setAttribute("c_b_s_PescrittoMB", pescrittoMB);
                }else{
                    ((HttpServletRequest) request).getSession().setAttribute("c_b_s_PescrittoMB", null);   
                }
                
                if(prescrizioneEsame.getData()!= null){
                    reportFattoMS = getMedicoSpecFattoReport(prescrizioneEsame.getId_MedicoSpec());
                    if(reportFattoMS != null){
                        ((HttpServletRequest) request).getSession().setAttribute("c_b_s_ReportFattoMS", reportFattoMS);
                    }
                }else{
                    ((HttpServletRequest) request).getSession().setAttribute("c_b_s_ReportFattoMS", null);   
                }
            }
        }
    }    
//------------------------------------------------------------------------------------------^ QUI IL FILTRO
        
    
    private void doAfterProcessing(ServletRequest request, ServletResponse response)
            throws IOException, ServletException {
        if (debug) {
            log("Esami_Filter:DoAfterProcessing");
        }
    }

    /**
     *
     * @param request The servlet request we are processing
     * @param response The servlet response we are creating
     * @param chain The filter chain we are processing
     *
     * @exception IOException if an input/output error occurs
     * @exception ServletException if a servlet error occurs
     */
    public void doFilter(ServletRequest request, ServletResponse response,
            FilterChain chain)
            throws IOException, ServletException {
        
        if (debug) {
            log("Esami_Filter:doFilter()");
        }
        
        doBeforeProcessing(request, response);
        
        Throwable problem = null;
        try {
            chain.doFilter(request, response);
        } catch (IOException | ServletException | RuntimeException ex) {
            // If an exception is thrown somewhere down the filter chain,
            // we still want to execute our after processing, and then
            // rethrow the problem after that.
            problem = ex;
            request.getServletContext().log("Impossible to propagate to the next filter", ex);
        }
        
        doAfterProcessing(request, response);

        // If there was a problem, we want to rethrow it if it is
        // a known type, otherwise log it.
        if (problem != null) {
            if (problem instanceof ServletException) {
                throw (ServletException) problem;
            }
            if (problem instanceof IOException) {
                throw (IOException) problem;
            }
            sendProcessingError(problem, response);
        }
    }

    /**
     * Return the filter configuration object for this filter.
     */
    public FilterConfig getFilterConfig() {
        return (this.filterConfig);
    }

    /**
     * Set the filter configuration object for this filter.
     *
     * @param filterConfig The filter configuration object
     */
    public void setFilterConfig(FilterConfig filterConfig) {
        this.filterConfig = filterConfig;
    }

    /**
     * Destroy method for this filter
     */
    public void destroy() {        
    }

    /**
     * Init method for this filter
     */
    public void init(FilterConfig filterConfig) {        
        this.filterConfig = filterConfig;
        if (filterConfig != null) {
            if (debug) {                
                log("Esami_Filter:Initializing filter");
            }
        }
    }

    /**
     * Return a String representation of this object.
     */
    @Override
    public String toString() {
        if (filterConfig == null) {
            return ("Esami_Filter()");
        }
        StringBuffer sb = new StringBuffer("Esami_Filter(");
        sb.append(filterConfig);
        sb.append(")");
        return (sb.toString());
    }
//-------------------------------------------------------------------------------------------EROR PAGE
    private void sendProcessingError(Throwable t, ServletResponse response) {
        String stackTrace = getStackTrace(t);        
        
        if (stackTrace != null && !stackTrace.equals("")) {
            try {
                response.setContentType("text/html");
                PrintStream ps = new PrintStream(response.getOutputStream());
                PrintWriter pw = new PrintWriter(ps);                
                pw.print("<html>\n<head>\n<title>Error</title>\n</head>\n<body>\n"); //NOI18N

                // PENDING! Localize this for next official release
                pw.print("<h1>The resource did not process correctly</h1>\n<pre>\n");                
                pw.print(stackTrace);                
                pw.print("</pre></body>\n</html>"); //NOI18N
                pw.close();
                ps.close();
                response.getOutputStream().close();
            } catch (Exception ex) {
            }
        } else {
            try {
                PrintStream ps = new PrintStream(response.getOutputStream());
                t.printStackTrace(ps);
                ps.close();
                response.getOutputStream().close();
            } catch (Exception ex) {
            }
        }
    }
    
    public static String getStackTrace(Throwable t) {
        String stackTrace = null;
        try {
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            t.printStackTrace(pw);
            pw.close();
            sw.close();
            stackTrace = sw.getBuffer().toString();
        } catch (Exception ex) {
        }
        return stackTrace;
    }
    
    public void log(String msg) {
        if (filterConfig != null) {
            filterConfig.getServletContext().log(msg);
        } else {
            Logger.getLogger(msg);
        }        
    }
//==============================================================================
    private Esame getTipoEsame(Integer id){
        Esame esame=null;
        
        try {
            Esame_DAO  esameDao  = daoFactory.getDAO(Esame_DAO.class);
            esame = esameDao.getByPrimaryKey(id);
        } catch (DAOFactoryException ex) {
            Logger.getLogger(getClass().getName()).severe(ex.toString());
        } catch (DAOException ex) {
            Logger.getLogger(getClass().getName()).severe(ex.toString());
        }
         
        return esame;
    }
//==============================================================================
    private MedicoBase getMedicoBaseFattoPrescrizione(Integer id){
        MedicoBase medicoBase=null;
        CartellaClinicaPerMB catrellaClinica = null;
        try {
            MedicoBase_DAO              medicoBaseDao       = daoFactory.getDAO(MedicoBase_DAO.class);
            CartellaClinicaPerMB_DAO    CartellaClinicaDao  = daoFactory.getDAO(CartellaClinicaPerMB_DAO.class);
            catrellaClinica = CartellaClinicaDao.getByPrimaryKey(id);
            if(catrellaClinica!= null){
                medicoBase = medicoBaseDao.getByPrimaryKey(catrellaClinica.getId_MedicoBase());
            }
        } catch (DAOFactoryException ex) {
            Logger.getLogger(getClass().getName()).severe(ex.toString());
        } catch (DAOException ex) {
            Logger.getLogger(getClass().getName()).severe(ex.toString());
        }
         
        return medicoBase;
    }
//==============================================================================
    private MedicoSpec getMedicoSpecFattoReport(String codFMS){
        MedicoSpec medicoSpec=null;
        try {
            MedicoSpec_DAO              medicoSpecDao       = daoFactory.getDAO(MedicoSpec_DAO.class);
            
            medicoSpec = medicoSpecDao.getByPrimaryKey(codFMS);
            
        } catch (DAOFactoryException ex) {
            Logger.getLogger(getClass().getName()).severe(ex.toString());
        } catch (DAOException ex) {
            Logger.getLogger(getClass().getName()).severe(ex.toString());
        }
         
        return medicoSpec;
    }
//==============================================================================
    
        
}
