
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div name="CambiaPassword" id="CambiaPassword"  class="modal fade" >
    <div class="modal-dialog modal-login modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Cambia Password</h4>
                <button type="button"
                        class="close"
                        data-dismiss="modal"
                        aria-hidden="true">
                    &times;
                </button>    
            </div> 
            
            
            <c:choose>
                <c:when test="${(c_Persona ne null)}">
            <form action="${pageContext.request.contextPath}/Cittadino/Cittadino.handler" method="POST" enctype="multipart/form-data">
            
                </c:when>
                <c:when test="${(b_Persona ne null)}">
            <form action="${pageContext.request.contextPath}/MedicoBase/MedicoBase.handler" method="POST" enctype="multipart/form-data">
            
                </c:when>
                <c:when test="${(s_Persona ne null)}">
            <form action="${pageContext.request.contextPath}/MedicoSpec/MedicoSpec.handler" method="POST" enctype="multipart/form-data">
            
                </c:when>
                <c:when test="${(f_Farmacia ne null)}">
            <form action="${pageContext.request.contextPath}/Farmacia/Farmacia.handler" method="POST" enctype="multipart/form-data">
            
                </c:when>
                <c:when test="${(p_ssProvinciale ne null)}">
            <form action="${pageContext.request.contextPath}/SSP/SSP.handler" method="POST" enctype="multipart/form-data">
            
                </c:when>
            </c:choose>
            
                
                
            <div class="modal-body">
               <div class="container-fluid justify-content-center ">  
                    <div class="container">
                        <input  type="hidden" id="typeCambia" name="typeCambia" value="Password">
                        <div class="clearfix">
                            <label>Vechia Password: </label>
                        </div>
                        <input  id="passwordVechia" 
                                name="passwordVechia" 
                                type="password"
                                class="form-control"
                                required="required"/>
                    </div>
                </div>
                <hr>
                <div class="container-fluid justify-content-center ">  
                    <div class="container">
                        <div class="clearfix">
                            <label>Nuova Password:</label>
                        </div>
                        <input  id="passwordNuova" 
                                name="passwordNuova" 
                                type="password"
                                class="form-control"
                                required="required"/>
                    </div>
                </div>
                <div class="container-fluid justify-content-center ">  
                    <div class="container">
                        <div class="clearfix">
                            <label>Ripeti Nuova Password:</label>
                        </div>
                        <input  id="passwordNuovaRipetuta" 
                                name="passwordNuovaRipetuta" 
                                type="password"
                                class="form-control"
                                required="required"/>
                    </div>
                </div>
            </div>
            <div class=" container-fluid modal-footer">
                <input  type="submit"
                        class="btn btn-primary pull-right"
                        accept=""value="Conferma"/>
            </div>
            </form>
        </div>
    </div>
</div>