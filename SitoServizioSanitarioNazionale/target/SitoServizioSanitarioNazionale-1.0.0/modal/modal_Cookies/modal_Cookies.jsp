
<%  String classListi = "position-fixed  fixed-bottom  bg-warning collapse show"; 
    Cookie[] cookies=request.getCookies();
    
    if(cookies != null){
        for(Cookie cookie : cookies){
            if(cookie.getName().equals("jcookiesallert")){
                classListi = "position-fixed  fixed-bottom  bg-warning collapse"; 
            }
        }
    }
%>

<div id="CookiesAllert"
     class="<%=classListi %>">
    <div class="container-fluid">
        <div class="row justify-content-between">
            <div class="col-sm-10">
                Questo sito utilizza cookie tecnici per capire come si usa il nostro 
                sito e per migliorare la tua esperienza. <br>
                Proseguendo nella navigazione accetti l'utilizzo dei cookie.
            </div>
            <div class="col-sm-2 justify-content-center">
                <button class="btn btn-light" 
                        onclick="allertCookies()"
                        type="button"
                        id="btn_Cookies"
                        data-toggle="collapse"
                        data-target="#CookiesAllert">
                            Accetto
                </button>
                <script>
                    function allertCookies(){
                        document.cookie="jcookiesallert=true";
                    }
                </script>
            </div>
        </div>
    </div>
</div>