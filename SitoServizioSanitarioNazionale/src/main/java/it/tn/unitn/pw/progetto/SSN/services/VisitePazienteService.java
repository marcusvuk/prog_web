/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.tn.unitn.pw.progetto.SSN.services;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Produces;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;
import javax.ws.rs.core.MediaType;

import com.google.gson.Gson;
import it.tn.unitn.pw.progetto.SSN.DAO.common.exception.DAOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;

import it.tn.unitn.pw.progetto.SSN.DAO.persistence.*;
import it.tn.unitn.pw.progetto.SSN.DAO.common.exception.DAOFactoryException;
import it.tn.unitn.pw.progetto.SSN.DAO.common.factory.DAOFactory;
import it.tn.unitn.pw.progetto.SSN.persistence.entities.*;
import java.sql.SQLException;
import java.sql.Date;
import java.util.Iterator;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.logging.Logger;
import sun.tools.tree.ThisExpression;

/**
 * REST Web Service
 *
 * @author Brian
 */
@Path("VisitePaziente")
public class VisitePazienteService {
    
    private static DAOFactory       daoFactory;

    private static VisitaMB_DAO             visitaMB_DAO;
    private static VisitaMSEReport_DAO      visitaMSEReport_DAO;
    private static MedicoBase_DAO           medicoBase_DAO;
    private static MedicoSpec_DAO           medicoSpec_DAO;
    private static CartellaClinicaPerMB_DAO cartellaClinica_DAO;
    
    static public void init(DAOFactory daoFactory1) {
        daoFactory =  daoFactory1;
    }
    
    @Context
    private UriInfo context;

    /**
     * Creates a new instance of VisitePazienteService
     */
    public VisitePazienteService() {
    }
//==============================================================================
//                                 PAZIENTI
//==============================================================================
//------------------------------------------------------------------------------
//                                 PER PAZIENTE  (fatti)
//------------------------------------------------------------------------------
    @GET
    @Path("Fatte/{pazienteCodF}")
    public String getVisitaPazientiFatti(@PathParam("pazienteCodF") String pazienteCodF){
        List<String[]> results = new ArrayList();
        
        List<String[]> listaEsamiDaFiltrare = new ArrayList();

        try {
            visitaMB_DAO          = daoFactory.getDAO(VisitaMB_DAO.class);
            visitaMSEReport_DAO   = daoFactory.getDAO(VisitaMSEReport_DAO.class);
            medicoBase_DAO        = daoFactory.getDAO(MedicoBase_DAO.class);
            medicoSpec_DAO        = daoFactory.getDAO(MedicoSpec_DAO.class);
            
            List<VisitaMB>        listaVisiteMB = visitaMB_DAO.getByPazienteId_Fatte(pazienteCodF);
            List<VisitaMSEReport> listaVisiteMS = visitaMSEReport_DAO.getByPazienteId_Fatte(pazienteCodF);
            
            List<Visita> listaTutteVisite= new LinkedList<Visita>();
            listaTutteVisite.addAll(listaVisiteMB);
            listaTutteVisite.addAll(listaVisiteMS);
            
            HashMap<String,Persona> mediciGiaCercati = new HashMap<>();
            
            Iterator<Visita> visitaIterator = listaTutteVisite.iterator();
            while (visitaIterator.hasNext()) {
                String[] rigaRis= new String[6];
                
                Visita element = visitaIterator.next();
                String CodFMedico = null;
                String timpoVisiata="";
                if(element instanceof VisitaMB){
                    timpoVisiata = "<b>Visita medica:</b> \"";
                    CodFMedico =  ((VisitaMB)element).getId_MedicoBase();
                }else if(element instanceof VisitaMSEReport){
                    timpoVisiata = "<b>Visita specialistica:</b> \"";
                    CodFMedico =  ((VisitaMSEReport)element).getId_MedicoSpec();
                }
                
                Persona medico = mediciGiaCercati.get(CodFMedico);
                if(medico == null){
                    if(element instanceof VisitaMB){
                        medico     = medicoBase_DAO.getByPrimaryKey(CodFMedico);
                    }else if(element instanceof VisitaMSEReport){
                        medico     = medicoSpec_DAO.getByPrimaryKey(CodFMedico);
                    }
                    mediciGiaCercati.put(CodFMedico, medico);
                }
                
               
                //Aggunta Esame è fatto in posizione 0
                rigaRis[0] = element.getVistoReportPaziente().toString();
                //Aggunta Data di prescrizione in posizione 1
                rigaRis[1] = element.getDataPrescrizione().toString();
                //Aggunta Nome medico base in posizione 2
                rigaRis[2] = medico.getNome();
                //Aggunta Cognome medico base in posizione 3
                rigaRis[3] = medico.getCognome();
                //Aggunta Riassunto  in posizione 4
                rigaRis[4] = timpoVisiata + element.getPrescrizione() +" \"";
                //Aggunta Pulsante  in posizione 5
                if(element instanceof VisitaMB){
                    rigaRis[5] = "VMB-"+element.getId().toString();
                }else if(element instanceof VisitaMSEReport){
                    rigaRis[5] = "VMS-"+element.getId().toString();
                }else{
                    rigaRis[5] = "UND-"+element.getId().toString();
                }
                
                listaEsamiDaFiltrare.add(rigaRis);
            }
              
        } catch (DAOFactoryException ex) {
            Logger.getLogger(getClass().getName()).severe(ex.toString());
        } catch (DAOException ex) {
            Logger.getLogger(getClass().getName()).severe(ex.toString());
        }catch(NumberFormatException ex){
             Logger.getLogger(getClass().getName()).severe(ex.toString());
        }

        results.addAll(listaEsamiDaFiltrare);

        Gson gson = new Gson();
        return gson.toJson(new Results(results.toArray(new String[0][6] ) ) );
    }
//------------------------------------------------------------------------------
    
//------------------------------------------------------------------------------
//                                 PER PAZIENTE  (da fare)
//------------------------------------------------------------------------------
    @GET
    @Path("DaFare/{pazienteCodF}")
    public String getPazientiDaFare(@PathParam("pazienteCodF") String pazienteCodF){
        List<String[]> results = new ArrayList();
        
        List<String[]> listaEsamiDaFiltrare = new ArrayList();

        try {
            visitaMB_DAO          = daoFactory.getDAO(VisitaMB_DAO.class);
            visitaMSEReport_DAO   = daoFactory.getDAO(VisitaMSEReport_DAO.class);
            medicoBase_DAO        = daoFactory.getDAO(MedicoBase_DAO.class);
            medicoSpec_DAO        = daoFactory.getDAO(MedicoSpec_DAO.class);
            
            List<VisitaMB>        listaVisiteMB = visitaMB_DAO.getByPazienteId_DaFare(pazienteCodF);
            List<VisitaMSEReport> listaVisiteMS = visitaMSEReport_DAO.getByPazienteId_DaFare(pazienteCodF);
            
            List<Visita> listaTutteVisite= new LinkedList<Visita>();
            listaTutteVisite.addAll(listaVisiteMB);
            listaTutteVisite.addAll(listaVisiteMS);
            
            HashMap<String,Persona> mediciGiaCercati = new HashMap<>();
            
            Iterator<Visita> visitaIterator = listaTutteVisite.iterator();
            while (visitaIterator.hasNext()) {
                String[] rigaRis= new String[6];
                
                Visita element = visitaIterator.next();
                String CodFMedico = null;
                String timpoVisiata="";
                if(element instanceof VisitaMB){
                    timpoVisiata = "<b>Visita medica:</b> \"";
                    CodFMedico =  ((VisitaMB)element).getId_MedicoBase();
                }else if(element instanceof VisitaMSEReport){
                    timpoVisiata = "<b>Visita specialistica:</b> \"";
                    CodFMedico =  ((VisitaMSEReport)element).getId_MedicoSpec();
                }
                
                Persona medico = mediciGiaCercati.get(CodFMedico);
                if(medico == null){
                    if(element instanceof VisitaMB){
                        medico     = medicoBase_DAO.getByPrimaryKey(CodFMedico);
                    }else if(element instanceof VisitaMSEReport){
                        medico     = medicoSpec_DAO.getByPrimaryKey(CodFMedico);
                    }
                    mediciGiaCercati.put(CodFMedico, medico);
                }
                
               
                //Aggunta Esame è fatto in posizione 0
                rigaRis[0] = element.getVistoPaziente().toString();
                //Aggunta Data di prescrizione in posizione 1
                rigaRis[1] = element.getDataPrescrizione().toString();
                //Aggunta Nome medico base in posizione 2
                rigaRis[2] = medico.getNome();
                //Aggunta Cognome medico base in posizione 3
                rigaRis[3] = medico.getCognome();
                //Aggunta Riassunto  in posizione 4
                rigaRis[4] = timpoVisiata + element.getPrescrizione() +" \"";
                //Aggunta Pulsante  in posizione 5
                if(element instanceof VisitaMB){
                    rigaRis[5] = "VMB-"+element.getId().toString();
                }else if(element instanceof VisitaMSEReport){
                    rigaRis[5] = "VMS-"+element.getId().toString();
                }else{
                    rigaRis[5] = "UND-"+element.getId().toString();
                }
                
                listaEsamiDaFiltrare.add(rigaRis);
            }
              
        } catch (DAOFactoryException ex) {
            Logger.getLogger(getClass().getName()).severe(ex.toString());
        } catch (DAOException ex) {
            Logger.getLogger(getClass().getName()).severe(ex.toString());
        }catch(NumberFormatException ex){
             Logger.getLogger(getClass().getName()).severe(ex.toString());
        }

        results.addAll(listaEsamiDaFiltrare);

        Gson gson = new Gson();
        return gson.toJson(new Results(results.toArray(new String[0][6] ) ) );
    }
//------------------------------------------------------------------------------
    
//==============================================================================
//                                 MEDICO BASE
//==============================================================================    
//------------------------------------------------------------------------------
//                                 PER MEDICO BASE  (fatti)
//------------------------------------------------------------------------------
    @GET
    @Path("Fatte/{pazienteCodF}/PerMedicoBase/{medicoBaseCodF}")
    public String getPazientiFattiPerMedicoBase(@PathParam("pazienteCodF") String pazienteCodF,@PathParam("medicoBaseCodF") String medicoBaseCodF){
        List<String[]> results = new ArrayList();
        
        List<String[]> listaEsamiDaFiltrare = new ArrayList();

        try {
            visitaMB_DAO          = daoFactory.getDAO(VisitaMB_DAO.class);
            visitaMSEReport_DAO   = daoFactory.getDAO(VisitaMSEReport_DAO.class);
            cartellaClinica_DAO   = daoFactory.getDAO(CartellaClinicaPerMB_DAO.class);
            medicoBase_DAO        = daoFactory.getDAO(MedicoBase_DAO.class);
            medicoSpec_DAO        = daoFactory.getDAO(MedicoSpec_DAO.class);
            
            CartellaClinicaPerMB cartellaClinicaAttiva = cartellaClinica_DAO.getByPazienteId_Attivo(pazienteCodF);
            if(cartellaClinicaAttiva.getId_MedicoBase().equals(medicoBaseCodF)){
                
                List<VisitaMB>        listaVisiteMB = visitaMB_DAO.getByPazienteId_Fatte(pazienteCodF);
                List<VisitaMSEReport> listaVisiteMS = visitaMSEReport_DAO.getByPazienteId_Fatte(pazienteCodF);

                List<Visita> listaTutteVisite= new LinkedList<Visita>();
                listaTutteVisite.addAll(listaVisiteMB);
                listaTutteVisite.addAll(listaVisiteMS);

                HashMap<String,Persona> mediciGiaCercati = new HashMap<>();

                Iterator<Visita> visitaIterator = listaTutteVisite.iterator();
                while (visitaIterator.hasNext()) {
                    String[] rigaRis= new String[6];

                    Visita element = visitaIterator.next();
                    String CodFMedico = null;
                    String timpoVisiata="";
                    if(element instanceof VisitaMB){
                        timpoVisiata = "<b>Visita medica:</b> \"";
                        CodFMedico =  ((VisitaMB)element).getId_MedicoBase();
                    }else if(element instanceof VisitaMSEReport){
                        timpoVisiata = "<b>Visita specialistica:</b> \"";
                        CodFMedico =  ((VisitaMSEReport)element).getId_MedicoSpec();
                    }

                    Persona medico = mediciGiaCercati.get(CodFMedico);
                    if(medico == null){
                        if(element instanceof VisitaMB){
                            medico     = medicoBase_DAO.getByPrimaryKey(CodFMedico);
                        }else if(element instanceof VisitaMSEReport){
                            medico     = medicoSpec_DAO.getByPrimaryKey(CodFMedico);
                        }
                        mediciGiaCercati.put(CodFMedico, medico);
                    }


                    //Aggunta Esame è fatto in posizione 0
                    Boolean visto= true;
                    if(element instanceof VisitaMB){
                        rigaRis[0] = visto.toString();
                    }else if(element instanceof VisitaMSEReport){
                        rigaRis[0] = ((VisitaMSEReport)element).getVistoReportMedicoBase().toString();
                    }else{
                        rigaRis[0] = visto.toString();
                    }
                    //Aggunta Data di prescrizione in posizione 1
                    rigaRis[1] = element.getDataPrescrizione().toString();
                    //Aggunta Nome medico base in posizione 2
                    rigaRis[2] = medico.getNome();
                    //Aggunta Cognome medico base in posizione 3
                    rigaRis[3] = medico.getCognome();
                    //Aggunta Riassunto  in posizione 4
                    rigaRis[4] = timpoVisiata + element.getPrescrizione() +" \"";
                    //Aggunta Pulsante  in posizione 5
                    if(element instanceof VisitaMB){
                        rigaRis[5] = "VMB-"+element.getId().toString();
                    }else if(element instanceof VisitaMSEReport){
                        rigaRis[5] = "VMS-"+element.getId().toString();
                    }else{
                        rigaRis[5] = "UND-"+element.getId().toString();
                    }

                    listaEsamiDaFiltrare.add(rigaRis);
                }
            }
        } catch (DAOFactoryException ex) {
            Logger.getLogger(getClass().getName()).severe(ex.toString());
        } catch (DAOException ex) {
            Logger.getLogger(getClass().getName()).severe(ex.toString());
        }catch(NumberFormatException ex){
             Logger.getLogger(getClass().getName()).severe(ex.toString());
        }

        results.addAll(listaEsamiDaFiltrare);

        Gson gson = new Gson();
        return gson.toJson(new Results(results.toArray(new String[0][6] ) ) );
    }
//------------------------------------------------------------------------------
    
//------------------------------------------------------------------------------
//                                 PER MEDICO BASE  (da fare)
//------------------------------------------------------------------------------
    @GET
    @Path("DaFare/{pazienteCodF}/PerMedicoBase/{medicoBaseCodF}")
    public String getPazientiDaFarePerMedicoBase(@PathParam("pazienteCodF") String pazienteCodF,@PathParam("medicoBaseCodF") String medicoBaseCodF){
        List<String[]> results = new ArrayList();
        
        List<String[]> listaEsamiDaFiltrare = new ArrayList();

        try {
            visitaMB_DAO          = daoFactory.getDAO(VisitaMB_DAO.class);
            visitaMSEReport_DAO   = daoFactory.getDAO(VisitaMSEReport_DAO.class);
            cartellaClinica_DAO   = daoFactory.getDAO(CartellaClinicaPerMB_DAO.class);
            medicoBase_DAO        = daoFactory.getDAO(MedicoBase_DAO.class);
            medicoSpec_DAO        = daoFactory.getDAO(MedicoSpec_DAO.class);
            
            CartellaClinicaPerMB cartellaClinicaAttiva = cartellaClinica_DAO.getByPazienteId_Attivo(pazienteCodF);
            if(cartellaClinicaAttiva.getId_MedicoBase().equals(medicoBaseCodF)){
                
                List<VisitaMB>        listaVisiteMB = visitaMB_DAO.getByPazienteId_DaFare(pazienteCodF);
                List<VisitaMSEReport> listaVisiteMS = visitaMSEReport_DAO.getByPazienteId_DaFare(pazienteCodF);

                List<Visita> listaTutteVisite= new LinkedList<Visita>();
                listaTutteVisite.addAll(listaVisiteMB);
                listaTutteVisite.addAll(listaVisiteMS);

                HashMap<String,Persona> mediciGiaCercati = new HashMap<>();

                Iterator<Visita> visitaIterator = listaTutteVisite.iterator();
                while (visitaIterator.hasNext()) {
                    String[] rigaRis= new String[6];

                    Visita element = visitaIterator.next();
                    String CodFMedico = null;
                    String timpoVisiata="";
                    if(element instanceof VisitaMB){
                        timpoVisiata = "<b>Visita medica:</b> \"";
                        CodFMedico =  ((VisitaMB)element).getId_MedicoBase();
                    }else if(element instanceof VisitaMSEReport){
                        timpoVisiata = "<b>Visita specialistica:</b> \"";
                        CodFMedico =  ((VisitaMSEReport)element).getId_MedicoSpec();
                    }

                    Persona medico = mediciGiaCercati.get(CodFMedico);
                    if(medico == null){
                        if(element instanceof VisitaMB){
                            medico     = medicoBase_DAO.getByPrimaryKey(CodFMedico);
                        }else if(element instanceof VisitaMSEReport){
                            medico     = medicoSpec_DAO.getByPrimaryKey(CodFMedico);
                        }
                        mediciGiaCercati.put(CodFMedico, medico);
                    }


                    //Aggunta Esame è fatto in posizione 0
                    Boolean visto= true;
                    rigaRis[0] = visto.toString();
                    //Aggunta Data di prescrizione in posizione 1
                    rigaRis[1] = element.getDataPrescrizione().toString();
                    //Aggunta Nome medico base in posizione 2
                    rigaRis[2] = medico.getNome();
                    //Aggunta Cognome medico base in posizione 3
                    rigaRis[3] = medico.getCognome();
                    //Aggunta Riassunto  in posizione 4
                    rigaRis[4] = timpoVisiata + element.getPrescrizione() +" \"";
                    //Aggunta Pulsante  in posizione 5
                    if(element instanceof VisitaMB){
                        rigaRis[5] = "VMB-"+element.getId().toString();
                    }else if(element instanceof VisitaMSEReport){
                        rigaRis[5] = "VMS-"+element.getId().toString();
                    }else{
                        rigaRis[5] = "UND-"+element.getId().toString();
                    }

                    listaEsamiDaFiltrare.add(rigaRis);
                }
            }
        } catch (DAOFactoryException ex) {
            Logger.getLogger(getClass().getName()).severe(ex.toString());
        } catch (DAOException ex) {
            Logger.getLogger(getClass().getName()).severe(ex.toString());
        }catch(NumberFormatException ex){
             Logger.getLogger(getClass().getName()).severe(ex.toString());
        }

        results.addAll(listaEsamiDaFiltrare);

        Gson gson = new Gson();
        return gson.toJson(new Results(results.toArray(new String[0][6] ) ) );
    }
//------------------------------------------------------------------------------

//==============================================================================
//                                 MEDICO SPECIALISTA
//==============================================================================     
//------------------------------------------------------------------------------
//                                 PER  MEDICO SPECIALISTA  
//------------------------------------------------------------------------------
    @GET
    @Path("{pazienteCodF}/PerMedicoSpec/{medicoSpecCodF}")
    public String getPazientiFattiPerMedicoSpec(@PathParam("pazienteCodF") String pazienteCodF,@PathParam("medicoSpecCodF") String medicoSpecCodF){
        List<String[]> results = new ArrayList();
        
        List<String[]> listaEsamiDaFiltrare = new ArrayList();

        try {
            visitaMB_DAO          = daoFactory.getDAO(VisitaMB_DAO.class);
            visitaMSEReport_DAO   = daoFactory.getDAO(VisitaMSEReport_DAO.class);
            cartellaClinica_DAO   = daoFactory.getDAO(CartellaClinicaPerMB_DAO.class);
            medicoBase_DAO        = daoFactory.getDAO(MedicoBase_DAO.class);
            medicoSpec_DAO        = daoFactory.getDAO(MedicoSpec_DAO.class);
            
            MedicoSpec medicoSpec = medicoSpec_DAO.getByPrimaryKey(medicoSpecCodF);
            if(medicoSpec!= null){
                
                List<VisitaMB>        listaVisiteMB = visitaMB_DAO.getByPazienteId(pazienteCodF);
                List<VisitaMSEReport> listaVisiteMS = visitaMSEReport_DAO.getByPazienteId(pazienteCodF);

                List<Visita> listaTutteVisite= new LinkedList<Visita>();
                listaTutteVisite.addAll(listaVisiteMB);
                listaTutteVisite.addAll(listaVisiteMS);

                HashMap<String,Persona> mediciGiaCercati = new HashMap<>();

                Iterator<Visita> visitaIterator = listaTutteVisite.iterator();
                while (visitaIterator.hasNext()) {
                    String[] rigaRis= new String[6];

                    Visita element = visitaIterator.next();
                    String CodFMedico = null;
                    String timpoVisiata="";
                    if(element instanceof VisitaMB){
                        timpoVisiata = "<b>Visita medica:</b> \"";
                        CodFMedico =  ((VisitaMB)element).getId_MedicoBase();
                    }else if(element instanceof VisitaMSEReport){
                        timpoVisiata = "<b>Visita specialistica:</b> \"";
                        CodFMedico =  ((VisitaMSEReport)element).getId_MedicoSpec();
                    }

                    Persona medico = mediciGiaCercati.get(CodFMedico);
                    if(medico == null){
                        if(element instanceof VisitaMB){
                            medico     = medicoBase_DAO.getByPrimaryKey(CodFMedico);
                        }else if(element instanceof VisitaMSEReport){
                            medico     = medicoSpec_DAO.getByPrimaryKey(CodFMedico);
                        }
                        mediciGiaCercati.put(CodFMedico, medico);
                    }


                    //Aggunta mia visite da fare è fatto in posizione 0
                    Boolean stato= !( (element instanceof VisitaMSEReport) &&
                                     (((VisitaMSEReport)element).getData()==null) &&
                                     (((VisitaMSEReport)element).getId_MedicoSpec().equals(medicoSpecCodF)) );
                    rigaRis[0] = stato.toString();
                    //Aggunta Data di prescrizione in posizione 1
                    rigaRis[1] = element.getDataPrescrizione().toString();
                    //Aggunta Nome medico base in posizione 2
                    rigaRis[2] = medico.getNome();
                    //Aggunta Cognome medico base in posizione 3
                    rigaRis[3] = medico.getCognome();
                    //Aggunta Riassunto  in posizione 4
                    rigaRis[4] = timpoVisiata + element.getPrescrizione() +" \"";
                    //Aggunta Pulsante  in posizione 5
                    if(element instanceof VisitaMB){
                        rigaRis[5] = "VMB-"+element.getId().toString();
                    }else if(element instanceof VisitaMSEReport){
                        rigaRis[5] = "VMS-"+element.getId().toString();
                    }else{
                        rigaRis[5] = "UND-"+element.getId().toString();
                    }

                    listaEsamiDaFiltrare.add(rigaRis);
                }
            }
        } catch (DAOFactoryException ex) {
            Logger.getLogger(getClass().getName()).severe(ex.toString());
        } catch (DAOException ex) {
            Logger.getLogger(getClass().getName()).severe(ex.toString());
        }catch(NumberFormatException ex){
             Logger.getLogger(getClass().getName()).severe(ex.toString());
        }

        results.addAll(listaEsamiDaFiltrare);

        Gson gson = new Gson();
        return gson.toJson(new Results(results.toArray(new String[0][6] ) ) );
    }
//------------------------------------------------------------------------------
//##############################################################################
    public static class Results implements Serializable {

        private String[][] results;

        public Results(String[][] results) {
            this.results = results;
        }

        public String[][] getResults() {
            return results;
        }

        public void setResults(String[][] results) {
            this.results = results;
        }
    }
}
