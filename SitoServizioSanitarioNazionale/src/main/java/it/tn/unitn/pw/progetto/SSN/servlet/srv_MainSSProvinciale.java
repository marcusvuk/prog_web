/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.tn.unitn.pw.progetto.SSN.servlet;

import it.tn.unitn.pw.progetto.SSN.DAO.common.exception.DAOException;
import it.tn.unitn.pw.progetto.SSN.DAO.common.exception.DAOFactoryException;
import it.tn.unitn.pw.progetto.SSN.DAO.common.factory.DAOFactory;
import it.tn.unitn.pw.progetto.SSN.DAO.persistence.Farmacia_DAO;
import it.tn.unitn.pw.progetto.SSN.DAO.persistence.MedicoBase_DAO;
import it.tn.unitn.pw.progetto.SSN.DAO.persistence.Foto_DAO;
import it.tn.unitn.pw.progetto.SSN.DAO.persistence.Persona_DAO;
import it.tn.unitn.pw.progetto.SSN.DAO.persistence.SSProvinciale_DAO;
import it.tn.unitn.pw.progetto.SSN.DAO.persistence.CartellaClinicaPerMB_DAO;
import it.tn.unitn.pw.progetto.SSN.servlet.srv_Index;
import it.tn.unitn.pw.progetto.SSN.persistence.entities.*;
import java.io.File;
import java.util.GregorianCalendar;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import java.util.Enumeration;
import java.util.List;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import jdk.internal.misc.VM;

/**
 *
 * @author Brian
 */
@MultipartConfig
public class srv_MainSSProvinciale extends HttpServlet {
    
    public  String          uploadFotoDir;
    
    private SSProvinciale_DAO    ssProvinciale_DAO;
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        uploadFotoDir = getServletContext().getInitParameter("uploadFotoDir");
        if (uploadFotoDir == null) {
            throw new ServletException("Please supply uploadDir parameter");
        }
        
        
        DAOFactory daoFactory = (DAOFactory) super.getServletContext().getAttribute("daoFactory");
        if (daoFactory == null) {
            throw new ServletException("Impossible to get dao factory for user storage system");
        }
        try {
            ssProvinciale_DAO            = daoFactory.getDAO(SSProvinciale_DAO.class);
        } catch (DAOFactoryException ex) {
            throw new ServletException("Impossible to get dao factory for user storage system", ex);
        }
    }

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        response.sendRedirect("SSP.jsp"); 
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String typeCambia = request.getParameter("typeCambia");
        
        if(typeCambia.equals("Password")){
            modificaPassword(request,response);
        }else{
            // ERROR ------------------------------------------------------------------------------------------
            response.sendRedirect("SSP.jsp");
            // ERROR ------------------------------------------------------------------------------------------
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

//####################################################################################
//####################################################################################
//==============================================================================
//                          Modifica  password attiva
//==============================================================================
    protected void modificaPassword (HttpServletRequest request, HttpServletResponse response)
                                                       throws ServletException, IOException {
        String contextPath = getServletContext().getContextPath();
        if (!contextPath.endsWith("/")) {
            contextPath += "/";
        }
        
        String passwordVechia           = request.getParameter("passwordVechia");
        String passwordNuova            = request.getParameter("passwordNuova");
        String passwordNuovaRipetuto    = request.getParameter("passwordNuovaRipetuta");
        
        SSProvinciale ssProvinciale     = (SSProvinciale) request.getSession(false).getAttribute("p_ssProvinciale");
        
        String passwordVechiaHashed     =ssProvinciale.getPassword();
        
        if(passwordNuova.equals(passwordNuovaRipetuto)){
            
            if(srv_Index.convertToHashPassword(passwordVechia, ssProvinciale.getSalt()).equals(passwordVechiaHashed)){
                ssProvinciale = cambiaPassword(passwordNuova,ssProvinciale);
                if(!ssProvinciale.getPassword().equals(passwordVechiaHashed)){
                    request.getSession().setAttribute("p_ssProvinciale", ssProvinciale);
                    response.sendRedirect(response.encodeRedirectURL(contextPath + "SSP/ConfermaModifica.jsp"));
                }else{
                    response.sendRedirect(response.encodeRedirectURL(contextPath + "SSP/SSP.jsp"));
                }
            }else{
                response.sendRedirect(response.encodeRedirectURL(contextPath + "SSP/SSP.jsp"));
            }
        }else{
            response.sendRedirect(response.encodeRedirectURL(contextPath + "SSP/SSP.jsp"));
        }
    }
//==============================================================================
    private SSProvinciale cambiaPassword(String password, SSProvinciale ssProvinciale){
        try {
            String passwordHashed=srv_Index.convertToHashPassword(password, ssProvinciale.getSalt());
            ssProvinciale_DAO.update_Password(ssProvinciale.getId(), passwordHashed);
            ssProvinciale.setPassword(passwordHashed);
        } catch (DAOException ex) {
            Logger.getLogger(getClass().getName()).severe(ex.toString());
        }
        
        return ssProvinciale;
    }
}
