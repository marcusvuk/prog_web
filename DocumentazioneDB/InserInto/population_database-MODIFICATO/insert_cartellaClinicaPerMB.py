#Python 3.6.7
#########################################FUNZIONI############################################
def scorri(stream):#funzione che ti scorre le righe di un file
    line = stream.readline()
    line=line.replace('\n', '')
    #print(line)
    stream.seek(stream.tell(), 0)
    return line

#############################################################################################



output=open("popola_cartellaClinicaMB.sql", "w")
#questo array ho dovuto scriverlo perchè ho popolato i mediciDiBase a mano quindi non avevo modo
#di prendere i codici fiscali solo dei medici di base.
mediciBase=['WNOSHN70B22I754B','DHMLYN56P05L483U', 'KNCDEI38M15D612H', 'MSSLSI02L55H294E', 'WTKSLN15A66L736V', 'BRYLWN05P02F704C', 'CYLSWN76D60A952Y']
codici = open("CodiciFiscali.txt", "r")
date = open("DateNascita.txt", "r")
luoghi = open("Luoghi.txt", "r")
for i in range(0,100):
    loco = scorri(luoghi)
    if loco=='Trento':
        medico=mediciBase[0]
    elif loco=='Rimini':
        medico=mediciBase[1]
    elif loco=='Firenze':
        medico=mediciBase[2]
    elif loco=='Bolzano':
        medico=mediciBase[3]
    elif loco=='Venezia':
        medico=mediciBase[4]
    elif loco=='Siracusa':
        medico=mediciBase[5]
    elif loco=='Ancona':
        medico=mediciBase[6]
    output.write("INSERT INTO cartellaclinicapermb(datainizio,datafine,id_medicobase,id_persona,vistomedicobase) VALUES ('"+scorri(date)+"', NULL,'"+medico+"','"+scorri(codici)+"', 'True');\n")