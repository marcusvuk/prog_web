/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.tn.unitn.pw.progetto.SSN.DAO.persistence.jdbc;

import java.sql.Date;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import it.tn.unitn.pw.progetto.SSN.DAO.common.persistence.dao.jdbc.JDBCDAO;
import it.tn.unitn.pw.progetto.SSN.DAO.common.exception.*;
import it.tn.unitn.pw.progetto.SSN.DAO.persistence.*;
import it.tn.unitn.pw.progetto.SSN.persistence.entities.*;
/**
 *
 * @author Brian
 */
public class JDBC_PrescrizioneEsameEReferto_DAO extends JDBCDAO<PrescrizioneEsameEReferto, Integer> implements PrescrizioneEsameEReferto_DAO {
/*============================================================================*/    
/*                                      METODI COSTRUTTORI                    */
/*============================================================================*/
    public JDBC_PrescrizioneEsameEReferto_DAO(Connection con) {
        super(con);
    }
/*============================================================================*/
    
/*============================================================================*/    
/*                      METODI GETTER DAO IMPLEMENTATION                      */
/*============================================================================*/
    @Override
    public Long getCount() throws DAOException {
        try (Statement stmt = CON.createStatement()) {
            ResultSet counter = stmt.executeQuery( 
                     "  SELECT COUNT(*) "               +
                     "  FROM PrescrizioneEsameEReferto "     );
            if (counter.next()) {
                return counter.getLong(1);
            }
        } catch (SQLException ex) {
            throw new DAOException( " Immpossibile contare gli elementi di PrescrizioneEsameEReferto  " , ex);
        }
        return 0L;
    }
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    @Override
    public PrescrizioneEsameEReferto getByPrimaryKey(Integer primaryKey) throws DAOException {
        if (primaryKey == null) {
            throw new DAOException( " primaryKey è null " );
        }
        try (PreparedStatement stm = CON.prepareStatement(
                             "  SELECT *  "                             +
                             "  FROM PrescrizioneEsameEReferto  "       +
                             "  WHERE id = ? "                          )) {
            // imposto i valori
            stm.setInt(1, primaryKey);
            
            try (ResultSet rs = stm.executeQuery()) {
                rs.next();
                PrescrizioneEsameEReferto esameEReferto = new PrescrizioneEsameEReferto();
                esameEReferto.setId                     (rs.getInt      ( "id" ));
                esameEReferto.setData                   (rs.getDate     ( "data" ));
                esameEReferto.setDataPrescrizione       (rs.getDate     ( "dataPrescrizione" ));
                esameEReferto.setRisultato              (rs.getString   ( "risultato" ));
                esameEReferto.setTichet                 (rs.getDouble   ( "tichet" ));
                esameEReferto.setPagato                 (rs.getBoolean  ( "pagato" ));
                esameEReferto.setCura                   (rs.getString   ( "cura" ));
                esameEReferto.setRichiamo               (rs.getBoolean  ( "richiamo" ));
                esameEReferto.setId_CartellaClinica     (rs.getInt      ( "id_CartellaClinica" ));
                esameEReferto.setId_MedicoSpec          (rs.getString   ( "id_MedicoSpec" ));
                esameEReferto.setId_Esame               (rs.getInt      ( "id_Esame" ));
                esameEReferto.setVistoPaziente          (rs.getBoolean  ( "vistoPaziente" ));
                esameEReferto.setVistoReportPaziente    (rs.getBoolean  ( "vistoReportPaziente" ));
                esameEReferto.setVistoReportMedicoBase  (rs.getBoolean  ( "vistoReportoMedicoBase" ));
                        
                return esameEReferto;
            }
        } catch (SQLException ex) {
            throw new DAOException( " Impossibile perendere la PrescrizioneEsameEReferto indicato dalla primary key " , ex);
        }
    }
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    @Override
    public List<PrescrizioneEsameEReferto> getAll() throws DAOException {
        List<PrescrizioneEsameEReferto> listaEsameEReferto = new ArrayList<>();

        try (Statement stm = CON.createStatement()) {
            try (ResultSet rs = stm.executeQuery(
                                 " SELECT *  "                          +
                                 " FROM PrescrizioneEsameEReferto  "     )){
                
                while (rs.next()) {
                    PrescrizioneEsameEReferto esameEReferto = new PrescrizioneEsameEReferto();
                    
                    esameEReferto.setId                     (rs.getInt      ( "id" ));
                    esameEReferto.setData                   (rs.getDate     ( "data" ));
                    esameEReferto.setDataPrescrizione       (rs.getDate     ( "dataPrescrizione" ));
                    esameEReferto.setRisultato              (rs.getString   ( "risultato" ));
                    esameEReferto.setTichet                 (rs.getDouble   ( "tichet" ));
                    esameEReferto.setPagato                 (rs.getBoolean  ( "pagato" ));
                    esameEReferto.setCura                   (rs.getString   ( "cura" ));
                    esameEReferto.setRichiamo               (rs.getBoolean  ( "richiamo" ));
                    esameEReferto.setId_CartellaClinica     (rs.getInt      ( "id_CartellaClinica" ));
                    esameEReferto.setId_MedicoSpec          (rs.getString   ( "id_MedicoSpec" ));
                    esameEReferto.setId_Esame               (rs.getInt      ( "id_Esame" ));
                    esameEReferto.setVistoPaziente          (rs.getBoolean  ( "vistoPaziente" ));
                    esameEReferto.setVistoReportPaziente    (rs.getBoolean  ( "vistoReportPaziente" ));
                    esameEReferto.setVistoReportMedicoBase  (rs.getBoolean  ( "vistoReportoMedicoBase" ));
                    
                    listaEsameEReferto.add(esameEReferto);
                }
            }
        } catch (SQLException ex) {
            throw new DAOException( " Imposibile estrarre tutta la lista di PrescrizioneEsameEReferto  " , ex);
        }

        return listaEsameEReferto;
    }
/*============================================================================*/
   
/*============================================================================*/    
/*                      METODI QUERY                                          */
/*============================================================================*/
//------------------------------------------------------------------------------
//                      INSERT
//------------------------------------------------------------------------------
    @Override
    public Integer insert(PrescrizioneEsameEReferto esameEReferto) throws DAOException {
        if (esameEReferto == null) {
            throw new DAOException( " La esameEReferto è null " );
        }
        try (PreparedStatement ps = CON.prepareStatement(
                     " INSERT INTO PrescrizioneEsameEReferto "  +
                             " ( data "                 +    "  ,  "   +
                             "  dataPrescrizione "      +    "  ,  "    +
                             "  risultato "             +    "  ,  "    +
                             "  tichet "                +    "  ,  "    +
                             "  pagato "                +    "  ,  "    +  
                             "  cura "                  +    "  ,  "    + 
                             "  richiamo "              +    "  ,  "    + 
                             "  id_CartellaClinica "    +    "  ,  "    +
                             "  id_MedicoSpec "         +    "  ,  "    + 
                             "  id_Esame "              +    "  ,  "    + 
                             "  vistoPaziente "         +    "  ,  "    + 
                             "  vistoReportPaziente "   +    "  ,  "    + 
                             "  vistoReportoMedicoBase " +    "  )  "    +  
                     " VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?) " , Statement.RETURN_GENERATED_KEYS)) {
            
            // imposto i valori
            ps.setDate      (1 , esameEReferto.getData());
            ps.setDate      (2 , esameEReferto.getDataPrescrizione());
            ps.setString    (3 , esameEReferto.getRisultato());
            ps.setDouble    (4 , esameEReferto.getTichet());
            ps.setBoolean   (5 , esameEReferto.getPagato());
            ps.setString    (6 , esameEReferto.getCura());
            ps.setBoolean   (7 , esameEReferto.getRichiamo());
            ps.setInt       (8 , esameEReferto.getId_CartellaClinica());
            ps.setString    (9 , esameEReferto.getId_MedicoSpec());
            ps.setInt       (10, esameEReferto.getId_Esame());
            ps.setBoolean   (11, esameEReferto.getVistoPaziente());
            ps.setBoolean   (12, esameEReferto.getVistoReportPaziente());
            ps.setBoolean   (13, esameEReferto.getVistoReportMedicoBase());
            
            ps.executeUpdate();
            
            ResultSet rs = ps.getGeneratedKeys();
            if (rs.next()) {
                esameEReferto.setId(rs.getInt(1));
            }
            
            return esameEReferto.getId();
        } catch (SQLException ex) {
            throw new DAOException( " Impossibile aggiungere un nuovo record di PrescrizioneEsameEReferto " , ex);
        }
    }
//------------------------------------------------------------------------------
//                      UPDATE
//------------------------------------------------------------------------------
    @Override
    public void update(PrescrizioneEsameEReferto esameEReferto) throws DAOException {
        if (esameEReferto == null) {
            throw new DAOException( " La esameEReferto è null " );
        }
        try (PreparedStatement ps = CON.prepareStatement(
                     " UPDATE PrescrizioneEsameEReferto "    +
                     " SET      data = ?  "                     +    "  ,  "    +
                             "  DATAPRESCRIZIONE = ?  "         +    "  ,  "    +
                             "  RISULTATO = ?  "                +    "  ,  "    +
                             "  TICHET = ?  "                   +    "  ,  "    +
                             "  PAGATO = ?  "                   +    "  ,  "    +  
                             "  CURA = ?  "                     +    "  ,  "    + 
                             "  RICHIAMO = ?  "                 +    "  ,  "    +
                             "  ID_MEDICOSPEC = ?  "            +    "  ,  "    + 
                             "  VISTOPAZIENTE = ?  "            +    "  ,  "    + 
                             "  VISTOREPORTPAZIENTE = ?  "      +    "  ,  "    + 
                             "  VISTOREPORTOMEDICOBASE = ?  "   +         
                     " WHERE id = ?  " )) {
            

            
            // imposto i valori
            ps.setDate      (1 , esameEReferto.getData());
            ps.setDate      (2 , esameEReferto.getDataPrescrizione());
            ps.setString    (3 , esameEReferto.getRisultato());
            ps.setDouble    (4 , esameEReferto.getTichet());
            ps.setBoolean   (5 , esameEReferto.getPagato());
            ps.setString    (6 , esameEReferto.getCura());
            ps.setBoolean   (7 , esameEReferto.getRichiamo());
            ps.setString    (8, esameEReferto.getId_MedicoSpec());
            ps.setBoolean   (9, esameEReferto.getVistoPaziente());
            ps.setBoolean   (10, esameEReferto.getVistoReportPaziente());
            ps.setBoolean   (11, esameEReferto.getVistoReportMedicoBase());
            //WHERE
            ps.setInt       (12, esameEReferto.getId());
            
            ps.executeUpdate();
            
            
        } catch (SQLException ex) {
            throw new DAOException( " Impossibile aggiornare il record di PrescrizioneEsameEReferto " , ex);
        }
    }
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    @Override
    public void update_dataEsequzioneEsame(Integer id, Date data) throws DAOException {
        if (id == null || data == null) {
            throw new DAOException( " id o data è null " );
        }
        try (PreparedStatement ps = CON.prepareStatement(
                     " UPDATE PrescrizioneEsameEReferto "  +
                     " SET    data = ?  "   +
                     " WHERE id = ?  " )) {
            
            // imposto i valori
            ps.setDate  (1, data);
            //WHERE
            ps.setInt   (2, id);
            
            ps.executeUpdate();
            
        } catch (SQLException ex) {
            throw new DAOException( " Impossibile aggiornare il record di PrescrizioneEsameEReferto " , ex);
        }
    }
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    @Override
    public void update_VistoPaziente(Integer id, Boolean visto) throws DAOException {
        if (id == null || visto == null) {
            throw new DAOException( " id o vistoPaziente è null " );
        }
        try (PreparedStatement ps = CON.prepareStatement(
                     " UPDATE PrescrizioneEsameEReferto "  +
                     " SET    vistoPaziente = ?  "   +
                     " WHERE id = ?  " )) {
            
            // imposto i valori
            ps.setBoolean   (1, visto);
            //WHERE
            ps.setInt       (2, id);
            
            ps.executeUpdate();
            
        } catch (SQLException ex) {
            throw new DAOException( " Impossibile aggiornare il record di PrescrizioneEsameEReferto " , ex);
        }
    }
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    @Override
    public void update_VistoRepotDaPaziente(Integer id, Boolean visto) throws DAOException {
        if (id == null || visto == null) {
            throw new DAOException( " id o vistoRepotDaPaziente è null " );
        }
        try (PreparedStatement ps = CON.prepareStatement(
                     " UPDATE PrescrizioneEsameEReferto "  +
                     " SET    VISTOREPORTPAZIENTE = ?  "   +
                     " WHERE id = ?  " )) {
            
            // imposto i valori
            ps.setBoolean   (1, visto);
            //WHERE
            ps.setInt       (2, id);
            
            ps.executeUpdate();
            
        } catch (SQLException ex) {
            throw new DAOException( " Impossibile aggiornare il record di PrescrizioneEsameEReferto " , ex);
        }
    }
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    @Override
    public void update_VistoRepotDaMedicoBase(Integer id, Boolean visto) throws DAOException {
        if (id == null || visto == null) {
            throw new DAOException( " id o vistoRepotMedicoBase è null " );
        }
        try (PreparedStatement ps = CON.prepareStatement(
                     " UPDATE PrescrizioneEsameEReferto "     +
                     " SET    vistoReportoMedicoBase = ?  "   +
                     " WHERE id = ?  " )) {
            
            // imposto i valori
            ps.setBoolean   (1, visto);
            //WHERE
            ps.setInt       (2, id);
            
            ps.executeUpdate();
            
        } catch (SQLException ex) {
            throw new DAOException( " Impossibile aggiornare il record di PrescrizioneEsameEReferto " , ex);
        }
    }
//------------------------------------------------------------------------------
//                      SELECT
//------------------------------------------------------------------------------
    @Override
    public List<PrescrizioneEsameEReferto> getByPazienteId(String id) throws DAOException {
        if (id == null ) {
            throw new DAOException( " id  è null " );
        }
        List<PrescrizioneEsameEReferto> listaEsameEReferto = new ArrayList<>();

        try (PreparedStatement stm = CON.prepareStatement(
                "  SELECT   ER.id AS id ,  "                                    +
                         "  ER.data AS data ,  "                                + 
                         "  ER.dataPrescrizione AS dataPrescrizione ,  "        +
                         "  ER.risultato AS risultato ,  "                      + 
                         "  ER.tichet AS tichet ,  "                            + 
                         "  ER.pagato AS pagato ,  "                            + 
                         "  ER.cura AS cura ,  "                                + 
                         "  ER.richiamo AS richiamo ,  "                        + 
                         "  ER.id_CartellaClinica AS id_CartellaClinica ,  "    + 
                         "  ER.id_MedicoSpec AS id_MedicoSpec ,  "              + 
                         "  ER.id_Esame AS id_Esame ,  "                        +
                         "  ER.vistoPaziente AS vistoPaziente ,  "              + 
                         "  ER.vistoReportPaziente AS vistoReportPaziente ,  "  + 
                         "  ER.vistoReportoMedicoBase AS vistoReportoMedicoBase "+ 
                "  FROM PrescrizioneEsameEReferto AS ER ,  "                    +
                     "  CartellaClinicaPerMB      AS CC    "                    +
                "  WHERE  ER.id_CartellaClinica = CC.id    AND  "               +
                        " CC.id_Persona = ?  " )) {
            
            stm.setString(1, id);
            try (ResultSet rs = stm.executeQuery()){
                
                while (rs.next()) {
                    PrescrizioneEsameEReferto esameEReferto = new PrescrizioneEsameEReferto();
                    
                    esameEReferto.setId                     (rs.getInt      ( "id" ));
                    esameEReferto.setData                   (rs.getDate     ( "data" ));
                    esameEReferto.setDataPrescrizione       (rs.getDate     ( "dataPrescrizione" ));
                    esameEReferto.setRisultato              (rs.getString   ( "risultato" ));
                    esameEReferto.setTichet                 (rs.getDouble   ( "tichet" ));
                    esameEReferto.setPagato                 (rs.getBoolean  ( "pagato" ));
                    esameEReferto.setCura                   (rs.getString   ( "cura" ));
                    esameEReferto.setRichiamo               (rs.getBoolean  ( "richiamo" ));
                    esameEReferto.setId_CartellaClinica     (rs.getInt      ( "id_CartellaClinica" ));
                    esameEReferto.setId_MedicoSpec          (rs.getString   ( "id_MedicoSpec" ));
                    esameEReferto.setId_Esame               (rs.getInt      ( "id_Esame" ));
                    esameEReferto.setVistoPaziente          (rs.getBoolean  ( "vistoPaziente" ));
                    esameEReferto.setVistoReportPaziente    (rs.getBoolean  ( "vistoReportPaziente" ));
                    esameEReferto.setVistoReportMedicoBase  (rs.getBoolean  ( "vistoReportoMedicoBase" ));
                    
                    listaEsameEReferto.add(esameEReferto);
                }
            }
        } catch (SQLException ex) {
            throw new DAOException( " Imposibile estrarre la lista di PrescrizioneEsameEReferto  " , ex);
        }

        return listaEsameEReferto;
    }
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    @Override
    public List<PrescrizioneEsameEReferto> getByMedicoSpecId(String id) throws DAOException {
        if (id == null ) {
            throw new DAOException( " id  è null " );
        }
        List<PrescrizioneEsameEReferto> listaEsameEReferto = new ArrayList<>();

        try (PreparedStatement stm = CON.prepareStatement(
                    "  SELECT *  " + 
                    "  FROM PrescrizioneEsameEReferto  " +
                    "  WHERE  id_MedicoSpec = ?  " )) {
            stm.setString(1, id);
            try (ResultSet rs = stm.executeQuery()){
                
                while (rs.next()) {
                    PrescrizioneEsameEReferto esameEReferto = new PrescrizioneEsameEReferto();
                    
                    esameEReferto.setId                     (rs.getInt      ( "id" ));
                    esameEReferto.setData                   (rs.getDate     ( "data" ));
                    esameEReferto.setDataPrescrizione       (rs.getDate     ( "dataPrescrizione" ));
                    esameEReferto.setRisultato              (rs.getString   ( "risultato" ));
                    esameEReferto.setTichet                 (rs.getDouble   ( "tichet" ));
                    esameEReferto.setPagato                 (rs.getBoolean  ( "pagato" ));
                    esameEReferto.setCura                   (rs.getString   ( "cura" ));
                    esameEReferto.setRichiamo               (rs.getBoolean  ( "richiamo" ));
                    esameEReferto.setId_CartellaClinica     (rs.getInt      ( "id_CartellaClinica" ));
                    esameEReferto.setId_MedicoSpec          (rs.getString   ( "id_MedicoSpec" ));
                    esameEReferto.setId_Esame               (rs.getInt      ( "id_Esame" ));
                    esameEReferto.setVistoPaziente          (rs.getBoolean  ( "vistoPaziente" ));
                    esameEReferto.setVistoReportPaziente    (rs.getBoolean  ( "vistoReportPaziente" ));
                    esameEReferto.setVistoReportMedicoBase  (rs.getBoolean  ( "vistoReportoMedicoBase" ));
                    
                    listaEsameEReferto.add(esameEReferto);
                }
            }
        } catch (SQLException ex) {
            throw new DAOException( " Imposibile estrarre la lista di PrescrizioneEsameEReferto  " , ex);
        }

        return listaEsameEReferto;
    }
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    @Override
    public List<PrescrizioneEsameEReferto> getByPazienteId_Fatte(String id) throws DAOException {
        if (id == null ) {
            throw new DAOException( " id  è null " );
        }
        List<PrescrizioneEsameEReferto> listaEsameEReferto = new ArrayList<>();

        try (PreparedStatement stm = CON.prepareStatement(
                " SELECT ER.id AS id ,  "                                       +
                "        ER.data AS data ,  "                                   +
                "        ER.dataPrescrizione AS dataPrescrizione , "            +
                "        ER.risultato AS risultato , "                          +        
                "        ER.tichet AS tichet , "                                +
                "        ER.pagato AS pagato , "                                +
                "        ER.cura AS cura ,  "                                   +
                "        ER.richiamo AS richiamo ,  "                           +
                "        ER.id_CartellaClinica AS id_CartellaClinica , "        +
                "        ER.id_MedicoSpec AS id_MedicoSpec ,  "                 +
                "        ER.id_Esame AS id_Esame , "                            +
                "        ER.vistoPaziente AS vistoPaziente , "                  +
                "        ER.vistoReportPaziente AS vistoReportPaziente ,   "    +
                "        ER.vistoReportoMedicoBase AS vistoReportoMedicoBase "  +
                "FROM PrescrizioneEsameEReferto AS ER , "                       +
                "     CartellaClinicaPerMB      AS CC  "                        +
                "WHERE  ER.id_CartellaClinica = CC.id AND "                     +
                "       ER.data IS NOT NULL   AND  "                            + 
                "       CC.id_Persona = ?")) {
            stm.setString(1, id);
            
            try (ResultSet rs = stm.executeQuery() ){
                
                while (rs.next()) {
                    PrescrizioneEsameEReferto esameEReferto = new PrescrizioneEsameEReferto();
                    
                    esameEReferto.setId                     (rs.getInt      ( "id" ));
                    esameEReferto.setData                   (rs.getDate     ( "data" ));
                    esameEReferto.setDataPrescrizione       (rs.getDate     ( "dataPrescrizione" ));
                    esameEReferto.setRisultato              (rs.getString   ( "risultato" ));
                    esameEReferto.setTichet                 (rs.getDouble   ( "tichet" ));
                    esameEReferto.setPagato                 (rs.getBoolean  ( "pagato" ));
                    esameEReferto.setCura                   (rs.getString   ( "cura" ));
                    esameEReferto.setRichiamo               (rs.getBoolean  ( "richiamo" ));
                    esameEReferto.setId_CartellaClinica     (rs.getInt      ( "id_CartellaClinica" ));
                    esameEReferto.setId_MedicoSpec          (rs.getString   ( "id_MedicoSpec" ));
                    esameEReferto.setId_Esame               (rs.getInt      ( "id_Esame" ));
                    esameEReferto.setVistoPaziente          (rs.getBoolean  ( "vistoPaziente" ));
                    esameEReferto.setVistoReportPaziente    (rs.getBoolean  ( "vistoReportPaziente" ));
                    esameEReferto.setVistoReportMedicoBase  (rs.getBoolean  ( "vistoReportoMedicoBase" ));
                    
                    listaEsameEReferto.add(esameEReferto);
                }
            }
        } catch (SQLException ex) {
            throw new DAOException( " Imposibile estrarre la lista di PrescrizioneEsameEReferto  " , ex);
        }

        return listaEsameEReferto;
    }
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    @Override
    public List<PrescrizioneEsameEReferto> getByPazienteId_DaFare(String id) throws DAOException {
        if (id == null ) {
            throw new DAOException( " id  è null " );
        }
        List<PrescrizioneEsameEReferto> listaEsameEReferto = new ArrayList<>();

        try (PreparedStatement stm = CON.prepareStatement(
                "  SELECT   ER.id AS id ,  "                                    +
                         "  ER.data AS data ,  "                                + 
                         "  ER.dataPrescrizione AS dataPrescrizione ,  "        +
                         "  ER.risultato AS risultato ,  "                      + 
                         "  ER.tichet AS tichet ,  "                            + 
                         "  ER.pagato AS pagato ,  "                            + 
                         "  ER.cura AS cura ,  "                                + 
                         "  ER.richiamo AS richiamo ,  "                        + 
                         "  ER.id_CartellaClinica AS id_CartellaClinica ,  "    + 
                         "  ER.id_MedicoSpec AS id_MedicoSpec ,  "              + 
                         "  ER.id_Esame AS id_Esame ,  "                        +
                         "  ER.vistoPaziente AS vistoPaziente ,  "              + 
                         "  ER.vistoReportPaziente AS vistoReportPaziente ,  "  + 
                         "  ER.vistoReportoMedicoBase AS vistoReportoMedicoBase "+ 
                "  FROM PrescrizioneEsameEReferto AS ER ,  "                    +
                     "  CartellaClinicaPerMB      AS CC    "                    +
                "  WHERE  ER.id_CartellaClinica = CC.id     AND  "              +
                       "  ER.data IS NULL   AND  "                              +
                       "  CC.id_Persona = ?  " )) {
            
            stm.setString(1, id);
            try (ResultSet rs = stm.executeQuery()){
                
                while (rs.next()) {
                    PrescrizioneEsameEReferto esameEReferto = new PrescrizioneEsameEReferto();
                    
                    esameEReferto.setId                     (rs.getInt      ( "id" ));
                    esameEReferto.setData                   (rs.getDate     ( "data" ));
                    esameEReferto.setDataPrescrizione       (rs.getDate     ( "dataPrescrizione" ));
                    esameEReferto.setRisultato              (rs.getString   ( "risultato" ));
                    esameEReferto.setTichet                 (rs.getDouble   ( "tichet" ));
                    esameEReferto.setPagato                 (rs.getBoolean  ( "pagato" ));
                    esameEReferto.setCura                   (rs.getString   ( "cura" ));
                    esameEReferto.setRichiamo               (rs.getBoolean  ( "richiamo" ));
                    esameEReferto.setId_CartellaClinica     (rs.getInt      ( "id_CartellaClinica" ));
                    esameEReferto.setId_MedicoSpec          (rs.getString   ( "id_MedicoSpec" ));
                    esameEReferto.setId_Esame               (rs.getInt      ( "id_Esame" ));
                    esameEReferto.setVistoPaziente          (rs.getBoolean  ( "vistoPaziente" ));
                    esameEReferto.setVistoReportPaziente    (rs.getBoolean  ( "vistoReportPaziente" ));
                    esameEReferto.setVistoReportMedicoBase  (rs.getBoolean  ( "vistoReportoMedicoBase" ));
                    
                    listaEsameEReferto.add(esameEReferto);
                }
            }
        } catch (SQLException ex) {
            throw new DAOException( " Imposibile estrarre la lista di PrescrizioneEsameEReferto  " , ex);
        }

        return listaEsameEReferto;
    }
/*============================================================================*/
}
