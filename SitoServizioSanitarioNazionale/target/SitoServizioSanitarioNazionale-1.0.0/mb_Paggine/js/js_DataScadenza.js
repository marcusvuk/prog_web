document.addEventListener("load", setScadenza()); 

function setScadenza() {
   var date = new Date();
    var currentMonth = 2+date.getMonth();
    var currentDate = date.getDate();
    var currentYear = date.getFullYear();

    if(currentMonth>12){
        currentMonth=1;
        currentYear=currentYear+1;
    }
    if(currentMonth<=9){
        currentMonth="0"+currentMonth;
    }
    if(currentDate<=9){
        currentDate="0"+currentDate;
    }
    var dataString= ''+currentYear+'-'+currentMonth+'-'+currentDate+'';
    var b = document.getElementById("dataScadenza"); 
    b.setAttribute("min", dataString);
    b.setAttribute("value", dataString);

}   