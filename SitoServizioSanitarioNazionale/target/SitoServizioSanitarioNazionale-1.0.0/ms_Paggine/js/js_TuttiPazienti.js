$(function () {
    $("#idPaziente").select2({
        placeholder: "Scegli paziente",
        allowClear: true,
        ajax: {
            url: function (request) {
                return "/SitoServizioSanitarioNazionale/services/pazienti/MedicoSpec_S2/"+$('#MyIdMedicoSpec').attr('value')+"/"+ request.term;
            },
            dataType: "json"
        }
    });
    $("#idPaziente").val(null).trigger("change");
});
