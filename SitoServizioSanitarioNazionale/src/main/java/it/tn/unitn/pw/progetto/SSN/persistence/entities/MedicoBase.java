/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.tn.unitn.pw.progetto.SSN.persistence.entities;

/**
 *
 * @author Brian
 */
public class MedicoBase extends Persona {
/*============================================================================*/
/*                                       ATTRIBUTI                            */
/*============================================================================*/
    private Integer     lavora_id_SSP;
/*============================================================================*/
    
/*============================================================================*/    
/*                                      METODI COSTRUTTORI                    */
/*============================================================================*/
    public MedicoBase() {
        super();
        lavora_id_SSP   = null;
    }
/*============================================================================*/

/*============================================================================*/    
/*                                      METODI GETTER E SETTER                */
/*============================================================================*/
    /*--------------------------------------*/   
    public Integer getLavora_id_SSP() {
        return lavora_id_SSP;
    }

    public void setLavora_id_SSP(Integer lavora_id_SSP) {
        this.lavora_id_SSP = lavora_id_SSP;
    }
    /*--------------------------------------*/
/*============================================================================*/
}
