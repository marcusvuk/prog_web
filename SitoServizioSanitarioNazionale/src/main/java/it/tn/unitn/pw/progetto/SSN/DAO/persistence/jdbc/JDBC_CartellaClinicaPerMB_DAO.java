/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.tn.unitn.pw.progetto.SSN.DAO.persistence.jdbc;

import java.sql.Date;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import it.tn.unitn.pw.progetto.SSN.DAO.common.persistence.dao.jdbc.JDBCDAO;
import it.tn.unitn.pw.progetto.SSN.DAO.common.exception.*;
import it.tn.unitn.pw.progetto.SSN.DAO.persistence.*;
import it.tn.unitn.pw.progetto.SSN.persistence.entities.*;
/**
 *
 * @author Brian
 */
public class JDBC_CartellaClinicaPerMB_DAO extends JDBCDAO<CartellaClinicaPerMB, Integer> implements CartellaClinicaPerMB_DAO {
/*============================================================================*/    
/*                                      METODI COSTRUTTORI                    */
/*============================================================================*/
    public JDBC_CartellaClinicaPerMB_DAO(Connection con) {
        super(con);
    }
/*============================================================================*/
    
/*============================================================================*/    
/*                      METODI GETTER DAO IMPLEMENTATION                      */
/*============================================================================*/
    @Override
    public Long getCount() throws DAOException {
        try (Statement stmt = CON.createStatement()) {
            ResultSet counter = stmt.executeQuery( 
                     "  SELECT COUNT(*) "               +
                     "  FROM CartellaClinicaPerMB "     );
            if (counter.next()) {
                return counter.getLong(1);
            }
        } catch (SQLException ex) {
            throw new DAOException( " Immpossibile contare gli elementi di CartellaClinicaPerMB  " , ex);
        }
        return 0L;
    }
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    @Override
    public CartellaClinicaPerMB getByPrimaryKey(Integer primaryKey) throws DAOException {
        if (primaryKey == null) {
            throw new DAOException( " primaryKey è null " );
        }
        try (PreparedStatement stm = CON.prepareStatement(
                                     "  SELECT *  "                      +
                                     "  FROM CartellaClinicaPerMB  "     +
                                     "  WHERE id = ? "                   )) {
            // imposto i valori
            stm.setInt(1, primaryKey);
            
            try (ResultSet rs = stm.executeQuery()) {
                rs.next();
                CartellaClinicaPerMB cartellaClinica = new CartellaClinicaPerMB();
                cartellaClinica.setId               (rs.getInt      ( "id" ));
                cartellaClinica.setDataInizio       (rs.getDate     ( "dataInizio" ));
                cartellaClinica.setDataFine         (rs.getDate     ( "dataFine" ));
                cartellaClinica.setId_MedicoBase    (rs.getString   ( "id_MedicoBase" ));
                cartellaClinica.setId_Persona       (rs.getString   ( "id_Persona" ));
                cartellaClinica.setVistoMedicoBase  (rs.getBoolean  ( "vistoMedicoBase" ));
                        
                return cartellaClinica;
            }
        } catch (SQLException ex) {
            throw new DAOException( " Impossibile perendere la CartellaClinicaPerMB indicato dalla primary key " , ex);
        }
    }
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    @Override
    public List<CartellaClinicaPerMB> getAll() throws DAOException {
        List<CartellaClinicaPerMB> listaCartellaClinica = new ArrayList<>();

        try (Statement stm = CON.createStatement()) {
            try (ResultSet rs = stm.executeQuery(
                                 " SELECT *  "                      +
                                 " FROM CartellaClinicaPerMB  "     +
                                 " ORDER BY dataInizio DESC  "      )){
                
                while (rs.next()) {
                    CartellaClinicaPerMB cartellaClinica = new CartellaClinicaPerMB();
                    
                    cartellaClinica.setId               (rs.getInt      ( "id" ));
                    cartellaClinica.setDataInizio       (rs.getDate     ( "dataInizio" ));
                    cartellaClinica.setDataFine         (rs.getDate     ( "dataFine" ));
                    cartellaClinica.setId_MedicoBase    (rs.getString   ( "id_MedicoBase" ));
                    cartellaClinica.setId_Persona       (rs.getString   ( "id_Persona" ));
                    cartellaClinica.setVistoMedicoBase  (rs.getBoolean  ( "VistoMedicoBase" ));
                    
                    listaCartellaClinica.add(cartellaClinica);
                }
            }
        } catch (SQLException ex) {
            throw new DAOException( " Imposibile estrarre tutta la lista di CartellaClinicaPerMB  " , ex);
        }

        return listaCartellaClinica;
    }
/*============================================================================*/
   
/*============================================================================*/    
/*                      METODI QUERY                                          */
/*============================================================================*/
//------------------------------------------------------------------------------
//                      INSERT
//------------------------------------------------------------------------------
    @Override
    public Integer insert(CartellaClinicaPerMB cartellaClinica) throws DAOException {
        if (cartellaClinica == null) {
            throw new DAOException( " La cartellaClinica è null " );
        }
        try (PreparedStatement ps = CON.prepareStatement(
                     " INSERT INTO CartellaClinicaPerMB "  +
                            " ( dataInizio "       +    "  ,  "   +
                            "  dataFine "          +    "  ,  "    +
                            "  id_MedicoBase "     +    "  ,  "    +
                            "  id_Persona "        +    "  ,  "    +
                            "  vistoMedicoBase "   +    "  )  "    +  
                     " VALUES (?,?,?,?,?) " , Statement.RETURN_GENERATED_KEYS)) {
            
            // imposto i valori
            ps.setDate      (1, cartellaClinica.getDataInizio());
            ps.setDate      (2, cartellaClinica.getDataFine());
            ps.setString    (3, cartellaClinica.getId_MedicoBase());
            ps.setString    (4, cartellaClinica.getId_Persona());
            ps.setBoolean   (5, cartellaClinica.getVistoMedicoBase());
      
            
            ps.executeUpdate();
            
            ResultSet rs = ps.getGeneratedKeys();
            if (rs.next()) {
                cartellaClinica.setId(rs.getInt(1));
            }
            
            return cartellaClinica.getId();
        } catch (SQLException ex) {
            throw new DAOException( " Impossibile aggiungere un nuovo record di CartellaClinicaPerMB " , ex);
        }
    }
//------------------------------------------------------------------------------
//                      UPDATE
//------------------------------------------------------------------------------
    @Override
    public void update(CartellaClinicaPerMB cartellaClinica) throws DAOException {
        if (cartellaClinica == null) {
            throw new DAOException( " La cartellaClinica è null " );
        }
        try (PreparedStatement ps = CON.prepareStatement(
                     " UPDATE CartellaClinicaPerMB "  +
                     " SET   id = ?  "               +     "  ,  "    +
                          "  dataInizio = ?  "       +     "  ,  "    +
                          "  dataFine = ?  "         +     "  ,  "    +
                          "  id_MedicoBase = ?  "    +     "  ,  "    +
                          "  id_Persona = ?  "       +     "  ,  "    +
                          "  vistoMedicoBase = ?  "  +     
                     " WHERE id = ?  " , Statement.RETURN_GENERATED_KEYS)) {
            
            // imposto i valori
            ps.setInt       (1, cartellaClinica.getId());
            
            ps.setDate      (2, cartellaClinica.getDataInizio());
            ps.setDate      (3, cartellaClinica.getDataFine());
            ps.setString    (4, cartellaClinica.getId_MedicoBase());
            ps.setString    (5, cartellaClinica.getId_Persona());
            ps.setBoolean   (6, cartellaClinica.getVistoMedicoBase());
            //WHERE
            ps.setInt       (7, cartellaClinica.getId());
            
            ps.executeUpdate();
            
            
        } catch (SQLException ex) {
            throw new DAOException( " Impossibile aggiornare il record di CartellaClinicaPerMB " , ex);
        }
    }
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    @Override
    public void update_FinePeriodo(Integer id, Date data) throws DAOException {
        if (id == null || data == null) {
            throw new DAOException( " id o data è null " );
        }
        try (PreparedStatement ps = CON.prepareStatement(
                     " UPDATE CartellaClinicaPerMB "  +
                     " SET   dataFine = ?  "   +
                     " WHERE id = ?  " , Statement.RETURN_GENERATED_KEYS)) {
            
            // imposto i valori
            ps.setDate  (1, data);
            //WHERE
            ps.setInt   (2, id);
            
            ps.executeUpdate();
            
        } catch (SQLException ex) {
            throw new DAOException( " Impossibile aggiornare il record di CartellaClinicaPerMB " , ex);
        }
    }
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    @Override
    public void update_VistoDaMedicoBase(Integer id, Boolean visto) throws DAOException {
        if (id == null || visto == null) {
            throw new DAOException( " id o visto è null " );
        }
        try (PreparedStatement ps = CON.prepareStatement(
                     " UPDATE CartellaClinicaPerMB "        +
                     " SET   vistoMedicoBase = ?  "  +
                     " WHERE id = ?  " , Statement.RETURN_GENERATED_KEYS)) {
            
            // imposto i valori
            ps.setBoolean   (1, visto);
            //WHERE
            ps.setInt       (2, id);
            
            ps.executeUpdate();
            
        } catch (SQLException ex) {
            throw new DAOException( " Impossibile aggiornare il record di CartellaClinicaPerMB " , ex);
        }
    }
//------------------------------------------------------------------------------
//                      SELECT
//------------------------------------------------------------------------------
    @Override
    public List<CartellaClinicaPerMB> getByPazienteId(String id) throws DAOException {
        if (id == null ) {
            throw new DAOException( " id  è null " );
        }
        List<CartellaClinicaPerMB> listaCartellaClinica = new ArrayList<>();

        try (PreparedStatement stm = CON.prepareStatement(
                                "  SELECT *  "                                  +
                                "  FROM CartellaClinicaPerMB  "                 +
                                "  WHERE id_Persona = ?  "                      +
                                "  ORDER BY dataInizio DESC  " )) {
            stm.setString(1, id);
            try (ResultSet rs = stm.executeQuery()){
                
                while (rs.next()) {
                    CartellaClinicaPerMB cartellaClinica = new CartellaClinicaPerMB();
                    
                    cartellaClinica.setId               (rs.getInt      ( "id" ));
                    cartellaClinica.setDataInizio       (rs.getDate     ( "dataInizio" ));
                    cartellaClinica.setDataFine         (rs.getDate     ( "dataFine" ));
                    cartellaClinica.setId_MedicoBase    (rs.getString   ( "id_MedicoBase" ));
                    cartellaClinica.setId_Persona       (rs.getString   ( "id_Persona" ));
                    cartellaClinica.setVistoMedicoBase  (rs.getBoolean  ( "VistoMedicoBase" ));
                    
                    listaCartellaClinica.add(cartellaClinica);
                }
            }
        } catch (SQLException ex) {
            throw new DAOException( " Imposibile estrarre la lista di CartellaClinicaPerMB  " , ex);
        }

        return listaCartellaClinica;
    }
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    @Override
    public List<CartellaClinicaPerMB> getByMedicoBaseId(String id) throws DAOException {
        if (id == null ) {
            throw new DAOException( " id  è null " );
        }
        List<CartellaClinicaPerMB> listaCartellaClinica = new ArrayList<>();

        try (PreparedStatement stm = CON.prepareStatement(
                                "  SELECT *  "                                  +
                                "  FROM CartellaClinicaPerMB  "                 +
                                "  WHERE id_MedicoBase = ? "                    +
                                "  ORDER BY dataInizio DESC  "     )) {
            stm.setString(1, id);
            try (ResultSet rs = stm.executeQuery()){
                
                while (rs.next()) {
                    CartellaClinicaPerMB cartellaClinica = new CartellaClinicaPerMB();
                    
                    cartellaClinica.setId               (rs.getInt      ( "id" ));
                    cartellaClinica.setDataInizio       (rs.getDate     ( "dataInizio" ));
                    cartellaClinica.setDataFine         (rs.getDate     ( "dataFine" ));
                    cartellaClinica.setId_MedicoBase    (rs.getString   ( "id_MedicoBase" ));
                    cartellaClinica.setId_Persona       (rs.getString   ( "id_Persona" ));
                    cartellaClinica.setVistoMedicoBase  (rs.getBoolean  ( "VistoMedicoBase" ));
                    
                    listaCartellaClinica.add(cartellaClinica);
                }
            }
        } catch (SQLException ex) {
            throw new DAOException( " Imposibile estrarre la lista di CartellaClinicaPerMB  " , ex);
        }

        return listaCartellaClinica;
    }
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    @Override
    public CartellaClinicaPerMB getByPazienteId_Attivo(String id) throws DAOException {
        if (id == null) {
            throw new DAOException( " Id pasato è null " );
        }
        try (PreparedStatement stm = CON.prepareStatement(
                                     "  SELECT *  "                     +
                                     "  FROM CartellaClinicaPerMB  "    +
                                     "  WHERE id_Persona = ? AND  "      +
                                             " dataFine IS NULL "       )) {
            // imposto i valori
            stm.setString(1, id);
            
            try (ResultSet rs = stm.executeQuery()) {
                rs.next();
                CartellaClinicaPerMB cartellaClinica = new CartellaClinicaPerMB();
                cartellaClinica.setId               (rs.getInt      ( "id" ));
                cartellaClinica.setDataInizio       (rs.getDate     ( "dataInizio" ));
                cartellaClinica.setDataFine         (rs.getDate     ( "dataFine" ));
                cartellaClinica.setId_MedicoBase    (rs.getString   ( "id_MedicoBase" ));
                cartellaClinica.setId_Persona       (rs.getString   ( "id_Persona" ));
                cartellaClinica.setVistoMedicoBase  (rs.getBoolean  ( "VistoMedicoBase" ));
                        
                return cartellaClinica;
            }
        } catch (SQLException ex) {
            throw new DAOException( " Impossibile perendere l'utente indicato dalla primary key " , ex);
        }
    }
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    @Override
    public List<CartellaClinicaPerMB> getByMedicoBaseId_Attivo(String id) throws DAOException {
        if (id == null ) {
            throw new DAOException( " id  è null " );
        }
        List<CartellaClinicaPerMB> listaCartellaClinica = new ArrayList<>();

        try (PreparedStatement stm = CON.prepareStatement(
                    "  SELECT *  "                     +
                    "  FROM CartellaClinicaPerMB  "    +
                    "  WHERE id_MedicoBase = ? AND  "      +
                           " dataFine IS NULL "       +   
                    "  ORDER BY dataInizio DESC  "      )) {
            
            stm.setString(1, id);
            try (ResultSet rs = stm.executeQuery()){
                
                while (rs.next()) {
                    CartellaClinicaPerMB cartellaClinica = new CartellaClinicaPerMB();
                    
                    cartellaClinica.setId               (rs.getInt      ( "id" ));
                    cartellaClinica.setDataInizio       (rs.getDate     ( "dataInizio" ));
                    cartellaClinica.setDataFine         (rs.getDate     ( "dataFine" ));
                    cartellaClinica.setId_MedicoBase    (rs.getString   ( "id_MedicoBase" ));
                    cartellaClinica.setId_Persona       (rs.getString   ( "id_Persona" ));
                    cartellaClinica.setVistoMedicoBase  (rs.getBoolean  ( "VistoMedicoBase" ));
                    
                    listaCartellaClinica.add(cartellaClinica);
                }
            }
        } catch (SQLException ex) {
            throw new DAOException( " Imposibile estrarre la lista di CartellaClinicaPerMB  " , ex);
        }

        return listaCartellaClinica;
    }
/*============================================================================*/
}
