/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.tn.unitn.pw.progetto.SSN.DAO.persistence.jdbc;

import java.sql.Date;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;


import it.tn.unitn.pw.progetto.SSN.DAO.common.persistence.dao.jdbc.JDBCDAO;
import it.tn.unitn.pw.progetto.SSN.DAO.common.exception.*;
import it.tn.unitn.pw.progetto.SSN.DAO.persistence.*;
import it.tn.unitn.pw.progetto.SSN.persistence.entities.*;
/**
 *
 * @author Brian
 */
public class JDBC_Esame_DAO extends JDBCDAO<Esame, Integer> implements Esame_DAO {
/*============================================================================*/    
/*                                      METODI COSTRUTTORI                    */
/*============================================================================*/
    public JDBC_Esame_DAO(Connection con) {
        super(con);
    }
/*============================================================================*/
    
/*============================================================================*/    
/*                      METODI GETTER DAO IMPLEMENTATION                      */
/*============================================================================*/
    @Override
    public Long getCount() throws DAOException {
        try (Statement stmt = CON.createStatement()) {
            ResultSet counter = stmt.executeQuery( 
                     "  SELECT COUNT(*) "               +
                     "  FROM Esame "     );
            if (counter.next()) {
                return counter.getLong(1);
            }
        } catch (SQLException ex) {
            throw new DAOException( " Immpossibile contare gli elementi di Esame  " , ex);
        }
        return 0L;
    }
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    @Override
    public Esame getByPrimaryKey(Integer primaryKey) throws DAOException {
        if (primaryKey == null) {
            throw new DAOException( " primaryKey è null " );
        }
        try (PreparedStatement stm = CON.prepareStatement(
                                     "  SELECT *  "                      +
                                     "  FROM Esame  "     +
                                     "  WHERE id = ? "                   )) {
            // imposto i valori
            stm.setInt(1, primaryKey);
            
            try (ResultSet rs = stm.executeQuery()) {
                rs.next();
                Esame esame = new Esame();
                esame.setId               (rs.getInt      ( "id" ));
                esame.setNome             (rs.getString   ( "nome" ));
                
                return esame;
            }
        } catch (SQLException ex) {
            throw new DAOException( " Impossibile perendere l'Esame indicato dalla primary key " , ex);
        }
    }
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    @Override
    public List<Esame> getAll() throws DAOException {
        List<Esame> listaEsame = new ArrayList<>();

        try (Statement stm = CON.createStatement()) {
            try (ResultSet rs = stm.executeQuery(
                                 " SELECT *  "      +
                                 " FROM Esame  "    )){
                
                while (rs.next()) {
                    Esame esame = new Esame();
                    
                    esame.setId               (rs.getInt      ( "id" ));
                    esame.setNome             (rs.getString   ( "nome" ));
                    
                    listaEsame.add(esame);
                }
            }
        } catch (SQLException ex) {
            throw new DAOException( " Imposibile estrarre tutta la lista di Esame  " , ex);
        }

        return listaEsame;
    }
/*============================================================================*/
   
/*============================================================================*/    
/*                      METODI QUERY                                          */
/*============================================================================*/
//------------------------------------------------------------------------------
//                      INSERT
//------------------------------------------------------------------------------
    @Override
    public Integer insert(Esame esame) throws DAOException {
        if (esame == null ) {
            throw new DAOException( " Esame è null " );
        }
        try (PreparedStatement ps = CON.prepareStatement(
                    " INSERT INTO Esame "   +    
                            " ( nome )  "   +  
                     " VALUES (?) " , Statement.RETURN_GENERATED_KEYS)) {
            
            // imposto i valori
            ps.setString(1, esame.getNome());
            
            ps.executeUpdate();
            
            ResultSet rs = ps.getGeneratedKeys();
            if (rs.next()) {
                esame.setId(rs.getInt(1));
            }
            
            return esame.getId();
        } catch (SQLException ex) {
            throw new DAOException( " Impossibile aggiungere un nuovo record di Esame " , ex);
        }
    }
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    @Override
    public void addLinkEsameSSP(Integer idEsame, Integer idSSProvinciale) throws DAOException {
        if (idEsame == null || idSSProvinciale == null ) {
            throw new DAOException( " idEsame o idSSProvinciale è null " );
        }
        try (PreparedStatement ps = CON.prepareStatement(
                     " INSERT INTO ErogaESSSP "  +
                        " (  "              +
                             " id_SSP , "   +
                             " id_Esame "   + 
                         "  )  " +  
                     " VALUES (?,?) " , Statement.RETURN_GENERATED_KEYS)) {
            
            // imposto i valori
            ps.setInt(1, idSSProvinciale);
            ps.setInt(2, idEsame);
            
            ps.executeUpdate();
            
       
        } catch (SQLException ex) {
            throw new DAOException( " Impossibile aggiungere un nuovo record di ErogaESSSP " , ex);
        } 
    }
//------------------------------------------------------------------------------
//                      SELECT
//------------------------------------------------------------------------------
    @Override
    public List<Esame> getBySSProvincialeId(Integer id) throws DAOException {
        if (id == null ) {
            throw new DAOException( " id è null " );
        }
        List<Esame> listaEsame = new ArrayList<>();

        try (PreparedStatement stm = CON.prepareStatement(
                                "  SELECT ES.id AS id, ES.nome AS nome  "       +
                                "  FROM Esame AS ES, ErogaESSSP AS ER   "       +
                                "  WHERE ES.id = ER.id_Esame     AND    "       +
                                      "  ER.id_SSP =  ? "  )) {
            stm.setInt(1, id);
            try (ResultSet rs = stm.executeQuery()){
                
                while (rs.next()) {
                    Esame esame = new Esame();
                    
                    esame.setId               (rs.getInt      ( "id" ));
                    esame.setNome             (rs.getString   ( "nome" ));
                    
                    listaEsame.add(esame);
                }
            }
        } catch (SQLException ex) {
            throw new DAOException( " Imposibile estrarre la lista di Esame  " , ex);
        }

        return listaEsame;
    }
/*============================================================================*/
}
