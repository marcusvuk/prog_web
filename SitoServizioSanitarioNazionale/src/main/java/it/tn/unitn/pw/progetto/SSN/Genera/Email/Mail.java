package it.tn.unitn.pw.progetto.SSN.Genera.Email;
//import per le Mail
import java.util.*;
import javax.mail.*;
import javax.mail.internet.*;
import javax.activation.*;



public class Mail{
   
    static private Boolean SICUREZZA_ANTI_SPAM=true;

    //In questo metodo nella Stringa dati, ci va il corpo della Mail.
    public static void sendEmail(String dati, String destinatario, String oggetto) throws Exception{

        //debug serve a inviare la Mail a se stesso se è true
        
        final String emailId = "bicchiobau@gmail.com"; //sender's email address
        String reciever=destinatario; //reciever's email address
        final String password = "Bicchiobau12";      //provide your password here
        
        if(SICUREZZA_ANTI_SPAM){ 
            reciever = emailId;
        }
        
        System.out.println("Sending Email from "+emailId+" to "+reciever);
        
        Properties pr = new Properties();
        
        pr.put("mail.smtp.auth","true");    //for username and password authentication
        pr.put("mail.smtp.starttls.enable","true");
        
        pr.put("mail.smtp.host","smtp.gmail.com");  //here host is gmail.com  ///FUNZIONATE
        //----------------------
        pr.put("mail.smtp.ssl.trust", "smtp.gmail.com");//BISOGNA AVERE FIDUCIA SENO' NON FUNZIONA
        //----------------------
        pr.put("mail.smtp.port","587");             //port no.
        
        Session gs = Session.getInstance(pr, new Authenticator(){
            @Override
            protected PasswordAuthentication getPasswordAuthentication(){
                return new PasswordAuthentication(emailId,password);  //pass your email id and password here
         
            }
        });
        
        Message ms = messageContent(gs,emailId,reciever, dati, oggetto);

        if(ms!=null){
            System.out.println("Message sent! ");
        }else{
            System.out.println("Message DON'T sent! ");
        }
        
    }
    
    private static Message messageContent(Session gs, String emailId, String reciever, String dati, String oggetto) throws Exception {
        try{
           
           Message msg = new MimeMessage(gs);
           msg.setFrom(new InternetAddress(emailId));
           msg.setRecipient(Message.RecipientType.TO,new InternetAddress(reciever));
           msg.setSubject(oggetto); //to set the subject (not mandatory)
           
           
           msg.setText(dati);
           Transport.send(msg);
           return msg;
        }catch(MessagingException e)
        {
            System.out.println(e);
        }
        return null;    
    }
//==============================================================================
//==============================================================================
//==============================================================================
    //In questo metodo nella Stringa dati, ci va il corpo della Mail.
    public static void sendMultipleEmail(String dati, String destinatari, String oggetto) throws Exception{

        //debug serve a inviare la Mail a se stesso se è true
        
        final String emailId = "bicchiobau@gmail.com"; //sender's email address
        String recieverCC=destinatari; //reciever's email address
        final String password = "Bicchiobau12";      //provide your password here
        
        if(SICUREZZA_ANTI_SPAM){ 
            recieverCC = emailId;
        }
        
        System.out.println("Sending Email from "+emailId+" to "+recieverCC);
        
        Properties pr = new Properties();
        
        pr.put("mail.smtp.auth","true");    //for username and password authentication
        pr.put("mail.smtp.starttls.enable","true");
        
        pr.put("mail.smtp.host","smtp.gmail.com");  //here host is gmail.com  ///FUNZIONATE
        //----------------------
        pr.put("mail.smtp.ssl.trust", "smtp.gmail.com");//BISOGNA AVERE FIDUCIA SENO' NON FUNZIONA
        //----------------------
        pr.put("mail.smtp.port","587");             //port no.
        
        Session gs = Session.getInstance(pr, new Authenticator(){
            @Override
            protected PasswordAuthentication getPasswordAuthentication(){
                return new PasswordAuthentication(emailId,password);  //pass your email id and password here
         
            }
        });
        
        Message ms = messageMultipleContent(gs,emailId,recieverCC, dati, oggetto);

        if(ms!=null){
            System.out.println("Message sent! ");
        }else{
            System.out.println("Message DON'T sent! ");
        }
        
    }
    
    private static Message messageMultipleContent(Session gs, String emailId, String reciever, String dati, String oggetto) throws Exception {
        try{
           
           Message msg = new MimeMessage(gs);
           msg.setFrom(new InternetAddress(emailId));
           msg.addRecipients(Message.RecipientType.BCC, InternetAddress.parse(reciever,true));
           msg.setSubject(oggetto); //to set the subject (not mandatory)
           
           
           msg.setText(dati);
           Transport.send(msg);
           return msg;
        }catch(MessagingException e)
        {
            System.out.println(e);
        }
        return null;    
    }

//==============================================================================
//==============================================================================
//==============================================================================
    public static void receiveEmail(String dati, String mittente, String oggetto) throws Exception{

        //debug serve a inviare la Mail a se stesso se è true
        
        final String emailId = "bicchiobau@gmail.com"; //sender's email address
        String reciever=mittente; //reciever's email address
        final String password = "Bicchiobau12";      //provide your password here
        
        if(SICUREZZA_ANTI_SPAM){ 
            reciever = emailId;
            dati=mittente.concat("\n"+dati);
        }
        
        System.out.println("Sending Email from "+emailId+" to "+reciever);
        
        Properties pr = new Properties();
        
        pr.put("mail.smtp.auth","true");    //for username and password authentication
        pr.put("mail.smtp.starttls.enable","true");
        
        pr.put("mail.smtp.host","smtp.gmail.com");  //here host is gmail.com  ///FUNZIONATE
        //----------------------
        pr.put("mail.smtp.ssl.trust", "smtp.gmail.com");//BISOGNA AVERE FIDUCIA SENO' NON FUNZIONA
        //----------------------
        pr.put("mail.smtp.port","587");             //port no.
        
        Session gs = Session.getInstance(pr, new Authenticator(){
            @Override
            protected PasswordAuthentication getPasswordAuthentication(){
                return new PasswordAuthentication(emailId,password);  //pass your email id and password here
         
            }
        });
        
        Message ms = messageContent(gs,reciever,emailId, dati, oggetto);

        if(ms!=null){
            System.out.println("Message sent! ");
        }else{
            System.out.println("Message DON'T sent! ");
        }
        
    }
    
}
