#Python 3.6.7
import re

output_file= open("dataCompatti.txt", "w")
#blocco per i codici fiscali
codiciF=[None]*100
i=0
for codiceF in open("CodiciFiscali.txt", "r"):
    codiceF=codiceF.replace('\n', '')
    codiciF[i]=codiceF
    i=i+1
#blocco per i nomi e cognomi
nomi=[None]*100
cognomi=[None]*100
i=0
for nome_completo in open("nomi_database.txt", "r"):
    nome_completo=nome_completo.replace('\xa0\n', '')
    nome=(re.search('(.+?) ', nome_completo)).group(1)
    cognome=(re.search(' (.+?) ', nome_completo)).group(1)
    nomi[i]=nome
    cognomi[i]=cognome
    i=i+1 
#blocco per le date di nascita
dateN=[None]*100
i=0
for dataN in open("DateNascita.txt", "r"):
    dataN=dataN.replace('\n', '')
    dateN[i]=dataN
    i=i+1
#blocco per i luoghi di nascita
luoghi=[None]*100
i=0
for luogo in open("Luoghi.txt", "r"):
    luogo=luogo.replace('\n', '')
    luoghi[i]=luogo
    i=i+1
#scrittura dei record finali in forma codiceF, nome, cognome, data, nascita
for i in range(0, 100):
    output_file.write(codiciF[i]+","+nomi[i]+","+cognomi[i]+","+luoghi[i]+","+dateN[i]+"\n")