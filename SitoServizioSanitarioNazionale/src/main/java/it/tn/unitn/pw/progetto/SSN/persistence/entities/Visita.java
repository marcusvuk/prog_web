/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.tn.unitn.pw.progetto.SSN.persistence.entities;

import java.sql.Date;

/**
 *
 * @author Brian
 */
public abstract class  Visita {
/*============================================================================*/
/*                                       ATTRIBUTI                            */
/*============================================================================*/
    private Integer     id;
    private String      riassuntoVisita;
    private String      prescrizione;
    private Date        data;
    private Date        dataPrescrizione;
    private String      id_Persona;
    private Boolean     vistoPaziente;
    private Boolean     vistoReportPaziente;
/*============================================================================*/
    
/*============================================================================*/    
/*                                      METODI COSTRUTTORI                    */
/*============================================================================*/
    public Visita() {
        id                  = null;
        riassuntoVisita     = null;
        prescrizione        = null;
        data                = null;
        dataPrescrizione    = null;
        id_Persona          = null;
        vistoPaziente       = null;
        vistoReportPaziente = null;
    }
/*============================================================================*/

/*============================================================================*/    
/*                                      METODI GETTER E SETTER                */
/*============================================================================*/
    /*--------------------------------------*/   
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
    /*--------------------------------------*/
    public String getRiassuntoVisita() {
        return riassuntoVisita;
    }

    public void setRiassuntoVisita(String riassuntoVisita) {
        this.riassuntoVisita = riassuntoVisita;
    }
    /*--------------------------------------*/
    public String getPrescrizione() {
        return prescrizione;
    }

    public void setPrescrizione(String prescrizione) {
        this.prescrizione = prescrizione;
    }
    /*--------------------------------------*/
    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }
    /*--------------------------------------*/
    public Date getDataPrescrizione() {
        return dataPrescrizione;
    }

    public void setDataPrescrizione(Date dataPrescrizione) {
        this.dataPrescrizione = dataPrescrizione;
    }
    /*--------------------------------------*/
    public String getId_Persona() {
        return id_Persona;
    }

    public void setId_Persona(String id_Persona) {
        this.id_Persona = id_Persona;
    }
    /*--------------------------------------*/
    public Boolean getVistoPaziente() {
        return vistoPaziente;
    }

    public void setVistoPaziente(Boolean vistoPaziente) {
        this.vistoPaziente = vistoPaziente;
    }
    /*--------------------------------------*/
    public Boolean getVistoReportPaziente() {
        return vistoReportPaziente;
    }

    public void setVistoReportPaziente(Boolean vistoReportPaziente) {
        this.vistoReportPaziente = vistoReportPaziente;
    }
    /*--------------------------------------*/
/*============================================================================*/
}
