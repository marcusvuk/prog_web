/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.tn.unitn.pw.progetto.SSN.DAO.persistence.jdbc;

import java.sql.Date;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import it.tn.unitn.pw.progetto.SSN.DAO.common.persistence.dao.jdbc.JDBCDAO;
import it.tn.unitn.pw.progetto.SSN.DAO.common.exception.*;
import it.tn.unitn.pw.progetto.SSN.DAO.persistence.*;
import it.tn.unitn.pw.progetto.SSN.persistence.entities.*;
/**
 *
 * @author Brian
 */
public class JDBC_Farmacia_DAO extends JDBCDAO<Farmacia, String> implements Farmacia_DAO {
/*============================================================================*/    
/*                                      METODI COSTRUTTORI                    */
/*============================================================================*/
    public JDBC_Farmacia_DAO(Connection con) {
        super(con);
    }
/*============================================================================*/
    
/*============================================================================*/    
/*                      METODI GETTER DAO IMPLEMENTATION                      */
/*============================================================================*/
    @Override
    public Long getCount() throws DAOException {
        try (Statement stmt = CON.createStatement()) {
            ResultSet counter = stmt.executeQuery( 
                     "  SELECT COUNT(*) "               +
                     "  FROM Farmacia "     );
            if (counter.next()) {
                return counter.getLong(1);
            }
        } catch (SQLException ex) {
            throw new DAOException( " Immpossibile contare gli elementi di Farmacia  " , ex);
        }
        return 0L;
    }
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    @Override
    public Farmacia getByPrimaryKey(String primaryKey) throws DAOException {
        if (primaryKey == null) {
            throw new DAOException( " primaryKey è null " );
        }
        try (PreparedStatement stm = CON.prepareStatement(
                                     "  SELECT *  "          +
                                     "  FROM Farmacia  "     +
                                     "  WHERE pIVA = ? "       )) {
            // imposto i valori 
            stm.setString(1, primaryKey);
            
            try (ResultSet rs = stm.executeQuery()) {
                rs.next();
                Farmacia farmacia = new Farmacia();
                farmacia.setpIVA             (rs.getString   ( "pIVA" ));
                farmacia.setNome             (rs.getString   ( "nome" ));
                farmacia.setEmail            (rs.getString   ( "email" ));
                farmacia.setPassword         (rs.getString   ( "password" ));
                farmacia.setSalt             (rs.getString   ( "salt" ));
                        
                return farmacia;
            }
        } catch (SQLException ex) {
            throw new DAOException( " Impossibile perendere la Farmacia indicato dalla primary key " , ex);
        }
    }
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    @Override
    public List<Farmacia> getAll() throws DAOException {
        List<Farmacia> listaFarmacia = new ArrayList<>();

        try (Statement stm = CON.createStatement()) {
            try (ResultSet rs = stm.executeQuery(
                                 " SELECT *  "           +
                                 " FROM Farmacia  "      )){
                
                while (rs.next()) {
                    Farmacia farmacia = new Farmacia();
                    
                    farmacia.setpIVA             (rs.getString   ( "pIVA" ));
                    farmacia.setNome             (rs.getString   ( "nome" ));
                    farmacia.setEmail            (rs.getString   ( "email" ));
                    farmacia.setPassword         (rs.getString   ( "password" ));
                    farmacia.setSalt             (rs.getString   ( "salt" ));
                    
                    listaFarmacia.add(farmacia);
                }
            }
        } catch (SQLException ex) {
            throw new DAOException( " Imposibile estrarre tutta la lista di Farmacia  " , ex);
        }

        return listaFarmacia;
    }
/*============================================================================*/
   
/*============================================================================*/    
/*                      METODI QUERY                                          */
/*============================================================================*/
//------------------------------------------------------------------------------
//                      INSERT
//------------------------------------------------------------------------------
    @Override
    public String insert(Farmacia farmacia) throws DAOException {
        if (farmacia == null ) {
            throw new DAOException( " Farmacia  è null " );
        }
        try (PreparedStatement ps = CON.prepareStatement(
                     " INSERT INTO Farmacia "  +
                                     " ( pIVA "         +    "  ,  "   +
                                     "  nome "          +    "  ,  "    +
                                     "  email "         +    "  ,  "    +
                                     "  password "      +    "  ,  "    +
                                     "  salt "          +    "  )  "    +  
                     " VALUES (?,?,?,?,?) " , Statement.RETURN_GENERATED_KEYS)) {
            
            // imposto i valori
            ps.setString(1, farmacia.getpIVA());
            ps.setString(2, farmacia.getNome());
            ps.setString(3, farmacia.getEmail());
            ps.setString(4, farmacia.getPassword());
            ps.setString(5, farmacia.getSalt());
            
            ps.executeUpdate();
            
            return farmacia.getpIVA();
        } catch (SQLException ex) {
            throw new DAOException( " Impossibile aggiungere un nuovo record di Farmacia " , ex);
        }
    }
//------------------------------------------------------------------------------
//                      UPDATE
//------------------------------------------------------------------------------
    @Override
    public void update(Farmacia farmacia) throws DAOException {
        if (farmacia == null ) {
            throw new DAOException( " Farmacia  è null " );
        }
        try (PreparedStatement ps = CON.prepareStatement(
                     " UPDATE Farmacia "  +
                     " SET  " +  "  pIVA = ?  "             +     "  ,  "    +
                                 "  nome = ?  "             +     "  ,  "    +
                                 "  email = ?  "            +     "  ,  "    +
                                 "  password = ?  "         +     "  ,  "    +
                                 "  salt = ?  "             +     
                     " WHERE pIVA = ?  " , Statement.RETURN_GENERATED_KEYS)) {
            
            // imposto i valori
            ps.setString(1, farmacia.getpIVA());
            
            ps.setString(2, farmacia.getNome());
            ps.setString(3, farmacia.getEmail());
            ps.setString(4, farmacia.getPassword());
            ps.setString(5, farmacia.getSalt());
            //WHERE
            ps.setString(6, farmacia.getpIVA());
            
            ps.executeUpdate();
            
            
        } catch (SQLException ex) {
            throw new DAOException( " Impossibile aggiornare il record di Farmacia " , ex);
        }
    }
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    @Override
    public void update_Password(String id, String Password) throws DAOException {
        if (id == null || Password == null ) {
            throw new DAOException( " id o Password è null " );
        }
        try (PreparedStatement ps = CON.prepareStatement(
                     " UPDATE Farmacia "  +
                     " SET  password = ?  "         +  
                     " WHERE pIVA = ?  " , Statement.RETURN_GENERATED_KEYS)) {
            
            // imposto i valori
            ps.setString(1, Password);
            //WHERE
            ps.setString(2, id);
            
            ps.executeUpdate();
            
        } catch (SQLException ex) {
            throw new DAOException( " Impossibile aggiornare il record di Farmacia " , ex);
        }
    }
//------------------------------------------------------------------------------
//                      SELECT
//------------------------------------------------------------------------------
    @Override
    public Farmacia getByFarmaciaEmailAndPassword(String email, String password) throws DAOException {
        if (email == null || password == null) {
            throw new DAOException( " email o password è null " );
        }
        try (PreparedStatement stm = CON.prepareStatement(
                                     "  SELECT *  "                 +
                                     "  FROM Farmacia  "            +
                                     "  WHERE email = ? AND  "      +    
                                           "  password = ?  "       )) {
            // imposto i valori 
            stm.setString(1, email);
            stm.setString(2, password);
            
            try (ResultSet rs = stm.executeQuery()) {
                rs.next();
                Farmacia farmacia = new Farmacia();
                farmacia.setpIVA             (rs.getString   ( "pIVA" ));
                farmacia.setNome             (rs.getString   ( "nome" ));
                farmacia.setEmail            (rs.getString   ( "email" ));
                farmacia.setPassword         (rs.getString   ( "password" ));
                farmacia.setSalt             (rs.getString   ( "salt" ));
                        
                return farmacia;
            }
        } catch (SQLException ex) {
            throw new DAOException( " Impossibile perendere la Farmacia indicato dalla email e password " , ex);
        }
    }
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    @Override
    public String getSaltFarmaciaByEmail(String email) throws DAOException {
        if (email == null ) {
            throw new DAOException( " email è null " );
        }
        try (PreparedStatement stm = CON.prepareStatement(
                                     "  SELECT salt  "              +
                                     "  FROM Farmacia  "            +
                                     "  WHERE email = ?  "          )) {
            // imposto i valori 
            stm.setString(1, email);
            
            try (ResultSet rs = stm.executeQuery()) {
                rs.next();
                String salt;
                salt= rs.getString( "salt" );
                        
                return salt;
            }
        } catch (SQLException ex) {
            throw new DAOException( " Impossibile perendere la Farmacia indicato dalla email " , ex);
        }
    }
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    @Override
    public Farmacia getByFarmaciaEmail(String email)throws DAOException{
        if (email == null ) {
            throw new DAOException( " email  è null " );
        }
        try (PreparedStatement stm = CON.prepareStatement(
                                     "  SELECT *  "                 +
                                     "  FROM Farmacia  "            +
                                     "  WHERE email = ?  ")) {
            // imposto i valori 
            stm.setString(1, email);
            
            try (ResultSet rs = stm.executeQuery()) {
                rs.next();
                Farmacia farmacia = new Farmacia();
                farmacia.setpIVA             (rs.getString   ( "pIVA" ));
                farmacia.setNome             (rs.getString   ( "nome" ));
                farmacia.setEmail            (rs.getString   ( "email" ));
                farmacia.setPassword         (rs.getString   ( "password" ));
                farmacia.setSalt             (rs.getString   ( "salt" ));
                        
                return farmacia;
            }
        } catch (SQLException ex) {
            throw new DAOException( " Impossibile perendere la Farmacia indicato dalla email  " , ex);
        }
    }
/*============================================================================*/
    
}
