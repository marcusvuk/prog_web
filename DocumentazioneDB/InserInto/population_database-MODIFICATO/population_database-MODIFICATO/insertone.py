#Python 3.6.7
# Questo file contiene tutti i file python che generano il file per popolare tutto il database.
# ATTENZ: necessita dei file di testo: Luoghi.txt, nomi_database.txt, CodiciFiscali.txt, ProvincieItaliane.txt, Farmacie.txt, Farmaci.txt, ListaEsami.txt
# ATTENZ: i file insert_medicoSpec.sql e popola_medicoBase sono stati fatti a mano essendo che erano corti da fare
import re
import random
import os
import hashlib, binascii

idfrom_ssprovinciale = 1 #primo id della tabella ssprovinciale
idto_ssprovinciale = 110 #utlimo id della tabella ssprovinciale

idfrom_esame = 1 #primo id della tabella esame
idto_esame = 60 #ultimo id della tabella esame

####### BLOCCO CHE PREPROCESSA I DATI IN MANIERA OPPORTUNA ########################################
#----------------------------------------COMPATTA I DATI------------------------------------------#
output_file= open("dataCompatti.txt", "w")
#blocco per i codici fiscali
codiciF=[None]*100
i=0
for codiceF in open("CodiciFiscali.txt", "r"):
    codiceF=codiceF.replace('\n', '')
    codiciF[i]=codiceF
    i=i+1
#blocco per i nomi e cognomi
nomi=[None]*100
cognomi=[None]*100
i=0
for nome_completo in open("nomi_database.txt", "r"):
    nome_completo=nome_completo.replace('\xa0\n', '')
    nome=(re.search('(.+?) ', nome_completo)).group(1)
    cognome=(re.search(' (.+?) ', nome_completo)).group(1)
    nomi[i]=nome
    cognomi[i]=cognome
    i=i+1 
#blocco per le date di nascita
dateN=[None]*100
i=0
for dataN in open("DateNascita.txt", "r"):
    dataN=dataN.replace('\n', '')
    dateN[i]=dataN
    i=i+1
#blocco per i luoghi di nascita
luoghi=[None]*100
i=0
for luogo in open("Luoghi.txt", "r"):
    luogo=luogo.replace('\n', '')
    luoghi[i]=luogo
    i=i+1
#scrittura dei record finali in forma codiceF, nome, cognome, data, nascita
for i in range(0, 100):
    output_file.write(codiciF[i]+","+nomi[i]+","+cognomi[i]+","+luoghi[i]+","+dateN[i]+"\n")
output_file.close()
#-----------------------------------------------------------------------------------------------------#
#######################################################################################################

#################################### POPOLA SSPROVINCIALE #############################################
output = open("popolone.sql", "a")
for provincia in open("ProvincieItaliane.txt", "r"):
    salt=hashlib.sha256(os.urandom(60)).hexdigest().encode('ascii')
    salt_bytes = salt
    salt = str(salt)
    salt = salt[+1:]
    provincia1 = provincia+salt
    hashedpssw = hashlib.pbkdf2_hmac('sha256', provincia1.encode('utf-8'), salt_bytes, 100000)
    hashedpssw = binascii.hexlify(hashedpssw)
    password = str(hashedpssw)
    password = password[+1:]
    provincia=provincia.replace('\n','')
    output.write("INSERT INTO SSPROVINCIALE(nome, password, salt) VALUES ('"+provincia+"', "+password+", "+salt+");\n")
output.close()
#######################################################################################################

#################################### POPOLA PERSONA ###################################################
output = open("popolone.sql", "a")

for cose in open("dataCompatti.txt", "r"):
    ricerca = re.search('(.+?),(.+?),(.+?),(.+?),(.+?)\n', cose)
    codiceF=ricerca.group(1)
    nome=ricerca.group(2)
    cognome=ricerca.group(3)
    luogo=ricerca.group(4)
    data=ricerca.group(5)
    salt=hashlib.sha256(os.urandom(60)).hexdigest().encode('ascii')
    salt_bytes = salt
    salt = str(salt)
    salt = salt[+1:]
    password1=nome+cognome+salt
    hashedpssw = hashlib.pbkdf2_hmac('sha256', password1.encode('utf-8'), salt_bytes, 100000)
    hashedpssw = binascii.hexlify(hashedpssw)
    password = str(hashedpssw)
    password = password[+1:]
    email=nome+"."+cognome+"@gmail.com"
    sesso=random.randint(0,1)
    if(sesso==0):
        sesso=False
    else:
        sesso=True
    sesso=str(sesso)
    vive_id_ssp = str(random.randint(idfrom_ssprovinciale, idto_ssprovinciale))#ATTENZIONE: questo range è da cambiare ogni volta che si fa l'inserimento per popolare la tabella SSProvinciale
    record="'"+codiceF+"'"+','+"'"+nome+"'"+','+"'"+cognome+"'"+','+password+','+salt+','+"'"+data+"'"+','+"'"+luogo+"'"+','+sesso+','+"'"+email+"'"+','+vive_id_ssp
    output.write("INSERT INTO Persona(codF, nome, cognome, password, salt, nascita, citta, sesso, email, vive_id_ssp) VALUES ("+record+");\n")
output.close()
#######################################################################################################

#################################### POPOLA FOTO ######################################################
giorno = ["01","02","03","04","05","06","07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31"]
mese = ["01","02","03","04","05","06","07", "08", "09", "10", "11", "12"]

output = open("popolone.sql", "a")
i=0
for codF in open("CodiciFiscali.txt","r"):
    codF=codF.replace('\n','')
    i=i+1
    #blocco per la creazione della data
    selezioneM = mese[random.randint(0,len(mese)-1)]
    if(selezioneM == "02"):
        selezioneG = giorno[random.randint(0,len(giorno)-4)]
    elif(selezioneM == "04" or selezioneM == "06" or selezioneM == "09" or selezioneM == "11"):
        selezioneG = giorno[random.randint(0,len(giorno)-2)]
    else:
        selezioneG = giorno[random.randint(0,len(giorno)-1)]
    data=str(random.randint(2017,2019))+"-"+selezioneM+"-"+selezioneG
    #fine del blocco
    #blocco per il path
    path="'/stylegan-master/foto_database/1m_faces_sample/'"#il nome della foto bisogna metterlo sequenziale
    #fine blocco
    #blocco per il nome
    nome=str(i)
    #fine blocco

    output.write("INSERT INTO foto(id_persona,data,path,nome,scelta) VALUES ('"+codF+"','"+data+"',"+path+",'"+nome+"', 'True');\n")
output.close()
#######################################################################################################

#################################### POPOLA MEDICOBASE ################################################
#in questa parte di codice viene solo letto riga per riga il file popola_medicoBase.sql e aggiunto al file sql finale
output=open("popolone.sql", "a")
for record in open("popola_medicoBase.sql", "r"):
    output.write(record)
output.close()
#######################################################################################################

#################################### POPOLA ESAME #####################################################
output = open("popolone.sql", "a")
for esame in open("ListaEsami.txt", "r"):
    esame=esame[:-1]
    output.write("INSERT INTO esame(nome) VALUES ('"+esame+"');\n")
output.close()
#######################################################################################################

#################################### POPOLA MEDICOSPEC ################################################
#in questa parte di codice viene solo letto riga per riga il file insert_medicoSpec.sql e aggiunto al file sql finale
output=open("popolone.sql", "a")
for record in open("insert_medicoSpec.sql", "r"):
    output.write(record)
output.close()
#######################################################################################################

#################################### POPOLA CARTELLACLINICAPERMB ######################################


#########################################FUNZIONI############################################
def scorri(stream):#funzione che ti scorre le righe di un file
    line = stream.readline()
    line=line.replace('\n', '')
    #print(line)
    stream.seek(stream.tell(), 0)
    return line

#############################################################################################
output=open("popolone.sql", "a")
#questo array ho dovuto scriverlo perchè ho popolato i mediciDiBase a mano quindi non avevo modo
#di prendere i codici fiscali solo dei medici di base.
mediciBase=['WNOSHN70B22I754B','DHMLYN56P05L483U', 'KNCDEI38M15D612H', 'MSSLSI02L55H294E', 'WTKSLN15A66L736V', 'BRYLWN05P02F704C', 'CYLSWN76D60A952Y']
codici = open("CodiciFiscali.txt", "r")
date = open("DateNascita.txt", "r")
luoghi = open("Luoghi.txt", "r")
for i in range(0,100):
    loco = scorri(luoghi)
    if loco=='Trento':
        medico=mediciBase[0]
    elif loco=='Rimini':
        medico=mediciBase[1]
    elif loco=='Firenze':
        medico=mediciBase[2]
    elif loco=='Bolzano':
        medico=mediciBase[3]
    elif loco=='Venezia':
        medico=mediciBase[4]
    elif loco=='Siracusa':
        medico=mediciBase[5]
    elif loco=='Ancona':
        medico=mediciBase[6]
    output.write("INSERT INTO cartellaclinicapermb(datainizio,datafine,id_medicobase,id_persona,vistomedicobase) VALUES ('"+scorri(date)+"', NULL,'"+medico+"','"+scorri(codici)+"', 'True');\n")
output.close()
#######################################################################################################

#################################### POPOLA FARMACO ###################################################
output = open("popola_farmaci.sql", "w")
for farmaco in open("Farmaci.txt", "r"):#si bugga all'ultimo farmaco, ma tutti gli altri li fa senza nessun problema
    substringhe=re.search('(.+?), (.+?)\n', farmaco)
    if substringhe==None:
        output.close()
    else:
        farmaco=substringhe.group(1)
        descrizione = substringhe.group(2)
        output.write("INSERT INTO farmaco(nome,descrizione) VALUES ('"+farmaco+"','"+descrizione+"');\n")
output.close()
#######################################################################################################

#################################### POPOLA FARMACIE ##################################################
output = open("popolone.sql", "a")
for farmacia in open("Farmacie.txt", "r"):
    farmacia=farmacia[:-1]
    salt=hashlib.sha256(os.urandom(60)).hexdigest().encode('ascii')
    salt_bytes = salt
    salt = str(salt)
    salt = salt[+1:]
    farmacia1 = farmacia+salt
    hashedpssw = hashlib.pbkdf2_hmac('sha256', farmacia1.encode('utf-8'), salt_bytes, 100000)
    hashedpssw = binascii.hexlify(hashedpssw)
    password = str(hashedpssw)
    password = password[+1:]
    piva=""
    for i in range(0, 11):#generazione partita iva
        piva=piva+str(random.randint(0,9))
    email=farmacia.replace(' ','')
    email = email+"@gmail.com"
    output.write("INSERT INTO farmacia(piva, nome, email, password, salt) VALUES ('"+piva+"','"+farmacia+"','"+email+"',"+password+","+salt+");\n")
output.close()
#######################################################################################################

#################################### POPOLA EROGAESSSP ################################################
output=open("popolone.sql", "a")

for i in range(idfrom_ssprovinciale, idto_ssprovinciale+1):
    esame=random.randint(idfrom_esame,idto_esame)
    output.write("INSERT INTO erogaesssp(id_ssp,id_esame) VALUES ({},{});\n".format(i, esame))
output.close()
#######################################################################################################