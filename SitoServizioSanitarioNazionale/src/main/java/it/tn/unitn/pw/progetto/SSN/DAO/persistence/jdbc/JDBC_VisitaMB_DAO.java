/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.tn.unitn.pw.progetto.SSN.DAO.persistence.jdbc;

import java.sql.Date;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import it.tn.unitn.pw.progetto.SSN.DAO.common.persistence.dao.jdbc.JDBCDAO;
import it.tn.unitn.pw.progetto.SSN.DAO.common.exception.*;
import it.tn.unitn.pw.progetto.SSN.DAO.persistence.*;
import it.tn.unitn.pw.progetto.SSN.persistence.entities.*;
/**
 *
 * @author Brian
 */
public class JDBC_VisitaMB_DAO  extends JDBCDAO<VisitaMB, Integer> implements VisitaMB_DAO {
/*============================================================================*/    
/*                                      METODI COSTRUTTORI                    */
/*============================================================================*/
    public JDBC_VisitaMB_DAO(Connection con) {
        super(con);
    }
/*============================================================================*/
    
/*============================================================================*/    
/*                      METODI GETTER DAO IMPLEMENTATION                      */
/*============================================================================*/
    @Override
    public Long getCount() throws DAOException {
        try (Statement stmt = CON.createStatement()) {
            ResultSet counter = stmt.executeQuery( 
                     "  SELECT COUNT(*) "   +
                     "  FROM VisitaMB "     );
            if (counter.next()) {
                return counter.getLong(1);
            }
        } catch (SQLException ex) {
            throw new DAOException( " Immpossibile contare gli elementi di VisitaMB  " , ex);
        }
        return 0L;
    }
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    @Override
    public VisitaMB getByPrimaryKey(Integer primaryKey) throws DAOException {
        if (primaryKey == null) {
            throw new DAOException( " primaryKey è null " );
        }
        try (PreparedStatement stm = CON.prepareStatement(
                                     "  SELECT *  "          +
                                     "  FROM VisitaMB  "     +
                                     "  WHERE id = ? "    )) {
            // imposto i valori
            stm.setInt(1, primaryKey);
            
            try (ResultSet rs = stm.executeQuery()) {
                rs.next();
                VisitaMB visitaMB = new VisitaMB();
                
                visitaMB.setId                  (rs.getInt      ( "id" ));
                visitaMB.setRiassuntoVisita     (rs.getString   ( "riassuntoVisita" ));
                visitaMB.setPrescrizione        (rs.getString   ( "prescrizione" ));
                visitaMB.setData                (rs.getDate     ( "data" ));
                visitaMB.setDataPrescrizione    (rs.getDate     ( "dataPrescrizione" ));
                visitaMB.setId_Persona          (rs.getString   ( "id_Persona" ));
                visitaMB.setId_MedicoBase       (rs.getString   ( "id_MedicoBase" ));
                visitaMB.setVistoPaziente       (rs.getBoolean  ( "vistoPaziente" ));
                visitaMB.setVistoReportPaziente (rs.getBoolean  ( "vistoReportPaziente" ));
                        
                return visitaMB;
            }
        } catch (SQLException ex) {
            throw new DAOException( " Impossibile perendere la VisitaMB indicato dalla primary key " , ex);
        }
    }
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    @Override
    public List<VisitaMB> getAll() throws DAOException {
        List<VisitaMB> listaVisitaMB = new ArrayList<>();

        try (Statement stm = CON.createStatement()) {
            try (ResultSet rs = stm.executeQuery(
                                 " SELECT *  "           +
                                 " FROM VisitaMB  "      )){
                
                while (rs.next()) {
                    VisitaMB visitaMB = new VisitaMB();
                    
                    visitaMB.setId                  (rs.getInt      ( "id" ));
                    visitaMB.setRiassuntoVisita     (rs.getString   ( "riassuntoVisita" ));
                    visitaMB.setPrescrizione        (rs.getString   ( "prescrizione" ));
                    visitaMB.setData                (rs.getDate     ( "data" ));
                    visitaMB.setDataPrescrizione    (rs.getDate     ( "dataPrescrizione" ));
                    visitaMB.setId_Persona          (rs.getString   ( "id_Persona" ));
                    visitaMB.setId_MedicoBase       (rs.getString   ( "id_MedicoBase" ));
                    visitaMB.setVistoPaziente       (rs.getBoolean  ( "vistoPaziente" ));
                    visitaMB.setVistoReportPaziente (rs.getBoolean  ( "vistoReportPaziente" ));
                    
                    listaVisitaMB.add(visitaMB);
                }
            }
        } catch (SQLException ex) {
            throw new DAOException( " Imposibile estrarre tutta la lista di VisitaMB  " , ex);
        }

        return listaVisitaMB;
    }
/*============================================================================*/
   
/*============================================================================*/    
/*                      METODI QUERY                                          */
/*============================================================================*/
//------------------------------------------------------------------------------
//                      INSERT
//-----------------------------------------------------------------------------
    @Override
    public Integer insert(VisitaMB visita) throws DAOException {
        if (visita == null) {
            throw new DAOException( " La visita è null " );
        }
        try (PreparedStatement ps = CON.prepareStatement(
                     " INSERT INTO VisitaMB "  +
                                 " ( riassuntoVisita  "     +    "  ,  "   +
                                 "  prescrizione  "         +    "  ,  "    +
                                 "  data  "                 +    "  ,  "    +
                                 "  dataPrescrizione  "     +    "  ,  "    +
                                 "  id_Persona  "           +    "  ,  "    +
                                 "  id_MedicoBase  "        +    "  ,  "    +
                                 "  vistoPaziente  "        +    "  ,  "    +
                                 "  vistoReportPaziente  "  +    "  )  "    +  
                     " VALUES (?,?,?,?,?,?,?,?) " , Statement.RETURN_GENERATED_KEYS)) {
            
            // imposto i valori
            ps.setString    (1, visita.getRiassuntoVisita());
            ps.setString    (2, visita.getPrescrizione());
            ps.setDate      (3, visita.getData());
            ps.setDate      (4, visita.getDataPrescrizione());
            ps.setString    (5, visita.getId_Persona());
            ps.setString    (6, visita.getId_MedicoBase());
            ps.setBoolean   (7, visita.getVistoPaziente());
            ps.setBoolean   (8, visita.getVistoReportPaziente());
            
            ps.executeUpdate();
            
            ResultSet rs = ps.getGeneratedKeys();
            if (rs.next()) {
                visita.setId(rs.getInt(1));
            }
            
            return visita.getId();
        } catch (SQLException ex) {
            throw new DAOException( " Impossibile aggiungere un nuovo record di VisitaMB " , ex);
        }
    }
//------------------------------------------------------------------------------
//                      UPDATE
//------------------------------------------------------------------------------
    @Override
    public void update(VisitaMB visita) throws DAOException {
        if (visita == null) {
            throw new DAOException( " La cartellaClinica è null " );
        }
        try (PreparedStatement ps = CON.prepareStatement(
                     " UPDATE VisitaMB "  +
                     " SET          riassuntoVisita = ?  "      +     "  ,  "    +
                                 "  prescrizione = ?  "         +     "  ,  "    +
                                 "  data = ?  "                 +     "  ,  "    +
                                 "  dataPrescrizione = ?  "     +     "  ,  "    +
                                 "  id_Persona = ?  "           +     "  ,  "    +
                                 "  id_MedicoBase = ?  "        +     "  ,  "    +
                                 "  vistoPaziente = ?  "        +     "  ,  "    +
                                 "  vistoReportPaziente = ?  "  +     
                     " WHERE id = ?  ")) {
            
            // imposto i valori
            ps.setString    (1, visita.getRiassuntoVisita());
            ps.setString    (2, visita.getPrescrizione());
            ps.setDate      (3, visita.getData());
            ps.setDate      (4, visita.getDataPrescrizione());
            ps.setString    (5, visita.getId_Persona());
            ps.setString    (6, visita.getId_MedicoBase());
            ps.setBoolean   (7, visita.getVistoPaziente());
            ps.setBoolean   (8, visita.getVistoReportPaziente());
            //WHERE
            ps.setInt       (9, visita.getId());
            
            ps.executeUpdate();
            
        } catch (SQLException ex) {
            throw new DAOException( " Impossibile aggiornare il record di VisitaMB " , ex);
        }
    }
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    @Override
    public void update_EsequzioneVisita(Integer id, Date data) throws DAOException {
        if (id == null || data == null) {
            throw new DAOException( " id o data è null " );
        }
        try (PreparedStatement ps = CON.prepareStatement(
                     " UPDATE VisitaMB "  +
                     " SET   data = ?  "   +
                     " WHERE id = ?  " , Statement.RETURN_GENERATED_KEYS)) {
            
            // imposto i valori
            ps.setDate  (1, data);
            //WHERE
            ps.setInt   (2, id);
            
            ps.executeUpdate();
            
        } catch (SQLException ex) {
            throw new DAOException( " Impossibile aggiornare il record di VisitaMB " , ex);
        }
    }
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    @Override
    public void update_VistoPaziente(Integer id, Boolean visto) throws DAOException {
        if (id == null || visto == null) {
            throw new DAOException( " id o visto dal paziente è null " );
        }
        try (PreparedStatement ps = CON.prepareStatement(
                     " UPDATE VisitaMB "  +
                     " SET   vistoPaziente = ?  "   +
                     " WHERE id = ?  " , Statement.RETURN_GENERATED_KEYS)) {
            
            // imposto i valori
            ps.setBoolean   (1, visto);
            //WHERE
            ps.setInt       (2, id);
            
            ps.executeUpdate();
            
        } catch (SQLException ex) {
            throw new DAOException( " Impossibile aggiornare il record di VisitaMB " , ex);
        }
    }
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    @Override
    public void update_VistoRepotDaPaziente(Integer id, Boolean visto) throws DAOException {
        if (id == null || visto == null) {
            throw new DAOException( " id o visto il report dal paziente è null " );
        }
        try (PreparedStatement ps = CON.prepareStatement(
                     " UPDATE VisitaMB "  +
                     " SET   vistoReportPaziente = ?  "   +
                     " WHERE id = ?  " , Statement.RETURN_GENERATED_KEYS)) {
            
            // imposto i valori
            ps.setBoolean   (1, visto);
            //WHERE
            ps.setInt       (2, id);
            
            ps.executeUpdate();
            
        } catch (SQLException ex) {
            throw new DAOException( " Impossibile aggiornare il record di VisitaMB " , ex);
        }
    }
//------------------------------------------------------------------------------
//                      SELECT
//------------------------------------------------------------------------------
    @Override
    public List<VisitaMB> getByPazienteId(String id) throws DAOException {
        if (id == null ) {
            throw new DAOException( " id  è null " );
        }
        List<VisitaMB> listaVisitaMB = new ArrayList<>();

        try (PreparedStatement stm = CON.prepareStatement(
                                "  SELECT *  "                      +
                                "  FROM VisitaMB  "                 +
                                "  WHERE id_Persona = ?  ")) {
            
            stm.setString(1, id);
            try (ResultSet rs = stm.executeQuery()){
                
                while (rs.next()) {
                    VisitaMB visitaMB = new VisitaMB();
                    
                    visitaMB.setId                  (rs.getInt      ( "id" ));
                    visitaMB.setRiassuntoVisita     (rs.getString   ( "riassuntoVisita" ));
                    visitaMB.setPrescrizione        (rs.getString   ( "prescrizione" ));
                    visitaMB.setData                (rs.getDate     ( "data" ));
                    visitaMB.setDataPrescrizione    (rs.getDate     ( "dataPrescrizione" ));
                    visitaMB.setId_Persona          (rs.getString   ( "id_Persona" ));
                    visitaMB.setId_MedicoBase       (rs.getString   ( "id_MedicoBase" ));
                    visitaMB.setVistoPaziente       (rs.getBoolean  ( "vistoPaziente" ));
                    visitaMB.setVistoReportPaziente (rs.getBoolean  ( "vistoReportPaziente" ));

                    listaVisitaMB.add(visitaMB);
                }
            }
        } catch (SQLException ex) {
            throw new DAOException( " Imposibile estrarre la lista di VisitaMB con chiave esterna Persona  " , ex);
        }

        return listaVisitaMB;
    }
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    @Override
    public List<VisitaMB> getByMedicoBaseId(String id) throws DAOException {
        if (id == null ) {
            throw new DAOException( " id  è null " );
        }
        List<VisitaMB> listaVisitaMB = new ArrayList<>();

        try (PreparedStatement stm = CON.prepareStatement(
                                "  SELECT *  "                      +
                                "  FROM VisitaMB  "                 +
                                "  WHERE id_MedicoBase = ?  "    )) {
            stm.setString(1, id);
            try (ResultSet rs = stm.executeQuery()){
                
                while (rs.next()) {
                    VisitaMB visitaMB = new VisitaMB();
                    
                    visitaMB.setId                  (rs.getInt      ( "id" ));
                    visitaMB.setRiassuntoVisita     (rs.getString   ( "riassuntoVisita" ));
                    visitaMB.setPrescrizione        (rs.getString   ( "prescrizione" ));
                    visitaMB.setData                (rs.getDate     ( "data" ));
                    visitaMB.setDataPrescrizione    (rs.getDate     ( "dataPrescrizione" ));
                    visitaMB.setId_Persona          (rs.getString   ( "id_Persona" ));
                    visitaMB.setId_MedicoBase       (rs.getString   ( "id_MedicoBase" ));
                    visitaMB.setVistoPaziente       (rs.getBoolean  ( "vistoPaziente" ));
                    visitaMB.setVistoReportPaziente (rs.getBoolean  ( "vistoReportPaziente" ));

                    listaVisitaMB.add(visitaMB);
                }
            }
        } catch (SQLException ex) {
            throw new DAOException( " Imposibile estrarre la lista di VisitaMB con chiave esterna MedicoBase " , ex);
        }

        return listaVisitaMB;
    }
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    @Override
    public List<VisitaMB> getByPazienteId_Fatte(String id) throws DAOException {
        if (id == null ) {
            throw new DAOException( " id  è null " );
        }
        List<VisitaMB> listaVisitaMB = new ArrayList<>();

        try (PreparedStatement stm = CON.prepareStatement(
                                "  SELECT *  "                                 +
                                "  FROM VisitaMB  "                            +
                                "  WHERE   data IS NOT NULL  AND  "            +
                                        "  id_MedicoBase IS NOT NULL  AND  "   +
                                        "  id_Persona = ?  ")) {
            
            stm.setString(1, id);
            try (ResultSet rs = stm.executeQuery()){
                
                while (rs.next()) {
                    VisitaMB visitaMB = new VisitaMB();
                    
                    visitaMB.setId                  (rs.getInt      ( "id" ));
                    visitaMB.setRiassuntoVisita     (rs.getString   ( "riassuntoVisita" ));
                    visitaMB.setPrescrizione        (rs.getString   ( "prescrizione" ));
                    visitaMB.setData                (rs.getDate     ( "data" ));
                    visitaMB.setDataPrescrizione    (rs.getDate     ( "dataPrescrizione" ));
                    visitaMB.setId_Persona          (rs.getString   ( "id_Persona" ));
                    visitaMB.setId_MedicoBase       (rs.getString   ( "id_MedicoBase" ));
                    visitaMB.setVistoPaziente       (rs.getBoolean  ( "vistoPaziente" ));
                    visitaMB.setVistoReportPaziente (rs.getBoolean  ( "vistoReportPaziente" ));

                    listaVisitaMB.add(visitaMB);
                }
            }
        } catch (SQLException ex) {
            throw new DAOException( " Imposibile estrarre la lista di VisitaMB con chiave esterna Persona fatte  " , ex);
        }

        return listaVisitaMB;
    }
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    @Override
    public List<VisitaMB> getByPazienteId_DaFare(String id) throws DAOException {
        if (id == null ) {
            throw new DAOException( " id  è null " );
        }
        List<VisitaMB> listaVisitaMB = new ArrayList<>();

        try (PreparedStatement stm = CON.prepareStatement(
                                "  SELECT *  "                                 +
                                "  FROM VisitaMB  "                            +
                                "  WHERE   data IS NULL  AND  "                +
                                        "  id_Persona = ? "    )) {
            stm.setString(1, id);
            try (ResultSet rs = stm.executeQuery()){
                
                while (rs.next()) {
                    VisitaMB visitaMB = new VisitaMB();
                    
                    visitaMB.setId                  (rs.getInt      ( "id" ));
                    visitaMB.setRiassuntoVisita     (rs.getString   ( "riassuntoVisita" ));
                    visitaMB.setPrescrizione        (rs.getString   ( "prescrizione" ));
                    visitaMB.setData                (rs.getDate     ( "data" ));
                    visitaMB.setDataPrescrizione    (rs.getDate     ( "dataPrescrizione" ));
                    visitaMB.setId_Persona          (rs.getString   ( "id_Persona" ));
                    visitaMB.setId_MedicoBase       (rs.getString   ( "id_MedicoBase" ));
                    visitaMB.setVistoPaziente       (rs.getBoolean  ( "vistoPaziente" ));
                    visitaMB.setVistoReportPaziente (rs.getBoolean  ( "vistoReportPaziente" ));

                    listaVisitaMB.add(visitaMB);
                }
            }
        } catch (SQLException ex) {
            throw new DAOException( " Imposibile estrarre la lista di VisitaMB con chiave esterna Persona da fare  " , ex);
        }

        return listaVisitaMB;
    }
/*============================================================================*/
}
