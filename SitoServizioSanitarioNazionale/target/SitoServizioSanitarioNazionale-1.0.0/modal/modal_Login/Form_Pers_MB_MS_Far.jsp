<div class="modal-body">
    <%--<div class="form-group">
        <label>Codice fiscale</label>
        <input id="codiceFiscale" 
               name="codiceFiscale"
               type="text"  
               class="form-control" />
    </div>--%>
    <div class="form-group">
        <label>Email</label>
        <input id="email" 
               name="email"
               type="email"  
               class="form-control" 
               required="required" />
    </div>
    <div class="form-group">
        <div class="clearfix">
            <label>Password</label>
            <a href="${pageContext.request.contextPath}/richiediPassword.jsp" class="pull-right text-muted">
                <small>Non ricordi la password?</small>
            </a>
        </div>
        <input  id="password" 
                name="password" 
                type="password"
                class="form-control"
                required="required"/>
    </div>
</div>    
<div class="modal-footer">
        <label class="checkbox-inline pull-left">
            <input  id="ricordami" 
                    name="ricordami" 
                    type="checkbox" /> Ricordami
       </label>
    <input  type="submit"
            class="btn btn-primary pull-right"
            accept=""value="Accedi"/>
</div>