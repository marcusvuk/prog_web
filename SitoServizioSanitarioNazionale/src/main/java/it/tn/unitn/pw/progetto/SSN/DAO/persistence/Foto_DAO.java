/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.tn.unitn.pw.progetto.SSN.DAO.persistence;

import java.util.List;
        
import it.tn.unitn.pw.progetto.SSN.persistence.entities.Foto;
import it.tn.unitn.pw.progetto.SSN.persistence.entities.Persona;
import it.tn.unitn.pw.progetto.SSN.DAO.common.DAO;
import it.tn.unitn.pw.progetto.SSN.DAO.common.exception.DAOException;
/**
 *
 * @author Brian
 */
public interface Foto_DAO extends DAO<Foto, Integer>{
/*============================================================================*/    
/*                         METODO INSERT                                      */
/*============================================================================*/
    public Integer insert(Foto foto)throws DAOException;
/*============================================================================*/    
/*============================================================================*/    
/*                         METODO UPDATE                                      */
/*============================================================================*/
    public void update(Foto foto)throws DAOException;
    public void update_Scelta(Integer id,String id_persona,Boolean scelto)throws DAOException;
/*============================================================================*/
/*============================================================================*/    
/*                         METODO GET Da ID                                   */
/*============================================================================*/
    public List<Foto> getByPazienteId(String id)throws DAOException;
/*============================================================================*/ 
/*============================================================================*/    
/*                         METODO GET Da ID solo Attivi                       */
/*============================================================================*/
    public Foto getByPazienteId_Attivo(String id)throws DAOException;
/*============================================================================*/   
    
}
