<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@include file="/modal/modal_Cookies/modal_Cookies.jsp" %>
<% if( (request.getSession(false).getAttribute("c_Persona")       !=null) ||
       (request.getSession(false).getAttribute("b_Persona")       !=null) ||
       (request.getSession(false).getAttribute("s_Persona")       !=null) ||
       (request.getSession(false).getAttribute("f_Farmacia")      !=null) || 
       (request.getSession(false).getAttribute("p_ssProvinciale") !=null)    ){%>
    <%@include file="/modal/modal_MenuUtente/modal_MenuUtente.jsp" %>
<%}%>
<%@include file="/modal/modal_Login/modal_Login.jsp" %>
<!---Login popup-->
<nav class="navbar navbar-expand-md navbar-light bg-light sticky-top">
    <div class="container-fluid">
        <a class="navbar-brand" href="/SitoServizioSanitarioNazionale/Index">
            <img src="${pageContext.request.contextPath}/sito_IMG/logo (1).png" onerror="this.src='..\\sito_IMG\\logo (1).png';"/>
        </a>
        <button
            class="navbar-toggler"
            type="button"
            data-toggle="collapse"
            data-target="#navbarResponsive">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item active">
                    <a class="nav-link" 
                       href="${pageContext.request.contextPath}/Index">
                        Home
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" 
                       href="${pageContext.request.contextPath}/Index#pag_CHI_SIAMO">
                        Chi siamo
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" 
                       href="${pageContext.request.contextPath}/Index#pag_COSA_FACCIAMO">
                        Cosa facciamo
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" 
                       href="${pageContext.request.contextPath}/Index#pag_MODULI_SERVIZZI">
                        Moduli e Servizi
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" 
                       href="${pageContext.request.contextPath}/Index#pag_CONTATTI">
                        Contatti
                    </a>
                </li>
                
                
            <%-- SE NON REGISTRATO --%>
            <% if(  (request.getSession(false).getAttribute("c_Persona")       ==null) &&
                    (request.getSession(false).getAttribute("b_Persona")       ==null) &&
                    (request.getSession(false).getAttribute("s_Persona")       ==null) &&
                    (request.getSession(false).getAttribute("f_Farmacia")      ==null) && 
                    (request.getSession(false).getAttribute("p_ssProvinciale") ==null) ){%>
                <li class="nav-item">
                    <a class="nav-link trigger-btn"
                       href="#"
                       data-toggle="modal" 
                       data-target="#LoginModal">
                        Accedi
                    </a>
                </li>
            <%-- SE REGISTRATO --%>
            <% }else{%>
                <li class="nav-item">
                    <a class="nav-link trigger-btn"
                       href="#"
                       data-toggle="modal" 
                       data-target="#MenuUtente">
                        Men&ugrave;  Profilo
                    </a>
                </li>
            <%}%>
            </ul>
        </div>
    </div>
</nav>