/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.tn.unitn.pw.progetto.SSN.servlet;

import it.tn.unitn.pw.progetto.SSN.DAO.common.exception.DAOException;
import it.tn.unitn.pw.progetto.SSN.DAO.common.exception.DAOFactoryException;
import it.tn.unitn.pw.progetto.SSN.DAO.common.factory.DAOFactory;
import it.tn.unitn.pw.progetto.SSN.DAO.persistence.Farmacia_DAO;
import it.tn.unitn.pw.progetto.SSN.DAO.persistence.MedicoBase_DAO;
import it.tn.unitn.pw.progetto.SSN.DAO.persistence.Foto_DAO;
import it.tn.unitn.pw.progetto.SSN.DAO.persistence.Persona_DAO;
import it.tn.unitn.pw.progetto.SSN.DAO.persistence.SSProvinciale_DAO;
import it.tn.unitn.pw.progetto.SSN.DAO.persistence.CartellaClinicaPerMB_DAO;
import it.tn.unitn.pw.progetto.SSN.servlet.srv_Index;
import it.tn.unitn.pw.progetto.SSN.persistence.entities.*;
import java.io.File;
import java.util.GregorianCalendar;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import java.util.Enumeration;
import java.util.List;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import jdk.internal.misc.VM;
/**
 *
 * @author Brian
 */
@MultipartConfig
public class srv_MainCittadino extends HttpServlet {
    
    public  String              uploadFotoDir;
    
    private Persona_DAO               persona_Dao;
    private MedicoBase_DAO            medicoBase_Dao;
    private Foto_DAO                  foto_Dao;
    private CartellaClinicaPerMB_DAO  cartellaClinicaPerMB_DAO;
    
    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        uploadFotoDir = getServletContext().getInitParameter("uploadFotoDir");
        if (uploadFotoDir == null) {
            throw new ServletException("Please supply uploadDir parameter");
        }
        
        
        DAOFactory daoFactory = (DAOFactory) super.getServletContext().getAttribute("daoFactory");
        if (daoFactory == null) {
            throw new ServletException("Impossible to get dao factory for user storage system");
        }
        try {
            persona_Dao                 = daoFactory.getDAO(Persona_DAO.class);
            medicoBase_Dao              = daoFactory.getDAO(MedicoBase_DAO.class);
            foto_Dao                    = daoFactory.getDAO(Foto_DAO.class);
            cartellaClinicaPerMB_DAO    = daoFactory.getDAO(CartellaClinicaPerMB_DAO.class);
        } catch (DAOFactoryException ex) {
            throw new ServletException("Impossible to get dao factory for user storage system", ex);
        }
    }
    


    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        response.sendRedirect("cittadino.jsp"); 
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String typeCambia = request.getParameter("typeCambia");
        
        if(typeCambia.equals("Foto")){
            modificaFoto(request,response);
            
        }else if(typeCambia.equals("Password")){
            modificaPassword(request,response);
            
        }else if(typeCambia.equals("MedicoBase")){
            modificaMedicoBase(request,response);
            
        }else{
            // ERROR ------------------------------------------------------------------------------------------
            response.sendRedirect("cittadino.jsp");
            // ERROR ------------------------------------------------------------------------------------------
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
//####################################################################################
//####################################################################################
//==============================================================================
//                          Modifica FOTO
//==============================================================================
    protected void modificaFoto (HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String contextPath = getServletContext().getContextPath();
        if (!contextPath.endsWith("/")) {
            contextPath += "/";
        }
        
        Boolean fileCaricato=false;
        Persona persona     = (Persona) request.getSession(false).getAttribute("c_Persona");

        Foto nuovaFoto = new Foto();
        
        
        Date date = new Date();
        LocalDate localDate = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        Integer year  = localDate.getYear();
        Integer month = localDate.getMonthValue();
        Integer day   = localDate.getDayOfMonth();
        
        String pathForDb=   "\\" + persona.getCodF()  +
                            "\\" + year.toString()    +
                            "\\" + month.toString()   +
                            "\\" + day.toString()     +
                            "\\" + date.getTime();
        nuovaFoto.setData(new java.sql.Date(date.getTime()));
        nuovaFoto.setId_Persona(persona.getCodF());
        nuovaFoto.setPath(pathForDb+ "\\" );
        nuovaFoto.setScelta(true);
        
        File uploadDirFile = new File(uploadFotoDir  +   pathForDb);
        
        Part filePart1 = request.getPart("file1");
        if ((filePart1 != null) && (filePart1.getSize() > 0)) {
           
            String filename = Paths.get(filePart1.getSubmittedFileName()).getFileName().toString();//MSIE  fix.
            
            nuovaFoto.setNome(filename);
            
            File file1 = new File(uploadDirFile, filename);
            file1.getParentFile().mkdirs();
            try (InputStream fileContent = filePart1.getInputStream()) {
                Files.copy(fileContent, file1.toPath());
                fileCaricato= true;
            }
        }
        if(fileCaricato){
            nuovaFoto = aggiornaFotoNelDB(request,response,nuovaFoto);
            if(nuovaFoto!=null){
                request.getSession().setAttribute("c_FotoAttiva", nuovaFoto);
                request.getSession().setAttribute("c_ListaFoto", getListaFoto(persona.getCodF()));
                request.getSession().removeAttribute("c_FotoMyMedicoBase");
                response.sendRedirect(response.encodeRedirectURL(contextPath + "Cittadino/ConfermaModifica.jsp"));
            }
        }
        response.sendRedirect(response.encodeRedirectURL(contextPath + "Cittadino/cittadino.jsp"));
    }
//==============================================================================
//                          Aggungi FOTO attiva
//==============================================================================
    protected Foto aggiornaFotoNelDB(   HttpServletRequest request,
                                        HttpServletResponse response,
                                        Foto nuovaFoto){
        
        Foto foto     = (Foto) request.getSession(false).getAttribute("c_FotoAttiva");
        
        try {
            foto_Dao.insert(nuovaFoto);
            
            foto_Dao.update_Scelta(foto.getId(), foto.getId_Persona(),!foto.getScelta());
            
        }catch (DAOException ex) {
            request.getServletContext().log("Imposibile recuperare il medico di base.", ex);
            nuovaFoto= null;
        }finally{
            return  nuovaFoto;
        }
    }
//==============================================================================
    private List<Foto> getListaFoto(String CodF){
        List<Foto> listaFoto=null;
        
        try {
            listaFoto = foto_Dao.getByPazienteId(CodF);
        } catch (DAOException ex) {
            Logger.getLogger(getClass().getName()).severe(ex.toString());
        }
         
        return listaFoto;
    }
//==============================================================================
//                          Modifica  password attiva
//==============================================================================
    protected void modificaPassword (HttpServletRequest request, HttpServletResponse response)
                                                       throws ServletException, IOException {
        String contextPath = getServletContext().getContextPath();
        if (!contextPath.endsWith("/")) {
            contextPath += "/";
        }
        
        String passwordVechia           = request.getParameter("passwordVechia");
        String passwordNuova            = request.getParameter("passwordNuova");
        String passwordNuovaRipetuto    = request.getParameter("passwordNuovaRipetuta");
        
        Persona persona     = (Persona) request.getSession(false).getAttribute("c_Persona");
        
        String passwordVechiaHashed     =persona.getPassword();
        
        if(passwordNuova.equals(passwordNuovaRipetuto)){
            
            if(srv_Index.convertToHashPassword(passwordVechia, persona.getSalt()).equals(passwordVechiaHashed)){
                persona = cambiaPassword(passwordNuova,persona);
                if(!persona.getPassword().equals(passwordVechiaHashed)){
                    request.getSession().setAttribute("c_Persona", persona);
                    response.sendRedirect(response.encodeRedirectURL(contextPath + "Cittadino/ConfermaModifica.jsp"));
                }else{
                    response.sendRedirect(response.encodeRedirectURL(contextPath + "Cittadino/cittadino.jsp"));
                }
            }else{
                response.sendRedirect(response.encodeRedirectURL(contextPath + "Cittadino/cittadino.jsp"));
            }
        }else{
            response.sendRedirect(response.encodeRedirectURL(contextPath + "Cittadino/cittadino.jsp"));
        }
    }
//==============================================================================
    protected Persona cambiaPassword(String password, Persona persona){
        try {
            String passwordHashed=srv_Index.convertToHashPassword(password, persona.getSalt());
            persona_Dao.update_Password(persona.getCodF(), passwordHashed);
            persona.setPassword(passwordHashed);
        } catch (DAOException ex) {
            Logger.getLogger(getClass().getName()).severe(ex.toString());
        }
        
        return persona;
    }
//==============================================================================
//                          Modifica Medico di Base
//==============================================================================
    protected void modificaMedicoBase (HttpServletRequest request, HttpServletResponse response)
                                                       throws ServletException, IOException {
        String contextPath = getServletContext().getContextPath();
        if (!contextPath.endsWith("/")) {
            contextPath += "/";
        }
        
        String codF_MedicoBase  = request.getParameter("autocomplete-medicoBase");
        String password         = request.getParameter("password");
        
        Persona persona     = (Persona) request.getSession(false).getAttribute("c_Persona");
        
 
        if(srv_Index.convertToHashPassword(password, persona.getSalt()).equals(persona.getPassword())){
            
            if(cambiaMedicoBase(codF_MedicoBase,persona)){
                request.getSession().setAttribute("c_MyMedicoBase", null);
                request.getSession().setAttribute("c_fotoMyMedicoBase", null);
                response.sendRedirect(response.encodeRedirectURL(contextPath + "Cittadino/ConfermaModifica.jsp"));
            }else{
                response.sendRedirect(response.encodeRedirectURL(contextPath + "Cittadino/cittadino.jsp"));
            }
        }else{
            response.sendRedirect(response.encodeRedirectURL(contextPath + "Cittadino/cittadino.jsp"));
        }
    }
//==============================================================================
    private Boolean cambiaMedicoBase(String codF_MedicoBase,Persona persona){
        Boolean esito = false;
        CartellaClinicaPerMB nuovaCatellaClinaica = new CartellaClinicaPerMB();
        Date dattaAtuale= new Date();
        
        nuovaCatellaClinaica.setId( null );
        nuovaCatellaClinaica.setDataFine( null );
        nuovaCatellaClinaica.setDataInizio( new java.sql.Date(dattaAtuale.getTime()) );
        nuovaCatellaClinaica.setId_Persona(persona.getCodF());
        nuovaCatellaClinaica.setId_MedicoBase(codF_MedicoBase);
        nuovaCatellaClinaica.setVistoMedicoBase(false);
                
        try {
            MedicoBase nuovoMedicoBase  = medicoBase_Dao.getByPrimaryKey(codF_MedicoBase);
            if(null != nuovoMedicoBase){
                CartellaClinicaPerMB catellaClinaicaAttuale= cartellaClinicaPerMB_DAO.getByPazienteId_Attivo(persona.getCodF());

                if((!catellaClinaicaAttuale.getId_MedicoBase().equals(codF_MedicoBase)) &&
                    nuovoMedicoBase.getLavora_id_SSP().equals(persona.getVive_id_SSP()) ){
                    
                    Integer id = cartellaClinicaPerMB_DAO.insert(nuovaCatellaClinaica);
                    if( id != null){
                        try {
                            cartellaClinicaPerMB_DAO.update_FinePeriodo(
                                                    catellaClinaicaAttuale.getId(), 
                                                    nuovaCatellaClinaica.getDataInizio() );
                            esito = true;
                        } catch (DAOException ex) {
                            Logger.getLogger(getClass().getName()).severe(ex.toString());
                        }
                    }
                }
            }
        } catch (DAOException ex) {
            Logger.getLogger(getClass().getName()).severe(ex.toString());
        }
        
        
        return esito;
    }
}
