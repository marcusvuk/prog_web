/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.tn.unitn.pw.progetto.SSN.listener;


import it.tn.unitn.pw.progetto.SSN.DAO.common.exception.DAOFactoryException;
import it.tn.unitn.pw.progetto.SSN.DAO.common.factories.jdbc.JDBCDAOFactory;
import it.tn.unitn.pw.progetto.SSN.DAO.common.factory.DAOFactory;

import it.tn.unitn.pw.progetto.SSN.servlet.srv_Index;
import it.tn.unitn.pw.progetto.SSN.filter.AuthenticationCookie;

import it.tn.unitn.pw.progetto.SSN.services.ProvincieService;
import it.tn.unitn.pw.progetto.SSN.services.MedicoBaseService;
import it.tn.unitn.pw.progetto.SSN.services.PazientiService;
import it.tn.unitn.pw.progetto.SSN.services.EsamiPazienteService;
import it.tn.unitn.pw.progetto.SSN.services.TipoEsameService;
import it.tn.unitn.pw.progetto.SSN.services.FarmaciService;
import it.tn.unitn.pw.progetto.SSN.services.RicettaPazienteService;
import it.tn.unitn.pw.progetto.SSN.services.VisitePazienteService;
import it.tn.unitn.pw.progetto.SSN.services.PagamentiService;

import it.tn.unitn.pw.progetto.SSN.filter.AuthenticationCittadino;
import it.tn.unitn.pw.progetto.SSN.filter.AuthenticationMedicoBase;
import it.tn.unitn.pw.progetto.SSN.filter.AuthenticationMedicoSpec;
import it.tn.unitn.pw.progetto.SSN.filter.AuthenticationFarmacia;
import it.tn.unitn.pw.progetto.SSN.filter.AuthenticationSSProvinciale;

import it.tn.unitn.pw.progetto.SSN.filter.PazienteMS_Filter;
import it.tn.unitn.pw.progetto.SSN.filter.PazienteMB_Filter;
import it.tn.unitn.pw.progetto.SSN.filter.Esame_Filter;
import it.tn.unitn.pw.progetto.SSN.filter.Visita_Filter;
import it.tn.unitn.pw.progetto.SSN.filter.Ricetta_Filter;

import java.util.logging.Logger;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import javax.servlet.ServletException;
import java.util.HashMap;

public class WebAppContextListener implements ServletContextListener {
    
    private final HashMap <String, Object> authenticatedUsers = new HashMap();
    
    
    @Override
    public void contextInitialized(ServletContextEvent sce) {
        String dburl = sce.getServletContext().getInitParameter("dburl");
        try {
            JDBCDAOFactory.configure(dburl);
            DAOFactory daoFactory = JDBCDAOFactory.getInstance();
            sce.getServletContext().setAttribute("daoFactory", daoFactory);
        
//------------------------------------------------------------------------------
//                          Start service 
//------------------------------------------------------------------------------
            srv_Index.myInit(authenticatedUsers);
            AuthenticationCookie.myInit(authenticatedUsers);
            
            ProvincieService.init(daoFactory);
            MedicoBaseService.init(daoFactory);
            PazientiService.init(daoFactory);
            EsamiPazienteService.init(daoFactory);
            TipoEsameService.init(daoFactory);
            FarmaciService.init(daoFactory);
            RicettaPazienteService.init(daoFactory);
            VisitePazienteService.init(daoFactory);
            PagamentiService.init(daoFactory);
                    
            AuthenticationCittadino.myInit(daoFactory);
            AuthenticationMedicoBase.myInit(daoFactory);
            AuthenticationMedicoSpec.myInit(daoFactory);
            AuthenticationFarmacia.myInit(daoFactory);
            AuthenticationSSProvinciale.myInit(daoFactory);
            
            PazienteMS_Filter.myInit(daoFactory);
            PazienteMB_Filter.myInit(daoFactory);
            Esame_Filter.myInit(daoFactory);
            Visita_Filter.myInit(daoFactory);
            Ricetta_Filter.myInit(daoFactory);
//------------------------------------------------------------------------------
//                          Return to normal
//------------------------------------------------------------------------------            
       } catch (DAOFactoryException ex) {
            Logger.getLogger(getClass().getName()).severe(ex.toString());
            throw new RuntimeException(ex);
        }
    }

   
    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        DAOFactory daoFactory = (DAOFactory) sce.getServletContext().getAttribute("daoFactory");
        if (daoFactory != null) {
            daoFactory.shutdown();
        }
        daoFactory = null;
    }
}
