/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.tn.unitn.pw.progetto.SSN.servlet;


import it.tn.unitn.pw.progetto.SSN.DAO.common.exception.DAOException;
import it.tn.unitn.pw.progetto.SSN.DAO.common.exception.DAOFactoryException;
import it.tn.unitn.pw.progetto.SSN.DAO.common.factory.DAOFactory;
import it.tn.unitn.pw.progetto.SSN.DAO.persistence.*;
import it.tn.unitn.pw.progetto.SSN.Genera.Email.Mail;
import it.tn.unitn.pw.progetto.SSN.persistence.entities.*;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.util.List;
import java.util.Iterator;

import java.util.GregorianCalendar;
import java.sql.Date;



/**
 *
 * @author Brian
 */
public class srv_Richiami extends HttpServlet {
//------------------------------------------------------------------------------   
    private PrescrizioneEsameEReferto_DAO    prescrizioneEsame_Dao;
    private CartellaClinicaPerMB_DAO         cartellaClinica_Dao;
    private Persona_DAO                      persona_DAO;
    
    @Override
    public void init() throws ServletException {
        DAOFactory daoFactory = (DAOFactory) super.getServletContext().getAttribute("daoFactory");
        if (daoFactory == null) {
            throw new ServletException("Impossible to get dao factory for user storage system");
        }
        try {
            prescrizioneEsame_Dao   = daoFactory.getDAO(PrescrizioneEsameEReferto_DAO.class);
            cartellaClinica_Dao     = daoFactory.getDAO(CartellaClinicaPerMB_DAO.class);
            persona_DAO             = daoFactory.getDAO(Persona_DAO.class);
            
        } catch (DAOFactoryException ex) {
            throw new ServletException("Impossible to get dao factory for user storage system", ex);
        }
    }
//------------------------------------------------------------------------------
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String contextPath = getServletContext().getContextPath();
        if (!contextPath.endsWith("/")) {
            contextPath += "/";
        }
        
        SSProvinciale ssProvincia = (SSProvinciale) request.getSession(false).getAttribute("p_ssProvinciale");
        
        if(ssProvincia == null){
            response.sendRedirect(response.encodeRedirectURL(contextPath + "Index"));
        }else{
            String s_annoMin = request.getParameter("annoMinimo");
            String s_annoMax = request.getParameter("annoMassimo");
            String s_idTipoEsame = request.getParameter("idTipoEsame");
            
            Integer annoMin     = null;
            Integer annoMax     = null;
            Integer idTipoEsame = null;
            Boolean datiCorreti=true;
            try{
                annoMin     = Integer.parseInt(s_annoMin);
                annoMax     = Integer.parseInt(s_annoMax);
                idTipoEsame = Integer.parseInt(s_idTipoEsame);
            }catch(Exception e){
                datiCorreti = false;
            }
            if((!datiCorreti)||(annoMax<annoMin)){ 
                response.sendRedirect(response.encodeRedirectURL(contextPath + "Index"));
            }else{
                PrescrizioneEsameEReferto richiamo=creaRichiamo(request,ssProvincia,idTipoEsame,annoMin,annoMax);
                if(richiamo!=null){
                    request.getSession().setAttribute("c_b_s_Esame", richiamo);
                    response.sendRedirect(response.encodeRedirectURL(contextPath + "SSP/ConfermaRichiamo.jsp"));
                }else{
                    request.getSession().setAttribute("c_b_s_Esame", null);
                    response.sendRedirect(response.encodeRedirectURL(contextPath + "Index"));
                }
            }
        }
        
        
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
//==============================================================================
//                          Genera richiamo per SSP
//==============================================================================
/**
 * Ritorna null se non crea un richiamo per tutti i cittadini
 */
    private PrescrizioneEsameEReferto creaRichiamo(HttpServletRequest request,SSProvinciale ssProvincia,Integer idTipoEsame,Integer annoMin,Integer annoMax )
                                   throws ServletException, IOException {
        String emailLocal="";
        int numRichiamiEffetuati=0;
        List<Persona> AbitantiDellaProvincia = null;
        PrescrizioneEsameEReferto richiamo=new PrescrizioneEsameEReferto();
        try {
            GregorianCalendar data= new GregorianCalendar();
            Date oggi= new Date(data.getTimeInMillis());
            
            richiamo.setId(null);
            richiamo.setData(null);
            richiamo.setDataPrescrizione(oggi);
            richiamo.setRisultato(null);
            richiamo.setTichet(0.0);
            richiamo.setPagato(true);
            richiamo.setCura(null);
            richiamo.setRichiamo(true);
            //richiamo.setId_CartellaClinica(catellaClinicaAttiva.getId());
            richiamo.setId_MedicoSpec(null);
            richiamo.setId_Esame(idTipoEsame);
            richiamo.setVistoPaziente(false);
            richiamo.setVistoReportMedicoBase(false);
            richiamo.setVistoReportPaziente(false);
            
            
            Date inizio= new Date((oggi.getYear()-annoMax), 0, 1);
            Date fine= new Date((oggi.getYear()-annoMin), 11, 31);
            
            
            AbitantiDellaProvincia = persona_DAO.getBySSProvincialeId_Vive_E_NatoNelIntervallo(ssProvincia.getId(), inizio, fine);
            Iterator<Persona> iteratorPersone=AbitantiDellaProvincia.iterator();
            while(iteratorPersone.hasNext()){
                
                Persona cittadino = iteratorPersone.next();
                CartellaClinicaPerMB cartellaClinica = cartellaClinica_Dao.getByPazienteId_Attivo(cittadino.getCodF());
                if(cartellaClinica!=null){
                    richiamo.setId(null);
                    richiamo.setId_CartellaClinica(cartellaClinica.getId());
                    prescrizioneEsame_Dao.insert(richiamo);
                    if(richiamo.getId()!=null){
                        numRichiamiEffetuati++;
                        //INVIA EMAIL
                        if(numRichiamiEffetuati!=1){
                            emailLocal=emailLocal.concat(",");
                        }
                        emailLocal= emailLocal.concat(cittadino.getEmail());
                        //INVIA EMAIL
                    }
                }
            }
            
        }catch (DAOException ex) {
            request.getServletContext().log("Imposibile recuperare il medico di base.", ex);
            richiamo= null;
        }catch (Exception ex) {
            request.getServletContext().log("Imposibile recuperare convertitre l'id del esame in un intero.", ex);
            richiamo= null;
        }
        if(numRichiamiEffetuati<AbitantiDellaProvincia.size()){
            richiamo= null;
        }
        //INVIA EMAIL
        inviaEmailRichiami(request,richiamo,emailLocal,ssProvincia);
        //INVIA EMAIL
        return  richiamo;
        
    }
//==============================================================================
//                          INVIO EMAIL CONFERMA PRESCRIZIONE
//==============================================================================
     private void inviaEmailRichiami(HttpServletRequest request,PrescrizioneEsameEReferto prescrizioneEsame,String emailsCittadini,SSProvinciale ssProvinciale){
        String nomeSSP = ssProvinciale.getNome();
        String data = prescrizioneEsame.getDataPrescrizione().toString();
        String messaggio;
        String oggetto;
        try{
            messaggio=  "Buongiorno,\n"+
                        "Le inviamo questa e-mail per informarla che Le è stato prescritto un nuovo richiamo di un'esame "+
                        "dal servizzio sanitario provinciale di "+nomeSSP+", il giorno "+data+".\n"+
                        "Per maggiori informazzioni consultare il sito web nella propria area privata,"+
                        " nella sezione \"esami da fare\".\n"+
                        "Cordiali saluti,\n"+
                        "Progetto Universitario Servizio Sanitario Provinciale di "+nomeSSP+" ";
           
            oggetto =    "Aggiunta Richiamo dal S.S.Proviniale di "+nomeSSP+" -- Progetto Universitario Servizio Sanitario Nazionale --";

            Mail.sendMultipleEmail(messaggio,emailsCittadini, oggetto);
        }catch(Exception e){
            request.getServletContext().log("Imposibile inviare le email ai pazienti.", e);
        }
     }
//==============================================================================
}
