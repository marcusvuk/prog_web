
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%@page import="java.util.List"%>
<%@page import="it.tn.unitn.pw.progetto.SSN.DAO.common.exception.*"%>
<%@page import="it.tn.unitn.pw.progetto.SSN.DAO.common.factories.jdbc.*"%>
<%@page import="it.tn.unitn.pw.progetto.SSN.DAO.common.factory.*"%>
<%@page import="it.tn.unitn.pw.progetto.SSN.DAO.common.persistence.dao.jdbc.*"%>
<%@page import="it.tn.unitn.pw.progetto.SSN.DAO.persistence.*"%>
<%@page import="it.tn.unitn.pw.progetto.SSN.persistence.entities.*"%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
        <title>
            Informativa sulla Privacy 
        </title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" crossorigin="anonymous">
    
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/css/select2.min.css">
  
        <!-- Cittadino -->
        <link rel="stylesheet" href="${pageContext.request.contextPath}/modal/modal_Login/css/floating-labels.css">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/modal/modal_Login/css/forms.css">
        <!-- Cittadino -->
        <link rel="icon" href="${pageContext.request.contextPath}/sito_IMG/icona.ico" type="image/x-icon" />
        <link href="${pageContext.request.contextPath}/sito_CSS/style.css" rel="stylesheet" />
        <link href="${pageContext.request.contextPath}/sito_CSS/style_MenuUtente.css" rel="stylesheet" />
    </head>
    <body onload="showEsitoModale()">
        <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/js/select2.min.js"></script>
        <%-- <script src="modal_Login/js/js_Login.js"></script> --%>
    <!-- Navigation -->
        <%@include file="../pag_ElementiComunni/pag_MenuPrincipale.jsp" %>
        
        <%@include file="../modal/modal_CambiaFoto/modal_CambiaFoto.jsp" %> 
        <script src="${pageContext.request.contextPath}/pag_Cittadino/js/js_showoEsitoModale.js"></script>
        <div class="container">
            <div>
                <div class="row justify-content-center">
                    <div class="col-5">
                        <h2>
                            Informativa sulla Privacy                              
                        </h2>
                    </div>
                </div>
                <div class="container-fluid padding" style="min-height:300px;">
                    <div class="row padding">
                        <div class="col-lg-6">
                            <p>
                                Questa pagina descrive le modalità di gestione del sito 
                                <a title="Home"  href="${pageContext.request.contextPath}/Index">
                                    ServizioSanitarioNazionale
                                </a>
                                relativamente al trattamento dei dati personali degli utenti 
                                che lo consultano.<br>
                                È un'informativa resa ai sensi dell’articolo 13 del regolamento 
                                UE/2016/679 del Parlamento europeo e del Consiglio del 27 aprile 
                                2016, relativo alla protezione delle persone fisiche con riguardo 
                                al trattamento dei dati personali, nonché alla libera circolazione 
                                di tali dati a coloro che interagiscono con i servizi web del sito
                                per la protezione dei dati personali.<br>
                                L'informativa si ispira anche alla Raccomandazione n. 2/2001
                                che le autorità europee per la protezione dei dati personali,
                                riunite nel Gruppo istituito dall'art. 29 della Direttiva n. 95/46/CE ,
                                hanno adottato il 17 maggio 2001 per individuare alcuni requisiti 
                                minimi per la raccolta di dati personali on-line, e, in particolare, 
                                le modalità, i tempi e la natura delle informazioni che i titolari del 
                                trattamento devono fornire agli utenti quando questi si collegano 
                                a pagine web, indipendentemente dagli scopi del collegamento.<br>
                            </p>
                            <br/>
                            <h4 class="text-center ">
                                Tipi di dati trattati
                            </h4>
                            <h5 class="text-center ">
                                Dati di navigazione
                            </h5>
                            <p>
                                I sistemi informatici e le procedure software preposte al funzionamento 
                                di questo sito web acquisiscono, nel corso del loro normale esercizio, 
                                alcuni dati personali la cui trasmissione è implicita nell'uso 
                                dei protocolli di comunicazione di Internet. Si tratta di informazioni 
                                che non sono raccolte per essere associate a interessati identificati,
                                ma che per loro stessa natura potrebbero, attraverso elaborazioni ed 
                                associazioni con dati detenuti da terzi, permettere di identificare 
                                gli utenti. In questa categoria di dati rientrano gli indirizzi IP o 
                                i nomi a dominio dei computer utilizzati dagli utenti che si connettono 
                                al sito, gli indirizzi in notazione URI (Uniform Resource Identifier) 
                                delle risorse richieste, l'orario della richiesta, il metodo utilizzato 
                                nel sottoporre la richiesta al server, la dimensione del file ottenuto 
                                in risposta, il codice numerico indicante lo stato della risposta data 
                                dal server (buon fine, errore, ecc.) ed altri parametri relativi al 
                                sistema operativo e all'ambiente informatico dell'utente. Questi dati 
                                vengono utilizzati al solo fine di ricavare informazioni statistiche 
                                anonime sull'uso del sito e per controllarne il corretto funzionamento 
                                e vengono cancellati immediatamente dopo l'elaborazione. I dati 
                                potrebbero essere utilizzati per l'accertamento di responsabilità 
                                in caso di ipotetici reati informatici ai danni del sito.
                            </p>
                            <br>
                            <h5 class="text-center ">
                                Dati forniti volontariamente dall'utente
                            </h5>
                            <p>
                                L'invio facoltativo, esplicito e volontario di posta elettronica 
                                agli indirizzi indicati su questo sito comporta la successiva 
                                acquisizione dell'indirizzo del mittente, necessario per rispondere 
                                alle richieste, nonché degli eventuali altri dati personali inseriti 
                                nella missiva.
                            </p>
                            <br>
                            <h5 class="text-center ">
                                Modalità del trattamento
                            </h5>
                            <p>
                                I dati personali sono trattati con strumenti automatizzati per 
                                il tempo strettamente necessario a conseguire gli scopi per cui 
                                sono stati raccolti. Specifiche misure di sicurezza sono osservate 
                                per prevenire la perdita dei dati, usi illeciti o non corretti ed 
                                accessi non autorizzati.
                            </p>
                        </div>
                        <div class="col-lg-5 ">
                            <picture>
                                <source media="(min-width: 992px)" srcset="sito_IMG/S_privacy.jpg">
                                <img src="sito_IMG/L_privacy.jpg" style="width:100%" class="img-fluid rounded img_SeguiArticolo" />
                            </picture>
                        </div>
                    </div>
                </div>
                <hr class="my-4" />
            </div>
        </div>
        <%@include file="../pag_ElementiComunni/pag_Footer.jsp" %>
    </body>
</html>
