$(function () {
    $("#EsamiVisiteTable").dataTable( {
        ajax: {
            url: '/SitoServizioSanitarioNazionale/services/EsamiPaziente/Fatte/'+
                    $('#MyIdPaziente').attr('value')+
                    '/PerMedicoBase/'+$('#MyIdMedicoBase').attr('value'),
            dataSrc: 'results'
        },
        columns: [
            { results: 0,//results: Corrispnde a data
              targets: 0, 
              render: function ( data, type, row, meta ) {
                    if(type === 'display'){
                        if(data === 'true'){
                            //Vechio utente
                            data ='<i class="far fa-eye" style="font-size:36px"></i>';
                        }else{
                            //Nuovo utente
                            data ='<i class="fas fa-eye-slash  text-primary" style="font-size:36px"></i>';
                        }
                    }
                    return data;
                }
            },
            { results: 1 },
            { results: 2 },
            { results: 3 },
            { results: 4 },
            { results: 5,//results: Corrispnde a data
              targets: -1, 
              render: function ( data, type, row, meta ) {
                    if(type === 'display'){
                        data = '<a href="Esami?idEsami=' + encodeURIComponent(data) + '">'+
                               '<i class="fas fa-notes-medical" style="font-size:36px" ></i>'+
                               '</a>';
                    }
                    return data;
                }
            },
        ]
    });

});