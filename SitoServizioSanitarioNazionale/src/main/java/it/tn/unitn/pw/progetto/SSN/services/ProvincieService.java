/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.tn.unitn.pw.progetto.SSN.services;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Produces;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;
import javax.ws.rs.core.MediaType;

import com.google.gson.Gson;
import it.tn.unitn.pw.progetto.SSN.DAO.common.exception.DAOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;

import it.tn.unitn.pw.progetto.SSN.DAO.persistence.SSProvinciale_DAO;
import it.tn.unitn.pw.progetto.SSN.DAO.common.exception.DAOFactoryException;
import it.tn.unitn.pw.progetto.SSN.DAO.common.factory.DAOFactory;
import it.tn.unitn.pw.progetto.SSN.persistence.entities.SSProvinciale;
import java.sql.SQLException;
import java.util.Iterator;
import java.util.logging.Logger;
import sun.tools.tree.ThisExpression;
/**
 * REST Web Service
 *
 * @author Brian
 */
@Path("provincie")
public class ProvincieService{
    
    private static SSProvinciale_DAO   ssProvinciale_DAO;
    private static DAOFactory          daoFactory;

    static public void init(DAOFactory daoFactory1) {
        daoFactory =  daoFactory1;
    }
    
    @Context
    private UriInfo context;

    /**
     * Creates a new instance of ProvincieService
     */
    public ProvincieService() {
    }

    /**
     * Retrieves representation of an instance of it.tn.unitn.pw.progetto.SSN.services.ProvincieService
     * @return an instance of java.lang.String
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public String getProvinie(@QueryParam("term") String queryTerm) {
        List<Provincie> listaProvincieForJson = new ArrayList<>();
        
        List<Provincie> results = new ArrayList<>();
        
        try {
            ssProvinciale_DAO   = daoFactory.getDAO(SSProvinciale_DAO.class);
            List<SSProvinciale> listaProvincie = ssProvinciale_DAO.getAll();
            for(int i=0; i<listaProvincie.size(); i++){
                listaProvincieForJson.add( new Provincie(listaProvincie.get(i).getNome() , listaProvincie.get(i).getNome() ) );
            }
        } catch (DAOFactoryException ex) {
            Logger.getLogger(getClass().getName()).severe(ex.toString());
        } catch (DAOException ex) {
            Logger.getLogger(getClass().getName()).severe(ex.toString());
        }
        
        
        if (queryTerm == null) {
            results.addAll(listaProvincieForJson);
        } else {
            Iterator<Provincie> provincieIterator = listaProvincieForJson.iterator();
            while (provincieIterator.hasNext()) {
                Provincie element = provincieIterator.next();
                if(element.getText().toLowerCase().startsWith(queryTerm.toLowerCase())) {
                    results.add(element);
                }
            }
        }
        
        
        Gson gson = new Gson();
        return gson.toJson(new Results(results.toArray(new Provincie[0])));

    }
//##############################################################################
    public static class Provincie implements Serializable {

        private String id;
        private String text;

        public Provincie(String id, String text) {
            this.id = id;
            this.text = text;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getText() {
            return text;
        }

        public void setText(String text) {
            this.text = text;
        }
    }
//##############################################################################
    public static class Results implements Serializable {

        private Provincie[] results;

        public Results(Provincie[] results) {
            this.results = results;
        }

        public Provincie[] getResults() {
            return results;
        }

        public void setResults(Provincie[] results) {
            this.results = results;
        }
    }
}
