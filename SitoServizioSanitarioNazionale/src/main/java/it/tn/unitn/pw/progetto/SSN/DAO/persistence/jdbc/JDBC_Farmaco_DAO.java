/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.tn.unitn.pw.progetto.SSN.DAO.persistence.jdbc;

import java.sql.Date;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import it.tn.unitn.pw.progetto.SSN.DAO.common.persistence.dao.jdbc.JDBCDAO;
import it.tn.unitn.pw.progetto.SSN.DAO.common.exception.*;
import it.tn.unitn.pw.progetto.SSN.DAO.persistence.*;
import it.tn.unitn.pw.progetto.SSN.persistence.entities.*;
/**
 *
 * @author Brian
 */
public class JDBC_Farmaco_DAO extends JDBCDAO<Farmaco, Integer> implements Farmaco_DAO {
/*============================================================================*/    
/*                                      METODI COSTRUTTORI                    */
/*============================================================================*/
    public JDBC_Farmaco_DAO(Connection con) {
        super(con);
    }
/*============================================================================*/
    
/*============================================================================*/    
/*                      METODI GETTER DAO IMPLEMENTATION                      */
/*============================================================================*/
    @Override
    public Long getCount() throws DAOException {
        try (Statement stmt = CON.createStatement()) {
            ResultSet counter = stmt.executeQuery( 
                     "  SELECT COUNT(*) "               +
                     "  FROM Farmaco "     );
            if (counter.next()) {
                return counter.getLong(1);
            }
        } catch (SQLException ex) {
            throw new DAOException( " Immpossibile contare gli elementi di Farmaco  " , ex);
        }
        return 0L;
    }
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    @Override
    public Farmaco getByPrimaryKey(Integer primaryKey) throws DAOException {
        if (primaryKey == null) {
            throw new DAOException( " primaryKey è null " );
        }
        try (PreparedStatement stm = CON.prepareStatement(
                                     "  SELECT *  "         +
                                     "  FROM Farmaco  "     +
                                     "  WHERE id = ? "      )) {
            // imposto i valori
            stm.setInt(1, primaryKey);
            
            try (ResultSet rs = stm.executeQuery()) {
                rs.next();
                Farmaco farmaco = new Farmaco();
                farmaco.setId               (rs.getInt      ( "id" ));
                farmaco.setNome             (rs.getString   ( "nome" ));
                farmaco.setDescrizione      (rs.getString   ( "descrizione" ));
                        
                return farmaco;
            }
        } catch (SQLException ex) {
            throw new DAOException( " Impossibile perendere la Farmaco indicato dalla primary key " , ex);
        }
    }
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    @Override
    public List<Farmaco> getAll() throws DAOException {
        List<Farmaco> listaFarmaco = new ArrayList<>();

        try (Statement stm = CON.createStatement()) {
            try (ResultSet rs = stm.executeQuery(
                                 " SELECT *  "          +
                                 " FROM Farmaco  "      )){
                
                while (rs.next()) {
                    Farmaco farmaco = new Farmaco();
                    
                    farmaco.setId               (rs.getInt      ( "id" ));
                    farmaco.setNome             (rs.getString   ( "nome" ));
                    farmaco.setDescrizione      (rs.getString   ( "descrizione" ));
                    
                    listaFarmaco.add(farmaco);
                }
            }
        } catch (SQLException ex) {
            throw new DAOException( " Imposibile estrarre tutta la lista di Farmaco  " , ex);
        }

        return listaFarmaco;
    }
/*============================================================================*/
   
/*============================================================================*/    
/*                      METODI QUERY                                          */
/*============================================================================*/
//------------------------------------------------------------------------------
//                      INSERT
//------------------------------------------------------------------------------
    @Override
    public Integer insert(Farmaco farmaco) throws DAOException {
        if (farmaco == null ) {
            throw new DAOException( " Farmaco  è null " );
        }
        try (PreparedStatement ps = CON.prepareStatement(
                     " INSERT INTO Farmaco "  +
                                 "  nome  "          +    "  ,  "    +
                                 "  descrizione  "   +    "  )  "    +  
                     " VALUES (?,?,?) " , Statement.RETURN_GENERATED_KEYS)) {
            
            // imposto i valori
            ps.setString(1, farmaco.getNome());
            ps.setString(2, farmaco.getDescrizione());
      
          
            ps.executeUpdate();
            
            ResultSet rs = ps.getGeneratedKeys();
            if (rs.next()) {
                farmaco.setId(rs.getInt(1));
            }
            
            return farmaco.getId();
        } catch (SQLException ex) {
            throw new DAOException( " Impossibile aggiungere un nuovo record di Farmaco " , ex);
        }
    }
/*============================================================================*/
}
