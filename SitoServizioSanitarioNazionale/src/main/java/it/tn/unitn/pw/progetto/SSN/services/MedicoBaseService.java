/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.tn.unitn.pw.progetto.SSN.services;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Produces;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;
import javax.ws.rs.core.MediaType;

import com.google.gson.Gson;
import it.tn.unitn.pw.progetto.SSN.DAO.common.exception.DAOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;

import it.tn.unitn.pw.progetto.SSN.DAO.persistence.*;
import it.tn.unitn.pw.progetto.SSN.DAO.common.exception.DAOFactoryException;
import it.tn.unitn.pw.progetto.SSN.DAO.common.factory.DAOFactory;
import it.tn.unitn.pw.progetto.SSN.persistence.entities.*;
import java.sql.SQLException;
import java.util.Iterator;
import java.util.logging.Logger;
import sun.tools.tree.ThisExpression;

import java.io.Serializable;

/**
 * REST Web Service
 *
 * @author Brian
 */
@Path("MedicoBase")
public class MedicoBaseService {
    
    private static MedicoBase_DAO   medicoBase_DAO;
    private static Persona_DAO      persona_DAO;
    private static DAOFactory daoFactory;

    static public void init(DAOFactory daoFactory1) {
        daoFactory =  daoFactory1;
    }
    
    @Context
    private UriInfo context;

    /**
     * Creates a new instance of MedicoBaseService
     */
    public MedicoBaseService() {
    }

    /**
     * Retrieves representation of an instance of it.tn.unitn.pw.progetto.SSN.services.MedicoBaseService
     * @return an instance of java.lang.String
     */
    @GET
    @Path("ForCittadino/{cittadinoId}/{term}")
    public String getMedicoBase(@PathParam("cittadinoId") String cittadinoId,@QueryParam("term") String term){
        List<Medico> listaMedicoForJson = new ArrayList<>();
        
        List<Medico> results = new ArrayList<>();
        
        
        
        try {
            medicoBase_DAO   = daoFactory.getDAO(MedicoBase_DAO.class);
            persona_DAO      = daoFactory.getDAO(Persona_DAO.class);
           
            Persona persona= persona_DAO.getByPrimaryKey(cittadinoId);
            
            List<MedicoBase> listaMedico = medicoBase_DAO.getBySSProvincialeId_Lavora(persona.getVive_id_SSP());
            for(int i=0; i<listaMedico.size(); i++){
                listaMedicoForJson.add( new Medico(listaMedico.get(i).getCodF(), listaMedico.get(i).getCognome()+" "+listaMedico.get(i).getNome() ) );
            }
        } catch (DAOFactoryException ex) {
            Logger.getLogger(getClass().getName()).severe(ex.toString());
        } catch (DAOException ex) {
            Logger.getLogger(getClass().getName()).severe(ex.toString());
        }catch(NumberFormatException ex){
             Logger.getLogger(getClass().getName()).severe(ex.toString());
        }
        
        
        if ((term == null) || "undefined".equals(term)) {
            results.addAll(listaMedicoForJson);
        } else {
            Iterator<Medico> provincieIterator = listaMedicoForJson.iterator();
            while (provincieIterator.hasNext()) {
                Medico element = provincieIterator.next();
                if(element.getText().toLowerCase().startsWith(term.toLowerCase())) {
                    results.add(element);
                }
            }
        }
        
        
        Gson gson = new Gson();
        return gson.toJson(new Results(results.toArray(new Medico[0])));
    }
    
    
//##############################################################################
    public static class Medico implements Serializable {

        private String id;
        private String text;

        public Medico(String id, String text) {
            this.id = id;
            this.text = text;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getText() {
            return text;
        }

        public void setText(String text) {
            this.text = text;
        }
    }
//##############################################################################
    public static class Results implements Serializable {

        private Medico[] results;

        public Results(Medico[] results) {
            this.results = results;
        }

        public Medico[] getResults() {
            return results;
        }

        public void setResults(Medico[] results) {
            this.results = results;
        }
    }
}
