/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.tn.unitn.pw.progetto.SSN.services;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Produces;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;
import javax.ws.rs.core.MediaType;

import com.google.gson.Gson;
import it.tn.unitn.pw.progetto.SSN.DAO.common.exception.DAOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;

import it.tn.unitn.pw.progetto.SSN.DAO.persistence.*;
import it.tn.unitn.pw.progetto.SSN.DAO.common.exception.DAOFactoryException;
import it.tn.unitn.pw.progetto.SSN.DAO.common.factory.DAOFactory;
import it.tn.unitn.pw.progetto.SSN.persistence.entities.*;
import java.sql.SQLException;
import java.util.Iterator;
import java.util.logging.Logger;
import sun.tools.tree.ThisExpression;

/**
 * REST Web Service
 *
 * @author Brian
 */
@Path("farmaci")
public class FarmaciService {
    private static MedicoBase_DAO   medicoBase_DAO;
    private static Farmaco_DAO      farmaco_DAO;
    
    private static DAOFactory          daoFactory;

    static public void init(DAOFactory daoFactory1) {
        daoFactory =  daoFactory1;
    }
    
    @Context
    private UriInfo context;

    /**
     * Creates a new instance of FarmaciService
     */
    public FarmaciService() {
    }

    @GET
    @Path("MedicoBase/{medicoBaseCodF}/{term}")
    public String getFarmaci(@PathParam("medicoBaseCodF") String medicoBaseCodF,@PathParam("term") String term){
        List<FarmacoScelta> listaFarmaciForJson = new ArrayList<>();
        
        List<FarmacoScelta> results = new ArrayList<>();
 
        try {
            farmaco_DAO             = daoFactory.getDAO(Farmaco_DAO.class);
            medicoBase_DAO          = daoFactory.getDAO(MedicoBase_DAO.class);
            
            if(medicoBase_DAO.getByPrimaryKey(medicoBaseCodF)!= null){
            
                List<Farmaco> listaFarmaci = farmaco_DAO.getAll();
                Iterator<Farmaco> iterFarmaci= listaFarmaci.iterator();
                while(iterFarmaci.hasNext()){
                    Farmaco farmaco= iterFarmaci.next();

                    listaFarmaciForJson.add(
                            new FarmacoScelta( farmaco.getId(),farmaco.getNome() )
                                        );

                }
            }
        } catch (DAOFactoryException ex) {
            Logger.getLogger(getClass().getName()).severe(ex.toString());
        } catch (DAOException ex) {
            Logger.getLogger(getClass().getName()).severe(ex.toString());
        }catch(NumberFormatException ex){
             Logger.getLogger(getClass().getName()).severe(ex.toString());
        }
        
        
        if ((term == null) || "undefined".equals(term)) {
            results.addAll(listaFarmaciForJson);
        } else {
            Iterator<FarmacoScelta> farmacoIterator = listaFarmaciForJson.iterator();
            while (farmacoIterator.hasNext()) {
                FarmacoScelta element = farmacoIterator.next();
                if(element.getText().toLowerCase().contains(term.toLowerCase())) {
                    results.add(element);
                }
            }
        }
        Gson gson = new Gson();
        return gson.toJson(new Results(results.toArray(new FarmacoScelta[0])));
    }
//##############################################################################
    public static class FarmacoScelta implements Serializable {

        private Integer id;
        private String text;

        public FarmacoScelta(Integer id, String text) {
            this.id = id;
            this.text = text;
        }

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getText() {
            return text;
        }

        public void setText(String text) {
            this.text = text;
        }
    }
//##############################################################################
    public static class Results implements Serializable {

        private FarmacoScelta[] results;

        public Results(FarmacoScelta[] results) {
            this.results = results;
        }

        public FarmacoScelta[] getResults() {
            return results;
        }

        public void setResults(FarmacoScelta[] results) {
            this.results = results;
        }
    }
}