<%--<table> � nella pagina in cui si importa--%>    
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<thead class="thead-light">
    <tr>
        <th>Foto</th>
        <c:if test="${b_Persona ne null}">
            <th>
                <c:if test="${b_NumNuoviPazienti ne 0}">
                    <span class="badge badge-primary badge-pill"><c:out value="${b_NumNuoviPazienti}"/></span>
                </c:if>
            </th>
        </c:if>
        <th>Nome</th>
        <th>Cognome</th>
        <th>Nato il</th>
        <th>Codice fiscale</th>
        <th>Data ultima prescrizione</th>    
        <th></th>
    </tr>
</thead>
<tbody class="align-middle">
</tbody>
<tfoot class="thead-light">
    <tr>
        <th>Foto</th>
        <c:if test="${b_Persona ne null}">
            <th></th>
        </c:if>
        <th>Nome</th>
        <th>Cognome</th>
        <th>Nato il</th>
        <th>Codice fiscale</th>
        <th>Data ultima prescrizione</th>
        <th></th>
    </tr>
</tfoot>
<%--</table>--%>
