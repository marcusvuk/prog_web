INSERT INTO SSPROVINCIALE(nome, password, salt) VALUES ('Agrigento', 'dfca7a76673bce1b7efba934d1b84c6e513240bf1305c11f3579903f15ee5774', '2e6ef8df286d5a68189041113bb741a7c8c72c934c8dde45a4ebe35b9df17df2');
INSERT INTO SSPROVINCIALE(nome, password, salt) VALUES ('Alessandria', 'd14b2c0b04f81cf1fe9e90b24dc2d993278fb870bac4157b4cea103ffde1e2d1', '0f337c77e5964965831de9c8999a27951eb583ba67d6dcaad5bdc89ff2fb5800');
INSERT INTO SSPROVINCIALE(nome, password, salt) VALUES ('Ancona', 'a80df6ba8d681cedb85203f3c2ca0c1bc476a6cdbf345cb8858bdd0ee90e1528', '5c1be301a793d059013da74a5457551a43e41d1cb71037ce295838d939a5f79c');
INSERT INTO SSPROVINCIALE(nome, password, salt) VALUES ('Aosta', '2edc76b83e81078040c71d120fc62af6492149dd2528aa395635a4a4ad77764f', '617fca21c69d8888c8717bbd62e68a4f1c9a3baa8228210f2cbd134ada1b459d');
INSERT INTO SSPROVINCIALE(nome, password, salt) VALUES ('Arezzo', 'baab1b24038685152871846b8e48e7906f2a77cc169798b8d8e712b32b29ae61', '502b0c4e839c3b661d22060ad918b78b59e7a394a5fb55d298f74431fa410f06');
INSERT INTO SSPROVINCIALE(nome, password, salt) VALUES ('Ascoli Piceno', 'd82535b8a72b8fabf267e57c2b8222ccbeddff7908059821363554664099bdb0', '54c8af4fb5a09bace1ffa2594737f08e24b0a0e84a1607df8914170261f282e9');
INSERT INTO SSPROVINCIALE(nome, password, salt) VALUES ('Asti', '876060532309a8210362adf56ce983941439afd26487a0fcbf7012671f4c4f6d', 'ce808082fb2b1340322d594401a9d23a28454687364c9f01ab45a27af6b79190');
INSERT INTO SSPROVINCIALE(nome, password, salt) VALUES ('Avellino', '2e50a3d9bfd777a93466dba189fb6281224a6593296210f72ca212073ee7d2b7', 'b89acc3064919959601bb1314127baab5918b5ce6f7448d676935810211b62c9');
INSERT INTO SSPROVINCIALE(nome, password, salt) VALUES ('Bari', 'aef3f26e24169b96707a0a235c0119c1d82732db07e7e3a7f8699ff6e21528bc', '505d9817689b6440a67764d78b45f00064f23771a11afa18a69390b6225868d1');
INSERT INTO SSPROVINCIALE(nome, password, salt) VALUES ('Barletta-Andria-Trani', '47a2a044d2b9da786c4adff59dfd702e760fba1b98002a25b80b90dd742229fa', '3fe06c6b52ca3b909f687b5f12c0221018ab4609e4c60ee7054dc887253d4a4e');
INSERT INTO SSPROVINCIALE(nome, password, salt) VALUES ('Belluno', '99be9b198728f3c5918f86f52a675dd39303f1d3326b2c6542ec62e51bf99b52', '717ce8660a4ae2bfc1b170741f43f817440d7e5ff597bc0d210a8a60841cf3fe');
INSERT INTO SSPROVINCIALE(nome, password, salt) VALUES ('Benevento', 'd85e71975b1ae625c3d4740e9b8a84afe252b34e4c395ecd2adc89b7804b201e', '650b0e4e229ed6ae8e62d23bd5e3fc67916c78d86eaff638befebc65be0356b7');
INSERT INTO SSPROVINCIALE(nome, password, salt) VALUES ('Bergamo', '675c3b5fe013db915377f9f43cfe55dd52b429fc778dd6cc655441611ed26578', '00e3e049b5134a956777c65c3c105a0f12e1bafe8f6b0eab6796ad737df84e68');
INSERT INTO SSPROVINCIALE(nome, password, salt) VALUES ('Biella', '99006255494856009201b89bf2339726ce6230154800948717744bda34c38d93', '18076866fd253af1fece4a4c86868be9041125327b166c4d99aadc2dcc8ac75b');
INSERT INTO SSPROVINCIALE(nome, password, salt) VALUES ('Bologna', '365dc6b609722c92ef666145d3e64fa2bd65991859746bed7c5417ab7c86c1de', 'a8fd3b388acd2384e61f4450baa5ba212b6db9a6f259c0783d6d35951b576c1c');
INSERT INTO SSPROVINCIALE(nome, password, salt) VALUES ('Bolzano', 'c92b17c375fc9f49c2c6b4ceaecd8a9e365123f71b5f403a552b86b6e18f9c06', '69ec2f3273e447332307d8ca3ada43006772126bba4e099c5994a64ec0ab05eb');
INSERT INTO SSPROVINCIALE(nome, password, salt) VALUES ('Brescia', 'e8d412b824d094633287093ba21f4f0e5e713b82264906e1f75602e3e667ce70', 'c3c4ec636956994e502cfa60addd486290093ef42b55b13db3acd28e77a70b35');
INSERT INTO SSPROVINCIALE(nome, password, salt) VALUES ('Brindisi', '79177eae9d2326cb92cad49be2e94dede5988509c0abf3bf812261c89f87dc94', '1fd5db9de77935a7ff05184d6500baa95fc19e458af8c518744034bf18e369fb');
INSERT INTO SSPROVINCIALE(nome, password, salt) VALUES ('Cagliari', 'ad76037df8706c8d87409a10f923f3926ecfd5200e6f0258e9d09a6a8d825562', '081bdef750ea63d935ca9aad0b7fc7d90aa79da1469cedbae0643576d0de2266');
INSERT INTO SSPROVINCIALE(nome, password, salt) VALUES ('Caltanissetta', '18715e4ddbc50cfc4a1b440cf65fa04cd65c3bbc6843898ad7d9c81adb4028f6', '1aa969eca7bc61c779214e4490b6a19275cebfc1b298a777cad50a166882936f');
INSERT INTO SSPROVINCIALE(nome, password, salt) VALUES ('Campobasso', '2b6b284e176cc4834a4d3ec248e31ed710d6301785e124d93695be7f21ed58bd', '0df98ee611847263338e8f79be5468382f24e7d3e6471a4e99e8d84bb15438e2');
INSERT INTO SSPROVINCIALE(nome, password, salt) VALUES ('Carbonia-Iglesias', 'ffefa0bd3ff87daacb86bae229fec25c773d8b463f09f22a40dbf506e6d4dffd', '7efcff57813781e5f0157391253746f6ea220aa43d766333ba99ce39edfc1678');
INSERT INTO SSPROVINCIALE(nome, password, salt) VALUES ('Caserta', '0374234a18f6be06d3dfb308034775269f1b60dc4309a19fe2d5dabc926b6271', 'd04d998fe3ca930b07cd6cbb8bf537f7b8c5372e26ed9545567dd8a8112b7563');
INSERT INTO SSPROVINCIALE(nome, password, salt) VALUES ('Catania', '304053f2a3229e6653d9ecd2201a3a5bdd7016efee494b923a4a1290abbc0443', '0b9e997e36a2dfc6d4dc3e71e593f20b47ffaa2e8b87f7e551924b793c427212');
INSERT INTO SSPROVINCIALE(nome, password, salt) VALUES ('Catanzaro', '2baf866a221074c3d34569ff403e9be623310db58d3ff89b48c0c7151629c16d', '9a4c71658229ef42a93cde74279b4ff93a744cf7d74f9ab9746fde1d6f35e47a');
INSERT INTO SSPROVINCIALE(nome, password, salt) VALUES ('Chieti', '6469538c8c106096be433ef048b7640cdcd36c75547d76f4662271b0f6c7061b', 'bdbdb6831c0b0d04760b6e2c0b3ee56d98088426eb69495b551a23fd2d450bf6');
INSERT INTO SSPROVINCIALE(nome, password, salt) VALUES ('Como', 'ce1985523ab87013be6cee84929cee3cf3406e931f8846ef9bfbe9f74dfa515c', '45a4a47e227ba8e093619d250c5868ca3ddb618d83248e9bf6c5c7aa50b622da');
INSERT INTO SSPROVINCIALE(nome, password, salt) VALUES ('Cosenza', '2b7a1847d8b168d9f881deb1524b2633f866c56ca27f82ace98399f722ae25c6', 'a4fa0d83a2b07df9afc6f0bf160d5cc98c2d16a46b713efc40d192d4d2e0a15e');
INSERT INTO SSPROVINCIALE(nome, password, salt) VALUES ('Cremona', '9b9885377e8be811957d274f0fa0c54a569973a1572f398be54644f6f6d8fc96', '7745ac67fb3cac87bfb89e2477160e4acd3a5f568db3560511eca025831f0131');
INSERT INTO SSPROVINCIALE(nome, password, salt) VALUES ('Crotone', 'b18b32545a1a5503ed483f743336bbc6d4a2d42611cce5724ce480f2c72a8c64', '31cb78fe5b2176d7f6f3902bfd9b6271e69e555a10fa9dd94e93af47a38cce82');
INSERT INTO SSPROVINCIALE(nome, password, salt) VALUES ('Cuneo', '41375a9ddd9961af785170bb2f2dbccd39cceb6780d007c2d24f1ce1bd26e9e8', '883f1248924f6e974f99cd20d10086f7995f0a0aaf4742be7078d1224233b9d5');
INSERT INTO SSPROVINCIALE(nome, password, salt) VALUES ('Enna', '0e9f75a3f8ce5a32869d2fd9a1f365cb9a31d1ec97ae4ee897efd102550c800c', 'c1b801489174e3f55d6f834f65f3be64047c93d9532a001d7eadafaedae30c1e');
INSERT INTO SSPROVINCIALE(nome, password, salt) VALUES ('Fermo', '74ac7b1a3064fdefbdaab45a5566bc4a32df9f05befda893d26c72c39eacf632', 'ed1b183818018f93597f9926ed03291b3388f9b9539c439ed4be0c80a2f96410');
INSERT INTO SSPROVINCIALE(nome, password, salt) VALUES ('Ferrara', '9bdef8ce438c22b87a5e1c9f58127e19d2c7c8a16c0a5bc22f2f42c1b56b58af', '3e62353eaf91e2461bdbc8f6093c7543837d4adc2e0c442be9a3dc5fe171698c');
INSERT INTO SSPROVINCIALE(nome, password, salt) VALUES ('Firenze', 'f7dc6026ecde6d26eecfab53a8368618e4354226d9a844842920fc5c57536e70', 'dd2b8193eb63cbf87d295ae94787fd73b7aa0b3c0be5f1eae90779755a3814c8');
INSERT INTO SSPROVINCIALE(nome, password, salt) VALUES ('Foggia', 'f02e9502d12c2d9e5419244f78f3b9cb6564bd2f35f74a0cfd6d474e41a55194', '60a4b78a30caeb8b0f12f527e889391f34d161d69a9b89378295be0d8a075152');
INSERT INTO SSPROVINCIALE(nome, password, salt) VALUES ('Forlì-Cesena', '986b2fb0c6a79ccfaa10144feef9c3b926f062fcaeaacbdb11b8973d57af25f5', '2d4312a27d8ecaf1978a3efdd31b4387fa47d5e4daeaa718bc8b4bec73b44b88');
INSERT INTO SSPROVINCIALE(nome, password, salt) VALUES ('Frosinone', '04c27588a4132dff1c42f5a8632612cf0db0afd4223f66c6cc9ee746af7c4881', '064f386c257d60f7c98ea9e6339cd4f0b9299a3682838d2b806bd96a998531f5');
INSERT INTO SSPROVINCIALE(nome, password, salt) VALUES ('Genova', 'bced23ef7afc26084c9288158abd6824a6f34aad002e2d02662858413e78f884', '74107d9a54151483cf5ae30bf7a2d292fcb66b76865892fd7c02f6dc35e476ce');
INSERT INTO SSPROVINCIALE(nome, password, salt) VALUES ('Gorizia', '63166137484298626553c2bf7b13e042bc17d3284bc667722f29b8b61dfeff19', '13fdb746791406f90ea19b0c293739bba31b975053ee450b0313e67336872aa5');
INSERT INTO SSPROVINCIALE(nome, password, salt) VALUES ('Grosseto', 'ceb184bb2eabf6bb22c7ccc6d7f0373b92bdcd4db3fda4211f0ed964d75b06b9', '347deb9a4d972fc3923d0f7d38b09f6b6e9b360796be3c8a1f0f98c7cd0db965');
INSERT INTO SSPROVINCIALE(nome, password, salt) VALUES ('Imperia', '0aad7e500ae15900860471fd23c449b5705a8c2f1b54bde933255452131b50bd', 'b75857caa4dbf132e606e70fd205623f3befcdd9f949bff0dfe4529bba9d4cd6');
INSERT INTO SSPROVINCIALE(nome, password, salt) VALUES ('Isernia', '9b3c02330d380c1670863488a3d8520da80ac0f6f1f8ba0b24edd9de9cce6d1c', '6bf6478f4a7f30cc996d37aac889d38bbca70d32f59f816513ec43ab61d1ba85');
INSERT INTO SSPROVINCIALE(nome, password, salt) VALUES ('La Spezia', 'ddc7bfdead4320fec7dc79811aae7d8e638fae83e6f4a3638ffd5d143d1df001', 'bc02d682c97cdeeb190d5494151cbe347ec72e4133f0e181da73422b9666af0a');
INSERT INTO SSPROVINCIALE(nome, password, salt) VALUES ('Aquila', 'bfaf77abcba47992915b5a440bb68d162014d4ed01b71784efb927fa14f5ae8c', 'bd69880c988969c379291be06dfbd8bd9768ed6a48dcb0a235329820a44e6832');
INSERT INTO SSPROVINCIALE(nome, password, salt) VALUES ('Latina', '74f8cf0367c0a93fc51d84397f96eeb745598948d9fdb64d024fcb0bc7a72f50', 'bc50af2b92a2ad25d935ebca4f94f8d3a58e60d1704745cf7c12537192bbc4c1');
INSERT INTO SSPROVINCIALE(nome, password, salt) VALUES ('Lecce', '55f32691e7778024af866feb3165ba6eb62de4a1d05b56a49f9fec4c28c82444', 'b52c6ec33970bef94e06a0f5e0b9e70754b75313a0d0ab17b491e6e55de33201');
INSERT INTO SSPROVINCIALE(nome, password, salt) VALUES ('Lecco', '81d45b7ac44dfaddca428c0aa2401bec27c1d51b4a723a011ade59aa148a3a86', 'e5d479c7643635f7098a9490e5612fa63f3d905ea4fdf505819337f481ea7402');
INSERT INTO SSPROVINCIALE(nome, password, salt) VALUES ('Livorno', 'e1a278e7deea1f808c7428d918999c36f3e0b97bf26634f1cf0761754bea9bfe', '8f6826d0e44120c96ea91928c68461492efdbc0ddcc78c7c36a553af7876c7af');
INSERT INTO SSPROVINCIALE(nome, password, salt) VALUES ('Lodi', 'a57c341bd58abd844919b31aca115bb4ee5488c21bb4b1d15f9a63540d259d93', '3f94449ad31bf64ddd533e6a720f52734dad8d2124dd68052e1017e2e08e65e7');
INSERT INTO SSPROVINCIALE(nome, password, salt) VALUES ('Lucca', '11c45903442a1db82d58571cd769cba684e495f9c768ffe1b2f1cff0a5790e77', 'e42bd9587bff4e88f760ac780a2e835fc28e851753d4ca8402965c1bcc060b10');
INSERT INTO SSPROVINCIALE(nome, password, salt) VALUES ('Macerata', '88e4e9feadfd37921321fecc70634235fec57de86b60a51524342888d54df59d', 'da2de74ee2649bf770b9dba37e425da06d2b13b17f476b9dec092225231dbc12');
INSERT INTO SSPROVINCIALE(nome, password, salt) VALUES ('Mantova', 'b1a7a23bae43bbfde95f105615b7b4b0a767e94ed6dafa8332f20e70f9bd6f5e', '8a987cd4816dfd65179f8abb7526febb76b53647c2e9ac8067ad65cc74829c38');
INSERT INTO SSPROVINCIALE(nome, password, salt) VALUES ('Massa-Carrara', 'afbcf00d3e5f28d714531d65470bc3a0fce6a9d73377f6c66333d65abedec10c', '00991824ba97f4950d843b4832e01d148dd2b25f7a6247b2341c919257e709e9');
INSERT INTO SSPROVINCIALE(nome, password, salt) VALUES ('Matera', 'fcaf17e9e05a3301fcb70d5bd82fc5449211d4467f63e3322d2f4e66661e747d', 'd926f7d89c54602afec50b628123de2989db92e73e41712d6642deeb405d5375');
INSERT INTO SSPROVINCIALE(nome, password, salt) VALUES ('Messina', '851af7ad9e0abb8c7a7316259115088e733e53e21b1120b5cef9e4af9c9428e0', '9c480823652eb0b70ab8d6dcdc86c6f23be06f82e1993733cc9aa85610384c8f');
INSERT INTO SSPROVINCIALE(nome, password, salt) VALUES ('Milano', '4b438230b148372b59ce166b38beb1e73b30fc1c467009b01125a672a5b996df', '792f169dba6d12a542e8780fb560e8505fae3a1c7b9652f3830bc4b03eac50b7');
INSERT INTO SSPROVINCIALE(nome, password, salt) VALUES ('Modena', '19a276a53d35b35bc0756c6403aae2fae98fa1c966fac3565f1c6379c04b93eb', '6020f74676bd900be23c46b076a5c04871d7b167ed4a315d2242541e963b1c7f');
INSERT INTO SSPROVINCIALE(nome, password, salt) VALUES ('Monza e della Brianza', 'd2b347325b183f8fb2eb156780cf84144a159dd086f7d443faeeac9193813c21', 'dfb4ac78f96b23b7e2dcc5fd062c6ec0d0f3473c27cfbb390379c92d3a5b31ba');
INSERT INTO SSPROVINCIALE(nome, password, salt) VALUES ('Napoli', '8d080abe897321c68f3f19e7f1ec2df35282c10f39f6a0d0f0f3266dff2a9e8c', '135b3fd0342dfdbb4524c024f2f63e2290c0b33a2a481da93df5d271227114eb');
INSERT INTO SSPROVINCIALE(nome, password, salt) VALUES ('Novara', '3feee0cfdbbb93a3bf4382388300f893f9748ceaa0baec61c8f5c5a3dadc2129', '294d9047f1f4724463fc970355f7797c388a67bbf0cd48d06987814c805e9874');
INSERT INTO SSPROVINCIALE(nome, password, salt) VALUES ('Nuoro', '2487ff12870dabf1c6003c0e51f0c916a84c3f8b4756e80bc7207c9d90922ae1', '44f8c2be0d0be24f7c13fdaec3aa4ceb03faa381a98f64fc8ebf878557d4bd79');
INSERT INTO SSPROVINCIALE(nome, password, salt) VALUES ('Olbia-Tempio', '410bfbb549c4cdc2fd7a52dc65701cbff328b254971b0a14f2fbbb1cd6ef03a4', 'd2650d02fd4e4027afc832d1f4032bdfb80ae0d480bd4223004793927e4d6a7c');
INSERT INTO SSPROVINCIALE(nome, password, salt) VALUES ('Oristano', 'cec4598e4b3ab19db9b42e9a30e1e92378f449f3fb8bff11bccd85ab7c5c13b3', '50318bfa9663df928095322273d5fb524248043e4065db1250672605c7ba540e');
INSERT INTO SSPROVINCIALE(nome, password, salt) VALUES ('Padova', 'fbb36a05c68bab371582743ae37d4f62d807624b4834249f34c01c50b2da30cb', 'afcae736c41ddc97ad57bac13c9ed5a67c9d7a4c26fb682f2b74ab1a38babc9b');
INSERT INTO SSPROVINCIALE(nome, password, salt) VALUES ('Palermo', 'd26aaaf0af3fe2aa76d41d293d9c7957c92df65d75c595b5a17783f1a051142c', '9ea7d7616bb6d06ed4064df399f315d2a68ff5c8197dcf2d9a4433f697be4f2f');
INSERT INTO SSPROVINCIALE(nome, password, salt) VALUES ('Parma', '70c0e57f30bf9c5b8d594333187978919f0d59e34f05c360d31a83bc00f8f42f', '1fd54b3995055868f1f7951cf7f517f7c9502bf5bea0c32bf8529bb988ef27e7');
INSERT INTO SSPROVINCIALE(nome, password, salt) VALUES ('Pavia', '4451db33ad2d0c2b976ec5c86b70260827f94f3f4ef574026392fd80403229c1', 'e613dcc8b4b428d31a38188324d54110443b0c63309a874b5b9dd91efdfde1b7');
INSERT INTO SSPROVINCIALE(nome, password, salt) VALUES ('Perugia', 'a8b08a1e25f45670faea763b69c476ac395701c629c329d1df9b57c3ab47da22', 'b62cc569cfdb9e42b632be96bf86ffa77611b7dd002461777195adcae1390725');
INSERT INTO SSPROVINCIALE(nome, password, salt) VALUES ('Pesaro e Urbino', '821681e64cc94ce5946c9c9d0186e02828500871bd855f56e7028b78a95f397f', '162f5b756922d1162d42772b0549bfca5784a24602e6944820c364dc888743b8');
INSERT INTO SSPROVINCIALE(nome, password, salt) VALUES ('Pescara', 'dbc778fb0d2c0229659fa42a96aff89c498592954a06254e1d011bd1c98316f9', '85f72f2ff5cee4651d51c8846deb921b7a88e6568f72ad8b52d088b71780e1df');
INSERT INTO SSPROVINCIALE(nome, password, salt) VALUES ('Piacenza', '2a8b43bf997d3676cdf4b3cbc2ac59571bf6c5acf493452548065548a83bee4f', '108a19440c293d599e8a91aa8b7d9e45e9283248fe7d29946183df617aa65d3a');
INSERT INTO SSPROVINCIALE(nome, password, salt) VALUES ('Pisa', '0fdbd79af8e3b566059dd0e421de3d67dab7e855e60ac9590133a882c7856f6a', '302e93f91f31dac1bc00c4ab3475e20e1f5795ac0e6cd2cb737dba6e040ff8b1');
INSERT INTO SSPROVINCIALE(nome, password, salt) VALUES ('Pistoia', '566a1cd955731950807522beb10b7d6e0dcbeeea83a12fb3fdbefc5d83e47b9a', '92be94bf20930b010f5d6dc12cb44d127926fb0de6157dede81adb41995aef44');
INSERT INTO SSPROVINCIALE(nome, password, salt) VALUES ('Pordenone', 'b2249114dea4c24ff128b9931b9abed26bb08ab34234e9e6b7d85401d003f170', '79561b16c09c164b0ede42f8ec9354f8ac291cbe452745acf03bfacd12806841');
INSERT INTO SSPROVINCIALE(nome, password, salt) VALUES ('Potenza', 'cbd925a064a59e1d1d9807c22149f422037fb31aa31cce326bdd069f608bb9a0', '36fd8ac356001892fd9d3a612d4f4888a23c2fbf340269ae11f957bc08742163');
INSERT INTO SSPROVINCIALE(nome, password, salt) VALUES ('Prato', 'bd432a7574072992481352403c6f4c3ff5b0c31b969e6028a34eaf057259f644', 'a3c4e04920b8123a5da085b3c1cc279929bc03428967e91da83418299e8d4b63');
INSERT INTO SSPROVINCIALE(nome, password, salt) VALUES ('Ragusa', '102ece51abe5701d14081c893082519d80959e4cb4347c35ba081ae365ce35e0', '2b7ac2743bfe5b50ee19cd4e748e07603ce53e62588f2c2e2c2b9865b8778afc');
INSERT INTO SSPROVINCIALE(nome, password, salt) VALUES ('Ravenna', '1f84ae92aa6c0b4a42e89b4bd2005f055f615df8d0d32338bcec722456bb1ed3', '71c88d625fb015cb084cb7d602b6c6b42a06b7e3f061938ec71a16f02bdd9319');
INSERT INTO SSPROVINCIALE(nome, password, salt) VALUES ('Reggio Calabria', '244e602ecfab84bc5a39deec5764c1bce2175abbcf80f304ae84566f6dde9254', 'ac90b6696c8ced56e6f839ccb1c6c5587108152010f9cb450df674d92e7850f9');
INSERT INTO SSPROVINCIALE(nome, password, salt) VALUES ('Reggio Emilia', 'b137b95c7d78a0fdf7b1340d3fe570f48a8d8db718d2d52f14cbe5d2d26effca', '37fe8482de6d31c84d094ebe80f32d40a6c8ad65a6bd7176a37a8723a11f41cf');
INSERT INTO SSPROVINCIALE(nome, password, salt) VALUES ('Rieti', '37a242e1518c322df77366656ad63fcca3939ef1979ede8c4d7814bcd46b8593', '09168d1e92e5bac2d5098c03fd3c1c949222a5a02bb35b0d0d4f225d25158fa9');
INSERT INTO SSPROVINCIALE(nome, password, salt) VALUES ('Rimini', 'ef5efa16fbdfe65a622a139890e351e448603b938040876c99d8e9efd8978f7a', '10b96d9fab859d6e4897d7f7ae145a0ac91abe2bac251b404ad6c48216822c38');
INSERT INTO SSPROVINCIALE(nome, password, salt) VALUES ('Roma', 'c1cfd0409a1f240ce733ca03068bee2833e66e4a11ee8cc7de73d1d4c524f252', 'a646930b947c37d72f9c8ab5ef2ce88650a57dd01eb4c54d4394868c24a3f43c');
INSERT INTO SSPROVINCIALE(nome, password, salt) VALUES ('Rovigo', '30d7f0c874701eefab86ec8931d87e539ff29cb712ea193aef279d0eeb16ca73', '149ec868047198fa015b11084abf2532f88a1209584a29e52f59ee17602cb8db');
INSERT INTO SSPROVINCIALE(nome, password, salt) VALUES ('Salerno', '5d80a96b1528f15792e88ca9b3f6af387b842093849ca082072fd425362b324c', '9cbf1bcf368bb5a4ceb266225f4880dde901f6138e6181a862c88a113b315f28');
INSERT INTO SSPROVINCIALE(nome, password, salt) VALUES ('Medio Campidano', '7b8df7e5101f8a7248cb05566529efdafc63574d4d31abb3ca2d2baa0f4abbc0', 'dca54c6b935d1d304c8599c7707be14f241e6e6c5027eaa2f3dfb97bc7bdc392');
INSERT INTO SSPROVINCIALE(nome, password, salt) VALUES ('Sassari', 'aadf00598d94965ca0ef44868e7340be1a220195851cf6a86e90b7c5ab299bd6', '0a9c4438ed207fc125a610f036e8833a4e994f5b35ac87d8583d56b22e46951a');
INSERT INTO SSPROVINCIALE(nome, password, salt) VALUES ('Savona', '12a25f6960bca20ec6debff9ab70ef35fc7cc06c3765844d2db6e1b98784e759', 'c0c716d347b34e650804cc1821063ddd271584178667f6ac65e00ea96efac41e');
INSERT INTO SSPROVINCIALE(nome, password, salt) VALUES ('Siena', '18edfbbc9b6296787f2d31746daec6a50d738593380a4c9d1be4b198bf4830a5', '37b464d50f340029a53540230d458a32c858ce92cb559a8a37f40db3ad07a265');
INSERT INTO SSPROVINCIALE(nome, password, salt) VALUES ('Siracusa', 'e7f54ceccdee2b1005256252e9281443a94b35314af38f9c800028ee7297abeb', 'b0a370a406f5ee9604aa391571bd0f7391900b4941156b7a38f11ee55f0ea078');
INSERT INTO SSPROVINCIALE(nome, password, salt) VALUES ('Sondrio', '47cf3b8683aad9f838783839831f05d53261e67ac126bc5d10898145b97ffa9e', '32ba0c408707a05945426a77b546a73df4983cb3ec96f6d6e0825c81aa27eea8');
INSERT INTO SSPROVINCIALE(nome, password, salt) VALUES ('Taranto', '05596d175f4f35253e7c3292fd12efd016ee3eafbb17674d88e89942bc6dc077', '94c76d6cebffe9ef94e644350c50649fea6304c9467eaf9d04fc7118dd5eb8a9');
INSERT INTO SSPROVINCIALE(nome, password, salt) VALUES ('Teramo', 'ffffb4cd9fd6f9ddd86b652e8519a56bd8cb31e6eedc27c8d884b9b136f5015f', '7c07c2eaafdb78117174de919dbbc86703c9aeb9f28cd358d7415bf3fc01c506');
INSERT INTO SSPROVINCIALE(nome, password, salt) VALUES ('Terni', 'bf6769b89ffaeed014533c13b45b81711e72511a3fd42f00deec4a4f489f3feb', 'f7c3d74f45eea93f16036eccf3ab2173b9dfbd076bfdd569fe374cfbbb130352');
INSERT INTO SSPROVINCIALE(nome, password, salt) VALUES ('Torino', 'cd3ce872b063a4f6836a191236792368aca1d6b525da1e621a7314be291f639f', '731c633de86c676c5f2d2fcfb21a21edfae4ee9984d1d6058a89da7f32ad82b1');
INSERT INTO SSPROVINCIALE(nome, password, salt) VALUES ('Ogliastra', '2ea88268970c1f960a55cd13bdbb010af64319206ebc16edd7118d1070f960a0', '30bc6f36a3fb19a1a5c6a7f947e66b207b8d41e6d397345f27f872a29d3d6959');
INSERT INTO SSPROVINCIALE(nome, password, salt) VALUES ('Trapani', 'e9183d521bf7c6be740afff52a0296f3d9c8527d78c30bcb01515fa71c856af6', '04ed27aa7e303e8f61bfffcf456c74375d7b089b4c118141a084a6e63da2ef1b');
INSERT INTO SSPROVINCIALE(nome, password, salt) VALUES ('Trento', 'd7ec66ba9c723a85532c72c22d4ce5d9fa81165eb2a839941da68242698ce69c', '295e55d3f0b07dea830069bf77c5cb8632021cb5a74991463ac07c0f776eef48');
INSERT INTO SSPROVINCIALE(nome, password, salt) VALUES ('Treviso', '0aaca9a820d0d7a059b1d7c2516de762776750333d2cfac1c8af409a37007370', '6c3651dab3c9560fcec63b30258a62b294570de46ca2e95faa7975ea2450128c');
INSERT INTO SSPROVINCIALE(nome, password, salt) VALUES ('Trieste', '9c6b1b9c168431deb26a4649408700d220807815b9803f02682b6b8b3634eb3b', 'e617344d79c0ccb86e73238cf447285bc14e481fb038ca712b084e7d2dab485a');
INSERT INTO SSPROVINCIALE(nome, password, salt) VALUES ('Udine', '6b03b33d06837c1fdc810679c5d5024fd4de8dcc7f0cc9e0338eea940a16d252', '9ca2c3aac1c067ea67e303d72395a14981ea1b5c82e5d5522e5dbd5e4cf89f3f');
INSERT INTO SSPROVINCIALE(nome, password, salt) VALUES ('Varese', 'd7b4466698eeba57407fc42fc9642aafe93360793af403cb656a5ae93fce5a67', '790cf8537ac8484acccee4bbc588b950fb423f5f191e1f9cf135af5a8acdf5e1');
INSERT INTO SSPROVINCIALE(nome, password, salt) VALUES ('Venezia', '7b781a10cbe6daff4b3dc7bb71f6e4404bd95d0dd4ac8173dbb07595da8fec89', 'a5cde0e7e7e41446bfeedd03ad4e5802f240c4df5cc95758fc287cfa597550a2');
INSERT INTO SSPROVINCIALE(nome, password, salt) VALUES ('Verbano-Cusio-Ossola', '2310eb7a5fb42ae8f7c1efe8b20aace532be7b4d1f63b768c6c5640ba335751b', 'd26dec3cd2979008107821dcf54bdf049b0e4101ffe5b7d461fa4cbc0b85215d');
INSERT INTO SSPROVINCIALE(nome, password, salt) VALUES ('Vercelli', '74f0f5532fcd34a743b173f1882cb2e1ce941ff9f442887775f3a0b151b30c6f', 'efc46d52fa80cebac74278a1f1a03eda60a77e622e2110ce0fd333a50d55612b');
INSERT INTO SSPROVINCIALE(nome, password, salt) VALUES ('Verona', '46156224c646d6998559050a988bcadc884edfa5a9214e41ba4a2f764da224ed', '240c9e2be413abe39aebc88bd6a4ef035f8c1a40cfeff5e5e942630484a65712');
INSERT INTO SSPROVINCIALE(nome, password, salt) VALUES ('Vibo Valentia', '8e003ca255412d552beff523c5e45c943e77e8fbbb721ae9c613fd97293ab86e', 'da6fb1191574cdf39462f3c3ca8b257b8be73e9e4bcf69baa23872d6728986b3');
INSERT INTO SSPROVINCIALE(nome, password, salt) VALUES ('Vicenza', 'f9973ebf577e0734d176de342466790eaba8265ecf9bea79df102d58b2fd37d9', 'd725881149d77783a467e43131cc07ea43da57732393679cfc99677ae0de65e2');
INSERT INTO SSPROVINCIALE(nome, password, salt) VALUES ('Viterb', '3d689ac9d6aafaedb3ed4941a390db3c2ff851fa19ff12520dfffbd5d03616f7', '9ffc7e677a2c77752145e47f144d67de00242ae6b3f795cdd3581179b22a5cfb');






INSERT INTO Persona(codF, nome, cognome, password, salt, nascita, citta, sesso, email, vive_id_ssp) VALUES ('WNOSHN70B22I754B','Shin','Owen','ab0286b972c8a2b4f72b7741bbfd5f7c010cadbf82dc208ed5677937a848e054','fbeb84a981a2492cebdddd9fe984e449a75a626c89a2d679420e934d46439e9a','1945-02-28','Trento',True,'Shin.Owen@gmail.com',42);
INSERT INTO Persona(codF, nome, cognome, password, salt, nascita, citta, sesso, email, vive_id_ssp) VALUES ('WTKCRG49T20E506W','Craig','Witek','66703adc57860bd9c36cb92df1e0486d840f61d37eba9394ba4b5d092cde2d54','e612e1250f1bc70bf2bd1e0a75b05ee4b04cf1dcecb766bfbe70bf3bcbcaed66','1957-11-25','Rimini',True,'Craig.Witek@gmail.com',46);
INSERT INTO Persona(codF, nome, cognome, password, salt, nascita, citta, sesso, email, vive_id_ssp) VALUES ('PFNJSH39A02B354W','Josh','Pfannenstiel','dae2dff0762e6cc4e5242113dce8645814f7f392f28b8d5afc97e1d0f9f42f30','adcfbb6fef89816e60cfa929f2f0ee0e68149273d39f64eb1bf6c0445a99fb76','2005-05-30','Firenze',False,'Josh.Pfannenstiel@gmail.com',52);
INSERT INTO Persona(codF, nome, cognome, password, salt, nascita, citta, sesso, email, vive_id_ssp) VALUES ('DNWLHW52C64A390O','Lashawna','Dunworth','ab39cf0e5819a5ab286a7146d13ca8e5315a345b0b9728d25c8acde48b421329','ffacec14c60dbe125be11a59b77e43051da8ec296e410ba527df6119c0e9a085','1949-03-08','Rimini',True,'Lashawna.Dunworth@gmail.com',14);
INSERT INTO Persona(codF, nome, cognome, password, salt, nascita, citta, sesso, email, vive_id_ssp) VALUES ('WRNCST48B59A285F','Celestina','Warnock','fb744791b7ab9c4872da2d716d64e10b821a212cfc608cfb77d48f5a18e89c3d','f6ad20aa2e1038da10f1034903ed4a0c633ee0a326b77e2f19d14ff46863646e','1983-03-30','Venezia',True,'Celestina.Warnock@gmail.com',59);
INSERT INTO Persona(codF, nome, cognome, password, salt, nascita, citta, sesso, email, vive_id_ssp) VALUES ('RNNGNY95R55A390E','Genny','Arnone','6a9c2c27857cc484ef77666a352fb72d634f960f4ae0124afc55aec544275c10','2dd025f3e889f7ebba046cd489b6354eaa049464dc0ab5d80b4f25ff271c285f','1996-08-13','Bolzano',False,'Genny.Arnone@gmail.com',57);
INSERT INTO Persona(codF, nome, cognome, password, salt, nascita, citta, sesso, email, vive_id_ssp) VALUES ('GLLVRE66T56I754B','Vera','Geller','f82779ff0b86f50ec5e1dce8ede72c11ab39ffa45d49561d18b55b95dfb1ccff','3150de0f3515f014a7a7bf916ff58dd4f9de9e864526d31b9f8320df7739c99b','1953-03-23','Firenze',False,'Vera.Geller@gmail.com',45);
INSERT INTO Persona(codF, nome, cognome, password, salt, nascita, citta, sesso, email, vive_id_ssp) VALUES ('FSADDA18M24H294F','Addie','Faas','34d3a46a8c97d15c9fedb7748692ba09859d5a6b2449715fc01c1317bc2427a1','c45ccd60e73d3d5d12e151ddd1dc6e82b16b72e4f2f13d8bb46fd3542af2d210','1930-03-15','Venezia',True,'Addie.Faas@gmail.com',15);
INSERT INTO Persona(codF, nome, cognome, password, salt, nascita, citta, sesso, email, vive_id_ssp) VALUES ('BSCSKU32H20A944W','Suk','Bischoff','8f6390e3b4afe4757d8e43a122b33d8528c56d7f322baa7b3fa82fe5327cf1f6','de58fcb0c234ee42bea9f447f7dc482e56c438711b4947a8023793c7bef99fcd','1998-08-31','Ancona',False,'Suk.Bischoff@gmail.com',9);
INSERT INTO Persona(codF, nome, cognome, password, salt, nascita, citta, sesso, email, vive_id_ssp) VALUES ('JQNBBR92B56L117R','Barabara','Joaquin','6b3f43b7f6a28dde56369a4e63a9c707bf5081737501e3bdbeb29efbdab12088','12d4d0cce3874b20cbf3d50f0a55790d6c66414b17171274d2331018a0dace50','1944-04-18','Venezia',True,'Barabara.Joaquin@gmail.com',38);
INSERT INTO Persona(codF, nome, cognome, password, salt, nascita, citta, sesso, email, vive_id_ssp) VALUES ('MKCLNT98M45E625Z','Lynetta','Makuch','aaa1311386769b943a9d61cd26586d82288d4943cc3a4c9cd9929d08fa65a9a0','e38c57ebb15d25cda6e8a5c50faf722b53cf4a753f6d8379fadc3385fa9b4025','1985-04-02','Siracusa',False,'Lynetta.Makuch@gmail.com',28);
INSERT INTO Persona(codF, nome, cognome, password, salt, nascita, citta, sesso, email, vive_id_ssp) VALUES ('SGDDMR77L60L736O','Edelmira','Sugden','deb6fe5da8fcfc54c24df0fad99f9dcdac74c6f8643ce4bc492e1d7cc6f244e9','5288028297aebf6ecdbdeca617cc3f6df658e621693c3c8efb462dd0572f2373','1951-08-15','Venezia',False,'Edelmira.Sugden@gmail.com',17);
INSERT INTO Persona(codF, nome, cognome, password, salt, nascita, citta, sesso, email, vive_id_ssp) VALUES ('DMMSNN39P60A390B','Savanna','Dimmick','69eccb53652e20fd822bf2ed92be0c3bc3505aac99cebf04ae5eca0fd6461a2c','5c092d94970a39a581884dce054ca62111a41643798a53e37fc9c4ebdca9e08b','1918-06-25','Siracusa',True,'Savanna.Dimmick@gmail.com',52);
INSERT INTO Persona(codF, nome, cognome, password, salt, nascita, citta, sesso, email, vive_id_ssp) VALUES ('WLSSPH35C43G478M','Stephnie','Woolston','a56918fee2f3efee9f6d3af77c126c5ce3f5fc5ca1e375dd4be9e9d8cb852394','f01c7bc2cfde2547d05e733433adfe5f5ea5efcb62b8c687f5bc3fe7a6be0905','1946-04-05','Venezia',True,'Stephnie.Woolston@gmail.com',105);
INSERT INTO Persona(codF, nome, cognome, password, salt, nascita, citta, sesso, email, vive_id_ssp) VALUES ('VRLTNN73R50F205K','Tawanna','Overland','80a66631ade06d1f4118e8fda961228afe260562395b5d61c0d7bf8678827048','e3bb9b54ceb75c3a3b4a3bf5ada787ee32515e5a45dd0696ea7d65790efc0830','1916-02-11','Ancona',True,'Tawanna.Overland@gmail.com',102);
INSERT INTO Persona(codF, nome, cognome, password, salt, nascita, citta, sesso, email, vive_id_ssp) VALUES ('LTHMRN58R54A271Y','Mirian','Leatherwood','41de3f4e472a0f43e6f558f582d888f76f3389871772c665a52a6b071c87538f','cb5e13fc2dec45db394b607af8dfeb9a2d6c74671ee8a831a957fa2501cd6b8f','1985-06-03','Venezia',True,'Mirian.Leatherwood@gmail.com',73);
INSERT INTO Persona(codF, nome, cognome, password, salt, nascita, citta, sesso, email, vive_id_ssp) VALUES ('DHMLYN56P05L483U','Lyn','Dahm','102c9abec164ba708eccc79603ce4c2135d34c7e806d65ecbff5a0e52b7a9307','e4ef2a81cc300697e2e5c35c915abd8aee83e2857ffdcf66f52a125ad8744b61','1917-09-02','Rimini',False,'Lyn.Dahm@gmail.com',2);
INSERT INTO Persona(codF, nome, cognome, password, salt, nascita, citta, sesso, email, vive_id_ssp) VALUES ('CLLCSL66P15I754S','Consuelo','Callihan','0a21050511b0157cae4a5b254757b376eb720cc47828a28036f02ab5dec462e0','562402e4b40369051de488b97590fc2feb2646ca96b7fcd89f98836527a3ee0e','1998-11-23','Siracusa',False,'Consuelo.Callihan@gmail.com',5);
INSERT INTO Persona(codF, nome, cognome, password, salt, nascita, citta, sesso, email, vive_id_ssp) VALUES ('GBLMLS49T56A952H','Malisa','Goble','c7a1655e3dab6b2ca64f95c9128204a914195c5bf16fc5c5ac509278d4ca9cfc','42839467a06f3635b1c199d77923cff78c2a83431089864648d8803c95f2d06c','1981-04-24','Firenze',False,'Malisa.Goble@gmail.com',86);
INSERT INTO Persona(codF, nome, cognome, password, salt, nascita, citta, sesso, email, vive_id_ssp) VALUES ('MCNSRY97B44E472A','Sheryll','Mcneil','5e832db02d8d408ccb468d0ccf271fb478f5748452381eb77caf8ba944ecc06d','86f248b0fc9e9f9369dcac0849402dca4a31c6370ce0a2bdc4e55220441ce8d2','1931-03-20','Rimini',False,'Sheryll.Mcneil@gmail.com',73);
INSERT INTO Persona(codF, nome, cognome, password, salt, nascita, citta, sesso, email, vive_id_ssp) VALUES ('BRYLWN05P02F704C','Ladawn','Brayboy','83778bc29b4658d4f0931ca24c30e6adf8f881b327bb5e9e3435c7e4bb1129fa','baec57840364e159993f6fc32fdaccc71e1c66368009a8c43486e6a4bec2ac86','1921-04-24','Siracusa',True,'Ladawn.Brayboy@gmail.com',15);
INSERT INTO Persona(codF, nome, cognome, password, salt, nascita, citta, sesso, email, vive_id_ssp) VALUES ('GRNLRN37P68H199H','Laurena','Grunwald','0308ee75cc208bb1e332b6936e43cec2ed501b5ee848955b03ad94260f34facc','da581a7fef94e2fa5dffe66f91788aec53ebca61df29c31a7cf459b30e10e3e4','1976-12-15','Bolzano',False,'Laurena.Grunwald@gmail.com',73);
INSERT INTO Persona(codF, nome, cognome, password, salt, nascita, citta, sesso, email, vive_id_ssp) VALUES ('BFFGTH35H45F839J','Agatha','Bufford','f85441ac8094adf690d5cf4be626d7c55f77863adf1ef5152ac561c1cebc1dc1','d63bad57861b4ff1975bfbac87b6e3d272cd56abf645e91b8349adccab4396b8','1989-07-26','Trento',False,'Agatha.Bufford@gmail.com',65);
INSERT INTO Persona(codF, nome, cognome, password, salt, nascita, citta, sesso, email, vive_id_ssp) VALUES ('MSSLSI02L55H294E','Lise','Messing','ff2be5a1fb1b894fd6a2433b93fd842d1cbc471424f235cdccceed94eae001d4','2c9780408c755d96fbeba5afa45d8f0d6d8db6b35097d12b4a1c2c51dabb1ae5','1928-05-19','Bolzano',False,'Lise.Messing@gmail.com',69);
INSERT INTO Persona(codF, nome, cognome, password, salt, nascita, citta, sesso, email, vive_id_ssp) VALUES ('BRRCTR59L23A944X','Carter','Brauer','c2ff724ecb5647465a0e9d7b6aebca3fc4f7b5798481f0c88b7d399104916f79','b50d8b9c18d08889a56b982e6835de095068f6e71268fc748df91f7d9521e531','2014-11-06','Venezia',False,'Carter.Brauer@gmail.com',33);
INSERT INTO Persona(codF, nome, cognome, password, salt, nascita, citta, sesso, email, vive_id_ssp) VALUES ('KNCDEI38M15D612H','Edie','Konecny','d77001d03cecfbf0359fe698a194303c21cf90c1211a48775b9c2c1fe4d1cc4d','c7fefdf9b3e070795c68dab8695e9895dba0e68fe56dda786fd7cd1389884b6c','1948-07-18','Firenze',True,'Edie.Konecny@gmail.com',55);
INSERT INTO Persona(codF, nome, cognome, password, salt, nascita, citta, sesso, email, vive_id_ssp) VALUES ('WHLJMM16T04H294B','Jimmie','Wheelock','6a797e5a5459daec76a2180036f2ffb079320379f1aad2c65b0626084d6e2a21','9f7a2a05ef288c992efb5111548a3b3e96db292a8ddfbd0e29bb5d300efadcd9','1951-01-18','Bolzano',False,'Jimmie.Wheelock@gmail.com',74);
INSERT INTO Persona(codF, nome, cognome, password, salt, nascita, citta, sesso, email, vive_id_ssp) VALUES ('STRMLY40R68E625Z','Marilynn','Strack','6e9dfc890adf8d1403150a3b2dbc63435111aa70f9fa5791d715d3deb77ab9db','e438d6492cda07e06b958ca9bb96a3beaf69e43aa3ee22019ade59bd80b7a52d','1943-12-04','Firenze',False,'Marilynn.Strack@gmail.com',85);
INSERT INTO Persona(codF, nome, cognome, password, salt, nascita, citta, sesso, email, vive_id_ssp) VALUES ('DFFGTR01E41C351N','Gertrud','Duff','b93d1763371daa5b3b9da0812943132d9b847563b8880c932cd607d75de6b980','fc21f658c91c195c89cfdbc57bbdd24cffdb479415135458721709e236c6b765','1959-01-30','Siracusa',True,'Gertrud.Duff@gmail.com',19);
INSERT INTO Persona(codF, nome, cognome, password, salt, nascita, citta, sesso, email, vive_id_ssp) VALUES ('TCHSRN68L30I452A','Spring','Utecht','1aa9c616c3d98a58eba19d4976e2516bb1b7d6996673c0ab9bed0dd403157956','57dea0cbcd3ae8c199a8b84ac7d0b495365b06357a6842cbe546df952427bc4c','1961-07-18','Bolzano',False,'Spring.Utecht@gmail.com',78);
INSERT INTO Persona(codF, nome, cognome, password, salt, nascita, citta, sesso, email, vive_id_ssp) VALUES ('LZLPGE98L18G482S','Peg','Lizaola','8e8e41167f4350bcef38978f4f3c342824f8e5c42e0e68f7cb53887ce06c382f','f1bc5cdd3d57b13f748cae26fa6e6a61dcbb6d7cbd3fceee354c87bb48b3f097','2019-11-03','Ancona',False,'Peg.Lizaola@gmail.com',101);
INSERT INTO Persona(codF, nome, cognome, password, salt, nascita, citta, sesso, email, vive_id_ssp) VALUES ('CYLSWN76D60A952Y','Shawanda','Caylor','56798a93e0137db552d4a78e8ba17c55daa8103ecc61b07e5d7ada5c1698e96e','8acf50b134819874d6a9e7d4398e1ff41b0a16e5ec321cd1d966ed6e78c83d3e','1992-10-01','Ancona',False,'Shawanda.Caylor@gmail.com',45);
INSERT INTO Persona(codF, nome, cognome, password, salt, nascita, citta, sesso, email, vive_id_ssp) VALUES ('CRMTTH28C64A285J','Tabatha','Croom','f2b2a78a8251003f7d1e7067bf9c0ebe69bc0059532717c4b219d0168ecf0374','73c8c5df9f489f0d50dd585af740fbc530902cf53781c6e34e01444c0166cea4','1921-06-09','Ancona',False,'Tabatha.Croom@gmail.com',30);
INSERT INTO Persona(codF, nome, cognome, password, salt, nascita, citta, sesso, email, vive_id_ssp) VALUES ('SNDMDE91P66F952H','Meda','Sandefur','92ba7a06804d4c35f4cdb73b79e7ef7cf1f2c19d8b15157c0e688b06a128221a','380fbc736f1dafbd0c007b0eb6fcfa5227898349e3a0a81f6bc09a0453057e7b','1934-01-21','Venezia',False,'Meda.Sandefur@gmail.com',33);
INSERT INTO Persona(codF, nome, cognome, password, salt, nascita, citta, sesso, email, vive_id_ssp) VALUES ('MCHNRD18M67E472F','Ingrid','Mauch','e4c466a1b3df83c47e26513862963a8fcb94d3d82e330dcc11e4ef8d43ed003f','50ce75b5b1ac66d8050894a51da8a6d8ca87a0df00d9a367d5855ddc313b2665','1938-03-25','Venezia',True,'Ingrid.Mauch@gmail.com',56);
INSERT INTO Persona(codF, nome, cognome, password, salt, nascita, citta, sesso, email, vive_id_ssp) VALUES ('NEULPU14D09H294G','Lupe','Neu','32b6d6d15936cdc60c78fddf89e21bb7ce7c70d8245acab9c45f57199c902846','48e491ddf92d222acea3ac860b74b9d2635588b028c159b94a1d5f5368202308','2006-10-02','Trento',False,'Lupe.Neu@gmail.com',76);
INSERT INTO Persona(codF, nome, cognome, password, salt, nascita, citta, sesso, email, vive_id_ssp) VALUES ('PTTCST16C19F839U','Celestine','Poteet','3d0d9fb31cf0cddab74084fa6689fe7b0a86fe37dc90d8467073c0dacd8feb2e','6ee9ce4a9648571dcb7d3c8ea64f6471f6af8b00448e129a4ec0a9bd50205a77','1965-10-06','Siracusa',True,'Celestine.Poteet@gmail.com',74);
INSERT INTO Persona(codF, nome, cognome, password, salt, nascita, citta, sesso, email, vive_id_ssp) VALUES ('NNOTNO53M05G999T','Tonie','Noon','dcc105bf90e36d7454704130589d16492a8a532e201b89c6b13e1d73efd2a5e8','ffe5c11650ff69d57892e73bd83265cee4aeeb37d11aa91c40cefa0f2cff34b4','1933-08-17','Ancona',False,'Tonie.Noon@gmail.com',10);
INSERT INTO Persona(codF, nome, cognome, password, salt, nascita, citta, sesso, email, vive_id_ssp) VALUES ('CDODTT60E08F205R','Dewitt','Coad','eef5c157f875f9dade5154cd446a2ea9d5836a8221c345aec5ac43a532336d38','9daa2cec1b3f5216fcc804398f1c75cb1463ae25307b5eac281856a934f3992c','1919-11-20','Ancona',False,'Dewitt.Coad@gmail.com',67);
INSERT INTO Persona(codF, nome, cognome, password, salt, nascita, citta, sesso, email, vive_id_ssp) VALUES ('CRTFLC16B24I754Z','Felice','Crater','77494f65a55725fa27cc62da906d651fc0e30413b15fcfcb71698e789c10a140','10e60e1346283e6c4af71ebb2c8550d2938a8fc064259652ee8e140667489960','1914-09-18','Trento',True,'Felice.Crater@gmail.com',39);
INSERT INTO Persona(codF, nome, cognome, password, salt, nascita, citta, sesso, email, vive_id_ssp) VALUES ('SKNDRN90D70A271K','Adriane','Askins','6ff9113aaf0d6421389c5675678d895470067df117d0f46c3b31a1008ab416f5','d966f8ad1b8a46aad082f63b7c88b6cc36a64ef08f50e881bfd836c5a6c8da8a','2013-11-12','Siracusa',True,'Adriane.Askins@gmail.com',93);
INSERT INTO Persona(codF, nome, cognome, password, salt, nascita, citta, sesso, email, vive_id_ssp) VALUES ('CDYMAE48C70D643E','Amee','Coday','d9085c71da1155411ef798f3d3204afdbfaf228eed2d3617019fb56e06e630ae','fdfd2860a1b07157e5b93391fdccf8d4a77159c2533734822418ede8fe7cfc55','1955-05-25','Ancona',False,'Amee.Coday@gmail.com',104);
INSERT INTO Persona(codF, nome, cognome, password, salt, nascita, citta, sesso, email, vive_id_ssp) VALUES ('VHLKMI82E56C351R','Kimi','Vahle','b72391e1cd1167a74cf6020c47899d107fefa6fb18a13af50c90d8ac06ec7b86','13601c0070efafc149fcb9d4157e5bd26ef8216c267a710efe660eb88d7e0ff3','1968-05-01','Rimini',True,'Kimi.Vahle@gmail.com',9);
INSERT INTO Persona(codF, nome, cognome, password, salt, nascita, citta, sesso, email, vive_id_ssp) VALUES ('LBRGGN44S54F952S','Georgina','Labarbera','1f60e3ffda663e1a51e202c1e686c9edc14c2c33d590c2eabd8323b65189a3aa','eb65d22994410f675e53703abea5eb73ffdc33318d8fd0c3ec7d8c005c482c70','1936-09-03','Siracusa',True,'Georgina.Labarbera@gmail.com',56);
INSERT INTO Persona(codF, nome, cognome, password, salt, nascita, citta, sesso, email, vive_id_ssp) VALUES ('BRKDLC83C54H199C','Delicia','Brookes','b04ac6424ea6cd7cf570181283acdfcef5df05c2a97440689ad62ad33291d40d','16c81458e429b68a2b04888312b9f8a1f1fa7eb054c00c6096860c40938b9f13','2003-09-13','Bolzano',True,'Delicia.Brookes@gmail.com',75);
INSERT INTO Persona(codF, nome, cognome, password, salt, nascita, citta, sesso, email, vive_id_ssp) VALUES ('SRCSLN16P53G535V','Salena','Sircy','daea9c93803b04dbc96bff030c35ba44cfe06e56d332cbea71750b371dc75fe9','64fff1569635702e2cef93fc7e3e0554d88bfa3d4e4a4fabc648e064c3573183','1990-12-15','Bolzano',True,'Salena.Sircy@gmail.com',6);
INSERT INTO Persona(codF, nome, cognome, password, salt, nascita, citta, sesso, email, vive_id_ssp) VALUES ('HSLBRN00P41G337Y','Berna','Heasley','3d8f8b57a51513172a3486d1eb68faefbcbd48933c2ae406f1f4ead4d9a09149','7013d960bc7fcab9ff6d393c4a3279f2aac4aca8203d465ad8292d5d05b6707b','1950-09-03','Siracusa',True,'Berna.Heasley@gmail.com',49);
INSERT INTO Persona(codF, nome, cognome, password, salt, nascita, citta, sesso, email, vive_id_ssp) VALUES ('ZNKDND14H43H703X','Delinda','Zink','8c5b9293f0f3ea27880fe96d98c82534a89158b2cc7a934eacdfba1b30e751af','8b6c7d617b0fe21e0e9683392a821f8a58daf26bcc79164340acc245c79c3d7a','1958-07-09','Bolzano',False,'Delinda.Zink@gmail.com',101);
INSERT INTO Persona(codF, nome, cognome, password, salt, nascita, citta, sesso, email, vive_id_ssp) VALUES ('MZRVVT11M18G535T','Velvet','Mazur','d2fec9a7e7928208ca8a034b7238274436dfdb691c0cb0de3decee8e14ca0e6a','c994c1358cb11742a7c8a83dbbeb2c06ad28653f87dd2283ccb6cb247baca868','1951-01-07','Rimini',True,'Velvet.Mazur@gmail.com',107);
INSERT INTO Persona(codF, nome, cognome, password, salt, nascita, citta, sesso, email, vive_id_ssp) VALUES ('ZGRKSH76T44A944U','Kesha','Zager','ad2a6098db5bb0afb2b3035c0bd730f80a11621c4bd35aef8729a3343ebea24d','c2f61f1930ce06c29fcb9280f3a01ae711d33a03b621a5771b6946627688dd83','1943-11-27','Ancona',False,'Kesha.Zager@gmail.com',89);
INSERT INTO Persona(codF, nome, cognome, password, salt, nascita, citta, sesso, email, vive_id_ssp) VALUES ('PLRFRD55R12D643V','Fred','Peeler','239398fa6f830e228d5fe5f76837ee94d063019ce127156889473db0239d6664','220fe4d5fec6fadfdd9010688b3477ce04efdbb86b06e47723e448a4bb7f9924','2018-11-06','Bolzano',False,'Fred.Peeler@gmail.com',7);
INSERT INTO Persona(codF, nome, cognome, password, salt, nascita, citta, sesso, email, vive_id_ssp) VALUES ('PLNBEA25D60F952A','Bea','Palencia','01f9a98fcdb7bfaede47e535cbc26caab8f701417a2730a278af70b3bbfddf74','968aebe950559ef79345ec17891bcb34123a4d5660a2800ef87a07fb52653701','1916-03-14','Bolzano',True,'Bea.Palencia@gmail.com',40);
INSERT INTO Persona(codF, nome, cognome, password, salt, nascita, citta, sesso, email, vive_id_ssp) VALUES ('DLBGGN98C42F704W','Georgiana','Dalbey','8c956d33bdc16e3f8c845d231897437754c08f0f51954a1c1bcb1c37dbf63a7a','ed0ef8e6ed89fce7ee690d9c726d75080adfabcb369308228bdc0d8b1ac63bca','2004-05-08','Ancona',True,'Georgiana.Dalbey@gmail.com',39);
INSERT INTO Persona(codF, nome, cognome, password, salt, nascita, citta, sesso, email, vive_id_ssp) VALUES ('CHPMRV39T43H018V','Marva','Chapman','fdafa246922e609179ad64a3dd46cee621a3ba3441c8a6ef278775d48bc9fb56','31fcd2b6818772a865d3d4c1b94c11d550714848aa920d0aeccdc0f831cd9b65','1970-07-01','Rimini',True,'Marva.Chapman@gmail.com',8);
INSERT INTO Persona(codF, nome, cognome, password, salt, nascita, citta, sesso, email, vive_id_ssp) VALUES ('SDGDSR11P26D643I','Desire','Sedgwick','1ca339a7f2d2556f4e2dd7d598495c873a7d7269475e21625b36d3e700ba2558','e04077808479787d99fca826b020576a61138b42316e571061cffacad88b45b6','2005-10-22','Siracusa',False,'Desire.Sedgwick@gmail.com',19);
INSERT INTO Persona(codF, nome, cognome, password, salt, nascita, citta, sesso, email, vive_id_ssp) VALUES ('SHCLNN49C50H199E','Lynna','Ashcraft','69762641b5ae216e26da172b86f47c6999d09f8498757c02241f933311476053','7b484ed4c366c4d1669f70bbc16cf8893d2f4c9733edcf16ad23d18c4aa9a521','1956-09-01','Firenze',True,'Lynna.Ashcraft@gmail.com',85);
INSERT INTO Persona(codF, nome, cognome, password, salt, nascita, citta, sesso, email, vive_id_ssp) VALUES ('CMNSRL85A45F158P','Shirely','Cimino','f5b7f87ecf5e7c7e94ff4f65e80c9b5af91f8236d8a7f3134e880d91b5f37886','177edd7c3a1b42deb19842f22672cab2d10c52cebfa1c55642e5566390bef32d','1938-10-14','Venezia',False,'Shirely.Cimino@gmail.com',63);
INSERT INTO Persona(codF, nome, cognome, password, salt, nascita, citta, sesso, email, vive_id_ssp) VALUES ('CLLJNY48L53C573F','Jenny','Collman','7bca901feae23afb59bc172fa3462c1d10100808e552c9df50292c2b3f45743e','204370549528bff1069d9deb48332bc230361a76a2ee17bdefc3d628ffd4f983','1942-12-05','Siracusa',False,'Jenny.Collman@gmail.com',38);
INSERT INTO Persona(codF, nome, cognome, password, salt, nascita, citta, sesso, email, vive_id_ssp) VALUES ('TRSZLL64M42D612X','Ozell','Treese','fe9ecc24a5a610c014699e8b8741c8badca56350750881688e2a434697c80492','362187e5f27997d10a3536121e9952ba63460e415612b754f6123169fad0d0e3','1993-03-01','Trento',False,'Ozell.Treese@gmail.com',58);
INSERT INTO Persona(codF, nome, cognome, password, salt, nascita, citta, sesso, email, vive_id_ssp) VALUES ('SHPPRN92S69H018S','Patrina','Shepler','28e1309c20f18991187cf3831b4a31038cd9fd478edb12a4e18aab1e5b46e8b5','44c74b65b634a8189d131c27b30467603fa39c98be2be68b49ec1643527e29eb','1929-06-03','Ancona',False,'Patrina.Shepler@gmail.com',41);
INSERT INTO Persona(codF, nome, cognome, password, salt, nascita, citta, sesso, email, vive_id_ssp) VALUES ('VCKLND99S61A794B','Elwanda','Vicks','7b75e1c33ae8ddef65f58513e44897e565092a1a24f53631309d83e7c1c8f54f','f1171f3d459b7c16e1cedbfefe9135fcfe21331fb886d75f53cf94f781e66f65','1997-12-07','Ancona',True,'Elwanda.Vicks@gmail.com',75);
INSERT INTO Persona(codF, nome, cognome, password, salt, nascita, citta, sesso, email, vive_id_ssp) VALUES ('KNSKVN32H10E054D','Kelvin','Knauss','1dcdcd184ebc3b23bb893fcefb00422ec00117b510ba73d2b4b9127975434abd','7c55e5b0faf5d343aef4d49aea1ab02fbcb334aad6e7d17272fda12c7fd7758e','2019-10-28','Venezia',False,'Kelvin.Knauss@gmail.com',82);
INSERT INTO Persona(codF, nome, cognome, password, salt, nascita, citta, sesso, email, vive_id_ssp) VALUES ('LFRMYL51D65A952F','Marylouise','Liefer','7add0b7a0fc6a9179ccec29451b38c772a5947bab7fa901974ae19104a1744b4','d6eaea9c6266b1604a819f6599aee2a86f80a2b43d2f5c37dd68d20ab8143a8a','1996-12-22','Ancona',False,'Marylouise.Liefer@gmail.com',67);
INSERT INTO Persona(codF, nome, cognome, password, salt, nascita, citta, sesso, email, vive_id_ssp) VALUES ('CLCMLY49B67A271K','Marilyn','Calcagno','b7f3fc6b1b3fa13d86d7c7c5f13d5111c5246aa475f1a6042e1a862441b531da','5b1e58c56a13432ecb0f64a1d2224e031df264daa0bb8ec3221ba900cdb16f89','1921-08-25','Venezia',False,'Marilyn.Calcagno@gmail.com',108);
INSERT INTO Persona(codF, nome, cognome, password, salt, nascita, citta, sesso, email, vive_id_ssp) VALUES ('BRGSRN46B57D612C','Sharen','Borgman','28c8920e9107a6430cb975af220c7d5f0037fc441c07b81bc09571258cedf91d','86f3aae7c459e4c549b8bf6750cad89d86e80a1af3cf52f0fcb4e6b949785eac','1997-12-04','Venezia',False,'Sharen.Borgman@gmail.com',64);
INSERT INTO Persona(codF, nome, cognome, password, salt, nascita, citta, sesso, email, vive_id_ssp) VALUES ('FRRMLN99B67L049B','Marleen','Freer','960de34ee6e1f8915d68778a935d354dcc3baae0cb360370cc2e726ca4c8f3f9','2b88db67150b2a523d50b1e14284a42b35f58cd58b23d8ffd016b034643f1c98','1930-07-19','Siracusa',True,'Marleen.Freer@gmail.com',4);
INSERT INTO Persona(codF, nome, cognome, password, salt, nascita, citta, sesso, email, vive_id_ssp) VALUES ('BSQDPH23M50G224N','Delphine','Bosque','5b87bd53c1bddc9ed821ec242ebebde3c789f4b3744478e436b2ccbd2399c659','a93717d8429a23835cdec8ac748e981c162459202e6f1882a652f1f77328fded','1983-10-26','Siracusa',True,'Delphine.Bosque@gmail.com',82);
INSERT INTO Persona(codF, nome, cognome, password, salt, nascita, citta, sesso, email, vive_id_ssp) VALUES ('JLNSRY27L57F205H','Shery','Julian','6c6e12d2ee7d4889d2d11ba220c1c0f0f01e558bcc9ecae09b4cb4ea8e415794','75ce1004d56eab14a42f35b8d378f741fbfa7bad725376538ea7b3a813d65889','1968-06-23','Firenze',False,'Shery.Julian@gmail.com',7);
INSERT INTO Persona(codF, nome, cognome, password, salt, nascita, citta, sesso, email, vive_id_ssp) VALUES ('DGGKCY43A63F158S','Kacy','Doggett','95740ecaa9da9cfb6945f0891c8c855ab63a394886953f554964998b5f5713e7','bece19e450abdf369ee37feecbe190f31f5fc59d788fb84fb27e56008a6dcd43','1936-12-31','Siracusa',True,'Kacy.Doggett@gmail.com',71);
INSERT INTO Persona(codF, nome, cognome, password, salt, nascita, citta, sesso, email, vive_id_ssp) VALUES ('CCOPHL05L62A952J','Ophelia','Coco','6ef505c4616e76c758c4948b9e7f749466a3ef927344aeba18749fdb57798cfc','08590d732ffd5340965c7a9d4f6b5fad43a47cd16c9be4ac1f5aac8321880819','1985-10-10','Rimini',True,'Ophelia.Coco@gmail.com',36);
INSERT INTO Persona(codF, nome, cognome, password, salt, nascita, citta, sesso, email, vive_id_ssp) VALUES ('SPKMNL56D43L049Q','Magnolia','Spiker','82f219e35395640346e9e246efa57c465cc1eb4b807e03c9a9e62a0d6527e7ea','24a526942fc3fcc05b3f3a65fcc361ffec497ada7fb41678c11ebbbc1c20b5f8','1977-09-19','Rimini',False,'Magnolia.Spiker@gmail.com',102);
INSERT INTO Persona(codF, nome, cognome, password, salt, nascita, citta, sesso, email, vive_id_ssp) VALUES ('BLSNKL96E43F839O','Nickole','Blasi','ad9c8f20bb9ce291ef1db23159ba301944d0ca6dce966267a42c5ba51f109de6','4d0c4b395fe42e43a3dbfc66e174d9c83b9fde97c79b09e0746dfa8fc38d24da','1945-06-18','Venezia',True,'Nickole.Blasi@gmail.com',61);
INSERT INTO Persona(codF, nome, cognome, password, salt, nascita, citta, sesso, email, vive_id_ssp) VALUES ('MRRJLY71L06I754A','Jerilyn','Merriweather','daa92914da6adebe6f28cf63bf03fb86008dbcefeafd142cad12c99e2026f4f6','3b4f26dc49bfd2e3f8d2d61c0e11181e1197f51e20669afa4f5518a0c12734d1','1976-09-28','Trento',False,'Jerilyn.Merriweather@gmail.com',95);
INSERT INTO Persona(codF, nome, cognome, password, salt, nascita, citta, sesso, email, vive_id_ssp) VALUES ('GBBJYO33H27L049Q','Joy','Gibbens','6389fbb8e044a33e1f7b6625ebb5a42aeaacfc4736e033136fbc9321c3bf5107','bed5a0b0363bdfa6902036bf1ca4910d16e7d57c9534c8d88fc07a6ef7d4b458','1931-07-11','Ancona',False,'Joy.Gibbens@gmail.com',51);
INSERT INTO Persona(codF, nome, cognome, password, salt, nascita, citta, sesso, email, vive_id_ssp) VALUES ('JWRNNC84E59H018Q','Annice','Jaworski','9c70e9f031d63f506704d23988c232ff7e5e3d2be126222e13af546884fc6c49','7931c9930ee9530fe303d9109d9809fc0ad622bbec0b246d401cbeccf2e451bf','2011-06-08','Siracusa',False,'Annice.Jaworski@gmail.com',58);
INSERT INTO Persona(codF, nome, cognome, password, salt, nascita, citta, sesso, email, vive_id_ssp) VALUES ('BRMDLC18M57I452P','Dulcie','Bromley','b95482b563a0129d728d206a4bfcc6b0cb37190a48aa05b112636b0e111315e1','89253f6d055999ec3ed205518daa0789cb08692244545d5f614e36c1f4a97111','2003-10-21','Trento',False,'Dulcie.Bromley@gmail.com',5);
INSERT INTO Persona(codF, nome, cognome, password, salt, nascita, citta, sesso, email, vive_id_ssp) VALUES ('MRNFND60C68C573B','Fonda','Amarante','fb1b9ef558e24602431219951dc0723f1a14b040822964c13a4dcd48af80b545','65e0515d1c934f00631768d2ac36ebf1d939c079b5ab1926efa217cb2630c57e','1962-12-05','Siracusa',True,'Fonda.Amarante@gmail.com',86);
INSERT INTO Persona(codF, nome, cognome, password, salt, nascita, citta, sesso, email, vive_id_ssp) VALUES ('CGLGLN56M25L219Y','Glen','Cagle','970b6adb3682c71d1ab27bafad20b57f44dd541d7602b450a5e2813e8e2642f6','5611e53488dd8788b527f3fd0fb623883d8c19eb452aa7d9de658296909530a5','2016-05-14','Trento',False,'Glen.Cagle@gmail.com',19);
INSERT INTO Persona(codF, nome, cognome, password, salt, nascita, citta, sesso, email, vive_id_ssp) VALUES ('WDLKZY17H42A271B','Kizzy','Wadlington','27ca354d62d4800596b895244a68d765cad09dd0bb8c9d7b7bcebb5d6e3e8acd','39050f56cb4adf2c99f13a7bc7836bf302967f2ada087321701bb1693337c47c','2014-06-22','Firenze',True,'Kizzy.Wadlington@gmail.com',30);
INSERT INTO Persona(codF, nome, cognome, password, salt, nascita, citta, sesso, email, vive_id_ssp) VALUES ('PTNTSH44L51L736S','Tonisha','Putnam','ef8db381a2e70602039922fded47043860983643a5262931e6c65b9718ef29c6','b79be76595eb2f671841c8d68cc6fe7fb756f03186098d227cce38dba1d8f7ed','1928-07-29','Firenze',False,'Tonisha.Putnam@gmail.com',106);
INSERT INTO Persona(codF, nome, cognome, password, salt, nascita, citta, sesso, email, vive_id_ssp) VALUES ('KCHRBC71B47F205Y','Rebeca','Keach','e154c5103a50166ba65f9faf26fc63b5c2fb6ea3c7ae43911758e87dbbea7cb8','2103c6e20323060961a13da8399ec3ee368ae2b953a5a65d428f196bd20f407a','1927-01-14','Trento',False,'Rebeca.Keach@gmail.com',46);
INSERT INTO Persona(codF, nome, cognome, password, salt, nascita, citta, sesso, email, vive_id_ssp) VALUES ('BLZVCN99H49L736J','Vincenza','Bolz','2c498a2de78592d54d93d9a5aee610cccefa7bdc2dc50b6d8c36ded8e784a53f','8ea0287e51980c7ede42b1b0636bd583a98d9dd4d8eddf418d56e649b80d7f98','1942-02-02','Bolzano',False,'Vincenza.Bolz@gmail.com',21);
INSERT INTO Persona(codF, nome, cognome, password, salt, nascita, citta, sesso, email, vive_id_ssp) VALUES ('STNHNY15S53L219N','Honey','Stanbery','33f5796c397c2f4fd048a10ab743cba567ddd0b91fd11354ff834aa1295bc957','1064f3c52ecdf968ebe97528cffcaeafe47b993cb36d34a65b189de216d24709','1955-01-05','Venezia',False,'Honey.Stanbery@gmail.com',72);
INSERT INTO Persona(codF, nome, cognome, password, salt, nascita, citta, sesso, email, vive_id_ssp) VALUES ('RTZLRS07S19D643C','Loris','Ritz','05249bb3efdada2898bce293e57d286f175b0b6e087fb3da271d320f8bf14fd8','13d137a6ff54b05da432c6d78261b39811d4a08a7f48fd171b74baeee422f068','1981-05-17','Firenze',False,'Loris.Ritz@gmail.com',18);
INSERT INTO Persona(codF, nome, cognome, password, salt, nascita, citta, sesso, email, vive_id_ssp) VALUES ('RSNDDR13S04D643W','Dedra','Rosenberg','3d7f4c5e15b1d6896ea8fa219856951de48ef665b01804bb64108edcec43eceb','7780fd3b08254f77d93e4cf8cb9fea473699e5cb8cd116c966bdcd1b668044f5','1923-03-14','Rimini',True,'Dedra.Rosenberg@gmail.com',46);
INSERT INTO Persona(codF, nome, cognome, password, salt, nascita, citta, sesso, email, vive_id_ssp) VALUES ('LCNSNG82H16I452V','Song','Laconte','7e98e871ad82d80a190b0145c30eefbd1d258af8c96f341f3fd822036046c41a','3f74c7dd7adfe2670b5199d1d39ba5e3dbd8148292117a2252c5430d4c204fcd','2009-02-16','Rimini',False,'Song.Laconte@gmail.com',50);
INSERT INTO Persona(codF, nome, cognome, password, salt, nascita, citta, sesso, email, vive_id_ssp) VALUES ('LSUNCK20B20A952B','Nickie','Luse','29bf37556bcc62c237d059d75a7cb00f2316d01c7e5033ce4d838d0d78997209','e84e9e73418b762627cf96046dc69767ef6e48f5a3cfda23f6cb43e42cb5f73b','1921-02-07','Siracusa',True,'Nickie.Luse@gmail.com',5);
INSERT INTO Persona(codF, nome, cognome, password, salt, nascita, citta, sesso, email, vive_id_ssp) VALUES ('CPNLRS70M05L736P','Latrisha','Capuano','7a4535cb28499b8d48866bc3bfaa329420f28949a0013d7418d83aa83f0ebbb4','0c220e3897c6ea351edf4c35585b07c8e8fbe0ef571be4cf0a6ddfbb12675663','1991-09-09','Rimini',False,'Latrisha.Capuano@gmail.com',97);
INSERT INTO Persona(codF, nome, cognome, password, salt, nascita, citta, sesso, email, vive_id_ssp) VALUES ('HBRMDT70D04H018V','Merideth','Haberle','d62d8852d288120fe2560de51c3fa772ad08dfe8db7cef4c8f51bcf0a5d35296','6426a589d8d187c19f69057451a3920162aba3fba17d4cd4f446e0048a921b4e','1987-12-22','Bolzano',False,'Merideth.Haberle@gmail.com',84);
INSERT INTO Persona(codF, nome, cognome, password, salt, nascita, citta, sesso, email, vive_id_ssp) VALUES ('MNSTNK42M26D548V','Taneka','Manus','b5db607cbb15bcb9ca365b7bf24da5c657e847a6f9b6c8c005465ef75882843f','caaec32011f22b6e0d6ec746a5a2a66b5bd280fe3e63070d949388355078c079','1995-04-29','Venezia',True,'Taneka.Manus@gmail.com',24);
INSERT INTO Persona(codF, nome, cognome, password, salt, nascita, citta, sesso, email, vive_id_ssp) VALUES ('MSSSRT29M66H294U','Sharita','Messerly','a820fa6aaf4407c6592a72cf0dd85d969625effcf2a863d8ae01bb8df74d570c','99f7dc8db3c39f3c7d2144dcf69ccda78bfc78823810e4ad751d6b0bb448b13e','1991-07-28','Trento',False,'Sharita.Messerly@gmail.com',18);
INSERT INTO Persona(codF, nome, cognome, password, salt, nascita, citta, sesso, email, vive_id_ssp) VALUES ('HMNNTR02A61H018B','Anitra','Ehmann','e58cafab01897a5086d44dc9250a015c6446750d6afeeb24950a2fb54072c12c','e4ec874d83d837345e45f727c552e986dffbc7ea24281d28fdf3f4cd4580ebf1','2016-07-25','Ancona',False,'Anitra.Ehmann@gmail.com',52);
INSERT INTO Persona(codF, nome, cognome, password, salt, nascita, citta, sesso, email, vive_id_ssp) VALUES ('MNTLGU31R21A952K','Luigi','Menter','594b57e5d0d96ad2acee6e3854c158245db9a87b0879d7fb0befa31da53ec6b6','5faac09bdbf4e6fe1bc587ac674f3cccccc01836d2c2c6f324a315d108251a01','1917-04-01','Trento',True,'Luigi.Menter@gmail.com',35);
INSERT INTO Persona(codF, nome, cognome, password, salt, nascita, citta, sesso, email, vive_id_ssp) VALUES ('GSSYKK73C52A944U','Yukiko','Gossman','e34cd73ca40db1f0e0fe30014323de3899cdda87a0dc823c9a496fea14eeb010','4a9aeb7cc03ed2ea779e7ac6ad760b663e2805371cc7283fa31e892f513e5573','1925-05-10','Venezia',True,'Yukiko.Gossman@gmail.com',82);
INSERT INTO Persona(codF, nome, cognome, password, salt, nascita, citta, sesso, email, vive_id_ssp) VALUES ('RKWLDR51S05B354G','Leandro','Rakowski','89e7a745afdbbdb13ae47dc15995935a3e1f2ede99e77c5f13dcbd065a5ed40a','6e0256535b0450df6040aa042e94bf58e928b3a71154f3cabad10f4f2b9eaba9','1924-01-03','Venezia',False,'Leandro.Rakowski@gmail.com',1);
INSERT INTO Persona(codF, nome, cognome, password, salt, nascita, citta, sesso, email, vive_id_ssp) VALUES ('WTKSLN15A66L736V','Suellen','Witek','1034d84052ae7d35c0da83efb66957ae85e44c77f7142104b88f1b9786f7dcd8','86e6a4c908c0853831e18fe602ee401a84fb8a80cc8389100698c3fd77b045d6','1959-03-17','Venezia',True,'Suellen.Witek@gmail.com',22);
INSERT INTO Persona(codF, nome, cognome, password, salt, nascita, citta, sesso, email, vive_id_ssp) VALUES ('SHTRBN15A26H018K','Reuben','Shott','da01f54175bbbe5e69239cf9fc2aa48a0630ca377a91fdaab7e31937d18e6641','026e1136d6c5102706828b8864dbca53bfb349f6a255eac8f87d7a76ebbafbd9','1994-04-08','Ancona',True,'Reuben.Shott@gmail.com',19);
INSERT INTO Persona(codF, nome, cognome, password, salt, nascita, citta, sesso, email, vive_id_ssp) VALUES ('GLDWNN53D24L736M','Winnie','Gauldin','1123860f51de67d40cba80f06105df51f05d7817b369a26de1ce054295a637c6','16fb1d970a5f3b6bf87b2d96fd997d6edcd080978f16b79a55337c2e9b3817ef','2017-07-19','Trento',False,'Winnie.Gauldin@gmail.com',4);
INSERT INTO Persona(codF, nome, cognome, password, salt, nascita, citta, sesso, email, vive_id_ssp) VALUES ('LNDCMN39R47H018A','Carmina','Lundquist','c215ed424e36b51aa36a48e109fdc47c19bceb17d82b01debdb7f8cad249fa08','ad9df8a406c946683331f922b6271836651b70e512f6db657221195fffc55165','1991-08-13','Siracusa',False,'Carmina.Lundquist@gmail.com',52);
INSERT INTO Persona(codF, nome, cognome, password, salt, nascita, citta, sesso, email, vive_id_ssp) VALUES ('SNMTMK71T47A271Z','Tomiko','Eisenmann','275559070a6159a640f6f32e10d7336efadc15c46ce3ec1f3c7dff48d2fad220','e184bb86bbb68fd756f2aa6104a4dba2bb2a8db99f2fd1136432114b5d515314','2017-04-19','Ancona',False,'Tomiko.Eisenmann@gmail.com',89);






INSERT INTO foto(id_persona,data,path,nome,scelta) VALUES ('WNOSHN70B22I754B','2018-07-18','/stylegan-master/foto_database/1m_faces_sample/','1.jpg', 'True');
INSERT INTO foto(id_persona,data,path,nome,scelta) VALUES ('WTKCRG49T20E506W','2019-03-29','/stylegan-master/foto_database/1m_faces_sample/','2.jpg', 'True');
INSERT INTO foto(id_persona,data,path,nome,scelta) VALUES ('PFNJSH39A02B354W','2018-10-24','/stylegan-master/foto_database/1m_faces_sample/','3.jpg', 'True');
INSERT INTO foto(id_persona,data,path,nome,scelta) VALUES ('DNWLHW52C64A390O','2017-10-08','/stylegan-master/foto_database/1m_faces_sample/','4.jpg', 'True');
INSERT INTO foto(id_persona,data,path,nome,scelta) VALUES ('WRNCST48B59A285F','2019-03-31','/stylegan-master/foto_database/1m_faces_sample/','5.jpg', 'True');
INSERT INTO foto(id_persona,data,path,nome,scelta) VALUES ('RNNGNY95R55A390E','2017-03-01','/stylegan-master/foto_database/1m_faces_sample/','6.jpg', 'True');
INSERT INTO foto(id_persona,data,path,nome,scelta) VALUES ('GLLVRE66T56I754B','2019-09-29','/stylegan-master/foto_database/1m_faces_sample/','7.jpg', 'True');
INSERT INTO foto(id_persona,data,path,nome,scelta) VALUES ('FSADDA18M24H294F','2019-12-29','/stylegan-master/foto_database/1m_faces_sample/','8.jpg', 'True');
INSERT INTO foto(id_persona,data,path,nome,scelta) VALUES ('BSCSKU32H20A944W','2019-08-11','/stylegan-master/foto_database/1m_faces_sample/','9.jpg', 'True');
INSERT INTO foto(id_persona,data,path,nome,scelta) VALUES ('JQNBBR92B56L117R','2019-01-24','/stylegan-master/foto_database/1m_faces_sample/','10.jpg', 'True');
INSERT INTO foto(id_persona,data,path,nome,scelta) VALUES ('MKCLNT98M45E625Z','2019-12-08','/stylegan-master/foto_database/1m_faces_sample/','11.jpg', 'True');
INSERT INTO foto(id_persona,data,path,nome,scelta) VALUES ('SGDDMR77L60L736O','2018-09-17','/stylegan-master/foto_database/1m_faces_sample/','12.jpg', 'True');
INSERT INTO foto(id_persona,data,path,nome,scelta) VALUES ('DMMSNN39P60A390B','2017-08-05','/stylegan-master/foto_database/1m_faces_sample/','13.jpg', 'True');
INSERT INTO foto(id_persona,data,path,nome,scelta) VALUES ('WLSSPH35C43G478M','2017-04-25','/stylegan-master/foto_database/1m_faces_sample/','14.jpg', 'True');
INSERT INTO foto(id_persona,data,path,nome,scelta) VALUES ('VRLTNN73R50F205K','2019-11-09','/stylegan-master/foto_database/1m_faces_sample/','15.jpg', 'True');
INSERT INTO foto(id_persona,data,path,nome,scelta) VALUES ('LTHMRN58R54A271Y','2018-05-08','/stylegan-master/foto_database/1m_faces_sample/','16.jpg', 'True');
INSERT INTO foto(id_persona,data,path,nome,scelta) VALUES ('DHMLYN56P05L483U','2017-05-09','/stylegan-master/foto_database/1m_faces_sample/','17.jpg', 'True');
INSERT INTO foto(id_persona,data,path,nome,scelta) VALUES ('CLLCSL66P15I754S','2017-10-01','/stylegan-master/foto_database/1m_faces_sample/','18.jpg', 'True');
INSERT INTO foto(id_persona,data,path,nome,scelta) VALUES ('GBLMLS49T56A952H','2019-12-11','/stylegan-master/foto_database/1m_faces_sample/','19.jpg', 'True');
INSERT INTO foto(id_persona,data,path,nome,scelta) VALUES ('MCNSRY97B44E472A','2018-04-13','/stylegan-master/foto_database/1m_faces_sample/','20.jpg', 'True');
INSERT INTO foto(id_persona,data,path,nome,scelta) VALUES ('BRYLWN05P02F704C','2019-01-28','/stylegan-master/foto_database/1m_faces_sample/','21.jpg', 'True');
INSERT INTO foto(id_persona,data,path,nome,scelta) VALUES ('GRNLRN37P68H199H','2019-02-02','/stylegan-master/foto_database/1m_faces_sample/','22.jpg', 'True');
INSERT INTO foto(id_persona,data,path,nome,scelta) VALUES ('BFFGTH35H45F839J','2017-07-29','/stylegan-master/foto_database/1m_faces_sample/','23.jpg', 'True');
INSERT INTO foto(id_persona,data,path,nome,scelta) VALUES ('MSSLSI02L55H294E','2018-12-18','/stylegan-master/foto_database/1m_faces_sample/','24.jpg', 'True');
INSERT INTO foto(id_persona,data,path,nome,scelta) VALUES ('BRRCTR59L23A944X','2019-04-04','/stylegan-master/foto_database/1m_faces_sample/','25.jpg', 'True');
INSERT INTO foto(id_persona,data,path,nome,scelta) VALUES ('KNCDEI38M15D612H','2018-12-06','/stylegan-master/foto_database/1m_faces_sample/','26.jpg', 'True');
INSERT INTO foto(id_persona,data,path,nome,scelta) VALUES ('WHLJMM16T04H294B','2018-12-18','/stylegan-master/foto_database/1m_faces_sample/','27.jpg', 'True');
INSERT INTO foto(id_persona,data,path,nome,scelta) VALUES ('STRMLY40R68E625Z','2019-01-21','/stylegan-master/foto_database/1m_faces_sample/','28.jpg', 'True');
INSERT INTO foto(id_persona,data,path,nome,scelta) VALUES ('DFFGTR01E41C351N','2018-10-26','/stylegan-master/foto_database/1m_faces_sample/','29.jpg', 'True');
INSERT INTO foto(id_persona,data,path,nome,scelta) VALUES ('TCHSRN68L30I452A','2017-03-15','/stylegan-master/foto_database/1m_faces_sample/','30.jpg', 'True');
INSERT INTO foto(id_persona,data,path,nome,scelta) VALUES ('LZLPGE98L18G482S','2019-10-24','/stylegan-master/foto_database/1m_faces_sample/','31.jpg', 'True');
INSERT INTO foto(id_persona,data,path,nome,scelta) VALUES ('CYLSWN76D60A952Y','2019-04-28','/stylegan-master/foto_database/1m_faces_sample/','32.jpg', 'True');
INSERT INTO foto(id_persona,data,path,nome,scelta) VALUES ('CRMTTH28C64A285J','2019-10-23','/stylegan-master/foto_database/1m_faces_sample/','33.jpg', 'True');
INSERT INTO foto(id_persona,data,path,nome,scelta) VALUES ('SNDMDE91P66F952H','2017-12-26','/stylegan-master/foto_database/1m_faces_sample/','34.jpg', 'True');
INSERT INTO foto(id_persona,data,path,nome,scelta) VALUES ('MCHNRD18M67E472F','2018-03-04','/stylegan-master/foto_database/1m_faces_sample/','35.jpg', 'True');
INSERT INTO foto(id_persona,data,path,nome,scelta) VALUES ('NEULPU14D09H294G','2017-12-31','/stylegan-master/foto_database/1m_faces_sample/','36.jpg', 'True');
INSERT INTO foto(id_persona,data,path,nome,scelta) VALUES ('PTTCST16C19F839U','2018-12-26','/stylegan-master/foto_database/1m_faces_sample/','37.jpg', 'True');
INSERT INTO foto(id_persona,data,path,nome,scelta) VALUES ('NNOTNO53M05G999T','2018-08-31','/stylegan-master/foto_database/1m_faces_sample/','38.jpg', 'True');
INSERT INTO foto(id_persona,data,path,nome,scelta) VALUES ('CDODTT60E08F205R','2018-08-03','/stylegan-master/foto_database/1m_faces_sample/','39.jpg', 'True');
INSERT INTO foto(id_persona,data,path,nome,scelta) VALUES ('CRTFLC16B24I754Z','2018-06-05','/stylegan-master/foto_database/1m_faces_sample/','40.jpg', 'True');
INSERT INTO foto(id_persona,data,path,nome,scelta) VALUES ('SKNDRN90D70A271K','2019-10-23','/stylegan-master/foto_database/1m_faces_sample/','41.jpg', 'True');
INSERT INTO foto(id_persona,data,path,nome,scelta) VALUES ('CDYMAE48C70D643E','2017-06-08','/stylegan-master/foto_database/1m_faces_sample/','42.jpg', 'True');
INSERT INTO foto(id_persona,data,path,nome,scelta) VALUES ('VHLKMI82E56C351R','2019-01-07','/stylegan-master/foto_database/1m_faces_sample/','43.jpg', 'True');
INSERT INTO foto(id_persona,data,path,nome,scelta) VALUES ('LBRGGN44S54F952S','2019-06-08','/stylegan-master/foto_database/1m_faces_sample/','44.jpg', 'True');
INSERT INTO foto(id_persona,data,path,nome,scelta) VALUES ('BRKDLC83C54H199C','2017-02-15','/stylegan-master/foto_database/1m_faces_sample/','45.jpg', 'True');
INSERT INTO foto(id_persona,data,path,nome,scelta) VALUES ('SRCSLN16P53G535V','2017-08-03','/stylegan-master/foto_database/1m_faces_sample/','46.jpg', 'True');
INSERT INTO foto(id_persona,data,path,nome,scelta) VALUES ('HSLBRN00P41G337Y','2017-04-24','/stylegan-master/foto_database/1m_faces_sample/','47.jpg', 'True');
INSERT INTO foto(id_persona,data,path,nome,scelta) VALUES ('ZNKDND14H43H703X','2017-02-10','/stylegan-master/foto_database/1m_faces_sample/','48.jpg', 'True');
INSERT INTO foto(id_persona,data,path,nome,scelta) VALUES ('MZRVVT11M18G535T','2017-09-18','/stylegan-master/foto_database/1m_faces_sample/','49.jpg', 'True');
INSERT INTO foto(id_persona,data,path,nome,scelta) VALUES ('ZGRKSH76T44A944U','2018-11-01','/stylegan-master/foto_database/1m_faces_sample/','50.jpg', 'True');
INSERT INTO foto(id_persona,data,path,nome,scelta) VALUES ('PLRFRD55R12D643V','2017-11-15','/stylegan-master/foto_database/1m_faces_sample/','51.jpg', 'True');
INSERT INTO foto(id_persona,data,path,nome,scelta) VALUES ('PLNBEA25D60F952A','2019-08-09','/stylegan-master/foto_database/1m_faces_sample/','52.jpg', 'True');
INSERT INTO foto(id_persona,data,path,nome,scelta) VALUES ('DLBGGN98C42F704W','2018-08-13','/stylegan-master/foto_database/1m_faces_sample/','53.jpg', 'True');
INSERT INTO foto(id_persona,data,path,nome,scelta) VALUES ('CHPMRV39T43H018V','2017-05-30','/stylegan-master/foto_database/1m_faces_sample/','54.jpg', 'True');
INSERT INTO foto(id_persona,data,path,nome,scelta) VALUES ('SDGDSR11P26D643I','2019-01-29','/stylegan-master/foto_database/1m_faces_sample/','55.jpg', 'True');
INSERT INTO foto(id_persona,data,path,nome,scelta) VALUES ('SHCLNN49C50H199E','2017-10-07','/stylegan-master/foto_database/1m_faces_sample/','56.jpg', 'True');
INSERT INTO foto(id_persona,data,path,nome,scelta) VALUES ('CMNSRL85A45F158P','2017-07-26','/stylegan-master/foto_database/1m_faces_sample/','57.jpg', 'True');
INSERT INTO foto(id_persona,data,path,nome,scelta) VALUES ('CLLJNY48L53C573F','2019-03-25','/stylegan-master/foto_database/1m_faces_sample/','58.jpg', 'True');
INSERT INTO foto(id_persona,data,path,nome,scelta) VALUES ('TRSZLL64M42D612X','2018-10-02','/stylegan-master/foto_database/1m_faces_sample/','59.jpg', 'True');
INSERT INTO foto(id_persona,data,path,nome,scelta) VALUES ('SHPPRN92S69H018S','2018-03-13','/stylegan-master/foto_database/1m_faces_sample/','60.jpg', 'True');
INSERT INTO foto(id_persona,data,path,nome,scelta) VALUES ('VCKLND99S61A794B','2018-02-27','/stylegan-master/foto_database/1m_faces_sample/','61.jpg', 'True');
INSERT INTO foto(id_persona,data,path,nome,scelta) VALUES ('KNSKVN32H10E054D','2018-02-21','/stylegan-master/foto_database/1m_faces_sample/','62.jpg', 'True');
INSERT INTO foto(id_persona,data,path,nome,scelta) VALUES ('LFRMYL51D65A952F','2019-12-28','/stylegan-master/foto_database/1m_faces_sample/','63.jpg', 'True');
INSERT INTO foto(id_persona,data,path,nome,scelta) VALUES ('CLCMLY49B67A271K','2017-01-22','/stylegan-master/foto_database/1m_faces_sample/','64.jpg', 'True');
INSERT INTO foto(id_persona,data,path,nome,scelta) VALUES ('BRGSRN46B57D612C','2018-04-22','/stylegan-master/foto_database/1m_faces_sample/','65.jpg', 'True');
INSERT INTO foto(id_persona,data,path,nome,scelta) VALUES ('FRRMLN99B67L049B','2017-04-03','/stylegan-master/foto_database/1m_faces_sample/','66.jpg', 'True');
INSERT INTO foto(id_persona,data,path,nome,scelta) VALUES ('BSQDPH23M50G224N','2017-06-24','/stylegan-master/foto_database/1m_faces_sample/','67.jpg', 'True');
INSERT INTO foto(id_persona,data,path,nome,scelta) VALUES ('JLNSRY27L57F205H','2017-04-23','/stylegan-master/foto_database/1m_faces_sample/','68.jpg', 'True');
INSERT INTO foto(id_persona,data,path,nome,scelta) VALUES ('DGGKCY43A63F158S','2017-03-22','/stylegan-master/foto_database/1m_faces_sample/','69.jpg', 'True');
INSERT INTO foto(id_persona,data,path,nome,scelta) VALUES ('CCOPHL05L62A952J','2018-03-05','/stylegan-master/foto_database/1m_faces_sample/','70.jpg', 'True');
INSERT INTO foto(id_persona,data,path,nome,scelta) VALUES ('SPKMNL56D43L049Q','2017-05-10','/stylegan-master/foto_database/1m_faces_sample/','71.jpg', 'True');
INSERT INTO foto(id_persona,data,path,nome,scelta) VALUES ('BLSNKL96E43F839O','2018-09-20','/stylegan-master/foto_database/1m_faces_sample/','72.jpg', 'True');
INSERT INTO foto(id_persona,data,path,nome,scelta) VALUES ('MRRJLY71L06I754A','2017-11-19','/stylegan-master/foto_database/1m_faces_sample/','73.jpg', 'True');
INSERT INTO foto(id_persona,data,path,nome,scelta) VALUES ('GBBJYO33H27L049Q','2018-07-10','/stylegan-master/foto_database/1m_faces_sample/','74.jpg', 'True');
INSERT INTO foto(id_persona,data,path,nome,scelta) VALUES ('JWRNNC84E59H018Q','2017-12-21','/stylegan-master/foto_database/1m_faces_sample/','75.jpg', 'True');
INSERT INTO foto(id_persona,data,path,nome,scelta) VALUES ('BRMDLC18M57I452P','2019-11-19','/stylegan-master/foto_database/1m_faces_sample/','76.jpg', 'True');
INSERT INTO foto(id_persona,data,path,nome,scelta) VALUES ('MRNFND60C68C573B','2017-11-15','/stylegan-master/foto_database/1m_faces_sample/','77.jpg', 'True');
INSERT INTO foto(id_persona,data,path,nome,scelta) VALUES ('CGLGLN56M25L219Y','2018-02-13','/stylegan-master/foto_database/1m_faces_sample/','78.jpg', 'True');
INSERT INTO foto(id_persona,data,path,nome,scelta) VALUES ('WDLKZY17H42A271B','2018-02-03','/stylegan-master/foto_database/1m_faces_sample/','79.jpg', 'True');
INSERT INTO foto(id_persona,data,path,nome,scelta) VALUES ('PTNTSH44L51L736S','2019-08-26','/stylegan-master/foto_database/1m_faces_sample/','80.jpg', 'True');
INSERT INTO foto(id_persona,data,path,nome,scelta) VALUES ('KCHRBC71B47F205Y','2019-09-01','/stylegan-master/foto_database/1m_faces_sample/','81.jpg', 'True');
INSERT INTO foto(id_persona,data,path,nome,scelta) VALUES ('BLZVCN99H49L736J','2019-05-10','/stylegan-master/foto_database/1m_faces_sample/','82.jpg', 'True');
INSERT INTO foto(id_persona,data,path,nome,scelta) VALUES ('STNHNY15S53L219N','2019-09-11','/stylegan-master/foto_database/1m_faces_sample/','83.jpg', 'True');
INSERT INTO foto(id_persona,data,path,nome,scelta) VALUES ('RTZLRS07S19D643C','2019-08-28','/stylegan-master/foto_database/1m_faces_sample/','84.jpg', 'True');
INSERT INTO foto(id_persona,data,path,nome,scelta) VALUES ('RSNDDR13S04D643W','2017-11-29','/stylegan-master/foto_database/1m_faces_sample/','85.jpg', 'True');
INSERT INTO foto(id_persona,data,path,nome,scelta) VALUES ('LCNSNG82H16I452V','2017-02-11','/stylegan-master/foto_database/1m_faces_sample/','86.jpg', 'True');
INSERT INTO foto(id_persona,data,path,nome,scelta) VALUES ('LSUNCK20B20A952B','2017-02-06','/stylegan-master/foto_database/1m_faces_sample/','87.jpg', 'True');
INSERT INTO foto(id_persona,data,path,nome,scelta) VALUES ('CPNLRS70M05L736P','2019-08-16','/stylegan-master/foto_database/1m_faces_sample/','88.jpg', 'True');
INSERT INTO foto(id_persona,data,path,nome,scelta) VALUES ('HBRMDT70D04H018V','2018-04-28','/stylegan-master/foto_database/1m_faces_sample/','89.jpg', 'True');
INSERT INTO foto(id_persona,data,path,nome,scelta) VALUES ('MNSTNK42M26D548V','2017-10-06','/stylegan-master/foto_database/1m_faces_sample/','90.jpg', 'True');
INSERT INTO foto(id_persona,data,path,nome,scelta) VALUES ('MSSSRT29M66H294U','2018-12-21','/stylegan-master/foto_database/1m_faces_sample/','91.jpg', 'True');
INSERT INTO foto(id_persona,data,path,nome,scelta) VALUES ('HMNNTR02A61H018B','2017-11-08','/stylegan-master/foto_database/1m_faces_sample/','92.jpg', 'True');
INSERT INTO foto(id_persona,data,path,nome,scelta) VALUES ('MNTLGU31R21A952K','2019-04-27','/stylegan-master/foto_database/1m_faces_sample/','93.jpg', 'True');
INSERT INTO foto(id_persona,data,path,nome,scelta) VALUES ('GSSYKK73C52A944U','2017-10-31','/stylegan-master/foto_database/1m_faces_sample/','94.jpg', 'True');
INSERT INTO foto(id_persona,data,path,nome,scelta) VALUES ('RKWLDR51S05B354G','2018-11-21','/stylegan-master/foto_database/1m_faces_sample/','95.jpg', 'True');
INSERT INTO foto(id_persona,data,path,nome,scelta) VALUES ('WTKSLN15A66L736V','2017-01-31','/stylegan-master/foto_database/1m_faces_sample/','96.jpg', 'True');
INSERT INTO foto(id_persona,data,path,nome,scelta) VALUES ('SHTRBN15A26H018K','2017-08-24','/stylegan-master/foto_database/1m_faces_sample/','97.jpg', 'True');
INSERT INTO foto(id_persona,data,path,nome,scelta) VALUES ('GLDWNN53D24L736M','2018-08-04','/stylegan-master/foto_database/1m_faces_sample/','98.jpg', 'True');
INSERT INTO foto(id_persona,data,path,nome,scelta) VALUES ('LNDCMN39R47H018A','2018-01-03','/stylegan-master/foto_database/1m_faces_sample/','99.jpg', 'True');
INSERT INTO foto(id_persona,data,path,nome,scelta) VALUES ('SNMTMK71T47A271Z','2018-10-11','/stylegan-master/foto_database/1m_faces_sample/','100.jpg', 'True');








insert into medicobase(codF, lavora_id_ssp) values('WNOSHN70B22I754B', 98);
insert into medicobase(codF, lavora_id_ssp) values('DHMLYN56P05L483U', 82);
insert into medicobase(codF, lavora_id_ssp) values('KNCDEI38M15D612H', 34);
insert into medicobase(codF, lavora_id_ssp) values('MSSLSI02L55H294E', 15);
insert into medicobase(codF, lavora_id_ssp) values('WTKSLN15A66L736V', 103);
insert into medicobase(codF, lavora_id_ssp) values('BRYLWN05P02F704C', 90);
insert into medicobase(codF, lavora_id_ssp) values('CYLSWN76D60A952Y', 2);
















insert into medicospec(codF) values('MSSLSI02L55H294E');
insert into medicospec(codF) values('ZNKDND14H43H703X');
insert into medicospec(codF) values('MZRVVT11M18G535T');
insert into medicospec(codF) values('RTZLRS07S19D643C');
insert into medicospec(codF) values('MNSTNK42M26D548V');














INSERT INTO esame(nome) VALUES ('Estrazione di dente deciduo');
INSERT INTO esame(nome) VALUES ('Estrazione di dente permanente');
INSERT INTO esame(nome) VALUES ('Altra estrazione chirurgica dente');
INSERT INTO esame(nome) VALUES ('Ricostruzione dente con otturazione');
INSERT INTO esame(nome) VALUES ('Ricostruzione dente mediante intarsio');
INSERT INTO esame(nome) VALUES ('Applicazione di corona');
INSERT INTO esame(nome) VALUES ('Applicazione di corona in lega aurea');
INSERT INTO esame(nome) VALUES ('Altra applicazione corona');
INSERT INTO esame(nome) VALUES ('Applicazione corona e perno');
INSERT INTO esame(nome) VALUES ('Altra applicazione corona e perno');
INSERT INTO esame(nome) VALUES ('Inserzione di ponte fisso');
INSERT INTO esame(nome) VALUES ('Inserzione di protesi rimovibile');
INSERT INTO esame(nome) VALUES ('Altra inserzione di protesi');
INSERT INTO esame(nome) VALUES ('Inserzione di protesi provvisoria');
INSERT INTO esame(nome) VALUES ('Altra riparazione dentaria');
INSERT INTO esame(nome) VALUES ('Impianto di dente');
INSERT INTO esame(nome) VALUES ('Impianto di protesi dentaria');
INSERT INTO esame(nome) VALUES ('Terapia canalare in monoradicolato');
INSERT INTO esame(nome) VALUES ('Terapia canalare in pluriradicolato');
INSERT INTO esame(nome) VALUES ('Apicectomia');
INSERT INTO esame(nome) VALUES ('Interventi su denti, gengive e alveoli');
INSERT INTO esame(nome) VALUES ('Gengivoplastica');
INSERT INTO esame(nome) VALUES ('Asportazione di tessuto della gengiva');
INSERT INTO esame(nome) VALUES ('Levigatura delle radici');
INSERT INTO esame(nome) VALUES ('Intervento chirurgico preprotesico');
INSERT INTO esame(nome) VALUES ('Trattamento con apparecchi fissi');
INSERT INTO esame(nome) VALUES ('Trattamento con apparecchi funzionali');
INSERT INTO esame(nome) VALUES ('Riparazione di apparecchio ortodontico');
INSERT INTO esame(nome) VALUES ('Radiologia diagnostica');
INSERT INTO esame(nome) VALUES ('Tomografia computerizzata del rachide');
INSERT INTO esame(nome) VALUES ('Tomografia computerizzata con contrasto');
INSERT INTO esame(nome) VALUES ('Densitometria ossera');
INSERT INTO esame(nome) VALUES ('Test resistenza proteina C ');
INSERT INTO esame(nome) VALUES ('ipizzazione genomica ');
INSERT INTO esame(nome) VALUES ('ipizzazione genomica HLA – B ');
INSERT INTO esame(nome) VALUES ('ipizzazione genomica HLA – C ');
INSERT INTO esame(nome) VALUES ('Tipizzazione sierologica HLA classe I');
INSERT INTO esame(nome) VALUES ('Tipizzazione sierologica HLA classe II');
INSERT INTO esame(nome) VALUES ('Analisi DNA e ibridazione con sonda');
INSERT INTO esame(nome) VALUES ('Analisi DNA per polimorfismo');
INSERT INTO esame(nome) VALUES ('Analisi di polimorfismi');
INSERT INTO esame(nome) VALUES ('Analisi di segmenti di DNA');
INSERT INTO esame(nome) VALUES ('Digestione DNA con enzimi');
INSERT INTO esame(nome) VALUES ('Estrazione DNA o RNA');
INSERT INTO esame(nome) VALUES ('Ibridazione con sonda molecolare');
INSERT INTO esame(nome) VALUES ('Ibridazione in SITU');
INSERT INTO esame(nome) VALUES ('Ricerca Mutazione');
INSERT INTO esame(nome) VALUES ('Analisi DNA studio citometrico');
INSERT INTO esame(nome) VALUES ('Tomoscintigrafia Cerebrale');
INSERT INTO esame(nome) VALUES ('Radioterapia stereotassica');
INSERT INTO esame(nome) VALUES ('Irradiazione cutanea');
INSERT INTO esame(nome) VALUES ('Tomoscintigrafia globale');
INSERT INTO esame(nome) VALUES ('Irradiazione cutanea');
INSERT INTO esame(nome) VALUES ('Terapia del dolore da metastasi ossee');
INSERT INTO esame(nome) VALUES ('Ablazione tartaro');
INSERT INTO esame(nome) VALUES ('Sigillatura solchi e fossette');
INSERT INTO esame(nome) VALUES ('Rimozione protesi dentarie');
INSERT INTO esame(nome) VALUES ('Immunizzazione allergia');
INSERT INTO esame(nome) VALUES ('Immunizzazione malattia autoimmune');
INSERT INTO esame(nome) VALUES ('Terapia luce ultraviolett');









INSERT INTO cartellaclinicapermb(datainizio,datafine,id_medicobase,id_persona,vistomedicobase) VALUES ('1945-02-28', NULL,'WNOSHN70B22I754B','WNOSHN70B22I754B', 'True');
INSERT INTO cartellaclinicapermb(datainizio,datafine,id_medicobase,id_persona,vistomedicobase) VALUES ('1957-11-25', NULL,'DHMLYN56P05L483U','WTKCRG49T20E506W', 'True');
INSERT INTO cartellaclinicapermb(datainizio,datafine,id_medicobase,id_persona,vistomedicobase) VALUES ('2005-05-30', NULL,'KNCDEI38M15D612H','PFNJSH39A02B354W', 'True');
INSERT INTO cartellaclinicapermb(datainizio,datafine,id_medicobase,id_persona,vistomedicobase) VALUES ('1949-03-08', NULL,'DHMLYN56P05L483U','DNWLHW52C64A390O', 'True');
INSERT INTO cartellaclinicapermb(datainizio,datafine,id_medicobase,id_persona,vistomedicobase) VALUES ('1983-03-30', NULL,'WTKSLN15A66L736V','WRNCST48B59A285F', 'True');
INSERT INTO cartellaclinicapermb(datainizio,datafine,id_medicobase,id_persona,vistomedicobase) VALUES ('1996-08-13', NULL,'MSSLSI02L55H294E','RNNGNY95R55A390E', 'True');
INSERT INTO cartellaclinicapermb(datainizio,datafine,id_medicobase,id_persona,vistomedicobase) VALUES ('1953-03-23', NULL,'KNCDEI38M15D612H','GLLVRE66T56I754B', 'True');
INSERT INTO cartellaclinicapermb(datainizio,datafine,id_medicobase,id_persona,vistomedicobase) VALUES ('1930-03-15', NULL,'WTKSLN15A66L736V','FSADDA18M24H294F', 'True');
INSERT INTO cartellaclinicapermb(datainizio,datafine,id_medicobase,id_persona,vistomedicobase) VALUES ('1998-08-31', NULL,'CYLSWN76D60A952Y','BSCSKU32H20A944W', 'True');
INSERT INTO cartellaclinicapermb(datainizio,datafine,id_medicobase,id_persona,vistomedicobase) VALUES ('1944-04-18', NULL,'WTKSLN15A66L736V','JQNBBR92B56L117R', 'True');
INSERT INTO cartellaclinicapermb(datainizio,datafine,id_medicobase,id_persona,vistomedicobase) VALUES ('1985-04-02', NULL,'BRYLWN05P02F704C','MKCLNT98M45E625Z', 'True');
INSERT INTO cartellaclinicapermb(datainizio,datafine,id_medicobase,id_persona,vistomedicobase) VALUES ('1951-08-15', NULL,'WTKSLN15A66L736V','SGDDMR77L60L736O', 'True');
INSERT INTO cartellaclinicapermb(datainizio,datafine,id_medicobase,id_persona,vistomedicobase) VALUES ('1918-06-25', NULL,'BRYLWN05P02F704C','DMMSNN39P60A390B', 'True');
INSERT INTO cartellaclinicapermb(datainizio,datafine,id_medicobase,id_persona,vistomedicobase) VALUES ('1946-04-05', NULL,'WTKSLN15A66L736V','WLSSPH35C43G478M', 'True');
INSERT INTO cartellaclinicapermb(datainizio,datafine,id_medicobase,id_persona,vistomedicobase) VALUES ('1916-02-11', NULL,'CYLSWN76D60A952Y','VRLTNN73R50F205K', 'True');
INSERT INTO cartellaclinicapermb(datainizio,datafine,id_medicobase,id_persona,vistomedicobase) VALUES ('1985-06-03', NULL,'WTKSLN15A66L736V','LTHMRN58R54A271Y', 'True');
INSERT INTO cartellaclinicapermb(datainizio,datafine,id_medicobase,id_persona,vistomedicobase) VALUES ('1917-09-02', NULL,'DHMLYN56P05L483U','DHMLYN56P05L483U', 'True');
INSERT INTO cartellaclinicapermb(datainizio,datafine,id_medicobase,id_persona,vistomedicobase) VALUES ('1998-11-23', NULL,'BRYLWN05P02F704C','CLLCSL66P15I754S', 'True');
INSERT INTO cartellaclinicapermb(datainizio,datafine,id_medicobase,id_persona,vistomedicobase) VALUES ('1981-04-24', NULL,'KNCDEI38M15D612H','GBLMLS49T56A952H', 'True');
INSERT INTO cartellaclinicapermb(datainizio,datafine,id_medicobase,id_persona,vistomedicobase) VALUES ('1931-03-20', NULL,'DHMLYN56P05L483U','MCNSRY97B44E472A', 'True');
INSERT INTO cartellaclinicapermb(datainizio,datafine,id_medicobase,id_persona,vistomedicobase) VALUES ('1921-04-24', NULL,'BRYLWN05P02F704C','BRYLWN05P02F704C', 'True');
INSERT INTO cartellaclinicapermb(datainizio,datafine,id_medicobase,id_persona,vistomedicobase) VALUES ('1976-12-15', NULL,'MSSLSI02L55H294E','GRNLRN37P68H199H', 'True');
INSERT INTO cartellaclinicapermb(datainizio,datafine,id_medicobase,id_persona,vistomedicobase) VALUES ('1989-07-26', NULL,'WNOSHN70B22I754B','BFFGTH35H45F839J', 'True');
INSERT INTO cartellaclinicapermb(datainizio,datafine,id_medicobase,id_persona,vistomedicobase) VALUES ('1928-05-19', NULL,'MSSLSI02L55H294E','MSSLSI02L55H294E', 'True');
INSERT INTO cartellaclinicapermb(datainizio,datafine,id_medicobase,id_persona,vistomedicobase) VALUES ('2014-11-06', NULL,'WTKSLN15A66L736V','BRRCTR59L23A944X', 'True');
INSERT INTO cartellaclinicapermb(datainizio,datafine,id_medicobase,id_persona,vistomedicobase) VALUES ('1948-07-18', NULL,'KNCDEI38M15D612H','KNCDEI38M15D612H', 'True');
INSERT INTO cartellaclinicapermb(datainizio,datafine,id_medicobase,id_persona,vistomedicobase) VALUES ('1951-01-18', NULL,'MSSLSI02L55H294E','WHLJMM16T04H294B', 'True');
INSERT INTO cartellaclinicapermb(datainizio,datafine,id_medicobase,id_persona,vistomedicobase) VALUES ('1943-12-04', NULL,'KNCDEI38M15D612H','STRMLY40R68E625Z', 'True');
INSERT INTO cartellaclinicapermb(datainizio,datafine,id_medicobase,id_persona,vistomedicobase) VALUES ('1959-01-30', NULL,'BRYLWN05P02F704C','DFFGTR01E41C351N', 'True');
INSERT INTO cartellaclinicapermb(datainizio,datafine,id_medicobase,id_persona,vistomedicobase) VALUES ('1961-07-18', NULL,'MSSLSI02L55H294E','TCHSRN68L30I452A', 'True');
INSERT INTO cartellaclinicapermb(datainizio,datafine,id_medicobase,id_persona,vistomedicobase) VALUES ('2019-11-03', NULL,'CYLSWN76D60A952Y','LZLPGE98L18G482S', 'True');
INSERT INTO cartellaclinicapermb(datainizio,datafine,id_medicobase,id_persona,vistomedicobase) VALUES ('1992-10-01', NULL,'CYLSWN76D60A952Y','CYLSWN76D60A952Y', 'True');
INSERT INTO cartellaclinicapermb(datainizio,datafine,id_medicobase,id_persona,vistomedicobase) VALUES ('1921-06-09', NULL,'CYLSWN76D60A952Y','CRMTTH28C64A285J', 'True');
INSERT INTO cartellaclinicapermb(datainizio,datafine,id_medicobase,id_persona,vistomedicobase) VALUES ('1934-01-21', NULL,'WTKSLN15A66L736V','SNDMDE91P66F952H', 'True');
INSERT INTO cartellaclinicapermb(datainizio,datafine,id_medicobase,id_persona,vistomedicobase) VALUES ('1938-03-25', NULL,'WTKSLN15A66L736V','MCHNRD18M67E472F', 'True');
INSERT INTO cartellaclinicapermb(datainizio,datafine,id_medicobase,id_persona,vistomedicobase) VALUES ('2006-10-02', NULL,'WNOSHN70B22I754B','NEULPU14D09H294G', 'True');
INSERT INTO cartellaclinicapermb(datainizio,datafine,id_medicobase,id_persona,vistomedicobase) VALUES ('1965-10-06', NULL,'BRYLWN05P02F704C','PTTCST16C19F839U', 'True');
INSERT INTO cartellaclinicapermb(datainizio,datafine,id_medicobase,id_persona,vistomedicobase) VALUES ('1933-08-17', NULL,'CYLSWN76D60A952Y','NNOTNO53M05G999T', 'True');
INSERT INTO cartellaclinicapermb(datainizio,datafine,id_medicobase,id_persona,vistomedicobase) VALUES ('1919-11-20', NULL,'CYLSWN76D60A952Y','CDODTT60E08F205R', 'True');
INSERT INTO cartellaclinicapermb(datainizio,datafine,id_medicobase,id_persona,vistomedicobase) VALUES ('1914-09-18', NULL,'WNOSHN70B22I754B','CRTFLC16B24I754Z', 'True');
INSERT INTO cartellaclinicapermb(datainizio,datafine,id_medicobase,id_persona,vistomedicobase) VALUES ('2013-11-12', NULL,'BRYLWN05P02F704C','SKNDRN90D70A271K', 'True');
INSERT INTO cartellaclinicapermb(datainizio,datafine,id_medicobase,id_persona,vistomedicobase) VALUES ('1955-05-25', NULL,'CYLSWN76D60A952Y','CDYMAE48C70D643E', 'True');
INSERT INTO cartellaclinicapermb(datainizio,datafine,id_medicobase,id_persona,vistomedicobase) VALUES ('1968-05-01', NULL,'DHMLYN56P05L483U','VHLKMI82E56C351R', 'True');
INSERT INTO cartellaclinicapermb(datainizio,datafine,id_medicobase,id_persona,vistomedicobase) VALUES ('1936-09-03', NULL,'BRYLWN05P02F704C','LBRGGN44S54F952S', 'True');
INSERT INTO cartellaclinicapermb(datainizio,datafine,id_medicobase,id_persona,vistomedicobase) VALUES ('2003-09-13', NULL,'MSSLSI02L55H294E','BRKDLC83C54H199C', 'True');
INSERT INTO cartellaclinicapermb(datainizio,datafine,id_medicobase,id_persona,vistomedicobase) VALUES ('1990-12-15', NULL,'MSSLSI02L55H294E','SRCSLN16P53G535V', 'True');
INSERT INTO cartellaclinicapermb(datainizio,datafine,id_medicobase,id_persona,vistomedicobase) VALUES ('1950-09-03', NULL,'BRYLWN05P02F704C','HSLBRN00P41G337Y', 'True');
INSERT INTO cartellaclinicapermb(datainizio,datafine,id_medicobase,id_persona,vistomedicobase) VALUES ('1958-07-09', NULL,'MSSLSI02L55H294E','ZNKDND14H43H703X', 'True');
INSERT INTO cartellaclinicapermb(datainizio,datafine,id_medicobase,id_persona,vistomedicobase) VALUES ('1951-01-07', NULL,'DHMLYN56P05L483U','MZRVVT11M18G535T', 'True');
INSERT INTO cartellaclinicapermb(datainizio,datafine,id_medicobase,id_persona,vistomedicobase) VALUES ('1943-11-27', NULL,'CYLSWN76D60A952Y','ZGRKSH76T44A944U', 'True');
INSERT INTO cartellaclinicapermb(datainizio,datafine,id_medicobase,id_persona,vistomedicobase) VALUES ('2018-11-06', NULL,'MSSLSI02L55H294E','PLRFRD55R12D643V', 'True');
INSERT INTO cartellaclinicapermb(datainizio,datafine,id_medicobase,id_persona,vistomedicobase) VALUES ('1916-03-14', NULL,'MSSLSI02L55H294E','PLNBEA25D60F952A', 'True');
INSERT INTO cartellaclinicapermb(datainizio,datafine,id_medicobase,id_persona,vistomedicobase) VALUES ('2004-05-08', NULL,'CYLSWN76D60A952Y','DLBGGN98C42F704W', 'True');
INSERT INTO cartellaclinicapermb(datainizio,datafine,id_medicobase,id_persona,vistomedicobase) VALUES ('1970-07-01', NULL,'DHMLYN56P05L483U','CHPMRV39T43H018V', 'True');
INSERT INTO cartellaclinicapermb(datainizio,datafine,id_medicobase,id_persona,vistomedicobase) VALUES ('2005-10-22', NULL,'BRYLWN05P02F704C','SDGDSR11P26D643I', 'True');
INSERT INTO cartellaclinicapermb(datainizio,datafine,id_medicobase,id_persona,vistomedicobase) VALUES ('1956-09-01', NULL,'KNCDEI38M15D612H','SHCLNN49C50H199E', 'True');
INSERT INTO cartellaclinicapermb(datainizio,datafine,id_medicobase,id_persona,vistomedicobase) VALUES ('1938-10-14', NULL,'WTKSLN15A66L736V','CMNSRL85A45F158P', 'True');
INSERT INTO cartellaclinicapermb(datainizio,datafine,id_medicobase,id_persona,vistomedicobase) VALUES ('1942-12-05', NULL,'BRYLWN05P02F704C','CLLJNY48L53C573F', 'True');
INSERT INTO cartellaclinicapermb(datainizio,datafine,id_medicobase,id_persona,vistomedicobase) VALUES ('1993-03-01', NULL,'WNOSHN70B22I754B','TRSZLL64M42D612X', 'True');
INSERT INTO cartellaclinicapermb(datainizio,datafine,id_medicobase,id_persona,vistomedicobase) VALUES ('1929-06-03', NULL,'CYLSWN76D60A952Y','SHPPRN92S69H018S', 'True');
INSERT INTO cartellaclinicapermb(datainizio,datafine,id_medicobase,id_persona,vistomedicobase) VALUES ('1997-12-07', NULL,'CYLSWN76D60A952Y','VCKLND99S61A794B', 'True');
INSERT INTO cartellaclinicapermb(datainizio,datafine,id_medicobase,id_persona,vistomedicobase) VALUES ('2019-10-28', NULL,'WTKSLN15A66L736V','KNSKVN32H10E054D', 'True');
INSERT INTO cartellaclinicapermb(datainizio,datafine,id_medicobase,id_persona,vistomedicobase) VALUES ('1996-12-22', NULL,'CYLSWN76D60A952Y','LFRMYL51D65A952F', 'True');
INSERT INTO cartellaclinicapermb(datainizio,datafine,id_medicobase,id_persona,vistomedicobase) VALUES ('1921-08-25', NULL,'WTKSLN15A66L736V','CLCMLY49B67A271K', 'True');
INSERT INTO cartellaclinicapermb(datainizio,datafine,id_medicobase,id_persona,vistomedicobase) VALUES ('1997-12-04', NULL,'WTKSLN15A66L736V','BRGSRN46B57D612C', 'True');
INSERT INTO cartellaclinicapermb(datainizio,datafine,id_medicobase,id_persona,vistomedicobase) VALUES ('1930-07-19', NULL,'BRYLWN05P02F704C','FRRMLN99B67L049B', 'True');
INSERT INTO cartellaclinicapermb(datainizio,datafine,id_medicobase,id_persona,vistomedicobase) VALUES ('1983-10-26', NULL,'BRYLWN05P02F704C','BSQDPH23M50G224N', 'True');
INSERT INTO cartellaclinicapermb(datainizio,datafine,id_medicobase,id_persona,vistomedicobase) VALUES ('1968-06-23', NULL,'KNCDEI38M15D612H','JLNSRY27L57F205H', 'True');
INSERT INTO cartellaclinicapermb(datainizio,datafine,id_medicobase,id_persona,vistomedicobase) VALUES ('1936-12-31', NULL,'BRYLWN05P02F704C','DGGKCY43A63F158S', 'True');
INSERT INTO cartellaclinicapermb(datainizio,datafine,id_medicobase,id_persona,vistomedicobase) VALUES ('1985-10-10', NULL,'DHMLYN56P05L483U','CCOPHL05L62A952J', 'True');
INSERT INTO cartellaclinicapermb(datainizio,datafine,id_medicobase,id_persona,vistomedicobase) VALUES ('1977-09-19', NULL,'DHMLYN56P05L483U','SPKMNL56D43L049Q', 'True');
INSERT INTO cartellaclinicapermb(datainizio,datafine,id_medicobase,id_persona,vistomedicobase) VALUES ('1945-06-18', NULL,'WTKSLN15A66L736V','BLSNKL96E43F839O', 'True');
INSERT INTO cartellaclinicapermb(datainizio,datafine,id_medicobase,id_persona,vistomedicobase) VALUES ('1976-09-28', NULL,'WNOSHN70B22I754B','MRRJLY71L06I754A', 'True');
INSERT INTO cartellaclinicapermb(datainizio,datafine,id_medicobase,id_persona,vistomedicobase) VALUES ('1931-07-11', NULL,'CYLSWN76D60A952Y','GBBJYO33H27L049Q', 'True');
INSERT INTO cartellaclinicapermb(datainizio,datafine,id_medicobase,id_persona,vistomedicobase) VALUES ('2011-06-08', NULL,'BRYLWN05P02F704C','JWRNNC84E59H018Q', 'True');
INSERT INTO cartellaclinicapermb(datainizio,datafine,id_medicobase,id_persona,vistomedicobase) VALUES ('2003-10-21', NULL,'WNOSHN70B22I754B','BRMDLC18M57I452P', 'True');
INSERT INTO cartellaclinicapermb(datainizio,datafine,id_medicobase,id_persona,vistomedicobase) VALUES ('1962-12-05', NULL,'BRYLWN05P02F704C','MRNFND60C68C573B', 'True');
INSERT INTO cartellaclinicapermb(datainizio,datafine,id_medicobase,id_persona,vistomedicobase) VALUES ('2016-05-14', NULL,'WNOSHN70B22I754B','CGLGLN56M25L219Y', 'True');
INSERT INTO cartellaclinicapermb(datainizio,datafine,id_medicobase,id_persona,vistomedicobase) VALUES ('2014-06-22', NULL,'KNCDEI38M15D612H','WDLKZY17H42A271B', 'True');
INSERT INTO cartellaclinicapermb(datainizio,datafine,id_medicobase,id_persona,vistomedicobase) VALUES ('1928-07-29', NULL,'KNCDEI38M15D612H','PTNTSH44L51L736S', 'True');
INSERT INTO cartellaclinicapermb(datainizio,datafine,id_medicobase,id_persona,vistomedicobase) VALUES ('1927-01-14', NULL,'WNOSHN70B22I754B','KCHRBC71B47F205Y', 'True');
INSERT INTO cartellaclinicapermb(datainizio,datafine,id_medicobase,id_persona,vistomedicobase) VALUES ('1942-02-02', NULL,'MSSLSI02L55H294E','BLZVCN99H49L736J', 'True');
INSERT INTO cartellaclinicapermb(datainizio,datafine,id_medicobase,id_persona,vistomedicobase) VALUES ('1955-01-05', NULL,'WTKSLN15A66L736V','STNHNY15S53L219N', 'True');
INSERT INTO cartellaclinicapermb(datainizio,datafine,id_medicobase,id_persona,vistomedicobase) VALUES ('1981-05-17', NULL,'KNCDEI38M15D612H','RTZLRS07S19D643C', 'True');
INSERT INTO cartellaclinicapermb(datainizio,datafine,id_medicobase,id_persona,vistomedicobase) VALUES ('1923-03-14', NULL,'DHMLYN56P05L483U','RSNDDR13S04D643W', 'True');
INSERT INTO cartellaclinicapermb(datainizio,datafine,id_medicobase,id_persona,vistomedicobase) VALUES ('2009-02-16', NULL,'DHMLYN56P05L483U','LCNSNG82H16I452V', 'True');
INSERT INTO cartellaclinicapermb(datainizio,datafine,id_medicobase,id_persona,vistomedicobase) VALUES ('1921-02-07', NULL,'BRYLWN05P02F704C','LSUNCK20B20A952B', 'True');
INSERT INTO cartellaclinicapermb(datainizio,datafine,id_medicobase,id_persona,vistomedicobase) VALUES ('1991-09-09', NULL,'DHMLYN56P05L483U','CPNLRS70M05L736P', 'True');
INSERT INTO cartellaclinicapermb(datainizio,datafine,id_medicobase,id_persona,vistomedicobase) VALUES ('1987-12-22', NULL,'MSSLSI02L55H294E','HBRMDT70D04H018V', 'True');
INSERT INTO cartellaclinicapermb(datainizio,datafine,id_medicobase,id_persona,vistomedicobase) VALUES ('1995-04-29', NULL,'WTKSLN15A66L736V','MNSTNK42M26D548V', 'True');
INSERT INTO cartellaclinicapermb(datainizio,datafine,id_medicobase,id_persona,vistomedicobase) VALUES ('1991-07-28', NULL,'WNOSHN70B22I754B','MSSSRT29M66H294U', 'True');
INSERT INTO cartellaclinicapermb(datainizio,datafine,id_medicobase,id_persona,vistomedicobase) VALUES ('2016-07-25', NULL,'CYLSWN76D60A952Y','HMNNTR02A61H018B', 'True');
INSERT INTO cartellaclinicapermb(datainizio,datafine,id_medicobase,id_persona,vistomedicobase) VALUES ('1917-04-01', NULL,'WNOSHN70B22I754B','MNTLGU31R21A952K', 'True');
INSERT INTO cartellaclinicapermb(datainizio,datafine,id_medicobase,id_persona,vistomedicobase) VALUES ('1925-05-10', NULL,'WTKSLN15A66L736V','GSSYKK73C52A944U', 'True');
INSERT INTO cartellaclinicapermb(datainizio,datafine,id_medicobase,id_persona,vistomedicobase) VALUES ('1924-01-03', NULL,'WTKSLN15A66L736V','RKWLDR51S05B354G', 'True');
INSERT INTO cartellaclinicapermb(datainizio,datafine,id_medicobase,id_persona,vistomedicobase) VALUES ('1959-03-17', NULL,'WTKSLN15A66L736V','WTKSLN15A66L736V', 'True');
INSERT INTO cartellaclinicapermb(datainizio,datafine,id_medicobase,id_persona,vistomedicobase) VALUES ('1994-04-08', NULL,'CYLSWN76D60A952Y','SHTRBN15A26H018K', 'True');
INSERT INTO cartellaclinicapermb(datainizio,datafine,id_medicobase,id_persona,vistomedicobase) VALUES ('2017-07-19', NULL,'WNOSHN70B22I754B','GLDWNN53D24L736M', 'True');
INSERT INTO cartellaclinicapermb(datainizio,datafine,id_medicobase,id_persona,vistomedicobase) VALUES ('1991-08-13', NULL,'BRYLWN05P02F704C','LNDCMN39R47H018A', 'True');
INSERT INTO cartellaclinicapermb(datainizio,datafine,id_medicobase,id_persona,vistomedicobase) VALUES ('2017-04-19', NULL,'CYLSWN76D60A952Y','SNMTMK71T47A271Z', 'True');










INSERT INTO farmaco(nome,descrizione) VALUES ('Abstral 100mcg','Fentanil');
INSERT INTO farmaco(nome,descrizione) VALUES ('Actraphane 30 10ml','Insulina(umana)');
INSERT INTO farmaco(nome,descrizione) VALUES ('Albital 25% 50ml','Albumina');
INSERT INTO farmaco(nome,descrizione) VALUES ('Capital 3f.4mg/1ml','Desametasone');
INSERT INTO farmaco(nome,descrizione) VALUES ('Clarisco 0.2ml','Eparina');
INSERT INTO farmaco(nome,descrizione) VALUES ('Depakin 30','Acido Valproico');
INSERT INTO farmaco(nome,descrizione) VALUES ('Epinitril 10mg','Nitroglicerina');
INSERT INTO farmaco(nome,descrizione) VALUES ('Lanoxin 2ml','Digossina');
INSERT INTO farmaco(nome,descrizione) VALUES ('Oramorph','Morfina');
INSERT INTO farmaco(nome,descrizione) VALUES ('Normosol RpH','Elettroliti');
INSERT INTO farmaco(nome,descrizione) VALUES ('Reoflus 10','Eparina');












INSERT INTO erogaesssp(id_ssp,id_esame) VALUES (1,34);
INSERT INTO erogaesssp(id_ssp,id_esame) VALUES (2,40);
INSERT INTO erogaesssp(id_ssp,id_esame) VALUES (3,49);
INSERT INTO erogaesssp(id_ssp,id_esame) VALUES (4,43);
INSERT INTO erogaesssp(id_ssp,id_esame) VALUES (5,49);
INSERT INTO erogaesssp(id_ssp,id_esame) VALUES (6,9);
INSERT INTO erogaesssp(id_ssp,id_esame) VALUES (7,50);
INSERT INTO erogaesssp(id_ssp,id_esame) VALUES (8,15);
INSERT INTO erogaesssp(id_ssp,id_esame) VALUES (9,41);
INSERT INTO erogaesssp(id_ssp,id_esame) VALUES (10,6);
INSERT INTO erogaesssp(id_ssp,id_esame) VALUES (11,8);
INSERT INTO erogaesssp(id_ssp,id_esame) VALUES (12,40);
INSERT INTO erogaesssp(id_ssp,id_esame) VALUES (13,56);
INSERT INTO erogaesssp(id_ssp,id_esame) VALUES (14,5);
INSERT INTO erogaesssp(id_ssp,id_esame) VALUES (15,16);
INSERT INTO erogaesssp(id_ssp,id_esame) VALUES (16,1);
INSERT INTO erogaesssp(id_ssp,id_esame) VALUES (17,4);
INSERT INTO erogaesssp(id_ssp,id_esame) VALUES (18,19);
INSERT INTO erogaesssp(id_ssp,id_esame) VALUES (19,32);
INSERT INTO erogaesssp(id_ssp,id_esame) VALUES (20,59);
INSERT INTO erogaesssp(id_ssp,id_esame) VALUES (21,28);
INSERT INTO erogaesssp(id_ssp,id_esame) VALUES (22,52);
INSERT INTO erogaesssp(id_ssp,id_esame) VALUES (23,10);
INSERT INTO erogaesssp(id_ssp,id_esame) VALUES (24,21);
INSERT INTO erogaesssp(id_ssp,id_esame) VALUES (25,20);
INSERT INTO erogaesssp(id_ssp,id_esame) VALUES (26,15);
INSERT INTO erogaesssp(id_ssp,id_esame) VALUES (27,29);
INSERT INTO erogaesssp(id_ssp,id_esame) VALUES (28,5);
INSERT INTO erogaesssp(id_ssp,id_esame) VALUES (29,17);
INSERT INTO erogaesssp(id_ssp,id_esame) VALUES (30,41);
INSERT INTO erogaesssp(id_ssp,id_esame) VALUES (31,7);
INSERT INTO erogaesssp(id_ssp,id_esame) VALUES (32,39);
INSERT INTO erogaesssp(id_ssp,id_esame) VALUES (33,44);
INSERT INTO erogaesssp(id_ssp,id_esame) VALUES (34,23);
INSERT INTO erogaesssp(id_ssp,id_esame) VALUES (35,40);
INSERT INTO erogaesssp(id_ssp,id_esame) VALUES (36,28);
INSERT INTO erogaesssp(id_ssp,id_esame) VALUES (37,44);
INSERT INTO erogaesssp(id_ssp,id_esame) VALUES (38,46);
INSERT INTO erogaesssp(id_ssp,id_esame) VALUES (39,47);
INSERT INTO erogaesssp(id_ssp,id_esame) VALUES (40,36);
INSERT INTO erogaesssp(id_ssp,id_esame) VALUES (41,48);
INSERT INTO erogaesssp(id_ssp,id_esame) VALUES (42,1);
INSERT INTO erogaesssp(id_ssp,id_esame) VALUES (43,12);
INSERT INTO erogaesssp(id_ssp,id_esame) VALUES (44,7);
INSERT INTO erogaesssp(id_ssp,id_esame) VALUES (45,22);
INSERT INTO erogaesssp(id_ssp,id_esame) VALUES (46,47);
INSERT INTO erogaesssp(id_ssp,id_esame) VALUES (47,39);
INSERT INTO erogaesssp(id_ssp,id_esame) VALUES (48,11);
INSERT INTO erogaesssp(id_ssp,id_esame) VALUES (49,15);
INSERT INTO erogaesssp(id_ssp,id_esame) VALUES (50,21);
INSERT INTO erogaesssp(id_ssp,id_esame) VALUES (51,58);
INSERT INTO erogaesssp(id_ssp,id_esame) VALUES (52,59);
INSERT INTO erogaesssp(id_ssp,id_esame) VALUES (53,40);
INSERT INTO erogaesssp(id_ssp,id_esame) VALUES (54,1);
INSERT INTO erogaesssp(id_ssp,id_esame) VALUES (55,42);
INSERT INTO erogaesssp(id_ssp,id_esame) VALUES (56,57);
INSERT INTO erogaesssp(id_ssp,id_esame) VALUES (57,14);
INSERT INTO erogaesssp(id_ssp,id_esame) VALUES (58,28);
INSERT INTO erogaesssp(id_ssp,id_esame) VALUES (59,20);
INSERT INTO erogaesssp(id_ssp,id_esame) VALUES (60,36);
INSERT INTO erogaesssp(id_ssp,id_esame) VALUES (61,45);
INSERT INTO erogaesssp(id_ssp,id_esame) VALUES (62,51);
INSERT INTO erogaesssp(id_ssp,id_esame) VALUES (63,56);
INSERT INTO erogaesssp(id_ssp,id_esame) VALUES (64,39);
INSERT INTO erogaesssp(id_ssp,id_esame) VALUES (65,1);
INSERT INTO erogaesssp(id_ssp,id_esame) VALUES (66,25);
INSERT INTO erogaesssp(id_ssp,id_esame) VALUES (67,17);
INSERT INTO erogaesssp(id_ssp,id_esame) VALUES (68,13);
INSERT INTO erogaesssp(id_ssp,id_esame) VALUES (69,22);
INSERT INTO erogaesssp(id_ssp,id_esame) VALUES (70,13);
INSERT INTO erogaesssp(id_ssp,id_esame) VALUES (71,50);
INSERT INTO erogaesssp(id_ssp,id_esame) VALUES (72,54);
INSERT INTO erogaesssp(id_ssp,id_esame) VALUES (73,23);
INSERT INTO erogaesssp(id_ssp,id_esame) VALUES (74,47);
INSERT INTO erogaesssp(id_ssp,id_esame) VALUES (75,8);
INSERT INTO erogaesssp(id_ssp,id_esame) VALUES (76,3);
INSERT INTO erogaesssp(id_ssp,id_esame) VALUES (77,19);
INSERT INTO erogaesssp(id_ssp,id_esame) VALUES (78,30);
INSERT INTO erogaesssp(id_ssp,id_esame) VALUES (79,59);
INSERT INTO erogaesssp(id_ssp,id_esame) VALUES (80,26);
INSERT INTO erogaesssp(id_ssp,id_esame) VALUES (81,45);
INSERT INTO erogaesssp(id_ssp,id_esame) VALUES (82,48);
INSERT INTO erogaesssp(id_ssp,id_esame) VALUES (83,45);
INSERT INTO erogaesssp(id_ssp,id_esame) VALUES (84,10);
INSERT INTO erogaesssp(id_ssp,id_esame) VALUES (85,40);
INSERT INTO erogaesssp(id_ssp,id_esame) VALUES (86,9);
INSERT INTO erogaesssp(id_ssp,id_esame) VALUES (87,4);
INSERT INTO erogaesssp(id_ssp,id_esame) VALUES (88,27);
INSERT INTO erogaesssp(id_ssp,id_esame) VALUES (89,56);
INSERT INTO erogaesssp(id_ssp,id_esame) VALUES (90,43);
INSERT INTO erogaesssp(id_ssp,id_esame) VALUES (91,21);
INSERT INTO erogaesssp(id_ssp,id_esame) VALUES (92,2);
INSERT INTO erogaesssp(id_ssp,id_esame) VALUES (93,54);
INSERT INTO erogaesssp(id_ssp,id_esame) VALUES (94,5);
INSERT INTO erogaesssp(id_ssp,id_esame) VALUES (95,46);
INSERT INTO erogaesssp(id_ssp,id_esame) VALUES (96,27);
INSERT INTO erogaesssp(id_ssp,id_esame) VALUES (97,26);
INSERT INTO erogaesssp(id_ssp,id_esame) VALUES (98,43);
INSERT INTO erogaesssp(id_ssp,id_esame) VALUES (99,29);
INSERT INTO erogaesssp(id_ssp,id_esame) VALUES (100,40);
INSERT INTO erogaesssp(id_ssp,id_esame) VALUES (101,44);
INSERT INTO erogaesssp(id_ssp,id_esame) VALUES (102,24);
INSERT INTO erogaesssp(id_ssp,id_esame) VALUES (103,56);
INSERT INTO erogaesssp(id_ssp,id_esame) VALUES (104,59);
INSERT INTO erogaesssp(id_ssp,id_esame) VALUES (105,18);
INSERT INTO erogaesssp(id_ssp,id_esame) VALUES (106,26);
INSERT INTO erogaesssp(id_ssp,id_esame) VALUES (107,24);
INSERT INTO erogaesssp(id_ssp,id_esame) VALUES (108,31);
INSERT INTO erogaesssp(id_ssp,id_esame) VALUES (109,5);
INSERT INTO erogaesssp(id_ssp,id_esame) VALUES (110,18);
