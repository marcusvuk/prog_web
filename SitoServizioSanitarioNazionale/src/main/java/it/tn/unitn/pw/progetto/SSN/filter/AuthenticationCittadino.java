/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.tn.unitn.pw.progetto.SSN.filter;

import it.tn.unitn.pw.progetto.SSN.DAO.common.exception.DAOException;
import it.tn.unitn.pw.progetto.SSN.DAO.common.exception.DAOFactoryException;
import it.tn.unitn.pw.progetto.SSN.DAO.common.factory.DAOFactory;
import it.tn.unitn.pw.progetto.SSN.DAO.persistence.*;
import it.tn.unitn.pw.progetto.SSN.persistence.entities.*;
import java.util.List;
import java.io.IOException;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.logging.Logger;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Brian
 */
public class AuthenticationCittadino implements Filter {
    
    private static final boolean debug = true;

    // The filter configuration object we are associated with.  If
    // this value is null, this filter instance is not currently
    // configured. 
    private FilterConfig filterConfig = null;
//############################################################################## 
    private static DAOFactory daoFactory;
    static public void myInit(DAOFactory daoFactory1) {
        daoFactory =  daoFactory1;
    }
//##############################################################################  
    
    
    public AuthenticationCittadino() {
    }    
//------------------------------------------------------------------------------------------QUI IL FILTRO    
    private void doBeforeProcessing(ServletRequest request, ServletResponse response)
            throws IOException, ServletException {
        if (debug) {
            log("AuthenticationCittadino:DoBeforeProcessing");
        }

        // Write code here to process the request and/or response before
        // the rest of the filter chain is invoked.
        // For example, a logging filter might log items on the request object,
        // such as the parameters.
        if (request instanceof HttpServletRequest) {
            ServletContext servletContext = ((HttpServletRequest) request).getServletContext();
            HttpSession session = ((HttpServletRequest) request).getSession(false);
            Persona         persona             = null;
            Foto            foto                = null;
            List<Foto>      listaFoto           = null;
            MedicoBase      medicoBase          = null;
            Foto            fotoMedicoBase      = null;
            SSProvinciale   viveProvincia       = null;
            Integer         numNuoviEsame       = null;
            Integer         numNuoviReportEsame = null;
            Integer         numNuoviRicette     = null;
            Integer         numNuoviVisiteDaFare= null;
            Integer         numVisiteConcluse   = null;
            
            if (session != null) {
                persona             = (Persona)         session.getAttribute("c_Persona");
                foto                = (Foto)            session.getAttribute("c_FotoAttiva");
                listaFoto           = (List<Foto>)      session.getAttribute("c_ListaFoto");
                medicoBase          = (MedicoBase)      session.getAttribute("c_MyMedicoBase");
                fotoMedicoBase      = (Foto)            session.getAttribute("c_fotoMyMedicoBase");
                viveProvincia       = (SSProvinciale)   session.getAttribute("c_ViveProvincia");
                numNuoviEsame       = (Integer)         session.getAttribute("c_NumNuoviEsami");
                numNuoviReportEsame = (Integer)         session.getAttribute("c_NumNuoviReportEsami");
                numNuoviRicette     = (Integer)         session.getAttribute("c_NumNuoviRicette");
                numNuoviVisiteDaFare= (Integer)         session.getAttribute("c_NumNuoviVisiteDaFare");
                numVisiteConcluse   = (Integer)         session.getAttribute("c_NumVisiteConcluse");
            }
            if (persona == null) {
                String contextPath = servletContext.getContextPath();
                if (!contextPath.endsWith("/")) {
                    contextPath += "/";
                }
                ((HttpServletResponse) response).sendRedirect(((HttpServletResponse) response).encodeRedirectURL(contextPath + "login.Cittadino.handler"));
            }else{
                if(foto == null){
                    foto = getFoto(persona.getCodF());
                    if(foto !=  null){
                        ((HttpServletRequest) request).getSession().setAttribute("c_FotoAttiva", foto);
                    }
                }
                if(listaFoto == null){
                    listaFoto = getListaFoto(persona.getCodF());
                    if(listaFoto !=  null){
                        ((HttpServletRequest) request).getSession().setAttribute("c_ListaFoto", listaFoto);
                    }
                }
                if(medicoBase == null){
                    medicoBase = getMedicoBase(persona.getCodF());
                    if(medicoBase !=  null){
                        ((HttpServletRequest) request).getSession().setAttribute("c_MyMedicoBase", medicoBase);  
                    }
                }

                if(medicoBase.getCodF().equals(persona.getCodF())){
                     ((HttpServletRequest) request).getSession().setAttribute("c_fotoMyMedicoBase", foto);
                }else{
                    if(fotoMedicoBase == null){
                        fotoMedicoBase = getFoto(medicoBase.getCodF());;
                        if(fotoMedicoBase !=  null){
                            ((HttpServletRequest) request).getSession().setAttribute("c_fotoMyMedicoBase", fotoMedicoBase);
                        }
                    }
                }

                if(viveProvincia == null){
                    viveProvincia = getSSP(persona.getVive_id_SSP());
                    if(viveProvincia !=  null){
                        ((HttpServletRequest) request).getSession().setAttribute("c_ViveProvincia", viveProvincia);
                    }
                }
                
                
                numNuoviEsame = getNumNuoviEsame(persona.getCodF());
                if(numNuoviEsame !=  null){
                    ((HttpServletRequest) request).getSession().setAttribute("c_NumNuoviEsami", numNuoviEsame);
                }else{
                    ((HttpServletRequest) request).getSession().setAttribute("c_NumNuoviEsami", 0);
                }
                
                numNuoviReportEsame = getNumNuoviReportEsame(persona.getCodF());
                if(numNuoviReportEsame !=  null){
                    ((HttpServletRequest) request).getSession().setAttribute("c_NumNuoviReportEsami", numNuoviReportEsame);
                }else{
                    ((HttpServletRequest) request).getSession().setAttribute("c_NumNuoviReportEsami", 0);
                }
                
                numNuoviRicette = getNumNuoviRicette(persona.getCodF());
                if(numNuoviRicette !=  null){
                    ((HttpServletRequest) request).getSession().setAttribute("c_NumNuoviRicette", numNuoviRicette);
                }else{
                    ((HttpServletRequest) request).getSession().setAttribute("c_NumNuoviRicette", 0);
                }
                
                numNuoviVisiteDaFare = getNumNuoviVisiteDaFare(persona.getCodF());
                if(numNuoviVisiteDaFare !=  null){
                    ((HttpServletRequest) request).getSession().setAttribute("c_NumNuoviVisiteDaFare", numNuoviVisiteDaFare);
                }else{
                    ((HttpServletRequest) request).getSession().setAttribute("c_NumNuoviVisiteDaFare", 0);
                }
                
                numVisiteConcluse = getNumVisiteConcluse(persona.getCodF());
                if(numVisiteConcluse !=  null){
                    ((HttpServletRequest) request).getSession().setAttribute("c_NumVisiteConcluse", numVisiteConcluse);
                }else{
                    ((HttpServletRequest) request).getSession().setAttribute("c_NumVisiteConcluse", 0);
                }
            }
        }
    }    
//------------------------------------------------------------------------------------------^ QUI IL FILTRO
    
    
    private void doAfterProcessing(ServletRequest request, ServletResponse response)
            throws IOException, ServletException {
        if (debug) {
            log("AuthenticationCittadino:DoAfterProcessing");
        }
    }

    /**
     *
     * @param request The servlet request we are processing
     * @param response The servlet response we are creating
     * @param chain The filter chain we are processing
     *
     * @exception IOException if an input/output error occurs
     * @exception ServletException if a servlet error occurs
     */
    public void doFilter(ServletRequest request, ServletResponse response,
            FilterChain chain)
            throws IOException, ServletException {
        
        if (debug) {
            log("AuthenticationCittadino:doFilter()");
        }
        
        doBeforeProcessing(request, response);
        
        Throwable problem = null;
        try {
            chain.doFilter(request, response);
        } catch (IOException | ServletException | RuntimeException ex) {
            // If an exception is thrown somewhere down the filter chain,
            // we still want to execute our after processing, and then
            // rethrow the problem after that.
            problem = ex;
            request.getServletContext().log("Impossible to propagate to the next filter", ex);
        }
        
        doAfterProcessing(request, response);

        // If there was a problem, we want to rethrow it if it is
        // a known type, otherwise log it.
        if (problem != null) {
            if (problem instanceof ServletException) {
                throw (ServletException) problem;
            }
            if (problem instanceof IOException) {
                throw (IOException) problem;
            }
            sendProcessingError(problem, response);
        }
    }

    /**
     * Return the filter configuration object for this filter.
     */
    public FilterConfig getFilterConfig() {
        return (this.filterConfig);
    }

    /**
     * Set the filter configuration object for this filter.
     *
     * @param filterConfig The filter configuration object
     */
    public void setFilterConfig(FilterConfig filterConfig) {
        this.filterConfig = filterConfig;
    }

    /**
     * Destroy method for this filter
     */
    public void destroy() {        
    }

    /**
     * Init method for this filter
     */
    public void init(FilterConfig filterConfig) {        
        this.filterConfig = filterConfig;
        if (filterConfig != null) {
            if (debug) {                
                log("AuthenticationCittadino:Initializing filter");
            }
        }
    }

    /**
     * Return a String representation of this object.
     */
    @Override
    public String toString() {
        if (filterConfig == null) {
            return ("AuthenticationCittadino()");
        }
        StringBuffer sb = new StringBuffer("AuthenticationCittadino(");
        sb.append(filterConfig);
        sb.append(")");
        return (sb.toString());
    }
//-------------------------------------------------------------------------------------------EROR PAGE
    private void sendProcessingError(Throwable t, ServletResponse response) {
        String stackTrace = getStackTrace(t);        
        
        if (stackTrace != null && !stackTrace.equals("")) {
            try {
                response.setContentType("text/html");
                PrintStream ps = new PrintStream(response.getOutputStream());
                PrintWriter pw = new PrintWriter(ps);                
                pw.print("<html>\n<head>\n<title>Error</title>\n</head>\n<body>\n"); //NOI18N

                // PENDING! Localize this for next official release
                pw.print("<h1>The resource did not process correctly</h1>\n<pre>\n");                
                pw.print(stackTrace);                
                pw.print("</pre></body>\n</html>"); //NOI18N
                pw.close();
                ps.close();
                response.getOutputStream().close();
            } catch (Exception ex) {
            }
        } else {
            try {
                PrintStream ps = new PrintStream(response.getOutputStream());
                t.printStackTrace(ps);
                ps.close();
                response.getOutputStream().close();
            } catch (Exception ex) {
            }
        }
    }
    
    public static String getStackTrace(Throwable t) {
        String stackTrace = null;
        try {
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            t.printStackTrace(pw);
            pw.close();
            sw.close();
            stackTrace = sw.getBuffer().toString();
        } catch (Exception ex) {
        }
        return stackTrace;
    }
    
    public void log(String msg) {
        if (filterConfig != null) {
            filterConfig.getServletContext().log(msg);
        } else {
            Logger.getLogger(msg);
        }     
    }
//==============================================================================
    private Foto getFoto(String CodF){
        Foto foto=null;
        
        try {
            Foto_DAO fotoDao   = daoFactory.getDAO(Foto_DAO.class);
            foto = fotoDao.getByPazienteId_Attivo(CodF);
        } catch (DAOFactoryException ex) {
            Logger.getLogger(getClass().getName()).severe(ex.toString());
        } catch (DAOException ex) {
            Logger.getLogger(getClass().getName()).severe(ex.toString());
        }
         
        return foto;
    }
//==============================================================================
    private List<Foto> getListaFoto(String CodF){
        List<Foto> listaFoto=null;
        
        try {
            Foto_DAO fotoDao   = daoFactory.getDAO(Foto_DAO.class);
            listaFoto = fotoDao.getByPazienteId(CodF);
        } catch (DAOFactoryException ex) {
            Logger.getLogger(getClass().getName()).severe(ex.toString());
        } catch (DAOException ex) {
            Logger.getLogger(getClass().getName()).severe(ex.toString());
        }
         
        return listaFoto;
    }
//==============================================================================
    private MedicoBase getMedicoBase(String CodF){
        MedicoBase              medicoBase=null;
        CartellaClinicaPerMB    cartClinicaAttiva=null;
        try {
            MedicoBase_DAO           medicoBaseDAO   = daoFactory.getDAO(MedicoBase_DAO.class);
            CartellaClinicaPerMB_DAO cartClinDAO     = daoFactory.getDAO(CartellaClinicaPerMB_DAO.class);
            
            cartClinicaAttiva = cartClinDAO.getByPazienteId_Attivo(CodF);
            if(cartClinicaAttiva!= null){
                medicoBase = medicoBaseDAO.getByMedicoBaseCodF(cartClinicaAttiva.getId_MedicoBase());
            }
        } catch (DAOFactoryException ex) {
            Logger.getLogger(getClass().getName()).severe(ex.toString());
        } catch (DAOException ex) {
            Logger.getLogger(getClass().getName()).severe(ex.toString());
        }
         
        return medicoBase;
    }
//==============================================================================
    private SSProvinciale getSSP(Integer id){
        SSProvinciale ssProvinciale=null;
        
        try {
            SSProvinciale_DAO ssProvincialeDao   = daoFactory.getDAO(SSProvinciale_DAO.class);
            ssProvinciale = ssProvincialeDao.getByPrimaryKey(id);
        } catch (DAOFactoryException ex) {
            Logger.getLogger(getClass().getName()).severe(ex.toString());
        } catch (DAOException ex) {
            Logger.getLogger(getClass().getName()).severe(ex.toString());
        }
         
        return ssProvinciale;
    }
    //==============================================================================
    private Integer getNumNuoviEsame(String CodF){
        Integer numeroNuoviEsam=0;
        
        try {
            Persona_DAO personaDao   = daoFactory.getDAO(Persona_DAO.class);
            numeroNuoviEsam = personaDao.getNumeroNuoviEsamiByPersonaCodF(CodF);
        } catch (DAOFactoryException ex) {
            Logger.getLogger(getClass().getName()).severe(ex.toString());
        } catch (DAOException ex) {
            Logger.getLogger(getClass().getName()).severe(ex.toString());
        }
         
        return numeroNuoviEsam;
    }
//==============================================================================
    private Integer getNumNuoviReportEsame(String CodF){
        Integer numeroNuoviPazienti =0;
        
        try {
            Persona_DAO personaDao   = daoFactory.getDAO(Persona_DAO.class);
            numeroNuoviPazienti = personaDao.getNumeroNuoviReportEsameForPazienteByPersonaCodF(CodF);
        } catch (DAOFactoryException ex) {
            Logger.getLogger(getClass().getName()).severe(ex.toString());
        } catch (DAOException ex) {
            Logger.getLogger(getClass().getName()).severe(ex.toString());
        }
         
        return numeroNuoviPazienti;
    }
//==============================================================================
    private Integer getNumNuoviRicette(String CodF){
        Integer numeroNuoveRicette =0;
        
        try {
            Persona_DAO personaDao   = daoFactory.getDAO(Persona_DAO.class);
            numeroNuoveRicette = personaDao.getNumeroNuoviRicetteByPersonaCodF(CodF);
        } catch (DAOFactoryException ex) {
            Logger.getLogger(getClass().getName()).severe(ex.toString());
        } catch (DAOException ex) {
            Logger.getLogger(getClass().getName()).severe(ex.toString());
        }
         
        return numeroNuoveRicette;
    }
//==============================================================================
    private Integer getNumNuoviVisiteDaFare(String CodF){
        Integer numeroNuoviVisiteDaFare =0;
        
        try {
            Persona_DAO personaDao   = daoFactory.getDAO(Persona_DAO.class);
            numeroNuoviVisiteDaFare  = personaDao.getNumeroNuoviVisiteByPersonaCodF(CodF);
        } catch (DAOFactoryException ex) {
            Logger.getLogger(getClass().getName()).severe(ex.toString());
        } catch (DAOException ex) {
            Logger.getLogger(getClass().getName()).severe(ex.toString());
        }
         
        return numeroNuoviVisiteDaFare;
    }
//==============================================================================
    private Integer getNumVisiteConcluse(String CodF){
        Integer numeroVisiteConcluse =0;
        
        try {
            Persona_DAO personaDao   = daoFactory.getDAO(Persona_DAO.class);
            numeroVisiteConcluse  = personaDao.getNumeroNuoviReportVisiteForPazienteByPersonaCodF(CodF);
        } catch (DAOFactoryException ex) {
            Logger.getLogger(getClass().getName()).severe(ex.toString());
        } catch (DAOException ex) {
            Logger.getLogger(getClass().getName()).severe(ex.toString());
        }
         
        return numeroVisiteConcluse;
    }
//==============================================================================

}

