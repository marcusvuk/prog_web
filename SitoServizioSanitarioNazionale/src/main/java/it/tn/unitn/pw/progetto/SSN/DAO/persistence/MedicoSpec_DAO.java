/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.tn.unitn.pw.progetto.SSN.DAO.persistence;

import java.util.List;
        
import it.tn.unitn.pw.progetto.SSN.persistence.entities.MedicoSpec;
import it.tn.unitn.pw.progetto.SSN.DAO.common.DAO;
import it.tn.unitn.pw.progetto.SSN.DAO.common.exception.DAOException;
/**
 *
 * @author Brian
 */
public interface MedicoSpec_DAO extends DAO<MedicoSpec, String>{
/*============================================================================*/    
/*                         METODO INSERT                                      */
/*============================================================================*/
    public String insert(MedicoSpec medicoSpec)throws DAOException;
/*============================================================================*/    
/*============================================================================*/    
/*                         METODO UPDATE                                      */
/*============================================================================*/
    public void update(MedicoSpec medicoSpec)throws DAOException;
    public void update_Password(String id,String Password )throws DAOException;
/*============================================================================*/
/*============================================================================*/    
/*                         METODO GET Da ID                                   */
/*============================================================================*/
    public List<MedicoSpec> getBySSProvincialeId_Vive(Integer id)throws DAOException;
/*============================================================================*/
/*============================================================================*/    
/*                         METODO GET LOGIN                                  */
/*============================================================================*/
    public MedicoSpec getByMedicoSpecEmailAndPassword(String email, String password)throws DAOException;
    public String getSaltMedicoSpecByEmail(String email)throws DAOException;
    
    public MedicoSpec getByMedicoSpecCodFAndPassword(String CodF, String password)throws DAOException;
    public String getSaltMedicoSpecByCodF(String CodF)throws DAOException;
/*============================================================================*/    
}
