<script src="${pageContext.request.contextPath}/modal/modal_MenuUtente\js\js_reloadImg.js"></script>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div class="modal fade left " id="MenuUtente" tabindex="-1" role="dialog">
    <div class="modal-dialog " role="document">
        <div class="modal-content" style="overflow-x: hidden;">
            <div class="modal-header" >
                <button type="button"
                    class="close mx-auto"
                    data-dismiss="modal"
                    aria-hidden="true"
                    aria-label="Close">
                    <i class="fas fa-arrow-left"></i>
                </button>
            </div>
            <div class="modal-header container-fluid" >
                <div class="img-container mx-auto">
                    <c:choose>
                        <c:when test="${(c_FotoAttiva ne null)}">
                            <img class="rounded-circle mx-5"
                                src="${pageContext.request.contextPath}/DB_FOTO${c_FotoAttiva.path}${c_FotoAttiva.nome}"
                                alt="Profilo Cittadino"
                                onerror ="imgError(this);"
                                style="width:200px;height:200px"/>
                        </c:when>
                        <c:when test="${(b_FotoAttiva ne null)}">
                            <img class="rounded-circle mx-5"
                                src="${pageContext.request.contextPath}/DB_FOTO${b_FotoAttiva.path}${b_FotoAttiva.nome}"
                                alt="Profilo Medico Base"
                                onerror ="imgError(this);"
                                style="width:200px;height:200px"/>
                        </c:when>
                        <c:when test="${(s_FotoAttiva ne null)}">
                            <img class="rounded-circle mx-5"
                                src="${pageContext.request.contextPath}/DB_FOTO${s_FotoAttiva.path}${s_FotoAttiva.nome}"
                                alt="Profilo Medico Specialista"
                                onerror ="imgError(this);"
                                style="width:200px;height:200px"/>
                        </c:when>
                        <c:when test="${(f_Farmacia ne null) || (p_ssProvinciale ne null)}">
                            <img class="rounded-circle mx-5"
                                src="${pageContext.request.contextPath}/sito_IMG/Farmacia.png"
                                alt="Profilo Medico Specialista"
                                onerror ="imgError(this);"
                                style="width:200px;height:200px"/>
                        </c:when>
                        <c:otherwise>
                            <img class="rounded-circle mx-5"
                                 src="sito_IMG/blank-profile-picture.png"
                                 alt="Profilo"
                                 onerror="this.src='${pageContext.request.contextPath}\\sito_IMG\\blank-profile-picture.png';"
                                 style="width:200px;height:200px"/>
                        </c:otherwise>
                    </c:choose>
                </div>
            </div>
            <div class="modal-header container-fluid" >
                <h3 class="modal-title" id="myModalLabel">
                    <c:choose>
                        <c:when test="${(c_Persona ne null)}">
                            <c:out value="${c_Persona.cognome}"/>
                            <br>
                            <c:out value="${c_Persona.nome}"/>
                        </c:when>
                        <c:when test="${(b_Persona ne null)}">
                            <c:out value="${b_Persona.cognome}"/>
                            <br>
                            <c:out value="${b_Persona.nome}"/>
                        </c:when>
                        <c:when test="${(s_Persona ne null)}">
                            <c:out value="${s_Persona.cognome}"/>
                            <br>
                            <c:out value="${s_Persona.nome}"/>
                        </c:when>
                        <c:when test="${(f_Farmacia ne null)}">
                            Farmacia <c:out value="${f_Farmacia.nome}"/>
                        </c:when>
                        <c:when test="${(p_ssProvinciale ne null)}">
                            S.S.Provinciale di <c:out value="${p_ssProvinciale.nome}"/>
                        </c:when>
                    </c:choose>
                </h3>
            </div>

            <div class="modal-body">
                <div class="btn-group btn-group-vertical container-fluid">
                    <c:choose>
                        <c:when test="${(c_Persona ne null)}">
                    <a  class="btn btn-light m-1 text-left"
                        href="${pageContext.request.contextPath}/Cittadino/cittadino.jsp">
                        </c:when>
                        <c:when test="${(b_Persona ne null)}">
                    <a  class="btn btn-light m-1 text-left"
                        href="${pageContext.request.contextPath}/MedicoBase/medicoBase.jsp">
                        </c:when>
                        <c:when test="${(s_Persona ne null)}">
                    <a  class="btn btn-light m-1 text-left"
                        href="${pageContext.request.contextPath}/MedicoSpec/medicoSpec.jsp">
                        </c:when>
                        <c:when test="${(f_Farmacia ne null)}">
                    <a  class="btn btn-light m-1 text-left"
                        href="${pageContext.request.contextPath}/Farmacia/farmacia.jsp">
                        </c:when>
                        <c:when test="${(p_ssProvinciale ne null)}">
                    <a  class="btn btn-light m-1 text-left"
                        href="${pageContext.request.contextPath}/SSP/SSP.jsp">
                        </c:when>
                    </c:choose>
                         Area Utente
                    </a>
                    <c:choose>
                        <c:when test="${(c_Persona ne null)}">
                            <%@include file="/modal/modal_MenuUtente/btn_Cittadino.jsp" %>
                        </c:when>
                        <c:when test="${(b_Persona ne null)}">
                            <%@include file="/modal/modal_MenuUtente/btn_MedicoBase.jsp" %>
                        </c:when>
                        <c:when test="${(s_Persona ne null)}">
                            <%@include file="/modal/modal_MenuUtente/btn_MedicoSpec.jsp" %>
                        </c:when>
                        <c:when test="${(f_Farmacia ne null)}">
                            <%@include file="/modal/modal_MenuUtente/btn_Farmacia.jsp" %>
                        </c:when>
                        <c:when test="${(p_ssProvinciale ne null)}">
                            <%@include file="/modal/modal_MenuUtente/btn_SSProvinciale.jsp" %>
                        </c:when>
                    </c:choose>    
                        
                    <br>
                    <hr>
                    <br>
                    <a class="btn btn-light m-1 text-left"
                       href="${pageContext.request.contextPath}/Logout">
                        Logout
                    </a>
                </div>
            </div>
            <div class="modal-footer">
                <div class="btn-group btn-group-vertical container-fluid">
                    <button type="button"
                            class="close mx-auto"
                            data-dismiss="modal"
                            aria-hidden="true"
                            aria-label="Close">
                        <i class="fas fa-arrow-left"></i>
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>




















