/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.tn.unitn.pw.progetto.SSN.DAO.persistence;

import java.util.List;
import java.sql.Date;
        
import it.tn.unitn.pw.progetto.SSN.persistence.entities.PrescrizioneEsameEReferto;
import it.tn.unitn.pw.progetto.SSN.DAO.common.DAO;
import it.tn.unitn.pw.progetto.SSN.DAO.common.exception.DAOException;
/**
 *
 * @author Brian
 */
public interface PrescrizioneEsameEReferto_DAO extends DAO<PrescrizioneEsameEReferto, Integer>{
/*============================================================================*/    
/*                         METODO INSERT                                      */
/*============================================================================*/
    public Integer insert(PrescrizioneEsameEReferto prescrizioneEsameEReferto)throws DAOException;
/*============================================================================*/    
/*============================================================================*/    
/*                         METODO UPDATE                                      */
/*============================================================================*/
    public void update(PrescrizioneEsameEReferto prescrizioneEsameEReferto)throws DAOException;
    public void update_dataEsequzioneEsame(Integer id,Date data)throws DAOException;
    public void update_VistoPaziente(Integer id,Boolean visto)throws DAOException;
    public void update_VistoRepotDaPaziente(Integer id,Boolean visto)throws DAOException;
    public void update_VistoRepotDaMedicoBase(Integer id,Boolean visto)throws DAOException;
/*============================================================================*/
/*============================================================================*/    
/*                         METODO GET Da ID                                   */
/*============================================================================*/
    public List<PrescrizioneEsameEReferto> getByPazienteId(String id)throws DAOException;
    public List<PrescrizioneEsameEReferto> getByMedicoSpecId(String id)throws DAOException; 
/*============================================================================*/ 
/*============================================================================*/    
/*                         METODO GET Da ID fatte/da fare                     */
/*============================================================================*/
    public List<PrescrizioneEsameEReferto> getByPazienteId_Fatte(String id)throws DAOException;
    public List<PrescrizioneEsameEReferto> getByPazienteId_DaFare(String id)throws DAOException;
/*============================================================================*/   
}
