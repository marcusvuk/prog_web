
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%@page import="java.util.List"%>
<%@page import="it.tn.unitn.pw.progetto.SSN.DAO.common.exception.*"%>
<%@page import="it.tn.unitn.pw.progetto.SSN.DAO.common.factories.jdbc.*"%>
<%@page import="it.tn.unitn.pw.progetto.SSN.DAO.common.factory.*"%>
<%@page import="it.tn.unitn.pw.progetto.SSN.DAO.common.persistence.dao.jdbc.*"%>
<%@page import="it.tn.unitn.pw.progetto.SSN.DAO.persistence.*"%>
<%@page import="it.tn.unitn.pw.progetto.SSN.persistence.entities.*"%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
        <title>
            Prescrivi Richiamo
        </title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" crossorigin="anonymous">
    
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/css/select2.min.css">
  
        <!-- Cittadino -->
        <link rel="stylesheet" href="${pageContext.request.contextPath}/modal/modal_Login/css/floating-labels.css">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/modal/modal_Login/css/forms.css">
        <!-- Cittadino -->
        <link rel="icon" href="${pageContext.request.contextPath}/sito_IMG/icona.ico" type="image/x-icon" />
        <link href="${pageContext.request.contextPath}/sito_CSS/style.css" rel="stylesheet" />
        <link href="${pageContext.request.contextPath}/sito_CSS/style_MenuUtente.css" rel="stylesheet" />
        
        <!-- Tabelle -->
        <link rel="stylesheet" href="https://cdn.datatables.net/1.10.18/css/dataTables.bootstrap4.min.css">
    </head>
    <body >
        <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/js/select2.min.js"></script>
      <!-- Tabelle -->   
        <script src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script>
        <script src="https://cdn.datatables.net/1.10.18/js/dataTables.bootstrap4.min.js"></script>
    <!-- JAVASCRIP -->
        <%@include file="../pag_ElementiComunni/pag_MenuPrincipale.jsp" %>
        
        <div class="container">
            <div style="min-height:300px;">
                <div class="row">
                    <div class="col-12">
                        <h2>
                            Prescrivi richiamo ai cittadini residenti in provincia di 
                            <b>
                                <c:out value="${p_ssProvinciale.nome}"/>
                            </b>
                        </h2>
                    </div>
                </div>
                            
                            
                <form action="Richiami.handler" method="POST">
                    <div class="row" >
                        <div class="col-md-4 form-group">
                            <label><h5><b>Ricerca esame:</b></h5></label>
                        </div>
                        <div class="col-md-8 form-group">
                            <select id="idTipoEsame" 
                                    name="idTipoEsame" 
                                    style="width: 100%"
                                    class="form-control  select2-allow-clear" 
                                    required="required">
                            </select>
                            <input  type="hidden" 
                                id="MyIdSSP" 
                                name="MyIdSSP" 
                                value="${p_ssProvinciale.id}">
                            <script src="${pageContext.request.contextPath}/ssp_Paggine/js/js_TuttiTipiEsami.js"></script>                    
                        </div>
                    </div>
                    <br>
                    <div class="row" >
                        <div class="col-md-12 form-group">
                            <h5>
                                Nel intervallo di et&aacute;
                            </h5>
                        </div>
                    </div>
                    <div class="row" >
                        <div class="col-md-2 form-group">
                            <label><h5><b>Tra gli</b></h5></label>
                        </div>
                        <div class="col-md-8 form-group">
                            <input  id="annoMinimo" 
                                    name="annoMinimo" 
                                    type="number"
                                    min="0"
                                    max="104"
                                    value="0"
                                    class="form-control"
                                    onchange="CambiaMinimo()"
                                    required="required"/>
                        </div>
                        <div class="col-md-2 form-group">
                            <label><h5><b>anni</b></h5></label>
                        </div>
                    </div>
                    <div class="row" >
                        <div class="col-md-2 form-group">
                            <label><h5><b>e </b></h5></label>
                        </div>
                        <div class="col-md-8 form-group">
                            <input  id="annoMassimo" 
                                    name="annoMassimo" 
                                    type="number"
                                    min="1"
                                    max="105"
                                    value="1"
                                    class="form-control"
                                    required="required"/>
                        </div>
                        <div class="col-md-2 form-group">
                            <label><h5><b>anni.</b></h5></label>
                        </div>
                    </div>
                    <script src="${pageContext.request.contextPath}/ssp_Paggine/js/js_ErroreAnno.js"></script>                    
                       
                    <div class="row">
                        <div class="col-md-12 form-group">
                            <input  type="submit"
                                    class="btn btn-primary pull-right float-right"
                                    accept=""value="Conferma"/>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <%@include file="../pag_ElementiComunni/pag_Footer.jsp" %>
    </body>
</html>
