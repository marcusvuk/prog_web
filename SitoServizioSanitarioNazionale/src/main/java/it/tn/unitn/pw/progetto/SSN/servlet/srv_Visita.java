/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.tn.unitn.pw.progetto.SSN.servlet;

import it.tn.unitn.pw.progetto.SSN.DAO.common.exception.DAOException;
import it.tn.unitn.pw.progetto.SSN.DAO.common.exception.DAOFactoryException;
import it.tn.unitn.pw.progetto.SSN.DAO.common.factory.DAOFactory;
import it.tn.unitn.pw.progetto.SSN.DAO.persistence.*;
import it.tn.unitn.pw.progetto.SSN.Genera.Email.Mail;
import it.tn.unitn.pw.progetto.SSN.persistence.entities.*;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.util.GregorianCalendar;
import java.sql.Date;

/**
 *
 * @author Brian
 */
public class srv_Visita extends HttpServlet {
//------------------------------------------------------------------------------   
    private VisitaMB_DAO         visitaMB_DAO;
    private VisitaMSEReport_DAO  visitaMSEReport_DAO;
    
    @Override
    public void init() throws ServletException {
        DAOFactory daoFactory = (DAOFactory) super.getServletContext().getAttribute("daoFactory");
        if (daoFactory == null) {
            throw new ServletException("Impossible to get dao factory for user storage system");
        }
        try {
            visitaMB_DAO            = daoFactory.getDAO(VisitaMB_DAO.class);
            visitaMSEReport_DAO     = daoFactory.getDAO(VisitaMSEReport_DAO.class);
            
        } catch (DAOFactoryException ex) {
            throw new ServletException("Impossible to get dao factory for user storage system", ex);
        }
    }
//------------------------------------------------------------------------------

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        String contextPath = getServletContext().getContextPath();
        if (!contextPath.endsWith("/")) {
            contextPath += "/";
        }
        
        Persona cittadino  = (Persona) request.getSession(false).getAttribute("c_Persona");
        Persona pazienteMB = (Persona) request.getSession(false).getAttribute("b_Paziente");
        Persona pazienteMS = (Persona) request.getSession(false).getAttribute("s_Paziente");
        
        if( (cittadino != null) && (pazienteMB == null) && (pazienteMS == null) ){
            getVisiteCittadino(request,response,cittadino);
            
        }else if( (cittadino == null) && (pazienteMB != null) && (pazienteMS == null) ){
            getVisitePazienteMB(request,response,pazienteMB);
            
        }else if( (cittadino == null) && (pazienteMB == null) && (pazienteMS != null) ){
            getVisitePazienteMS(request,response,pazienteMS);
            
        }else {
            //ERRORE--------------------------------------------------------------------------------------------
            response.sendRedirect(response.encodeRedirectURL(contextPath + "Index"));
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String contextPath = getServletContext().getContextPath();
        if (!contextPath.endsWith("/")) {
            contextPath += "/";
        }
        MedicoSpec medicoSpec       = (MedicoSpec) request.getSession(false).getAttribute("s_Persona");
        MedicoBase medicoBase       = (MedicoBase) request.getSession(false).getAttribute("b_Persona");
        String tipoInserimetoVisita = request.getParameter("TipoInserimetoVisita");
        
        if( ( tipoInserimetoVisita.equals("AggiungiVisita") ) && 
            ( medicoSpec == null) && ( medicoBase != null ) ){//Nuova Visita Medioco Base 
            creaNuovaVisitaMB(request,response,medicoBase);
            
        }else if( ( tipoInserimetoVisita.equals("AggiungiVisita") ) && 
            ( medicoSpec != null) && ( medicoBase == null ) ){//Nuova Visita Medioco Spec 
            creaNuovaVisitaMS(request,response,medicoSpec);
            
        }else if( ( tipoInserimetoVisita.equals("ConcludiVisita") ) && 
            ( medicoSpec == null) && ( medicoBase != null ) ){//Nuova Visita Medioco Base 
            concludiVisitaMB(request,response,medicoBase);
            
        }else if( ( tipoInserimetoVisita.equals("ConcludiVisita") ) && 
            ( medicoSpec != null) && ( medicoBase == null ) ){//Nuova Visita Medioco Spec 
            concludiVisitaMS(request,response,medicoSpec);
        
        }else if( ( tipoInserimetoVisita.equals("EffetuaPagamento") ) && 
            ( medicoSpec != null) && ( medicoBase == null ) ){//Nuova Visita Medioco Spec 
            effetuaPagamentoMS(request,response,medicoSpec);
            
        }else{
            //ERRORE--------------------------------------------------------------------------------------------
            response.sendRedirect(response.encodeRedirectURL(contextPath + "Index"));
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
//==============================================================================
//                          Get VISITE per cittadino
//==============================================================================
    private void getVisiteCittadino(HttpServletRequest request,HttpServletResponse response,Persona persona)
                                   throws ServletException, IOException {
        String contextPath = getServletContext().getContextPath();
        if (!contextPath.endsWith("/")) {
            contextPath += "/";
        }
        
        Visita visita;
        String idVisita = request.getParameter("idVisita");
        
        if(idVisita == null || idVisita.equals("")){
            response.sendRedirect("cittadino.jsp");
        }else{
            visita = getVisite(request,persona,idVisita);
            if(visita == null){
                response.sendRedirect("cittadino.jsp");
            }else{
                
                if(!visita.getVistoPaziente()){//Se non è sato mai visto
                    setVistoPaziente(request,visita);
                }
                if( (!visita.getVistoReportPaziente()) && 
                    (visita.getData() != null ) ){//Se non è sato mai visto il report
                    setVistoReportPaziente(request,visita);
                }
                
                request.getSession().setAttribute("c_b_s_Visita", visita);
                response.sendRedirect(response.encodeRedirectURL(contextPath + "Cittadino/Visita/visita.jsp"));
            }
        }
    }
//==============================================================================
//                          Get VISITE per medico di base
//==============================================================================
    private void getVisitePazienteMB(HttpServletRequest request,HttpServletResponse response,Persona persona)
                                   throws ServletException, IOException {
        String contextPath = getServletContext().getContextPath();
        if (!contextPath.endsWith("/")) {
            contextPath += "/";
        }
        
        Visita visita;
        String idVisita = request.getParameter("idVisita");
        
        if(idVisita == null || idVisita.equals("")){
            response.sendRedirect("cittadino.jsp");
        }else{
            visita = getVisite(request,persona,idVisita);
            if(visita == null){
                response.sendRedirect("cittadino.jsp");
            }else{
                
                if( (visita instanceof VisitaMSEReport)&&
                    (!((VisitaMSEReport)visita).getVistoReportMedicoBase()) ){//Se non è sato mai visto
                    setVistoReportMedicoBase(request,visita);
                }
                
                request.getSession().setAttribute("c_b_s_Visita", visita);
                response.sendRedirect(response.encodeRedirectURL(contextPath + "MedicoBase/PazienteMB/Visita/visita.jsp"));
            }
        }
    }
//==============================================================================
//                          Get VISITE per medico di base
//==============================================================================
    private void getVisitePazienteMS(HttpServletRequest request,HttpServletResponse response,Persona persona)
                                   throws ServletException, IOException {
        String contextPath = getServletContext().getContextPath();
        if (!contextPath.endsWith("/")) {
            contextPath += "/";
        }
        
        Visita visita;
        String idVisita = request.getParameter("idVisita");
        
        if(idVisita == null || idVisita.equals("")){
            response.sendRedirect("cittadino.jsp");
        }else{
            visita = getVisite(request,persona,idVisita);
            if(visita == null){
                response.sendRedirect("cittadino.jsp");
            }else{
                
                request.getSession().setAttribute("c_b_s_Visita", visita);
                response.sendRedirect(response.encodeRedirectURL(contextPath + "MedicoSpec/PazienteMS/Visita/visita.jsp"));
            }
        }
    }
//==============================================================================
//                     VERIFICA CHE L'ESAME SIA DEL PAZIENTE
//==============================================================================
    private Visita getVisite(HttpServletRequest request,Persona persona,String CodId){
        Visita  visita = null;
        try {
            String[] arrayID=CodId.split("-");
            if(arrayID.length != 2){
                visita =null;
            }else{
                Integer idVisite = Integer.parseInt(arrayID[1]);
                if(arrayID[0].equals("VMB")){
                    visita = visitaMB_DAO.getByPrimaryKey(idVisite);
                }else if(arrayID[0].equals("VMS")){
                    visita = visitaMSEReport_DAO.getByPrimaryKey(idVisite);
                }else{
                    visita = null;
                }
                
                if( (visita==null) && (!visita.getId_Persona().equals(persona.getCodF())) ){
                     visita = null;
                }
            }
        }catch (DAOException ex) {
            request.getServletContext().log("Imposibile recuperare la visita.", ex);
            visita= null;
        }catch (Exception ex) {
            request.getServletContext().log("Imposibile recuperare convertitre l'id della Visita in un intero.", ex);
            visita= null;
        }finally{
            return  visita;
        }
    }
//==============================================================================
//                    Imposta come visto dal PAZIENTE
//==============================================================================
    private void setVistoPaziente(HttpServletRequest request,Visita visita){
        try {
            if(visita instanceof VisitaMB){
                visitaMB_DAO.update_VistoPaziente(visita.getId(),true);
            }else if(visita instanceof VisitaMSEReport){
                visitaMSEReport_DAO.update_VistoPaziente(visita.getId(),true);
            }
        }catch (DAOException ex) {
            request.getServletContext().log("Imposibile impostare la visita come gia visto.", ex);
        }
    }
//==============================================================================
//                    Imposta come visto il report dal PAZIENTE
//==============================================================================
    private void setVistoReportPaziente(HttpServletRequest request,Visita visita){
        try {
            if(visita instanceof VisitaMB){
                visitaMB_DAO.update_VistoRepotDaPaziente(visita.getId(),true);
            }else if(visita instanceof VisitaMSEReport){
                visitaMSEReport_DAO.update_VistoRepotDaPaziente(visita.getId(),true);
            }
        }catch (DAOException ex) {
            request.getServletContext().log("Imposibile impostare il report della visita come gia visto.", ex);
        }
    }
//==============================================================================
//                    Imposta come visto il report dal PAZIENTE
//==============================================================================
    private void setVistoReportMedicoBase(HttpServletRequest request,Visita visita){
        try {
            if(visita instanceof VisitaMSEReport){
                visitaMSEReport_DAO.update_VistoRepotDaMedicoBase(visita.getId(),true);
            }
        }catch (DAOException ex) {
            request.getServletContext().log("Imposibile impostare il report della visita come gia visto.", ex);
        }
    }

//==============================================================================
//                          Crea un nuova Visita Medico Base
//==============================================================================
    private void creaNuovaVisitaMB(HttpServletRequest request,HttpServletResponse response,MedicoBase medicoBase)
                                    throws ServletException, IOException {
        String contextPath = getServletContext().getContextPath();
        if (!contextPath.endsWith("/")) {
            contextPath += "/";
        }
        Persona paziente = (Persona) request.getSession(false).getAttribute("b_Paziente");
        String prescrizioneVisita = request.getParameter("prescrizioneVisita");
        
        Visita visita=aggiungiVisitaMB(request,medicoBase,paziente,prescrizioneVisita);
        if(visita!=null){
            request.getSession().setAttribute("c_b_s_Visita", visita);
            response.sendRedirect(response.encodeRedirectURL(contextPath + "MedicoBase/PazienteMB/Visita/visita.jsp"));
            //INVIA EMAIL
            inviaEmailVisita(request,visita,paziente,medicoBase);
            //INVIA EMAIL
        }else{
            response.sendRedirect(response.encodeRedirectURL(contextPath + "MedicoBase/PazienteMB/erogaVisita.jsp"));
        }
    }
//==============================================================================
//                          Crea un nuova Visita Medico Spec
//==============================================================================
    private void creaNuovaVisitaMS(HttpServletRequest request,HttpServletResponse response,MedicoSpec medicoSpec)
                                    throws ServletException, IOException {
        String contextPath = getServletContext().getContextPath();
        if (!contextPath.endsWith("/")) {
            contextPath += "/";
        }
        Persona paziente = (Persona) request.getSession(false).getAttribute("s_Paziente");
        String prescrizioneVisita = request.getParameter("prescrizioneVisita");
        
        Visita visita= aggiungiVisitaMS(request,medicoSpec,paziente,prescrizioneVisita);
        if(visita != null){
            request.getSession().setAttribute("c_b_s_Visita", visita);
            response.sendRedirect(response.encodeRedirectURL(contextPath + "MedicoSpec/PazienteMS/Visita/visita.jsp"));
            //INVIA EMAIL
            inviaEmailVisita(request,visita,paziente,medicoSpec);
            //INVIA EMAIL
        }else{
            response.sendRedirect(response.encodeRedirectURL(contextPath + "MedicoSpec/PazienteMS/erogaVisita.jsp"));
        }
    }
//==============================================================================
//                          Concludi Visita Medico Base
//==============================================================================
    private void concludiVisitaMB(HttpServletRequest request,HttpServletResponse response,MedicoBase medicoBase)
                                    throws ServletException, IOException {
        String contextPath = getServletContext().getContextPath();
        if (!contextPath.endsWith("/")) {
            contextPath += "/";
        }
        String risultatoVisita = request.getParameter("risultatoVisita");
        Persona paziente = (Persona) request.getSession(false).getAttribute("b_Paziente");
        Visita  visita   = (Visita) request.getSession(false).getAttribute("c_b_s_Visita");
        
        Visita ris = modificaVisitaMB(request,visita,medicoBase,paziente,risultatoVisita);
        if(ris != null){
            request.getSession().setAttribute("c_b_s_Visita", ris);
            response.sendRedirect(response.encodeRedirectURL(contextPath + "MedicoBase/PazienteMB/ConfermaCreazione.jsp"));
            //INVIA EMAIL
            inviaEmailRepotVisita(request,visita,paziente,medicoBase);
            //INVIA EMAIL
            
        }else{
            response.sendRedirect(response.encodeRedirectURL(contextPath + "MedicoBase/PazienteMB/Visita/visita.jsp"));
        }
    }
//==============================================================================
//                          Concludi Visita Medico Spec
//==============================================================================
    private void concludiVisitaMS(HttpServletRequest request,HttpServletResponse response,MedicoSpec medicoSpec)
                                    throws ServletException, IOException {
        String contextPath = getServletContext().getContextPath();
        if (!contextPath.endsWith("/")) {
            contextPath += "/";
        }
        String risultatoVisita = request.getParameter("risultatoVisita");
        String reportVisita = request.getParameter("reportVisita");
        String pagatoVisita = request.getParameter("pagatoVisita");
        Persona paziente = (Persona) request.getSession(false).getAttribute("s_Paziente");
        Visita  visita   = (Visita) request.getSession(false).getAttribute("c_b_s_Visita");
        
        Visita ris = modificaVisitaMS(request,visita,medicoSpec,paziente,risultatoVisita,reportVisita,pagatoVisita);
        if(ris != null){
            request.getSession().setAttribute("c_b_s_Visita", ris);
            response.sendRedirect(response.encodeRedirectURL(contextPath + "MedicoSpec/PazienteMS/ConfermaVisita.jsp"));
            //INVIA EMAIL
            inviaEmailRepotVisita(request,visita,paziente,medicoSpec);
            //INVIA EMAIL
            
        }else{
            response.sendRedirect(response.encodeRedirectURL(contextPath + "MedicoSpec/PazienteMS/Visita/visita.jsp"));
        }
    }
//==============================================================================
//                          Concludi Visita Medico Spec
//==============================================================================
    private void effetuaPagamentoMS(HttpServletRequest request,HttpServletResponse response,MedicoSpec medicoSpec)
                                    throws ServletException, IOException {
        String contextPath = getServletContext().getContextPath();
        if (!contextPath.endsWith("/")) {
            contextPath += "/";
        }
        
        String pagatoVisita = request.getParameter("pagatoVisite");
        Visita  visita   = (Visita) request.getSession(false).getAttribute("c_b_s_Visita");
        
        if(setPagamentoMS(request,visita,medicoSpec,pagatoVisita)){
            request.getSession().setAttribute("c_b_s_Visita", visita);
            response.sendRedirect(response.encodeRedirectURL(contextPath + "MedicoSpec/PazienteMS/Visita/visita.jsp"));
            
        }else{
            response.sendRedirect(response.encodeRedirectURL(contextPath + "MedicoSpec/PazienteMS/Visita/visita.jsp"));
        }
    }

//==============================================================================
//                          Aggiungi VisitaMB
//==============================================================================
    private VisitaMB aggiungiVisitaMB(HttpServletRequest request,MedicoBase medicoBase,Persona paziente,String prescrizioneVisita){
        VisitaMB visita=null;
        try {
            GregorianCalendar data= new GregorianCalendar();
            Date oggi=new Date(data.getTimeInMillis());

            visita = new VisitaMB();

            visita.setId(null);
            visita.setRiassuntoVisita(null);
            visita.setPrescrizione(prescrizioneVisita);
            visita.setData(null);
            visita.setDataPrescrizione(oggi);
            visita.setId_Persona(paziente.getCodF());
            visita.setId_MedicoBase(medicoBase.getCodF());
            visita.setVistoPaziente(false);
            visita.setVistoReportPaziente(false);

            if(visitaMB_DAO.insert(visita)==null){
                visita = null;
            }
        }catch (DAOException ex) {
            visita =null;
            request.getServletContext().log("Imposibile impostare  crare la prescrizione di un esame.", ex);
        }finally{
            return  visita;
        }
    }
//==============================================================================
//                          Aggiungi VisitaMS
//==============================================================================
    private VisitaMSEReport aggiungiVisitaMS(HttpServletRequest request,MedicoSpec medicoSpec,Persona paziente,String prescrizioneVisita){
        VisitaMSEReport visita=null;
        try {
            GregorianCalendar data= new GregorianCalendar();
            Date oggi=new Date(data.getTimeInMillis());

            visita = new VisitaMSEReport();

            visita.setId(null);
            visita.setRiassuntoVisita(null);
            visita.setPrescrizione(prescrizioneVisita);
            visita.setData(null);
            visita.setDataPrescrizione(oggi);
            visita.setTichet(50.0);
            visita.setPagato(false);
            visita.setReport(null);
            visita.setId_Persona(paziente.getCodF());
            visita.setId_MedicoSpec(medicoSpec.getCodF());
            visita.setVistoPaziente(false);
            visita.setVistoReportMedicoBase(false);
            visita.setVistoReportPaziente(false);
            

            if(visitaMSEReport_DAO.insert(visita)==null){
                visita = null;
            }
        }catch (DAOException ex) {
            visita =null;
            request.getServletContext().log("Imposibile impostare  crare la prescrizione di un esame.", ex);
        }finally{
            return  visita;
        }
    }
//==============================================================================
//                          conclusione VisitaMB
//==============================================================================
    private Visita modificaVisitaMB(HttpServletRequest request,Visita visita,MedicoBase medicoBase,Persona paziente,String risultatoVisita){
        if(visita instanceof VisitaMB){
            try {
                if( (visita.getId_Persona().equals(paziente.getCodF())) &&
                    (((VisitaMB)visita).getId_MedicoBase().equals(medicoBase.getCodF())) ){
                    
                    GregorianCalendar data= new GregorianCalendar();
                    Date oggi=new Date(data.getTimeInMillis());

                    visita.setRiassuntoVisita(risultatoVisita);
                    visita.setData(oggi);
                    visita.setVistoReportPaziente(false);
               
                    visitaMB_DAO.update((VisitaMB)visita);
                }else{
                    visita =null;
                }
            }catch (DAOException ex) {
                visita.setData(null);
                visita.setRiassuntoVisita(null);
                visita.setVistoReportPaziente(false);
                visita =null;
                request.getServletContext().log("Imposibile impostare  crare la prescrizione di un esame.", ex);
            }
            return visita;
        }else{
            return null;
        }
    }
//==============================================================================
//                          conclusione VisitaMS
//==============================================================================
    private Visita modificaVisitaMS(HttpServletRequest request,Visita visita,MedicoSpec medicoSpec,Persona paziente,
                                        String risultatoVisita,String report,String pagatoVisita){
        
        if(visita instanceof VisitaMSEReport){
            try {
                if( (visita.getId_Persona().equals(paziente.getCodF())) &&
                    (((VisitaMSEReport)visita).getId_MedicoSpec().equals(medicoSpec.getCodF())) ){
                    
                    if(pagatoVisita== null){
                        ((VisitaMSEReport)visita).setPagato(false);
                    }else{
                        ((VisitaMSEReport)visita).setPagato(true);
                    }

                    GregorianCalendar data= new GregorianCalendar();
                    Date oggi=new Date(data.getTimeInMillis());


                    visita.setRiassuntoVisita(risultatoVisita);
                    visita.setData(oggi);
                    ((VisitaMSEReport)visita).setTichet(50.0);
                    ((VisitaMSEReport)visita).setReport(report);
                    

                    visita.setVistoReportPaziente(false);
                    ((VisitaMSEReport)visita).setVistoReportMedicoBase(false);
                    
                    visitaMSEReport_DAO.update((VisitaMSEReport)visita);
                }else{
                    ((VisitaMSEReport)visita).setPagato(false);
                    visita.setRiassuntoVisita(null);
                    visita.setData(null);
                    ((VisitaMSEReport)visita).setReport(null);
                    visita =null;
                }
            }catch (DAOException ex) {
                visita =null;
                request.getServletContext().log("Imposibile impostare  crare la prescrizione di un esame.", ex);
            }
            return visita;
        }else{
            return null;
        }
    }
//==============================================================================
//                          conclusione VisitaMB
//==============================================================================
    private Boolean setPagamentoMS(HttpServletRequest request,Visita visita,MedicoSpec medicoSpec,String pagatoVisita){
        Boolean ris = false;
        if(visita instanceof VisitaMSEReport){
            Boolean vechioStatoPagamento= ((VisitaMSEReport)visita).getPagato();
            try {
                if(((VisitaMSEReport)visita).getId_MedicoSpec().equals(medicoSpec.getCodF())){
                    Boolean pagato = false;
                    if(pagatoVisita != null){
                        pagato = true;
                    }
                    ((VisitaMSEReport)visita).setPagato(pagato);
                    visitaMSEReport_DAO.update(((VisitaMSEReport)visita));
                    ris = true;
                }
            }catch (DAOException ex) {
                ((VisitaMSEReport)visita).setPagato(vechioStatoPagamento);
                
                ris = false;
                request.getServletContext().log("Imposibile impostare  crare la prescrizione di un esame.", ex);
            }
            
        }else{
            ris = false;
        }
        return ris;
    }
//==============================================================================
//                          INVIO EMAIL Visita
//==============================================================================
     private void inviaEmailVisita(HttpServletRequest request,Visita visita,Persona paziente,Persona medico){
        String cognomeM = medico.getCognome();
        String nomeM = medico.getNome();
        String data = visita.getDataPrescrizione().toString();
        String messaggio;
        String oggetto;
        try{
            messaggio=  "Buongiorno "+paziente.getCognome()+" "+paziente.getNome()+",\n"+
                        "Le inviamo questa e-mail per informarla che Le è stato prescritto una nuova visita "+
                        "dal Dr. "+cognomeM+" "+nomeM+", il giorno "+data+".\n"+
                        "Per maggiori informazzioni consultare il sito web nella propria area privata,"+
                        " nella sezione \"visite da fare\".\n"+
                        "Cordiali saluti,\n"+
                        "Progetto Universitario Servizio Sanitario Nazionale";
            if(visita instanceof VisitaMB){
                 oggetto =    "Aggiunta Visita Medica -- Progetto Universitario Servizio Sanitario Nazionale --";
            }else if(visita instanceof VisitaMSEReport){
                 oggetto =    "Aggiunta Visita Medica Specialista -- Progetto Universitario Servizio Sanitario Nazionale --";
            }else{
                oggetto =    "Aggiunta Visita -- Progetto Universitario Servizio Sanitario Nazionale --";         
            }
            Mail.sendEmail(messaggio,paziente.getEmail(), oggetto);
        }catch(Exception e){
            request.getServletContext().log("Imposibile inviare le email ai pazienti.", e);
        }
     }
//==============================================================================  
//==============================================================================
//                          INVIO EMAIL Report Visita
//==============================================================================
     private void inviaEmailRepotVisita(HttpServletRequest request,Visita visita,Persona paziente,Persona medico){
        String cognomeM = medico.getCognome();
        String nomeM = medico.getNome();
        String data = visita.getDataPrescrizione().toString();
        String messaggio;
        String oggetto;
        try{
            messaggio=  "Buongiorno "+paziente.getCognome()+" "+paziente.getNome()+",\n"+
                        "Le inviamo questa e-mail per informarla che Le è stato prescritto una nuova visita "+
                        "dal Dr. "+cognomeM+" "+nomeM+", il giorno "+data+".\n"+
                        "Per maggiori informazzioni consultare il sito web nella propria area privata,"+
                        " nella sezione \"visite da fare\".\n"+
                        "Cordiali saluti,\n"+
                        "Progetto Universitario Servizio Sanitario Nazionale";
           
            if(visita instanceof VisitaMB){
                 oggetto =    "Aggiunta Report Visita Medica -- Progetto Universitario Servizio Sanitario Nazionale --";
            }else if(visita instanceof VisitaMSEReport){
                 oggetto =    "Aggiunta Report Visita Medica Specialista -- Progetto Universitario Servizio Sanitario Nazionale --";
            }else{
                oggetto =     "Aggiunta Report Visita -- Progetto Universitario Servizio Sanitario Nazionale --";         
            }
            Mail.sendEmail(messaggio,paziente.getEmail(), oggetto);
        }catch(Exception e){
            request.getServletContext().log("Imposibile inviare le email ai pazienti.", e);
        }
     }
//==============================================================================  
}
