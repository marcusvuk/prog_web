
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div name="ConcludiVisitaModal" id="ConcludiVisitaModal"  class="modal fade" >
    <div class="modal-dialog modal-login modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Scrivi esito visita</h4>
                <button type="button"
                        class="close"
                        data-dismiss="modal"
                        aria-hidden="true">
                    &times;
                </button>    
            </div> 

            <form action="Visita.handler" method="POST">
                <input  type="hidden" id="TipoInserimetoVisita" name="TipoInserimetoVisita" value="ConcludiVisita">
                
                
            <div class="modal-body">
                <div class="container-fluid justify-content-center ">  
                    <div class="container">
                        <div class="clearfix">
                            <label><h5><b>Riassunto:</b></h5></label>
                        </div>
                        <textarea class="form-control" id="risultatoVisita" name="risultatoVisita" required="required" rows="5"></textarea>
                    </div>
                </div>
                <c:if test="${s_Persona ne null}">
                    <hr>
                    <div class="container-fluid justify-content-center ">  
                        <div class="container">
                            <div class="clearfix">
                                <label><h5><b>Report: </b></h5></label>
                            </div>
                            <textarea class="form-control" id="reportVisita" name="reportVisita" required="required" rows="5"></textarea>
                        </div>
                    </div>
                </c:if>
            </div>
            <div class=" container-fluid modal-footer">
                <c:if test="${s_Persona ne null}">
                    <h5>
                        <b>
                        <input class="pull-right"
                               type="checkbox" 
                               id="pagatoVisita"
                               name="pagatoVisita"/>
                               <label class="form-check-label" for="pagatoVisita">Pagato</label>  
                        </b>
                    </h5>
                </c:if>
                <input  type="submit"
                        class="btn btn-primary pull-right"
                        accept=""value="Conferma"/>
            </div>
            </form>
        </div>
    </div>
</div>