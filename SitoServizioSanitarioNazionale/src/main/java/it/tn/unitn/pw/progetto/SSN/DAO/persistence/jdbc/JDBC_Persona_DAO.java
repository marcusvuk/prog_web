/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.tn.unitn.pw.progetto.SSN.DAO.persistence.jdbc;

import java.sql.Date;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import it.tn.unitn.pw.progetto.SSN.DAO.common.persistence.dao.jdbc.JDBCDAO;
import it.tn.unitn.pw.progetto.SSN.DAO.common.exception.*;
import it.tn.unitn.pw.progetto.SSN.DAO.persistence.*;
import it.tn.unitn.pw.progetto.SSN.persistence.entities.*;
/**
 *
 * @author Brian
 */
public class JDBC_Persona_DAO extends JDBCDAO<Persona, String> implements Persona_DAO {
/*============================================================================*/    
/*                                      METODI COSTRUTTORI                    */
/*============================================================================*/
    public JDBC_Persona_DAO(Connection con) {
        super(con);
    }
/*============================================================================*/
    
/*============================================================================*/    
/*                      METODI GETTER DAO IMPLEMENTATION                      */
/*============================================================================*/
    @Override
    public Long getCount() throws DAOException {
        try (Statement stmt = CON.createStatement()) {
            ResultSet counter = stmt.executeQuery( 
                     "  SELECT COUNT(*) "               +
                     "  FROM Persona "     );
            if (counter.next()) {
                return counter.getLong(1);
            }
        } catch (SQLException ex) {
            throw new DAOException( " Immpossibile contare gli elementi di Persona  " , ex);
        }
        return 0L;
    }
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    @Override
    public Persona getByPrimaryKey(String primaryKey) throws DAOException {
        if (primaryKey == null) {
            throw new DAOException( " primaryKey è null " );
        }
        try (PreparedStatement stm = CON.prepareStatement(
                                     "  SELECT *  "                      +
                                     "  FROM Persona  "     +
                                     "  WHERE codF = ? "                   )) {
            // imposto i valori
            stm.setString(1, primaryKey);
            
            try (ResultSet rs = stm.executeQuery()) {
                rs.next();
                Persona persona = new Persona();
                persona.setCodF             (rs.getString   ( "codF" ));
                persona.setNome             (rs.getString   ( "nome" ));
                persona.setCognome          (rs.getString   ( "cognome" ));
                persona.setPassword         (rs.getString   ( "password" ));
                persona.setSalt             (rs.getString   ( "salt" ));
                persona.setSesso            (rs.getBoolean  ( "sesso" ));
                persona.setNascita          (rs.getDate     ( "nascita" ));
                persona.setCitta            (rs.getString   ( "citta" ));
                persona.setEmail            (rs.getString   ( "email" ));
                persona.setVive_id_SSP      (rs.getInt      ( "vive_id_SSP" ));
                        
                return persona;
            }
        } catch (SQLException ex) {
            throw new DAOException( " Impossibile perendere la Persona indicato dalla primary key " , ex);
        }
    }
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    @Override
    public List<Persona> getAll() throws DAOException {
        List<Persona> listaPersona = new ArrayList<>();

        try (Statement stm = CON.createStatement()) {
            try (ResultSet rs = stm.executeQuery(
                                 " SELECT *  "            +
                                 " FROM Persona  "        )){
                
                while (rs.next()) {
                    Persona persona = new Persona();
                    persona.setCodF             (rs.getString   ( "codF" ));
                    persona.setNome             (rs.getString   ( "nome" ));
                    persona.setCognome          (rs.getString   ( "cognome" ));
                    persona.setPassword         (rs.getString   ( "password" ));
                    persona.setSalt             (rs.getString   ( "salt" ));
                    persona.setSesso            (rs.getBoolean  ( "sesso" ));
                    persona.setNascita          (rs.getDate     ( "nascita" ));
                    persona.setCitta            (rs.getString   ( "citta" ));
                    persona.setEmail            (rs.getString   ( "email" ));
                    persona.setVive_id_SSP      (rs.getInt      ( "vive_id_SSP" ));
                    
                    listaPersona.add(persona);
                }
            }
        } catch (SQLException ex) {
            throw new DAOException( " Imposibile estrarre tutta la lista di Persona  " , ex);
        }

        return listaPersona;
    }
/*============================================================================*/
   
/*============================================================================*/    
/*                      METODI QUERY                                          */
/*============================================================================*/
//------------------------------------------------------------------------------
//                      INSERT
//------------------------------------------------------------------------------
    @Override
    public  String insert(Persona persona) throws DAOException {
        if (persona == null) {
            throw new DAOException( " La persona è null " );
        }
        try (PreparedStatement ps = CON.prepareStatement(
                     " INSERT INTO Persona "  +
                            " ( codF  "            +    "  ,  "    +
                            "  nome  "             +    "  ,  "    +
                            "  cognome "           +    "  ,  "    +
                            "  password "          +    "  ,  "    +
                            "  salt "              +    "  ,  "    +
                            "  sesso "             +    "  ,  "    +
                            "  nascita "           +    "  ,  "    +
                            "  citta "             +    "  ,  "    +
                            "  email "             +    "  ,  "    +
                            "  vive_id_SSP "       +    "  )  "    +  
                     " VALUES (?,?,?,?,?,?,?,?,?,?) " , Statement.RETURN_GENERATED_KEYS)) {
            
            // imposto i valori
            ps.setString    (1,  persona.getCodF());
            ps.setString    (2,  persona.getNome());
            ps.setString    (3,  persona.getCognome());
            ps.setString    (4,  persona.getPassword());
            ps.setString    (5,  persona.getSalt());
            ps.setBoolean   (6,  persona.getSesso());
            ps.setDate      (7,  persona.getNascita());
            ps.setString    (8,  persona.getCitta());
            ps.setString    (9,  persona.getEmail());
            ps.setInt       (10, persona.getVive_id_SSP());
      //------------------------------------------------------------------------------------------------------------------------------------------------------
            
            ps.executeUpdate();
            
            return persona.getCodF();
        } catch (SQLException ex) {
            throw new DAOException( " Impossibile aggiungere un nuovo record di Persona " , ex);
        }
    }
//------------------------------------------------------------------------------
//                      UPDATE
//------------------------------------------------------------------------------
    @Override
    public void update(Persona persona) throws DAOException {
        if (persona == null) {
            throw new DAOException( " La persona è null " );
        }
        try (PreparedStatement ps = CON.prepareStatement(
                     " UPDATE Persona "  +
                     " SET  "  + "  codF = ?  "             +     "  ,  "    +
                                 "  nome = ?  "             +     "  ,  "    +
                                 "  cognome = ?  "          +     "  ,  "    +
                                 "  password = ?  "         +     "  ,  "    +
                                 "  salt = ?  "             +     "  ,  "    +
                                 "  sesso = ?  "            +     "  ,  "    +
                                 "  nascita = ?  "          +     "  ,  "    +
                                 "  citta = ?  "            +     "  ,  "    +
                                 "  email = ?  "            +     "  ,  "    +
                                 "  vive_id_SSP = ?  "      +     
                     " WHERE codF = ?  " , Statement.RETURN_GENERATED_KEYS)) {
            
            // imposto i valori
            ps.setString    (1,  persona.getCodF());
            
            ps.setString    (2,  persona.getNome());
            ps.setString    (3,  persona.getCognome());
            ps.setString    (4,  persona.getPassword());
            ps.setString    (5,  persona.getSalt());
            ps.setBoolean   (6,  persona.getSesso());
            ps.setDate      (7,  persona.getNascita());
            ps.setString    (8,  persona.getCitta());
            ps.setString    (9,  persona.getEmail());
            ps.setInt       (10, persona.getVive_id_SSP());
            //WHERE
            ps.setString    (11, persona.getCodF());
            
            ps.executeUpdate();
            
            
        } catch (SQLException ex) {
            throw new DAOException( " Impossibile aggiornare il record di Persona " , ex);
        }
    }
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    @Override
    public void update_Password(String id, String Password) throws DAOException {
        if (id == null || Password == null ) {
            throw new DAOException( " id o Password è null " );
        }
        try (PreparedStatement ps = CON.prepareStatement(
                     "  UPDATE Persona  "  +
                     "  SET  password = ?  "         +  
                     "  WHERE codF = ?  " , Statement.RETURN_GENERATED_KEYS)) {
            
            // imposto i valori
            ps.setString(1, Password);
            //WHERE
            ps.setString(2, id);
            
            ps.executeUpdate();
            
        } catch (SQLException ex) {
            throw new DAOException( " Impossibile aggiornare il record di Persona " , ex);
        }
    }

//------------------------------------------------------------------------------
//                      SELECT
//------------------------------------------------------------------------------
    @Override
    public List<Persona> getBySSProvincialeId_Vive(Integer id) throws DAOException {
        if (id == null ) {
            throw new DAOException( " id  è null " );
        }
        List<Persona> listaPersona = new ArrayList<>();

        try (PreparedStatement stm = CON.prepareStatement(
                                 "  SELECT *  "                     +
                                 "  FROM Persona  "                 +
                                 "  WHERE vive_id_SSP =  ?  "  )) {
            
            stm.setInt(1, id);
            try (ResultSet rs = stm.executeQuery()){
                
                while (rs.next()) {
                    Persona persona = new Persona();
                    
                    persona.setCodF             (rs.getString   ( "codF" ));
                    persona.setNome             (rs.getString   ( "nome" ));
                    persona.setCognome          (rs.getString   ( "cognome" ));
                    persona.setPassword         (rs.getString   ( "password" ));
                    persona.setSalt             (rs.getString   ( "salt" ));
                    persona.setSesso            (rs.getBoolean  ( "sesso" ));
                    persona.setNascita          (rs.getDate     ( "nascita" ));
                    persona.setCitta            (rs.getString   ( "citta" ));
                    persona.setEmail            (rs.getString   ( "email" ));
                    persona.setVive_id_SSP      (rs.getInt      ( "vive_id_SSP" ));
                    
                    listaPersona.add(persona);
                }
            }
        } catch (SQLException ex) {
            throw new DAOException( " Imposibile estrarre la lista di Persona  " , ex);
        }

        return listaPersona;
    }
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!<--ok
    public List<Persona> getByNomeCognome(String nome,String cognome)throws DAOException{
        if ( (nome == null) || (cognome == null) ) {
            throw new DAOException( " O il nome o il cognome è null " );
        }
        List<Persona> listaPersona = new ArrayList<>();

        try (PreparedStatement stm = CON.prepareStatement(
                                 "  SELECT *  "                     +
                                 "  FROM Persona  "                 +
                                 "  WHERE nome =  ? AND "           +
                                 "        cognome =  ?  "           )) {
            
            stm.setString(1, nome);
            stm.setString(2, cognome);
            try (ResultSet rs = stm.executeQuery()){
                
                while (rs.next()) {
                    Persona persona = new Persona();
                    
                    persona.setCodF             (rs.getString   ( "codF" ));
                    persona.setNome             (rs.getString   ( "nome" ));
                    persona.setCognome          (rs.getString   ( "cognome" ));
                    persona.setPassword         (rs.getString   ( "password" ));
                    persona.setSalt             (rs.getString   ( "salt" ));
                    persona.setSesso            (rs.getBoolean  ( "sesso" ));
                    persona.setNascita          (rs.getDate     ( "nascita" ));
                    persona.setCitta            (rs.getString   ( "citta" ));
                    persona.setEmail            (rs.getString   ( "email" ));
                    persona.setVive_id_SSP      (rs.getInt      ( "vive_id_SSP" ));
                    
                    listaPersona.add(persona);
                }
            }
        } catch (SQLException ex) {
            throw new DAOException( " Imposibile estrarre la lista di Persona  " , ex);
        }

        return listaPersona;
    }
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!<--ok
    @Override
    public Persona getByPersonaEmailAndPassword(String email, String password) throws DAOException {
        if (email == null || password == null ) {
            throw new DAOException( " email o password è null " );
        }
        try (PreparedStatement stm = CON.prepareStatement(
                                     " SELECT *  "                      +
                                     " FROM Persona  "                  +
                                     " WHERE email = ? "  +    " AND "  +
                                           " password = ? "            )) {
            // imposto i valori
            stm.setString(1, email);
            stm.setString(2, password);
            
            try (ResultSet rs = stm.executeQuery()) {
                rs.next();
                Persona persona = new Persona();
                persona.setCodF             (rs.getString   ( "codF" ));
                persona.setNome             (rs.getString   ( "nome" ));
                persona.setCognome          (rs.getString   ( "cognome" ));
                persona.setPassword         (rs.getString   ( "password" ));
                persona.setSalt             (rs.getString   ( "salt" ));
                persona.setSesso            (rs.getBoolean  ( "sesso" ));
                persona.setNascita          (rs.getDate     ( "nascita" ));
                persona.setCitta            (rs.getString   ( "citta" ));
                persona.setEmail            (rs.getString   ( "email" ));
                persona.setVive_id_SSP      (rs.getInt      ( "vive_id_SSP" ));
                        
                return persona;
            }
        } catch (SQLException ex) {
            throw new DAOException( " Impossibile perendere la Persona indicato dall'email e password " , ex);
        }
    }
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!<--
    public Persona getByPersonaEmail(String email)throws DAOException{
        if (email == null ) {
            throw new DAOException( " email è null " );
        }
        try (PreparedStatement stm = CON.prepareStatement(
                                     " SELECT *  "                      +
                                     " FROM Persona  "                  +
                                     " WHERE email = ? "             )) {
            // imposto i valori
            stm.setString(1, email);
            
            try (ResultSet rs = stm.executeQuery()) {
                rs.next();
                Persona persona = new Persona();
                persona.setCodF             (rs.getString   ( "codF" ));
                persona.setNome             (rs.getString   ( "nome" ));
                persona.setCognome          (rs.getString   ( "cognome" ));
                persona.setPassword         (rs.getString   ( "password" ));
                persona.setSalt             (rs.getString   ( "salt" ));
                persona.setSesso            (rs.getBoolean  ( "sesso" ));
                persona.setNascita          (rs.getDate     ( "nascita" ));
                persona.setCitta            (rs.getString   ( "citta" ));
                persona.setEmail            (rs.getString   ( "email" ));
                persona.setVive_id_SSP      (rs.getInt      ( "vive_id_SSP" ));
                        
                return persona;
            }
        } catch (SQLException ex) {
            throw new DAOException( " Impossibile perendere la Persona indicato dall'email " , ex);
        }
    }
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!<--ok
    @Override
    public String getSaltPersonaByEmail(String email) throws DAOException {
        if (email == null ) {
            throw new DAOException( " email è null " );
        }
        try (PreparedStatement stm = CON.prepareStatement(
                                     " SELECT salt "              +
                                     " FROM Persona "             +
                                     " WHERE email = ? "          )) {
            // imposto i valori 
            stm.setString(1, email);
            
            try (ResultSet rs = stm.executeQuery()) {
                rs.next();
                String salt;
                salt= rs.getString( "salt" );
                        
                return salt;
            }
        } catch (SQLException ex) {
            throw new DAOException( " Impossibile perendere la Persona indicato dall'email " , ex);
        }
    }
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    @Override
    public Persona getByPersonaCodFAndPassword(String codF, String password) throws DAOException {
        if (codF == null || password == null ) {
            throw new DAOException( " codice ficale o password è null " );
        }
        try (PreparedStatement stm = CON.prepareStatement(
                                     "  SELECT *  "                      +
                                     "  FROM Persona  "                  +
                                     "  WHERE codF = ?  "  + "  AND  "   +
                                           "  password = ?  "         )) {
            // imposto i valori
            stm.setString(1, codF);
            stm.setString(2, password);
            
            try (ResultSet rs = stm.executeQuery()) {
                rs.next();
                Persona persona = new Persona();
                persona.setCodF             (rs.getString   ( "codF" ));
                persona.setNome             (rs.getString   ( "nome" ));
                persona.setCognome          (rs.getString   ( "cognome" ));
                persona.setPassword         (rs.getString   ( "password" ));
                persona.setSalt             (rs.getString   ( "salt" ));
                persona.setSesso            (rs.getBoolean  ( "sesso" ));
                persona.setNascita          (rs.getDate     ( "nascita" ));
                persona.setCitta            (rs.getString   ( "citta" ));
                persona.setEmail            (rs.getString   ( "email" ));
                persona.setVive_id_SSP      (rs.getInt      ( "vive_id_SSP" ));
                        
                return persona;
            }
        } catch (SQLException ex) {
            throw new DAOException( " Impossibile perendere la Persona indicato dal codice ficale e password " , ex);
        }
    }
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!<<--OK!!!
    @Override
    public String getSaltPersonaByCodF(String codF) throws DAOException {
        if (codF == null ) {
            throw new DAOException( " Codice fiscale è null " );
        }
        try (PreparedStatement stm = CON.prepareStatement(
                                     "  SELECT salt   "         +
                                     "  FROM Persona  "         +
                                     "  WHERE codF = ? "      )) {
            // imposto i valori 
            stm.setString(1, codF);
            
            try (ResultSet rs = stm.executeQuery()) {
                rs.next();
                String salt ;
                salt= rs.getString( "salt" );
                        
                return salt;
            }
        } catch (SQLException ex) {
            throw new DAOException( " Impossibile perendere la Persona indicato dal codice fiscale " , ex);
        }
    }

/*============================================================================*/
/*============================================================================*/    
/*                         METODO GET Data ultima Prescrizione                */
/*============================================================================*/
    @Override
    public Date getUltimaPrescrizione(String codF)throws DAOException{
        if (codF == null ) {
            throw new DAOException( " Codice fiscale è null " );
        }
        try (PreparedStatement stm = CON.prepareStatement(
            "SELECT MAX(temp) AS minTemp "                                      +
            "FROM (  SELECT DISTINCT VMS.DATAPRESCRIZIONE AS temp "             +
                "    FROM VisitaMSEReport AS VMS "                              +
                "    WHERE VMS.id_Persona = ? "                                 +
                " UNION "                                                       +
                "    SELECT DISTINCT VMB.DATAPRESCRIZIONE  AS temp "            +
                "    FROM VisitaMB AS VMB "                                     +
                "    WHERE VMB.id_Persona = ? "                                 +
                " UNION "                                                       +
                "    SELECT DISTINCT R.DATAPRESCRIZIONE AS temp "               +
                "     FROM Ricetta AS R, CartellaClinicaPerMB AS CC1 "          +
                "    WHERE CC1.id_Persona = ? AND "                             +
                "                  CC1.id = R.id_CartellaClinica "              +
                " UNION "                                                       +
                "    SELECT DISTINCT  PE.DATAPRESCRIZIONE AS temp "             +
                "    FROM PrescrizioneEsameEReferto AS PE, CartellaClinicaPerMB AS CC1 "+
                "    WHERE CC1.id_Persona = ? AND "                             +
                "                  CC1.id = PE.id_CartellaClinica "             +
                ") AS t1  ")) {
            // imposto i valori 
            stm.setString(1, codF);
            stm.setString(2, codF);
            stm.setString(3, codF);
            stm.setString(4, codF);
            try (ResultSet rs = stm.executeQuery()) {
                rs.next();
                Date minTep = rs.getDate("minTemp" );
                        
                return minTep;
            }
        } catch (SQLException ex) {
            throw new DAOException( " Impossibile perendere la data del ultima prescrizione della Persona dal codice fiscale " , ex);
        }
    }
/*============================================================================*/
/*============================================================================*/    
/*                         METODO AGGIUNTIVI                                  */
/*============================================================================*/
    @Override
    public Integer getNumeroNuoviReportEsameForPazienteByPersonaCodF(String codF)throws DAOException{
        if (codF == null ) {
            throw new DAOException( " Codice fiscale è null " );
        }
        try (PreparedStatement stm = CON.prepareStatement(
            " SELECT COUNT(DISTINCT PE.id) AS num "                             +
            " FROM PrescrizioneEsameEReferto AS PE, CartellaClinicaPerMB AS CC "+
            " WHERE CC.id_Persona = ? AND  "                                    +
            "       CC.id = PE.id_CartellaClinica AND "                         +
            "       PE.data IS NOT NULL  AND "                                  +
            "       PE.VISTOREPORTPAZIENTE = false"
                        )) {
            // imposto i valori 
            stm.setString(1, codF);
            try (ResultSet rs = stm.executeQuery()) {
                rs.next();
                Integer num = rs.getInt("num" );
                        
                return num;
            }
        } catch (SQLException ex) {
            throw new DAOException( " Impossibile perendere il numero di esami della Persona da visulizare dal codice fiscale " , ex);
        }
    }
/*============================================================================*/
    @Override
    public Integer getNumeroNuoviReportEsameForMedicoBaseByPersonaCodF(String codF)throws DAOException{
        if (codF == null ) {
            throw new DAOException( " Codice fiscale è null " );
        }
        try (PreparedStatement stm = CON.prepareStatement(
            " SELECT COUNT(DISTINCT PE.id) AS num  "                            +
            " FROM PrescrizioneEsameEReferto AS PE, CartellaClinicaPerMB AS CC "+
            " WHERE CC.id_Persona = ? AND "                                     +
            "       CC.id = PE.id_CartellaClinica AND "                         +
            "       PE.data IS NOT NULL  AND "                                  +
            "       PE.VISTOREPORTOMEDICOBASE = false"
                        )) {
            // imposto i valori 
            stm.setString(1, codF);
            try (ResultSet rs = stm.executeQuery()) {
                rs.next();
                Integer num = rs.getInt("num" );
                        
                return num;
            }
        } catch (SQLException ex) {
            throw new DAOException( " Impossibile perendere il numero di esami della Persona da visulizare dal medico di base dal codice fiscale " , ex);
        }
    }
/*============================================================================*/
    @Override
    public Integer getNumeroNuoviReportVisiteForPazienteByPersonaCodF(String codF)throws DAOException{
        if (codF == null ) {
            throw new DAOException( " Codice fiscale è null " );
        }
        try (PreparedStatement stm = CON.prepareStatement(
                " SELECT SUM(num) AS num "                                      +
                " FROM ( "                                                      +
                "         SELECT COUNT(DISTINCT VMB.id) AS num, 'b' AS T  "     +
                "         FROM  VisitaMB AS VMB   "                             +
                "         WHERE VMB.id_Persona = ? AND  "                       +
                "               VMB.data IS NOT NULL  AND "                     +
                "               VMB.VISTOREPORTPAZIENTE = false "               +
                "         UNION "                                               +
                "         SELECT COUNT(DISTINCT VMS.id) AS num, 's' AS T  "     +
                "         FROM  VisitaMSEReport AS VMS    "                     +
                "         WHERE VMS.id_Persona = ? AND  "                       +
                "               VMS.data IS NOT NULL  AND "                     +
                "               VMS.VISTOREPORTPAZIENTE = false "               +
                "       ) AS Temp "
                        )) {
            // imposto i valori 
            stm.setString(1, codF);
            stm.setString(2, codF);
            try (ResultSet rs = stm.executeQuery()) {
                rs.next();
                Integer num = rs.getInt("num" );
                        
                return num;
            }
        } catch (SQLException ex) {
            throw new DAOException( " Impossibile perendere il numero di Report delle visita della Persona da visulizare dal codice fiscale " , ex);
        }
    }
/*============================================================================*/
    @Override
    public Integer getNumeroNuoviReportVisiteForMedicoBaseByPersonaCodF(String codF)throws DAOException{
        if (codF == null ) {
            throw new DAOException( " Codice fiscale è null " );
        }
        try (PreparedStatement stm = CON.prepareStatement(
                "SELECT COUNT(DISTINCT VMS.id) AS num "                         +
                "FROM  VisitaMSEReport AS VMS    "                              +
                "WHERE VMS.id_Persona = ? AND  "                                +
                "      VMS.data IS NOT NULL  AND "                              +
                "      VMS.VISTOREPORTMEDICOBASE = false "
                        )) {
            // imposto i valori 
            stm.setString(1, codF);
            try (ResultSet rs = stm.executeQuery()) {
                rs.next();
                Integer num = rs.getInt("num" );
                        
                return num;
            }
        } catch (SQLException ex) {
            throw new DAOException( " Impossibile perendere il numero di Report delle visita della Persona del medico Base da visulizare dal codice fiscale " , ex);
        }
    }
/*============================================================================*/
    @Override
    public Integer getNumeroNuoviRicetteByPersonaCodF(String codF)throws DAOException{
        if (codF == null ) {
            throw new DAOException( " Codice fiscale è null " );
        }
        try (PreparedStatement stm = CON.prepareStatement(
            " SELECT COUNT(DISTINCT R.id) AS num  "                             +
            " FROM Ricetta AS R, CartellaClinicaPerMB AS CC "                   +
            " WHERE CC.id_Persona = ? AND  "                                    +
            "       CC.id = R.id_CartellaClinica AND  "                         +
            "       R.VISTOPAZIENTE = false "
                        )) {
            // imposto i valori 
            stm.setString(1, codF);
            try (ResultSet rs = stm.executeQuery()) {
                rs.next();
                Integer num = rs.getInt("num" );
                        
                return num;
            }
        } catch (SQLException ex) {
            throw new DAOException( " Impossibile perendere il numero di ricetta della Persona da visulizare dal codice fiscale " , ex);
        }
    }
/*============================================================================*/
    @Override
    public Integer getNumeroNuoviVisiteByPersonaCodF(String codF)throws DAOException{
        if (codF == null ) {
            throw new DAOException( " Codice fiscale è null " );
        }
        try (PreparedStatement stm = CON.prepareStatement(
                " SELECT SUM(num) AS num "                                      +
                " FROM ( "                                                      +
                "         SELECT COUNT(DISTINCT VMB.id) AS num,'b' AS T  "      +
                "         FROM  VisitaMB AS VMB   "                             +
                "         WHERE VMB.id_Persona = ? AND "                        +
                "               VMB.data IS NULL  AND "                         +
                "               VMB.VISTOPAZIENTE = false "                     +
                "         UNION "                                               +
                "         SELECT COUNT(DISTINCT VMS.id) AS num , 's' AS T "     +
                "         FROM  VisitaMSEReport AS VMS    "                     +
                "         WHERE VMS.id_Persona = ? AND  "                       +
                "               VMS.data IS NULL  AND "                         +
                "               VMS.VISTOPAZIENTE = false "                     +
                "       ) AS Temp"
                        )) {
            // imposto i valori 
            stm.setString(1, codF);
            stm.setString(2, codF);
            try (ResultSet rs = stm.executeQuery()) {
                rs.next();
                Integer num = rs.getInt("num" );
                        
                return num;
            }
        } catch (SQLException ex) {
            throw new DAOException( " Impossibile perendere il numero di visita della Persona da visulizare dal codice fiscale " , ex);
        }
    }
/*============================================================================*/
    @Override
    public Integer getNumeroNuoviEsamiByPersonaCodF(String codF)throws DAOException{
        if (codF == null ) {
            throw new DAOException( " Codice fiscale è null " );
        }
        try (PreparedStatement stm = CON.prepareStatement(
            " SELECT COUNT(DISTINCT PE.id) AS num  "                            +
            " FROM PrescrizioneEsameEReferto AS PE, CartellaClinicaPerMB AS CC "+
            " WHERE CC.id_Persona = ? AND "                                     +
            "       CC.id = PE.id_CartellaClinica AND "                         +
            "       PE.data IS NULL  AND "                                      +
            "       PE.VISTOPAZIENTE = false"
                        )) {
            // imposto i valori 
            stm.setString(1, codF);
            try (ResultSet rs = stm.executeQuery()) {
                rs.next();
                Integer num = rs.getInt("num" );
                        
                return num;
            }
        } catch (SQLException ex) {
            throw new DAOException( " Impossibile perendere il numero di esami della Persona da visulizare  dal codice fiscale " , ex);
        }
    }
            
/*============================================================================*/
    public Integer getNumeroEsamiSenzaReportByPersonaCodF(String codF)throws DAOException{
        if (codF == null ) {
            throw new DAOException( " Codice fiscale è null " );
        }
        try (PreparedStatement stm = CON.prepareStatement(
            " SELECT COUNT(DISTINCT PE.id) AS num   "                           +
            " FROM PrescrizioneEsameEReferto AS PE, CartellaClinicaPerMB AS CC "+
            " WHERE CC.id_Persona = ? AND  "                                    +
            "      CC.id = PE.id_CartellaClinica AND   "                        +
            "      PE.data IS NULL"
                        )) {
            // imposto i valori 
            stm.setString(1, codF);
            try (ResultSet rs = stm.executeQuery()) {
                rs.next();
                Integer num = rs.getInt("num" );
                        
                return num;
            }
        } catch (SQLException ex) {
            throw new DAOException( " Impossibile perendere il numero di esami della Persona da visulizare  dal codice fiscale " , ex);
        }
    }
            
/*============================================================================*/
    public Integer getNumeroVisiteSenzaReportByPersonaCodF(String codF)throws DAOException{
        if (codF == null ) {
            throw new DAOException( " Codice fiscale è null " );
        }
        try (PreparedStatement stm = CON.prepareStatement(
            " SELECT COUNT(DISTINCT VMS.id) AS num   "                          +
            " FROM VisitaMSEReport AS VMS "                                     +
            " WHERE VMS.id_Persona = ? AND  "                                   +
            "       VMS.data IS NULL"
                        )) {
            // imposto i valori 
            stm.setString(1, codF);
            try (ResultSet rs = stm.executeQuery()) {
                rs.next();
                Integer num = rs.getInt("num" );
                        
                return num;
            }
        } catch (SQLException ex) {
            throw new DAOException( " Impossibile perendere il numero di visita della Persona da visulizare  dal codice fiscale " , ex);
        }
    }
/*============================================================================*/
/*============================================================================*/    
/*                         METODO GER PERSONA Intervallo di età               */
/*============================================================================*/
    public List<Persona> getBySSProvincialeId_Vive_E_NatoNelIntervallo(Integer id,Date inizio , Date fine)throws DAOException{
        if ( (id == null)||(inizio == null)||(fine == null) ) {
            throw new DAOException( "inizio, fine o id  è null " );
        }else if(inizio.after(fine)){
            
            throw new DAOException( "La fine è prima del inizio. " );
        }
        List<Persona> listaPersona = new ArrayList<>();

        try (PreparedStatement stm = CON.prepareStatement(
                                 "  SELECT *  "                     +
                                 "  FROM Persona  "                 +
                                 "  WHERE vive_id_SSP =  ?  AND "   +
                                 "        nascita BETWEEN ? AND ? ")) {
            
            stm.setInt(1, id);
            stm.setDate(2, inizio);
            stm.setDate(3, fine);
            try (ResultSet rs = stm.executeQuery()){
                
                while (rs.next()) {
                    Persona persona = new Persona();
                    
                    persona.setCodF             (rs.getString   ( "codF" ));
                    persona.setNome             (rs.getString   ( "nome" ));
                    persona.setCognome          (rs.getString   ( "cognome" ));
                    persona.setPassword         (rs.getString   ( "password" ));
                    persona.setSalt             (rs.getString   ( "salt" ));
                    persona.setSesso            (rs.getBoolean  ( "sesso" ));
                    persona.setNascita          (rs.getDate     ( "nascita" ));
                    persona.setCitta            (rs.getString   ( "citta" ));
                    persona.setEmail            (rs.getString   ( "email" ));
                    persona.setVive_id_SSP      (rs.getInt      ( "vive_id_SSP" ));
                    
                    listaPersona.add(persona);
                }
            }
        } catch (SQLException ex) {
            throw new DAOException( " Imposibile estrarre la lista di Persona  " , ex);
        }

        return listaPersona;
    }
/*============================================================================*/
}
