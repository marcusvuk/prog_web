/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.tn.unitn.pw.progetto.SSN.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.security.MessageDigest;
import javax.servlet.http.Cookie;

import it.tn.unitn.pw.progetto.SSN.DAO.common.exception.DAOException;
import it.tn.unitn.pw.progetto.SSN.DAO.common.exception.DAOFactoryException;
import it.tn.unitn.pw.progetto.SSN.DAO.common.factory.DAOFactory;
import it.tn.unitn.pw.progetto.SSN.DAO.persistence.*;
import it.tn.unitn.pw.progetto.SSN.persistence.entities.Persona;
import it.tn.unitn.pw.progetto.SSN.persistence.entities.Farmacia;
import it.tn.unitn.pw.progetto.SSN.persistence.entities.MedicoBase;
import it.tn.unitn.pw.progetto.SSN.persistence.entities.MedicoSpec;
import it.tn.unitn.pw.progetto.SSN.persistence.entities.SSProvinciale;

import it.tn.unitn.pw.progetto.SSN.listener.WebAppContextListener;

import java.nio.charset.StandardCharsets;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;

/**
 *
 * @author Brian
 */
public class srv_Index extends HttpServlet {
    
    public srv_Index() {
        super();
    }
//------------------------------------------------------------------------------
    private Integer TEMPO_VITA_COOKIES=3600;//3600 = 1 ora
//------------------------------------------------------------------------------
    private static  HashMap<String, Object> authenticatedUsers=null;

    static public void myInit(HashMap<String, Object> authenticatedUsers1){
        if(authenticatedUsers == null){            authenticatedUsers=authenticatedUsers1;
        }
    }
    private Object retrieveUsername(String jSessionId) {
        return authenticatedUsers.get(jSessionId);
    }
//------------------------------------------------------------------------------
    
    
    
    private Persona_DAO         persona_Dao;
    private MedicoBase_DAO      medicoBase_Dao;
    private MedicoSpec_DAO      medicoSpec_Dao;
    private Farmacia_DAO        farmacia_DAO;
    private SSProvinciale_DAO   ssProvinciale_DAO;
        
    private Persona             persona;
    private Farmacia            farmacia;
    private SSProvinciale       ssProvinciale;
    
    @Override
    public void init() throws ServletException {
        DAOFactory daoFactory = (DAOFactory) super.getServletContext().getAttribute("daoFactory");
        if (daoFactory == null) {
            throw new ServletException("Impossible to get dao factory for user storage system");
        }
        try {
            persona_Dao         = daoFactory.getDAO(Persona_DAO.class);
            medicoBase_Dao      = daoFactory.getDAO(MedicoBase_DAO.class);
            medicoSpec_Dao      = daoFactory.getDAO(MedicoSpec_DAO.class);
            farmacia_DAO        = daoFactory.getDAO(Farmacia_DAO.class);
            ssProvinciale_DAO   = daoFactory.getDAO(SSProvinciale_DAO.class);
            
        } catch (DAOFactoryException ex) {
            throw new ServletException("Impossible to get dao factory for user storage system", ex);
        }
    }
//==============================================================================
//                          GET and add Cookies
//==============================================================================
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        response.sendRedirect("index.jsp");
    }
//==============================================================================
//==============================================================================
//                          POST and Loggin all type of user  
//==============================================================================
    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * Questa funzione permette di capire quale tipo di utente sta richedendo 
     * di registrarsi
     * 
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String typeLogin        = request.getParameter("login");
        if(typeLogin.equals("Cittadino")){
            loginPersona(request,response);
            
        }else if(typeLogin.equals("MBase")){
            loginMedicoBase(request,response);
            
        }else if(typeLogin.equals("MSpecialista")){
            loginMedicoSpecialista(request,response);
            
        }else if(typeLogin.equals("Farmacia")){
            loginFarmacia(request, response);
            
        }else if(typeLogin.equals("SSProvinciale")){
            loginSSProvinciale(request, response);
            
        }else{
            // ERROR ------------------------------------------------------------------------------------------
            response.sendRedirect("index.jsp");
            // ERROR ------------------------------------------------------------------------------------------
        }
    }

//####################################################################################
//####################################################################################
//==============================================================================
//                          Login PERSONA
//==============================================================================
    protected void loginPersona (HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //String codFicale    = request.getParameter("codiceFiscale");
        String email        = request.getParameter("email");
        String password     = request.getParameter("password");
        String ricordami     = request.getParameter("ricordami");
                
        String contextPath = getServletContext().getContextPath();
        if (!contextPath.endsWith("/")) {
            contextPath += "/";
        }

        if( (password   == ""    || password  == null )   | 
            ( email == ""   || email == null ) ){
            response.sendRedirect(response.encodeRedirectURL(contextPath + "index.jsp"));
        }else{
            persona= getPersonaLoged(request,email,password);
            if(persona != null){
                request.getSession().setAttribute("c_Persona", persona);
                //Cookies set
                String jSessionId = Long.toHexString(Double.doubleToLongBits(Math.random()));
                authenticatedUsers.put(jSessionId, persona);
                Cookie cookie = new Cookie("jsessionid", jSessionId);
                if(ricordami==null){
                    cookie.setMaxAge(-1);
                }else{
                    cookie.setMaxAge(TEMPO_VITA_COOKIES);
                }
                response.addCookie(cookie);
                //Cookies set

                response.sendRedirect(response.encodeRedirectURL(contextPath + "Cittadino/cittadino.jsp"));
            }else{
                response.sendRedirect(response.encodeRedirectURL(contextPath + "index.jsp"));
            } 
        }
        
    }
//==============================================================================
//                          Login Medico Di Base
//==============================================================================
    protected void loginMedicoBase (HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //String codFicale    = request.getParameter("codiceFiscale");
        String email        = request.getParameter("email");
        String password     = request.getParameter("password");
        String ricordami     = request.getParameter("ricordami");
        
        String contextPath = getServletContext().getContextPath();
        if (!contextPath.endsWith("/")) {
            contextPath += "/";
        }
        
       
        if( (password   == ""    || password  == null )   | 
            ( email == ""   || email == null ) ){
            response.sendRedirect(response.encodeRedirectURL(contextPath + "index.jsp"));
        }else{
            persona= getMedicoBaseLoged(request,email,password);
            if(persona != null){
                request.getSession().setAttribute("b_Persona", persona);
            
                //Cookies set
                String jSessionId = Long.toHexString(Double.doubleToLongBits(Math.random()));
                authenticatedUsers.put(jSessionId, persona);
                Cookie cookie = new Cookie("jsessionid", jSessionId);
                if(ricordami==null){
                    cookie.setMaxAge(-1);
                }else{
                    cookie.setMaxAge(TEMPO_VITA_COOKIES);
                }
                response.addCookie(cookie);
                //Cookies set
                
                response.sendRedirect(response.encodeRedirectURL(contextPath + "MedicoBase/medicoBase.jsp"));
            }else{
                response.sendRedirect(response.encodeRedirectURL(contextPath + "index.jsp"));
            } 
        }
        
    }
//==============================================================================
//                          Login Medico Specialita
//==============================================================================
    protected void loginMedicoSpecialista (HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //String codFicale    = request.getParameter("codiceFiscale");
        String email        = request.getParameter("email");
        String password     = request.getParameter("password");
        String ricordami     = request.getParameter("ricordami");
                
        String contextPath = getServletContext().getContextPath();
        if (!contextPath.endsWith("/")) {
            contextPath += "/";
        }
        
        
        if( (password   == ""    || password  == null )   | 
            ( email == ""   || email == null ) ){
            response.sendRedirect(response.encodeRedirectURL(contextPath + "index.jsp"));
        }else{
            persona= getMedicoSpecialistaLoged(request,email,password);
            if(persona != null){
                request.getSession().setAttribute("s_Persona", persona);
                
                //Cookies set
                String jSessionId = Long.toHexString(Double.doubleToLongBits(Math.random()));
                authenticatedUsers.put(jSessionId, persona);
                Cookie cookie = new Cookie("jsessionid", jSessionId);
                if(ricordami==null){
                    cookie.setMaxAge(-1);
                }else{
                    cookie.setMaxAge(TEMPO_VITA_COOKIES);
                }
                response.addCookie(cookie);
                //Cookies set
                
                response.sendRedirect(response.encodeRedirectURL(contextPath + "MedicoSpec/medicoSpec.jsp"));
            }else{
                response.sendRedirect(response.encodeRedirectURL(contextPath + "index.jsp"));
            } 
        }
        
    }
//==============================================================================
//                          Login Farmacia 
//==============================================================================
    protected void loginFarmacia (HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //String codFicale    = request.getParameter("codiceFiscale");
        String email        = request.getParameter("email");
        String password     = request.getParameter("password");
        String ricordami     = request.getParameter("ricordami");
        
        String contextPath = getServletContext().getContextPath();
        if (!contextPath.endsWith("/")) {
            contextPath += "/";
        }
        
        
        if( (password   == ""    || password  == null )   | 
            ( email == ""   || email == null ) ){
            response.sendRedirect(response.encodeRedirectURL(contextPath + "index.jsp"));
        }else{
            farmacia= getFarmaciaLoged(request,email,password);
            if(farmacia != null){
                request.getSession().setAttribute("f_Farmacia", farmacia);
                
                //Cookies set
                String jSessionId = Long.toHexString(Double.doubleToLongBits(Math.random()));
                authenticatedUsers.put(jSessionId, farmacia);
                Cookie cookie = new Cookie("jsessionid", jSessionId);
                if(ricordami==null){
                    cookie.setMaxAge(-1);
                }else{
                    cookie.setMaxAge(TEMPO_VITA_COOKIES);
                }
                response.addCookie(cookie);
                //Cookies set
                
                response.sendRedirect(response.encodeRedirectURL(contextPath + "Farmacia/farmacia.jsp"));
            }else{
                response.sendRedirect(response.encodeRedirectURL(contextPath + "index.jsp"));
            } 
        }
        
    }
//==============================================================================
//                          Login SSProvinciale 
//==============================================================================
    protected void loginSSProvinciale  (HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //String codFicale    = request.getParameter("codiceFiscale");
        String provincia     = request.getParameter("autocomplete-provincia");
        String password      = request.getParameter("password");
        String ricordami     = request.getParameter("ricordami");
        
        String contextPath = getServletContext().getContextPath();
        if (!contextPath.endsWith("/")) {
            contextPath += "/";
        }
        
        
        if( (password   == ""    || password  == null )   | 
            ( provincia == ""   || provincia == null ) ){
            response.sendRedirect(response.encodeRedirectURL(contextPath + "index.jsp"));
        }else{
            ssProvinciale = getSSProvincialeLoged(request,provincia,password);
            if(ssProvinciale != null){
                request.getSession().setAttribute("p_ssProvinciale", ssProvinciale);
                
                //Cookies set
                String jSessionId = Long.toHexString(Double.doubleToLongBits(Math.random()));
                authenticatedUsers.put(jSessionId, ssProvinciale);
                Cookie cookie = new Cookie("jsessionid", jSessionId);
                if(ricordami==null){
                    cookie.setMaxAge(-1);
                }else{
                    cookie.setMaxAge(TEMPO_VITA_COOKIES);
                }
                response.addCookie(cookie);
                //Cookies set
                
                response.sendRedirect(response.encodeRedirectURL(contextPath + "SSP/SSP.jsp"));
            }else{
                response.sendRedirect(response.encodeRedirectURL(contextPath + "index.jsp"));
            } 
        }
        
    }
//####################################################################################
//####################################################################################
    
//==============================================================================
//          Verifica credenziali Persona
//==============================================================================
    private Persona getPersonaLoged(HttpServletRequest request,String email, String password){
        Persona persona=new Persona();
        try {
            /*Solo persona*/
            String saltPersona = persona_Dao.getSaltPersonaByEmail(email);
            if( saltPersona == null){
                return null;
            }
            String hashPassword = convertToHashPassword(password,saltPersona);
            persona= persona_Dao.getByPersonaEmailAndPassword(email, hashPassword);
        } catch (DAOException ex) {
            //TODO: log exception
            request.getServletContext().log("Impossibile recuperare la persona.", ex);
            persona= null;
        }finally{
            return persona;
        }
        
    }
//==============================================================================  
//          Verifica credenziali Medico Specialista
//==============================================================================
    private Persona getMedicoSpecialistaLoged(HttpServletRequest request,String email, String password){
        Persona persona=new MedicoSpec();
        try {
            /*Solo persona*/
            String saltPersona = medicoSpec_Dao.getSaltMedicoSpecByEmail(email);
            if( saltPersona == null){
                return null;
            }
            String hashPassword = convertToHashPassword(password,saltPersona);
            persona= medicoSpec_Dao.getByMedicoSpecEmailAndPassword(email, hashPassword);
        } catch (DAOException ex) {
            //TODO: log exception
            request.getServletContext().log("Imposibile recuperare il medico specialista.", ex);
            persona= null;
        }finally{
            return persona;
        }
        
    }
//==============================================================================    
//==============================================================================
//          Verifica credenziali Medico Base
//==============================================================================
    private Persona getMedicoBaseLoged(HttpServletRequest request,String email, String password){
        Persona persona=new MedicoBase();
        try {
            /*Solo persona*/
            String saltPersona = medicoBase_Dao.getSaltMedicoBaseByEmail(email);
            if( saltPersona == null){
                return null;
            }
            String hashPassword = convertToHashPassword(password,saltPersona);
            persona= medicoBase_Dao.getByMedicoBaseEmailAndPassword(email, hashPassword);
        } catch (DAOException ex) {
            //TODO: log exception
            request.getServletContext().log("Imposibile recuperare il medico di base.", ex);
            persona= null;
        }finally{
            return persona;
        }
        
    }
//============================================================================== 
//==============================================================================
//          Verifica credenziali Medico Base
//==============================================================================
    private Farmacia getFarmaciaLoged(HttpServletRequest request,String email, String password){
        Farmacia farmacia=new Farmacia();
        try {
            /*Solo persona*/
            String saltFarmacia = farmacia_DAO.getSaltFarmaciaByEmail(email);
            if( saltFarmacia == null){
                return null;
            }
            String hashPassword = convertToHashPassword(password,saltFarmacia);
            farmacia= farmacia_DAO.getByFarmaciaEmailAndPassword(email, hashPassword);
        } catch (DAOException ex) {
            //TODO: log exception
            request.getServletContext().log("Imposibile recuperare il medico di base.", ex);
            farmacia= null;
        }finally{
            return farmacia;
        }
        
    }
//==============================================================================//==============================================================================
//          Verifica credenziali Medico Base
//==============================================================================
    private SSProvinciale getSSProvincialeLoged(HttpServletRequest request,String provicia, String password){
        SSProvinciale ssProvinciale=new SSProvinciale();
        try {
            /*Solo persona*/
            String saltProvinciale = ssProvinciale_DAO.getSaltSSProvincialeByNome(provicia);
            if( saltProvinciale == null){
                return null;
            }
            String hashPassword = convertToHashPassword(password,saltProvinciale);
            ssProvinciale= ssProvinciale_DAO.getBySSProvincialeNomeAndPassword(provicia, hashPassword);
        } catch (DAOException ex) {
            //TODO: log exception
            request.getServletContext().log("Imposibile recuperare il medico di base.", ex);
            ssProvinciale= null;
        }finally{
            return ssProvinciale;
        }
        
    }   
//####################################################################################
//####################################################################################
//==============================================================================
//      Hash la password e il salt
//==============================================================================
    public static String convertToHashPassword(String password, String saltPersona) {
        String originalString= password+saltPersona;
        String hashPassword = sha256(originalString);

        return hashPassword;
    }
    
//==============================================================================
//==============================================================================
//==============================================================================
    public static String sha256(String passwordToHash) {
        String password  = null;
        try {
  
            MessageDigest digest = MessageDigest.getInstance("SHA-256");
            byte[] encodedhash = digest.digest(
                passwordToHash.getBytes(StandardCharsets.UTF_8));
            
            StringBuffer hexString = new StringBuffer();
            
            for (int i = 0; i < encodedhash.length; i++) {
                String hex = Integer.toHexString(0xff & encodedhash[i]);
                if(hex.length() == 1) hexString.append('0');
                    hexString.append(hex);
            }
            password =new String(hexString);
            return password;
            
            
        } catch (Exception e) {
            System.out.println("Exception while calculating SHA-256 digest value"+e);
        }
        return password;
    }
//==============================================================================
//==============================================================================
//==============================================================================

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
