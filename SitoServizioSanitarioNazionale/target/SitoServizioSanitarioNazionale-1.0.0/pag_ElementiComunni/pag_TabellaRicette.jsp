<%--<table> � nella pagina in cui si importa--%>    
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<thead class="thead-light">
    <tr>
        <c:if test="${f_Farmacia eq null}">
            <th></th>
            <th>Ritirati</th>
        </c:if>
        <th>Scadenza</th>
        <th>Nome Farmaco</th>
        <th>Quantit&agrave;</th>
        <th>Nome medico</th>
        <th>Cognome medico</th>
        <th></th>
    </tr>
</thead>
<tbody class="align-middle">
</tbody>
<tfoot class="thead-light">
    <tr>
        <c:if test="${f_Farmacia eq null}">
            <th></th>
            <th>Ritirati</th>
        </c:if>
        <th>Scadenza</th>
        <th>Nome Farmaco</th>
        <th>Quantit&agrave;</th>
        <th>Nome medico</th>
        <th>Cognome medico</th>
        <th></th>
    </tr>
</tfoot>
<%--</table>--%>
