/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.tn.unitn.pw.progetto.SSN.persistence.entities;

import java.sql.Date;
/**
 *
 * @author Brian
 */
public class Persona {
/*============================================================================*/
/*                                       ATTRIBUTI                            */
/*============================================================================*/
    private String      codF;
    private String      nome;
    private String      cognome;
    private String      password;
    private String      salt;
    private Boolean     sesso;
    private Date        nascita;
    private String      citta;
    private String      email;
    private Integer     vive_id_SSP;
    
/*============================================================================*/
    
/*============================================================================*/    
/*                                      METODI COSTRUTTORI                    */
/*============================================================================*/
    public Persona() {
        codF        = null;
        nome        = null;
        cognome     = null;
        password    = null;
        salt        = null;
        sesso       = null;
        nascita     = null;
        citta       = null;
        email       = null;
        vive_id_SSP = null;
    }
/*============================================================================*/

/*============================================================================*/    
/*                                      METODI GETTER E SETTER                */
/*============================================================================*/
    /*--------------------------------------*/    
    public String getCodF() {
        return codF;
    }

    public void setCodF(String codF) {
        this.codF = codF;
    }
    /*--------------------------------------*/
    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }
    /*--------------------------------------*/
    public String getCognome() {
        return cognome;
    }

    public void setCognome(String cognome) {
        this.cognome = cognome;
    }
    /*--------------------------------------*/
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
    /*--------------------------------------*/
    public String getSalt() {
        return salt;
    }

    public void setSalt(String salt) {
        this.salt = salt;
    }
    /*--------------------------------------*/
    public Boolean getSesso() {
        return sesso;
    }

    public void setSesso(Boolean sesso) {
        this.sesso = sesso;
    }
    /*--------------------------------------*/
    public Date getNascita() {
        return nascita;
    }

    public void setNascita(Date nascita) {
        this.nascita = nascita;
    }
    /*--------------------------------------*/
    public String getCitta() {
        return citta;
    }

    public void setCitta(String citta) {
        this.citta = citta;
    }
    /*--------------------------------------*/
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
    /*--------------------------------------*/
    public Integer getVive_id_SSP() {
        return vive_id_SSP;
    }

    public void setVive_id_SSP(Integer vive_id_SSP) {
        this.vive_id_SSP = vive_id_SSP;
    }
    /*--------------------------------------*/
/*============================================================================*/
}
