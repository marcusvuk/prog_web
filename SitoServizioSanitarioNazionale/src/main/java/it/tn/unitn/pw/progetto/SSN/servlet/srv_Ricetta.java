/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.tn.unitn.pw.progetto.SSN.servlet;

import it.tn.unitn.pw.progetto.SSN.DAO.common.exception.DAOException;
import it.tn.unitn.pw.progetto.SSN.DAO.common.exception.DAOFactoryException;
import it.tn.unitn.pw.progetto.SSN.DAO.common.factory.DAOFactory;
import it.tn.unitn.pw.progetto.SSN.DAO.persistence.*;
import it.tn.unitn.pw.progetto.SSN.Genera.Email.Mail;
import it.tn.unitn.pw.progetto.SSN.persistence.entities.*;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.util.GregorianCalendar;
import java.sql.Date;


/**
 *
 * @author Brian
 */
public class srv_Ricetta extends HttpServlet {
//------------------------------------------------------------------------------   
    private Farmacia_DAO                farmacia_DAO;
    private Ricetta_DAO                 ricetta_DAO;
    private CartellaClinicaPerMB_DAO    cartellaClinica_Dao;
    
    @Override
    public void init() throws ServletException {
        DAOFactory daoFactory = (DAOFactory) super.getServletContext().getAttribute("daoFactory");
        if (daoFactory == null) {
            throw new ServletException("Impossible to get dao factory for user storage system");
        }
        try {
            ricetta_DAO         = daoFactory.getDAO(Ricetta_DAO.class);
            farmacia_DAO        = daoFactory.getDAO(Farmacia_DAO.class);
            cartellaClinica_Dao = daoFactory.getDAO(CartellaClinicaPerMB_DAO.class);
            
        } catch (DAOFactoryException ex) {
            throw new ServletException("Impossible to get dao factory for user storage system", ex);
        }
    }
//------------------------------------------------------------------------------
   
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        String contextPath = getServletContext().getContextPath();
        if (!contextPath.endsWith("/")) {
            contextPath += "/";
        }
        
        Persona cittadino        = (Persona)  request.getSession(false).getAttribute("c_Persona");
        
        Farmacia farmacia        = (Farmacia) request.getSession(false).getAttribute("f_Farmacia");
        Persona clienteFarmacia  = (Persona)  request.getSession(false).getAttribute("f_Cliente");
        
        if( (cittadino != null) && (farmacia == null) ){
            getRicettaCittadino(request,response,cittadino);
            
        }else if( (cittadino == null) && (farmacia != null) && (clienteFarmacia!=null)){
            getRicettaFarmacia(request,response,clienteFarmacia);
        }else {
            //ERRORE--------------------------------------------------------------------------------------------
            response.sendRedirect(response.encodeRedirectURL(contextPath + "Index"));
        }
    }
    
//############################################################################## 
    
    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String contextPath = getServletContext().getContextPath();
        if (!contextPath.endsWith("/")) {
            contextPath += "/";
        }
        
        String tipoInserimetoEsame = request.getParameter("TipoInserimetoRicetta");
        
        if(tipoInserimetoEsame.equals("ErogaRicetta")){//Medioco Specialita 
             erogaRicetta(request,response);
            
        }else if(tipoInserimetoEsame.equals("AggiungiRicetta")){// Medico Base
            creaNuovaRicetta(request,response);
            
        }else{
            //ERRORE--------------------------------------------------------------------------------------------
            response.sendRedirect(response.encodeRedirectURL(contextPath + "Index"));
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
//==============================================================================
//                          Crea un nuova ricetta
//==============================================================================
    private void creaNuovaRicetta(HttpServletRequest request,HttpServletResponse response)
                                    throws ServletException, IOException {
        String contextPath = getServletContext().getContextPath();
        if (!contextPath.endsWith("/")) {
            contextPath += "/";
        }
        
        String     idFarmaci     = request.getParameter("idFarmaci");
        String     quantita      = request.getParameter("quantita");
        String     dataScadenza  = request.getParameter("dataScadenza");
        
        MedicoBase medicoBase = (MedicoBase) request.getSession(false).getAttribute("b_Persona");
        Persona    paziente   = (Persona) request.getSession(false).getAttribute("b_Paziente");
       
        Ricetta ricetta =null;
        if( (idFarmaci != null) && (quantita != null) && (dataScadenza != null)) {
            ricetta = creaRicetta(request,medicoBase,paziente,idFarmaci,quantita,dataScadenza);
        }
        
        if(ricetta != null){
            response.sendRedirect(response.encodeRedirectURL(contextPath + "MedicoBase/PazienteMB/ConfermaCreazione.jsp"));
            //INVIA EMAIL
            inviaRicettePrescrizione(request,ricetta,paziente,medicoBase);
            //INVIA EMAIL
        }else{
            response.sendRedirect(response.encodeRedirectURL(contextPath + "MedicoBase/PazienteMB/prescriviFarmaci.jsp"));
        }
    }
//==============================================================================
//                          Crea Prescrizione Ricetta
//==============================================================================
    private Ricetta creaRicetta(HttpServletRequest request,MedicoBase medicoBase,
                                Persona paziente,String idFarmaciString,String quantitaString,String dataScadenza){
        Ricetta ricetta=null;
        try {
            Integer idFarmaci = Integer.parseInt(idFarmaciString);
            Integer quantita = Integer.parseInt(quantitaString);
            
            String[] arrayScadenza = dataScadenza.split("-");//YYYY-MM-DD
            
            if(arrayScadenza.length==3){
                Integer anno    = Integer.parseInt(arrayScadenza[0]);
                Integer mese    = Integer.parseInt(arrayScadenza[1]);
                Integer giorno  = Integer.parseInt(arrayScadenza[2]);
                
                
                CartellaClinicaPerMB catellaClinicaAttiva= cartellaClinica_Dao.getByPazienteId_Attivo(paziente.getCodF());
                if(catellaClinicaAttiva!= null){
                    GregorianCalendar data= new GregorianCalendar();
                    Date oggi=new Date(data.getTimeInMillis());
                    
                    GregorianCalendar daraS =new GregorianCalendar(anno, mese, giorno);
                    Date scadenza   = new Date(daraS.getTimeInMillis());
                    
                    if(scadenza.after(oggi)){
                        ricetta = new Ricetta();

                        ricetta.setId(null);
                        ricetta.setScadenza(scadenza);
                        ricetta.setDataEvalsa(null);
                        ricetta.setDataPrescrizione(oggi);
                        ricetta.setQuantita(quantita);
                        ricetta.setPrezzoFarmacia(3.0);
                        ricetta.setId_CartellaClinica(catellaClinicaAttiva.getId());
                        ricetta.setId_Farmaco(idFarmaci);
                        ricetta.setId_Farmacia(null);
                        ricetta.setVistoPaziente(false);

                        if(ricetta_DAO.insert(ricetta)==null){
                            ricetta = null;
                        }
                    }
                }
            }
        }catch (DAOException ex) {
            ricetta =null;
            request.getServletContext().log("Imposibile impostare  crare la prescrizione di un esame.", ex);
        }catch (Exception ex) {
            request.getServletContext().log("Imposibile recuperare convertitre l'id del esame in un intero.", ex);
            ricetta= null;
        }finally{
            return  ricetta;
        }
    }
//==============================================================================
//                          Get Ricetta per cittadino
//==============================================================================
    private void getRicettaCittadino(HttpServletRequest request,HttpServletResponse response,Persona persona)
                                   throws ServletException, IOException {
        
        String contextPath = getServletContext().getContextPath();
        if (!contextPath.endsWith("/")) {
            contextPath += "/";
        }
        
        Ricetta ricetta;
        String idRicetta = request.getParameter("idRicetta");
        
        if(idRicetta == null || idRicetta.equals("")){
            response.sendRedirect("cittadino.jsp");
        }else{
            ricetta = getRicetta(request,persona,idRicetta);
            if(ricetta == null){
                response.sendRedirect("cittadino.jsp");
            }else{
                
                
                if(!ricetta.getVistoPaziente()){//Se non èe sato mai visto
                    setVistoPaziente(request,ricetta);
                }
                
                request.getSession().setAttribute("c_b_s_f_Ricetta", ricetta);
                response.sendRedirect(response.encodeRedirectURL(contextPath + "Cittadino/Ricetta/ricetta.jsp"));
            }
        }
    }
//==============================================================================
//                     VERIFICA CHE LA RICETTA SIA DEL PAZIENTE
//==============================================================================
    private Ricetta getRicetta(HttpServletRequest request,Persona persona,String id){
        Ricetta ricetta = null;
        CartellaClinicaPerMB      cartellaClinica   = null;
        try {
            Integer idRiceta = Integer.parseInt(id);
            
            ricetta = ricetta_DAO.getByPrimaryKey(idRiceta);
            if(ricetta != null){
                cartellaClinica   = cartellaClinica_Dao.getByPrimaryKey(ricetta.getId_CartellaClinica());
                if( ( cartellaClinica == null ) || 
                    (! cartellaClinica.getId_Persona().equals(persona.getCodF()) ) ){
                    ricetta =null;
                }
            }
        }catch (DAOException ex) {
            request.getServletContext().log("Imposibile recuperare la ricetta.", ex);
            ricetta= null;
        }catch (Exception ex) {
            request.getServletContext().log("Imposibile recuperare convertitre l'id della ricetta in un intero.", ex);
            ricetta= null;
        }finally{
            return  ricetta;
        }
    }
//==============================================================================
//                    Imposta come visto dal PAZIENTE
//==============================================================================
    private void setVistoPaziente(HttpServletRequest request,Ricetta ricetta){
        try {
            ricetta_DAO.update_VistoPaziente(ricetta.getId(),true);
        }catch (DAOException ex) {
            request.getServletContext().log("Imposibile impostare la ricetta come gia visto.", ex);
        }
    }
    
    
//==============================================================================
//                          Get Ricetta per farmacia 
//==============================================================================
    private void getRicettaFarmacia(HttpServletRequest request,HttpServletResponse response,Persona persona)
                                   throws ServletException, IOException {
        
        String contextPath = getServletContext().getContextPath();
        if (!contextPath.endsWith("/")) {
            contextPath += "/";
        }
        
        Ricetta ricetta;
        String idRicetta = request.getParameter("idRicetta");
        
        if(idRicetta == null || idRicetta.equals("")){
            response.sendRedirect("listaRicette.jsp");
        }else{
            ricetta = getRicetta(request,persona,idRicetta);
            if(ricetta == null){
                response.sendRedirect("listaRicette.jsp");
            }else{
                
     
                request.getSession().setAttribute("c_b_s_f_Ricetta", ricetta);
                response.sendRedirect(response.encodeRedirectURL(contextPath + "Farmacia/Cliente/Ricetta/ricetta.jsp"));
            }
        }
    }
    
    
//==============================================================================
//                          Eroga ricetta (Farmacia)
//==============================================================================
    private void erogaRicetta(HttpServletRequest request,HttpServletResponse response)
                                    throws ServletException, IOException {
        String contextPath = getServletContext().getContextPath();
        if (!contextPath.endsWith("/")) {
            contextPath += "/";
        }
        
        String     pagatoFarmaco     = request.getParameter("pagatoFarmaco");
        
        Farmacia   farmacia     = (Farmacia) request.getSession(false).getAttribute("f_Farmacia");
        Persona    cliente      = (Persona)  request.getSession(false).getAttribute("f_Cliente");
        Ricetta    ricettaOld   = (Ricetta)  request.getSession(false).getAttribute("c_b_s_f_Ricetta");
        
        Ricetta ricetta =null;
        if( (farmacia != null) && (cliente != null)&& (pagatoFarmaco != null)) {
            ricetta = concludiRicetta(request,farmacia,cliente,pagatoFarmaco);
        }
        
        if((ricetta != null) && (ricetta.getId_Farmacia().equals(farmacia.getpIVA())) ){
            ((HttpServletRequest) request).getSession().setAttribute("c_b_s_f_Ricetta", ricetta);
            response.sendRedirect(response.encodeRedirectURL(contextPath + "Farmacia/Cliente/ConfermaRicettaEvalsa.jsp"));
        }else{
            response.sendRedirect(response.encodeRedirectURL(contextPath + "Farmacia/Cliente/listaRicette.jsp"));
        }
    }
//==============================================================================
//                          Concludi ricetta (Farmacia)
//==============================================================================
    private Ricetta concludiRicetta(HttpServletRequest request,Farmacia farmacia,Persona cliente,String pagatoFarmaco)
                                    throws ServletException, IOException {
        
        Ricetta ricetta  = (Ricetta)  request.getSession(false).getAttribute("c_b_s_f_Ricetta");
        Ricetta ris=null;
        try {
            GregorianCalendar data = new GregorianCalendar();
            Date oggi = new Date(data.getTimeInMillis());
            
            ricetta_DAO.update_Evalsa(ricetta.getId(),oggi,3.0,farmacia.getpIVA());
            ris = ricetta_DAO.getByPrimaryKey(ricetta.getId());
        }catch (DAOException ex) {
            ris=null;
            request.getServletContext().log("Imposibile impostare la ricetta come gia visto.", ex);
        }
        return ris;
    }
//==============================================================================
//                          INVIO EMAIL CONFERMA PRESCRIZIONE
//==============================================================================
     private void inviaRicettePrescrizione(HttpServletRequest request,Ricetta ricetta,Persona paziente,MedicoBase medicoBase){
        String cognomeMB = medicoBase.getCognome();
        String nomeMB = medicoBase.getNome();
        String data = ricetta.getDataPrescrizione().toString();
        String messaggio;
        String oggetto;
        try{
            messaggio=  "Buongiorno "+paziente.getCognome()+" "+paziente.getNome()+",\n"+
                        "Le inviamo questa e-mail per informarla che Le è stato prescritto un nuova ricetta "+
                        "dal Dr. "+cognomeMB+" "+nomeMB+", il giorno "+data+".\n"+
                        "Per maggiori informazzioni consultare il sito web nella propria area privata,"+
                        " nella sezione \"ricette\".\n"+
                        "Cordiali saluti,\n"+
                        "Progetto Universitario Servizio Sanitario Nazionale";
           
            oggetto =    "Aggiunta Ricetta -- Progetto Universitario Servizio Sanitario Nazionale --";

            Mail.sendEmail(messaggio,paziente.getEmail(), oggetto);
        }catch(Exception e){
            request.getServletContext().log("Imposibile inviare le email ai pazienti.", e);
        }
     }
//==============================================================================
}
