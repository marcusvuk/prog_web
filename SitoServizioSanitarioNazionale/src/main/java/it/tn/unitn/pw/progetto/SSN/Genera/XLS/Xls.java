/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.tn.unitn.pw.progetto.SSN.Genera.XLS;

import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.xssf.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
//librerie di appoggio
import java.util.List;

import java.io.IOException;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.*;

import java.util.GregorianCalendar;
import java.sql.Date;
import it.tn.unitn.pw.progetto.SSN.Genera.XLS.Ricette;

public class Xls {
    //Questi qua sono i vari titoli delle colonne

    private static final String[] titoli = {
            "Ricetta",	"Data e ora", "farmaco", "famacia", "medico di base", "paziente", "ticket"
    };
   
    public static void Xls(ArrayList<Ricette> comevuoi, HttpServletResponse response) throws IOException{

        // dichiaro un nuovo woorkbook
        Workbook wb;

        wb = new XSSFWorkbook();

        //assegno la mappa di stringhe e celle alla funzione createStyle
        Map<String, CellStyle> styles = createStyles(wb);

        Sheet sheet = wb.createSheet("Report");
        PrintSetup printSetup = sheet.getPrintSetup();
        printSetup.setLandscape(true);
        sheet.setFitToPage(true);
        sheet.setHorizontallyCenter(true);

        //corrisponde alla riga del titolo
        Row titleRow = sheet.createRow(0); //creo una nuova riga
        titleRow.setHeightInPoints(45);  //assegna la larghezza della prima riga
        Cell titleCell = titleRow.createCell(0);
        titleCell.setCellValue("Report");
        titleCell.setCellStyle(styles.get("title"));
        sheet.addMergedRegion(CellRangeAddress.valueOf("$A$1:$G$1")); //mergia le varie colonne della prima riga

        //riga header dive ci sono le ricette data e ora e blabla
        Row headerRow = sheet.createRow(1); //creo la riga successiva
        headerRow.setHeightInPoints(40); //assegno la sua larghezza
        Cell headerCell;
        for (int i = 0; i < titoli.length; i++) {
            headerCell = headerRow.createCell(i);
            headerCell.setCellValue(titoli[i]); //mette il valore delle cella ai titoli
            headerCell.setCellStyle(styles.get("header"));
        }
        //crea le varie celle
        int rownum = 2;
        
        for(Ricette r : comevuoi) {

            Row row = sheet.createRow(rownum++);
            for (int j = 0; j < titoli.length; j++) {
                Cell cell = row.createCell(j);
                cell.setCellStyle(styles.get("cell"));
            }
        }
        //metti i dati nelle celle
        int i=0;
        for (Ricette r : comevuoi) {

            Row row = sheet.getRow(2 + i);
                
            row.getCell(0).setCellValue(r.ricetta);
            row.getCell(1).setCellValue(r.data.toString());
            row.getCell(2).setCellValue(r.farmaco);
            row.getCell(3).setCellValue(r.farmacia);
            row.getCell(4).setCellValue(r.mb);
            row.getCell(5).setCellValue(r.paziente);
            row.getCell(6).setCellValue(r.ticket);

            i=i+1;
        }

        //mette la dimensione delle celle
        sheet.setColumnWidth(0, 30 * 256); //larghezza trenta caratteri
        for (int j = 1; j < 9; j++) {
            sheet.setColumnWidth(j, 30 * 256);
        }
        sheet.setColumnWidth(11, 30 * 256);

        // Scrivi l'output sul file magico
        /*String name="prova2.xls";
        String file = "C:\\Users\\Brian\\Downloads\\temp\\"+name;
        if (wb instanceof XSSFWorkbook){
            file += "x";
        }
        try{
            FileOutputStream out = new FileOutputStream(new File(file));
            wb.write(out);
            out.close();
        }catch(Exception e){
                System.out.println("Error XLS file 1: "+e);
        }*/
        
        GregorianCalendar data=new GregorianCalendar();
        Date oggi = new Date(data.getTimeInMillis());
        
        try{
            //ByteArrayOutputStream outByteStream = new ByteArrayOutputStream();
            //wb.write(outByteStream);
            //byte [] outArray = outByteStream.toByteArray();
            response.setContentType("application/xls");
            //response.setContentLength(outArray.length); 
            //response.setHeader("Expires:", "0"); // eliminates browser caching
            response.setHeader("Content-Disposition", "attachment; filename=\"Ricette Ritirate "+oggi+".xlsx\"");
            //OutputStream outStream = response.getOutputStream();
            //outStream.write(outArray);
            //outStream.flush();
     
            wb.write(response.getOutputStream());
            response.getOutputStream().flush();
        }catch(Exception e){
            System.out.println("Error XLS file 2: "+e);   
            response.sendRedirect("SSP.jsp");
        }
        
        
        
        
        
    }

    /**
     * ci sono gli stili delle varie celle
     */
    private static Map<String, CellStyle> createStyles(Workbook wb) {
        Map<String, CellStyle> styles = new HashMap<String, CellStyle>();
        CellStyle style;
        Font titleFont = wb.createFont(); // crea un nuovo font
        titleFont.setFontHeightInPoints((short) 18); // questo mi dà la grandezza del font
        titleFont.setBold(true); //lo mette in grassetto
        style = wb.createCellStyle();
        style.setAlignment(HorizontalAlignment.CENTER);  // allineamento orizzontale centrale del titolo
        style.setVerticalAlignment(VerticalAlignment.CENTER); //allineamento verticale centrale
        style.setFont(titleFont);
        styles.put("title", style);
        //altre cose del stile
        Font monthFont = wb.createFont();
        monthFont.setFontHeightInPoints((short) 18);
        monthFont.setColor(IndexedColors.WHITE.getIndex());
        style = wb.createCellStyle();
        style.setAlignment(HorizontalAlignment.CENTER);
        style.setVerticalAlignment(VerticalAlignment.CENTER);
        style.setFillForegroundColor(IndexedColors.GREY_50_PERCENT.getIndex());
        style.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        style.setFont(monthFont);
        style.setWrapText(true);
        styles.put("header", style);

        style = wb.createCellStyle();
        style.setAlignment(HorizontalAlignment.CENTER);
        style.setWrapText(true);
        style.setBorderRight(BorderStyle.THIN);
        style.setRightBorderColor(IndexedColors.BLACK.getIndex());
        style.setBorderLeft(BorderStyle.THIN);
        style.setLeftBorderColor(IndexedColors.BLACK.getIndex());
        style.setBorderTop(BorderStyle.THIN);
        style.setTopBorderColor(IndexedColors.BLACK.getIndex());
        style.setBorderBottom(BorderStyle.THIN);
        style.setBottomBorderColor(IndexedColors.BLACK.getIndex());
        styles.put("cell", style);

        return styles;
    }
    
    
}
