/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.tn.unitn.pw.progetto.SSN.servlet;

import it.tn.unitn.pw.progetto.SSN.DAO.common.exception.DAOException;
import it.tn.unitn.pw.progetto.SSN.DAO.common.exception.DAOFactoryException;
import it.tn.unitn.pw.progetto.SSN.DAO.common.factory.DAOFactory;
import it.tn.unitn.pw.progetto.SSN.DAO.persistence.*;
import it.tn.unitn.pw.progetto.SSN.persistence.entities.*;
import it.tn.unitn.pw.progetto.SSN.Genera.Email.Mail;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.util.GregorianCalendar;
import java.sql.Date;
/**
 *
 * @author Brian
 */
public class srv_QR_Ricetta extends HttpServlet {
//------------------------------------------------------------------------------   
    private Persona_DAO  persona_Dao;
        
    private Persona      paziente;
    
    @Override
    public void init() throws ServletException {
        DAOFactory daoFactory = (DAOFactory) super.getServletContext().getAttribute("daoFactory");
        if (daoFactory == null) {
            throw new ServletException("Impossible to get dao factory for user storage system");
        }
        try {
            persona_Dao         = daoFactory.getDAO(Persona_DAO.class);
        } catch (DAOFactoryException ex) {
            throw new ServletException("Impossible to get dao factory for user storage system", ex);
        }
    }
    
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        String contextPath = getServletContext().getContextPath();
        if (!contextPath.endsWith("/")) {
            contextPath += "/";
        }
        
        String     idRicetta     = request.getParameter("idRicetta");
        String     codFPersona   = request.getParameter("personaCodF");
        String     dataScadenza  = request.getParameter("dataScadenza");
        
        try{
            GregorianCalendar data= new GregorianCalendar();
            Date oggi = new Date(data.getTimeInMillis());

            String[] a_dataScad =dataScadenza.split("-");

            if(a_dataScad.length==3){
                int anno    = Integer.parseInt(a_dataScad[0]);
                int mese    = Integer.parseInt(a_dataScad[1]);
                int giorno  = Integer.parseInt(a_dataScad[2]);

                
                Date scadenza = new Date(anno-1900, mese-1, giorno);


                Persona cittadino        = (Persona)  request.getSession(false).getAttribute("c_Persona");

                Farmacia farmacia        = (Farmacia) request.getSession(false).getAttribute("f_Farmacia");

                if( (cittadino != null) && (farmacia == null) && (cittadino.getCodF().equals(codFPersona)) ){
                    response.sendRedirect(response.encodeRedirectURL(contextPath + "Cittadino/Ricette?idRicetta="+idRicetta));
                }else if( (cittadino == null) && (farmacia != null) &&
                          ( oggi.before(scadenza) )){
                    Persona cliente = getCliente(request,codFPersona);
                    if(cliente!=null){
                        request.getSession().setAttribute("f_Cliente", cliente);
                        response.sendRedirect(response.encodeRedirectURL(contextPath + "Farmacia/Cliente/Ricette?idRicetta="+idRicetta));
                    }else{
                        response.sendRedirect(response.encodeRedirectURL(contextPath + "Farmacia/cercaPazienti.jsp"));
                    }
                }else {
                    response.sendRedirect(response.encodeRedirectURL(contextPath + "Index"));
                }
            }else{
                 response.sendRedirect(response.encodeRedirectURL(contextPath + "Index"));
            }
        }catch(Exception e){
            response.sendRedirect(response.encodeRedirectURL(contextPath + "Index"));
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
//==============================================================================
//                          GET CLIENTE
//============================================================================== 
    private Persona getCliente(HttpServletRequest request,String codF){
        Persona cliente=null;
        try {
            cliente = persona_Dao.getByPrimaryKey(codF);
            
        } catch (DAOException ex) {
            request.getServletContext().log("Impossibile recuperare la persona.", ex);
            cliente= null;
        }
        return cliente;
    }
//==============================================================================
}
