
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<a class="btn btn-light m-1 text-left"
   href="${pageContext.request.contextPath}/MedicoBase/CercaPazienti.jsp">
    Cerca Paziente
    <c:if test="${b_NumNuoviPazienti ne 0}">
        <span class="badge badge-primary badge-pill float-right"><c:out value="${b_NumNuoviPazienti}"/></span>
    </c:if>
</a>
<c:if test="${b_Paziente ne null}">
<br>
    <h5>
        Ultimo paziente cercato:
        <br>
        <b> 
            <c:out value="${b_Paziente.cognome}"/>
            <c:out value="${b_Paziente.nome}"/>      
        </b>
    </h5>
    <a class="btn btn-light m-1 text-left"
        href="${pageContext.request.contextPath}/MedicoBase/PazienteMB/CartellaClinica.jsp">
        Dati Anagrafici
    </a>
    <a class="btn btn-light m-1 text-left"
        href="${pageContext.request.contextPath}/MedicoBase/PazienteMB/listaEsamiFatti.jsp">
        Esami Fatti
        <c:if test="${b_NumNuoviReportEsami ne 0}">
            <span class="badge badge-primary badge-pill float-right"><c:out value="${b_NumNuoviReportEsami}"/></span>
        </c:if>
    </a>
    <a class="btn btn-light m-1 text-left"
        href="${pageContext.request.contextPath}/MedicoBase/PazienteMB/listaEsamiDaFare.jsp">
        Esami Da Fare
    </a>
    <a class="btn btn-light m-1 text-left"
        href="${pageContext.request.contextPath}/MedicoBase/PazienteMB/listaVisiteFatte.jsp">
        Visite Fatte
        <c:if test="${b_NumNuoviReportVisite ne 0}">
            <span class="badge badge-primary badge-pill float-right"><c:out value="${b_NumNuoviReportVisite}"/></span>
        </c:if>
    </a>
    <a class="btn btn-light m-1 text-left"
        href="${pageContext.request.contextPath}/MedicoBase/PazienteMB/listaVisiteDaFare.jsp">
        Visite Da Fare
    </a>
    <br>
    <hr>
    <a class="btn btn-light m-1 text-left"
        href="${pageContext.request.contextPath}/MedicoBase/PazienteMB/prescriviEsame.jsp">
        Prescrivi Esame
    </a> 
    <a class="btn btn-light m-1 text-left"
        href="${pageContext.request.contextPath}/MedicoBase/PazienteMB/prescriviFarmaci.jsp">
        Prescrivi Farmaco
    </a> 
    <a class="btn btn-light m-1 text-left"
        href="${pageContext.request.contextPath}/MedicoBase/PazienteMB/erogaVisita.jsp">
        Eroga Vistita
    </a> 
</c:if>