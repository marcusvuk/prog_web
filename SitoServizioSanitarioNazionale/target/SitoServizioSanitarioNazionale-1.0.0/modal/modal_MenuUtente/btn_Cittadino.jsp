<br>
<a class="btn btn-light m-1 text-left"
    href="${pageContext.request.contextPath}/Cittadino/listaEsamiFatti.jsp">
    Esami Fatti
    <c:if test="${c_NumNuoviReportEsami ne 0}">
        <span class="badge badge-primary badge-pill float-right"><c:out value="${c_NumNuoviReportEsami}"/></span>
    </c:if>
</a>
<a class="btn btn-light m-1 text-left"
    href="${pageContext.request.contextPath}/Cittadino/listaEsamiDaFare.jsp">
    Esami Da Fare
    <c:if test="${c_NumNuoviEsami  ne 0}">
        <span class="badge badge-primary badge-pill float-right"><c:out value="${c_NumNuoviEsami}"/></span>
    </c:if>
</a>
<br>
<hr>
<a class="btn btn-light m-1 text-left"
    href="${pageContext.request.contextPath}/Cittadino/listaVisiteFatte.jsp">
    Visite Fatte
    <c:if test="${c_NumVisiteConcluse  ne 0}">
        <span class="badge badge-primary badge-pill float-right"><c:out value="${c_NumVisiteConcluse}"/></span>
    </c:if>
</a>
<a class="btn btn-light m-1 text-left"
    href="${pageContext.request.contextPath}/Cittadino/listaVisiteDaFare.jsp">
    Visite Da Fare
    <c:if test="${c_NumNuoviVisiteDaFare  ne 0}">
        <span class="badge badge-primary badge-pill float-right"><c:out value="${c_NumNuoviVisiteDaFare}"/></span>
    </c:if>
</a>
<br>
<hr>
<a class="btn btn-light m-1 text-left"
    href="${pageContext.request.contextPath}/Cittadino/listaRicette.jsp">
    Riette 
    <c:if test="${c_NumNuoviRicette  ne 0}">
        <span class="badge badge-primary badge-pill float-right"><c:out value="${c_NumNuoviRicette}"/></span>
    </c:if>
</a>
<br>
<hr>
<a class="btn btn-light m-1 text-left"
    href="${pageContext.request.contextPath}/Cittadino/listaPagamenti.jsp">
    Repot Pagamenti 
</a>