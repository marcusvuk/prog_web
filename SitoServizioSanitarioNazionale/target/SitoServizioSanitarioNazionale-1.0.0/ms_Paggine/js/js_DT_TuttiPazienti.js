$(function () {
    $("#tuttiPazientiTable").dataTable( {
        ajax: {
            url: '/SitoServizioSanitarioNazionale/services/pazienti/ForMedicoSpec/'+$('#MyIdMedicoSpec').attr('value'),
            dataSrc: 'results'
        },
        columns: [
            { results: 0,
              targets: 0, // Start with the first
              render: function ( data, type, row, meta ) {
                    if(type === 'display'){
                        data = '<img class="rounded-circle"'+
                                'src="/SitoServizioSanitarioNazionale/DB_FOTO'+data+'"'+
                                'onerror ="imgError(this);"'+
                                'style="width:80px;height:80px"/>';
                    }
                    return data;
                }
            },
            { results: 1 },
            { results: 2 },
            { results: 3 },
            { results: 4 },
            { results: 5 },
            { results: 6,//results: Corrispnde a data
              targets: -1, // Start with the last
              render: function ( data, type, row, meta ) {
                    if(type === 'display'){
                        data = '<a href="MS_Paziente?idPaziente=' + encodeURIComponent(data) + '">'+
                               '<i class="fas fa-address-card"  style="font-size:36px" ></i>'+
                               '</a>';
                    }
                    return data;
                }
            }
        ]
    });

});