/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.tn.unitn.pw.progetto.SSN.servlet;


import it.tn.unitn.pw.progetto.SSN.DAO.common.exception.DAOException;
import it.tn.unitn.pw.progetto.SSN.DAO.common.exception.DAOFactoryException;
import it.tn.unitn.pw.progetto.SSN.DAO.common.factory.DAOFactory;
import it.tn.unitn.pw.progetto.SSN.DAO.persistence.*;
import it.tn.unitn.pw.progetto.SSN.persistence.entities.*;
import it.tn.unitn.pw.progetto.SSN.Genera.Email.Mail;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import javax.servlet.ServletConfig;

import java.util.GregorianCalendar;
import java.sql.Date;


/**
 *
 * @author Brian
 */
public class srv_Esami extends HttpServlet {
//------------------------------------------------------------------------------   
    private PrescrizioneEsameEReferto_DAO    prescrizioneEsame_Dao;
    private CartellaClinicaPerMB_DAO         cartellaClinica_Dao;
    
    
    @Override
    public void init() throws ServletException {
        DAOFactory daoFactory = (DAOFactory) super.getServletContext().getAttribute("daoFactory");
        if (daoFactory == null) {
            throw new ServletException("Impossible to get dao factory for user storage system");
        }
        try {
            prescrizioneEsame_Dao   = daoFactory.getDAO(PrescrizioneEsameEReferto_DAO.class);
            cartellaClinica_Dao     = daoFactory.getDAO(CartellaClinicaPerMB_DAO.class);
            
        } catch (DAOFactoryException ex) {
            throw new ServletException("Impossible to get dao factory for user storage system", ex);
        }
    }
//------------------------------------------------------------------------------
    
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        String contextPath = getServletContext().getContextPath();
        if (!contextPath.endsWith("/")) {
            contextPath += "/";
        }
        
        Persona cittadino  = (Persona) request.getSession(false).getAttribute("c_Persona");
        Persona pazienteMB = (Persona) request.getSession(false).getAttribute("b_Paziente");
        Persona pazienteMS = (Persona) request.getSession(false).getAttribute("s_Paziente");
        
        if( (cittadino != null) && (pazienteMB == null) && (pazienteMS == null) ){
            getEsameCittadino(request,response,cittadino);
            
        }else if( (cittadino == null) && (pazienteMB != null) && (pazienteMS == null) ){
            getEsamePazienteMB(request,response,pazienteMB);
            
        }else if( (cittadino == null) && (pazienteMB == null) && (pazienteMS != null) ){
            getEsamePazienteMS(request,response,pazienteMS);
            
        }else {
            //ERRORE--------------------------------------------------------------------------------------------
            response.sendRedirect(response.encodeRedirectURL(contextPath + "Index"));
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String contextPath = getServletContext().getContextPath();
        if (!contextPath.endsWith("/")) {
            contextPath += "/";
        }
        
        String tipoInserimetoEsame = request.getParameter("TipoInserimetoEsame");
        
        if(tipoInserimetoEsame.equals("InserimetoReport")){//Medioco Specialita 
            scriviReportEsame(request,response);
            
        }else if(tipoInserimetoEsame.equals("AggiungiEsame")){// Medico Base
            creaNuovoEsame(request,response);
            
        }else if(tipoInserimetoEsame.equals("EffetuaPagamento")){//Medioco Specialita 
            effetuaPagamento(request,response);
            
        }else{
            //ERRORE--------------------------------------------------------------------------------------------
            response.sendRedirect(response.encodeRedirectURL(contextPath + "Index"));
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
//==============================================================================
//                          Get ESAME per cittadino
//==============================================================================
    private void getEsameCittadino(HttpServletRequest request,HttpServletResponse response,Persona persona)
                                   throws ServletException, IOException {
        
        String contextPath = getServletContext().getContextPath();
        if (!contextPath.endsWith("/")) {
            contextPath += "/";
        }
        
        PrescrizioneEsameEReferto prescrizioneEsame;
        String idEsami = request.getParameter("idEsami");
        
        if(idEsami == null || idEsami.equals("")){
            response.sendRedirect("cittadino.jsp");
        }else{
            prescrizioneEsame = getPrescrizioneEsame(request,persona,idEsami);
            if(prescrizioneEsame == null){
                response.sendRedirect("cittadino.jsp");
            }else{
                
                
                if(!prescrizioneEsame.getVistoPaziente()){//Se non èe sato mai visto
                    setVistoPaziente(request,prescrizioneEsame);
                }
                if( (!prescrizioneEsame.getVistoReportPaziente()) && 
                    (prescrizioneEsame.getData() != null ) ){//Se non èe sato mai visto il report
                    setVistoReportPaziente(request,prescrizioneEsame);
                }
                
                request.getSession().setAttribute("c_b_s_Esame", prescrizioneEsame);
                response.sendRedirect(response.encodeRedirectURL(contextPath + "Cittadino/Esame/Esami.jsp"));
            }
        }
    
    }
//==============================================================================
//                          Get ESAME per MedicoBase
//==============================================================================
    private void getEsamePazienteMB(HttpServletRequest request,HttpServletResponse response,Persona persona)
                                   throws ServletException, IOException {
        
        String contextPath = getServletContext().getContextPath();
        if (!contextPath.endsWith("/")) {
            contextPath += "/";
        }
        
        PrescrizioneEsameEReferto prescrizioneEsame;
        String idEsami = request.getParameter("idEsami");
        
        if(idEsami == null || idEsami.equals("")){
            response.sendRedirect("CartellaClinica.jsp");
        }else{
            prescrizioneEsame = getPrescrizioneEsame(request,persona,idEsami);
            if(prescrizioneEsame == null){
                response.sendRedirect("CartellaClinica.jsp");
            }else{
                if( (!prescrizioneEsame.getVistoReportMedicoBase()) && 
                    (prescrizioneEsame.getData() != null ) ){//Se non èe sato mai visto il report
                    setVistoReportMedicoBase(request,prescrizioneEsame);
                }
                
                request.getSession().setAttribute("c_b_s_Esame", prescrizioneEsame);
                response.sendRedirect(response.encodeRedirectURL(contextPath + "MedicoBase/PazienteMB/Esame/Esami.jsp"));
            }
        }
    
    }
//==============================================================================
//                          Get ESAME per MedicoSpec
//==============================================================================
    private void getEsamePazienteMS(HttpServletRequest request,HttpServletResponse response,Persona persona)
                                   throws ServletException, IOException {
        
        String contextPath = getServletContext().getContextPath();
        if (!contextPath.endsWith("/")) {
            contextPath += "/";
        }
        
        PrescrizioneEsameEReferto prescrizioneEsame;
        String idEsami = request.getParameter("idEsami");
        
        if(idEsami == null || idEsami.equals("")){
            response.sendRedirect("CartellaClinica.jsp");
        }else{
            prescrizioneEsame = getPrescrizioneEsame(request,persona,idEsami);
            if(prescrizioneEsame == null){
                response.sendRedirect("CartellaClinica.jsp");
            }else{
                
                request.getSession().setAttribute("c_b_s_Esame", prescrizioneEsame);
                response.sendRedirect(response.encodeRedirectURL(contextPath + "MedicoSpec/PazienteMS/Esame/Esami.jsp"));
            }
        }
    
    }
//==============================================================================
//                     VERIFICA CHE L'ESAME SIA DEL PAZIENTE
//==============================================================================
    private PrescrizioneEsameEReferto getPrescrizioneEsame(HttpServletRequest request,Persona persona,String id){
        PrescrizioneEsameEReferto prescrizioneEsame = null;
        CartellaClinicaPerMB      cartellaClinica   = null;
        try {
            Integer idEsami = Integer.parseInt(id);
            
            prescrizioneEsame = prescrizioneEsame_Dao.getByPrimaryKey(idEsami);
            if(prescrizioneEsame != null){
                cartellaClinica   = cartellaClinica_Dao.getByPrimaryKey(prescrizioneEsame.getId_CartellaClinica());
                if( ( cartellaClinica == null ) || 
                    (! cartellaClinica.getId_Persona().equals(persona.getCodF()) ) ){
                    prescrizioneEsame =null;
                }
            }
        }catch (DAOException ex) {
            request.getServletContext().log("Imposibile recuperare il medico di base.", ex);
            prescrizioneEsame= null;
        }catch (Exception ex) {
            request.getServletContext().log("Imposibile recuperare convertitre l'id del esame in un intero.", ex);
            prescrizioneEsame= null;
        }finally{
            return  prescrizioneEsame;
        }
    }
//==============================================================================
//                    Imposta come visto dal PAZIENTE
//==============================================================================
    private void setVistoPaziente(HttpServletRequest request,PrescrizioneEsameEReferto prescrizioneEsame){
        try {
            prescrizioneEsame_Dao.update_VistoPaziente(prescrizioneEsame.getId(),true);
        }catch (DAOException ex) {
            request.getServletContext().log("Imposibile impostare l'esame come gia visto.", ex);
        }
    }
//==============================================================================
//                    Imposta come visto il repot dal PAZIENTE
//==============================================================================
    private void setVistoReportPaziente(HttpServletRequest request,PrescrizioneEsameEReferto prescrizioneEsame){
        try {
            prescrizioneEsame_Dao.update_VistoRepotDaPaziente(prescrizioneEsame.getId(),true);
        }catch (DAOException ex) {
            request.getServletContext().log("Imposibile impostare l'esame come gia visto.", ex);
        }
    }
//==============================================================================
//                    Imposta come visto il repot dal PAZIENTE
//==============================================================================
    private void setVistoReportMedicoBase(HttpServletRequest request,PrescrizioneEsameEReferto prescrizioneEsame){
        try {
            prescrizioneEsame_Dao.update_VistoRepotDaMedicoBase(prescrizioneEsame.getId(),true);
        }catch (DAOException ex) {
            request.getServletContext().log("Imposibile impostare l'esame come gia visto.", ex);
        }
    }
//==============================================================================
//                    Imposta come visto il repot dal PAZIENTE
//==============================================================================
    private Boolean agguiuntaReportEsame(HttpServletRequest request,PrescrizioneEsameEReferto prescrizioneEsame,
                             MedicoSpec medicoSpec,String risultatoEsame,String curaEsame,String pagatoEsame){
        Boolean ris = false;
        
        if( (risultatoEsame!=null) && (!risultatoEsame.equals("")) && 
            (curaEsame!=null) && (!curaEsame.equals(""))  ){
            
            if(!prescrizioneEsame.getRichiamo()){
                prescrizioneEsame.setTichet(11.0);
                if(pagatoEsame == null){
                    prescrizioneEsame.setPagato(false);
                }else{
                    prescrizioneEsame.setPagato(true);
                }
            }else{
                prescrizioneEsame.setTichet(0.0);
                prescrizioneEsame.setPagato(true);
            }

            prescrizioneEsame.setCura(curaEsame);
            prescrizioneEsame.setRisultato(risultatoEsame);
            
            Long time = new GregorianCalendar().getTimeInMillis();
            Date dataOggi = new Date(time);
            prescrizioneEsame.setData(dataOggi);
            
            prescrizioneEsame.setId_MedicoSpec(medicoSpec.getCodF());
            
            prescrizioneEsame.setVistoReportMedicoBase(false);
            prescrizioneEsame.setVistoReportPaziente(false);
            try {
                prescrizioneEsame_Dao.update(prescrizioneEsame);
                ris = true;
            }catch (DAOException ex) {
                ris = false;
                request.getServletContext().log("Imposibile impostare l'esame come gia visto.", ex);
            }
        }
        return ris;
    }
//==============================================================================
//                          Scrivi Report di un esame
//==============================================================================
    private void scriviReportEsame(HttpServletRequest request,HttpServletResponse response)
                                    throws ServletException, IOException {
        
        String contextPath = getServletContext().getContextPath();
        if (!contextPath.endsWith("/")) {
            contextPath += "/";
        }
        
       String risultatoEsame                       = request.getParameter("risultatoEsame");
       String curaEsame                            = request.getParameter("curaEsame");
       String pagatoEsame                          = request.getParameter("pagatoEsame");
       Persona paziente                            = (Persona) request.getSession(false).getAttribute("s_Paziente");
       MedicoSpec medicoSpec                       = (MedicoSpec) request.getSession(false).getAttribute("s_Persona");
       PrescrizioneEsameEReferto prescrizioneEsame = (PrescrizioneEsameEReferto) request.getSession(false).getAttribute("c_b_s_Esame");
       
        if(agguiuntaReportEsame(request,prescrizioneEsame,medicoSpec,risultatoEsame,curaEsame,pagatoEsame)){
            response.sendRedirect(response.encodeRedirectURL(contextPath + "MedicoSpec/PazienteMS/Esame/ConfermaReportEsame.jsp"));
            //INVIA EMAIL
            inviaEmailReport(request,prescrizioneEsame,paziente,medicoSpec);
            //INVIA EMAIL
        }else{
            response.sendRedirect(response.encodeRedirectURL(contextPath + "MedicoSpec/PazienteMS/listaEsami.jsp"));
        }
    }
//==============================================================================
//                          Crea un nuovo esame
//==============================================================================
    private void creaNuovoEsame(HttpServletRequest request,HttpServletResponse response)
                                    throws ServletException, IOException {
        String contextPath = getServletContext().getContextPath();
        if (!contextPath.endsWith("/")) {
            contextPath += "/";
        }
        String idTipoEsame = request.getParameter("idTipoEsame");
        MedicoBase medicoBase = (MedicoBase) request.getSession(false).getAttribute("b_Persona");
        Persona    paziente   = (Persona) request.getSession(false).getAttribute("b_Paziente");
        
        PrescrizioneEsameEReferto prescrizioneEsame= creaPrescrizioneEsame(request,medicoBase,paziente,idTipoEsame);
        if(prescrizioneEsame != null){
            response.sendRedirect(response.encodeRedirectURL(contextPath + "MedicoBase/PazienteMB/ConfermaCreazione.jsp"));
            //INVIA EMAIL
            inviaEmailPrescrizione(request,prescrizioneEsame,paziente,medicoBase);
            //INVIA EMAIL
        }else{
            response.sendRedirect(response.encodeRedirectURL(contextPath + "MedicoBase/PazienteMB/prescriviEsame.jsp"));
        }
    }
//==============================================================================
//                          Crea Prescrizione Esame
//==============================================================================
    private PrescrizioneEsameEReferto creaPrescrizioneEsame(HttpServletRequest request,MedicoBase medicoBase,Persona paziente,String idTipoEsameStringa){
        PrescrizioneEsameEReferto prescrizioneEsame=null;
        try {
            Integer idTipoEsame = Integer.parseInt(idTipoEsameStringa);
            
        
            CartellaClinicaPerMB catellaClinicaAttiva= cartellaClinica_Dao.getByPazienteId_Attivo(paziente.getCodF());
            if(catellaClinicaAttiva!= null){
                GregorianCalendar data= new GregorianCalendar();
                Date oggi=new Date(data.getTimeInMillis());
                
                prescrizioneEsame = new PrescrizioneEsameEReferto();
                
                prescrizioneEsame.setId(null);
                prescrizioneEsame.setData(null);
                prescrizioneEsame.setDataPrescrizione(oggi);
                prescrizioneEsame.setRisultato(null);
                prescrizioneEsame.setTichet(11.0);
                prescrizioneEsame.setPagato(false);
                prescrizioneEsame.setCura(null);
                prescrizioneEsame.setRichiamo(false);
                prescrizioneEsame.setId_CartellaClinica(catellaClinicaAttiva.getId());
                prescrizioneEsame.setId_MedicoSpec(null);
                prescrizioneEsame.setId_Esame(idTipoEsame);
                prescrizioneEsame.setVistoPaziente(false);
                prescrizioneEsame.setVistoReportMedicoBase(false);
                prescrizioneEsame.setVistoReportPaziente(false);
                
                if(prescrizioneEsame_Dao.insert(prescrizioneEsame)==null){
                    prescrizioneEsame = null;
                }
            }
        }catch (DAOException ex) {
            prescrizioneEsame =null;
            request.getServletContext().log("Imposibile impostare  crare la prescrizione di un esame.", ex);
        }catch (Exception ex) {
            request.getServletContext().log("Imposibile recuperare convertitre l'id del esame in un intero.", ex);
            prescrizioneEsame= null;
        }finally{
            return  prescrizioneEsame;
        }
    }
//==============================================================================
//                          Paga esame
//==============================================================================
    private void effetuaPagamento(HttpServletRequest request,HttpServletResponse response)
                                    throws ServletException, IOException {
        String contextPath = getServletContext().getContextPath();
        if (!contextPath.endsWith("/")) {
            contextPath += "/";
        }
       
        String pagatoEsame                          = request.getParameter("pagatoEsame");
        MedicoSpec medicoSpec                       = (MedicoSpec) request.getSession(false).getAttribute("s_Persona");
        PrescrizioneEsameEReferto prescrizioneEsame = (PrescrizioneEsameEReferto) request.getSession(false).getAttribute("c_b_s_Esame");
        
        
        if(setPagamento(request,prescrizioneEsame ,medicoSpec,pagatoEsame)){
            
            request.getSession().setAttribute("c_b_s_Esame", prescrizioneEsame);
            response.sendRedirect(response.encodeRedirectURL(contextPath + "MedicoSpec/PazienteMS/Esame/Esami.jsp"));
        }else{
            response.sendRedirect(response.encodeRedirectURL(contextPath + "MedicoBase/PazienteMB/Esame/esami.jsp"));
        }
    }
//==============================================================================
//                          Imposta Pagamento
//==============================================================================
     private Boolean setPagamento(HttpServletRequest request,PrescrizioneEsameEReferto Esame,MedicoSpec medicoSpec, String pagatoEsame){
        Boolean ris=false;
        Boolean statoPagamentoVechio=Esame.getPagato();
        try {
            if(Esame.getId_MedicoSpec().equals(medicoSpec.getCodF())){
                Boolean pagamento=false;
                if(pagatoEsame!=null){
                    pagamento=true;
                }
                Esame.setPagato(pagamento);
                prescrizioneEsame_Dao.update(Esame);
                ris=true;
            }
        }catch (DAOException ex) {
            ris=false;
            Esame.setPagato(statoPagamentoVechio);
            request.getServletContext().log("Imposibile impostare  crare la prescrizione di un esame.", ex);
        }finally{
            return  ris;
        }
    }
//==============================================================================
//                          INVIO EMAIL CONFERMA PRESCRIZIONE
//==============================================================================
     private void inviaEmailPrescrizione(HttpServletRequest request,PrescrizioneEsameEReferto prescrizioneEsame,Persona paziente,MedicoBase medicoBase){
        String cognomeMB = medicoBase.getCognome();
        String nomeMB = medicoBase.getNome();
        String data = prescrizioneEsame.getDataPrescrizione().toString();
        String messaggio;
        String oggetto;
        try{
            messaggio=  "Buongiorno "+paziente.getCognome()+" "+paziente.getNome()+",\n"+
                        "Le inviamo questa e-mail per informarla che Le è stato prescritto un nuovo esame "+
                        "dal Dr. "+cognomeMB+" "+nomeMB+", il giorno "+data+".\n"+
                        "Per maggiori informazzioni consultare il sito web nella propria area privata,"+
                        " nella sezione \"esami da fare\".\n"+
                        "Cordiali saluti,\n"+
                        "Progetto Universitario Servizio Sanitario Nazionale";
           
            oggetto =    "Aggiunta Esame -- Progetto Universitario Servizio Sanitario Nazionale --";

            Mail.sendEmail(messaggio,paziente.getEmail(), oggetto);
        }catch(Exception e){
            request.getServletContext().log("Imposibile inviare le email ai pazienti.", e);
        }
     }
//==============================================================================
//                          INVIO EMAIL CONFERMA PRESCRIZIONE
//==============================================================================
     private void inviaEmailReport(HttpServletRequest request,PrescrizioneEsameEReferto prescrizioneEsame,Persona paziente,MedicoSpec medicoSpec){
        String cognomeMS = medicoSpec.getCognome();
        String nomeMS = medicoSpec.getNome();
        String dataPresc = prescrizioneEsame.getDataPrescrizione().toString();
        String data = prescrizioneEsame.getData().toString();
        String messaggio;
        String oggetto;
        try{
            messaggio=  "Buongiorno "+paziente.getCognome()+" "+paziente.getNome()+",\n"+
                        "Le inviamo questa e-mail per informarla che è stato aggiunto all'esame, "+
                        "effetuato il giorno "+dataPresc+", il report conclusivo, scritto "+
                        "dal Dr. "+cognomeMS+" "+nomeMS+" il giorno "+data+".\n"+
                        "Per maggiori informazzioni consultare il sito web nella propria area privata, "+
                        "nella sezione \"esami fatti\".\n"+
                        "Cordiali saluti,\n"+
                        "Progetto Universitario Servizio Sanitario Nazionale";
           
            oggetto =    "Aggiunta Report Esame -- Progetto Universitario Servizio Sanitario Nazionale --";

            Mail.sendEmail(messaggio,paziente.getEmail(), oggetto);
        }catch(Exception e){
            request.getServletContext().log("Imposibile inviare le email ai pazienti.", e);
        }
     }
}
