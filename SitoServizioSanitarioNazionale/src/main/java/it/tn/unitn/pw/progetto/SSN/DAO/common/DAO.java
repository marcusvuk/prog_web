/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.tn.unitn.pw.progetto.SSN.DAO.common;
import it.tn.unitn.pw.progetto.SSN.DAO.common.exception.DAOFactoryException;
import it.tn.unitn.pw.progetto.SSN.DAO.common.exception.DAOException;
import java.util.List;

/**
 * L' interfaccia DAO è una parte fondamentale del DAO pattern che viene 
 * poi specializzata.
 *
 * @param <ENTITY_CLASS> è la classe che poi si dovra specificare, da gestire.
 * @param <PRIMARY_KEY_CLASS> è la classe di appartenenza della the primary key 
 * dell'entita di qui si vule gestire con l'uso del DAO.  
 *
 * 
 * @author Brian
 * @since 1.0.0
 */
public interface DAO <ENTITY_CLASS, PRIMARY_KEY_CLASS> {
/*==============================================================================*/
    /**
     * Restituisce il numero di records della classe {@code ENTITY_CLASS} 
     * salvata nel sistema      * che si vule gestire.
     *
     * @return Il numero di records presenti, di questa entità, nel sistema.
     * @throws DAOException si verifica qunado c'è un errore nel recopero 
     * dell'informazione dal sistema.
     *
     * @author Brian Emanuel
     * @since 1.0.0
     */
    public Long getCount() throws DAOException;
/*==============================================================================*/

/*==============================================================================*/
    /**
     * Restituisce un istanza di {@code ENTITY_CLASS} dal sisteme in cui è 
     * salvato tale record avente come primary key quella passata come 
     * parametro.
     *
     * @param primaryKey the primary key used to obtain the entity instance.
     * @return the {@code ENTITY_CLASS} instance of the storage system record
     * with the primary key equals to the one passed as parameter or
     * {@code null} if no entities with that primary key is present into the
     * storage system.
     * @throws DAOException if an error occurred during the information
     * retrieving.
     *
     * @author Brian Emanuel
     * @since 1.0.0
     */
    public ENTITY_CLASS getByPrimaryKey(PRIMARY_KEY_CLASS primaryKey) throws DAOException;
/*==============================================================================*/
    
/*==============================================================================*/
    /**
     * Returns the list of all the valid entities of type {@code ENTITY_CLASS}
     * stored by the storage system.
     *
     * @return the list of all the valid entities of type {@code ENTITY_CLASS}.
     * @throws DAOException if an error occurred during the information
     * retrieving.
     *
     * @author Brian Emanuel
     * @since 1.0.0
     */
    public List<ENTITY_CLASS> getAll() throws DAOException;
/*==============================================================================*/
    
/*==============================================================================*/
    /**
     * If this DAO can interact with it, then returns the DAO of class passed as
     * parameter.
     *
     * @param <DAO_CLASS> the class name of the DAO that can interact with this
     * DAO.
     * @param daoClass the class of the DAO that can interact with this DAO.
     * @return the instance of the DAO or null if no DAO of the type passed as
     * parameter can interact with this DAO.
     * @throws DAOFactoryException if an error occurred.
     *
     * @author Brian Emanuel
     * @since 1.0.0
     */
    public <DAO_CLASS extends DAO> DAO_CLASS getDAO(Class<DAO_CLASS> daoClass) throws DAOFactoryException;
/*==============================================================================*/
}
