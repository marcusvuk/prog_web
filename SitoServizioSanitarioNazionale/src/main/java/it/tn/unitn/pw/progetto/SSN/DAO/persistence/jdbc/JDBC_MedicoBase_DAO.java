/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.tn.unitn.pw.progetto.SSN.DAO.persistence.jdbc;

import java.sql.Date;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import it.tn.unitn.pw.progetto.SSN.DAO.common.persistence.dao.jdbc.JDBCDAO;
import it.tn.unitn.pw.progetto.SSN.DAO.common.exception.*;
import it.tn.unitn.pw.progetto.SSN.DAO.persistence.*;
import it.tn.unitn.pw.progetto.SSN.persistence.entities.*;
/**
 *
 * @author Brian
 */
public class JDBC_MedicoBase_DAO  extends JDBCDAO<MedicoBase, String> implements MedicoBase_DAO {
/*============================================================================*/    
/*                                      METODI COSTRUTTORI                    */
/*============================================================================*/
    public JDBC_MedicoBase_DAO(Connection con) {
        super(con);
    }
/*============================================================================*/
    
/*============================================================================*/    
/*                      METODI GETTER DAO IMPLEMENTATION                      */
/*============================================================================*/
    @Override
    public Long getCount() throws DAOException {
        try (Statement stmt = CON.createStatement()) {
            ResultSet counter = stmt.executeQuery( 
                     "  SELECT COUNT(*) "               +
                     "  FROM MedicoBase "     );
            if (counter.next()) {
                return counter.getLong(1);
            }
        } catch (SQLException ex) {
            throw new DAOException( " Immpossibile contare gli elementi di MedicoBase  " , ex);
        }
        return 0L;
    }
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    @Override
    public MedicoBase getByPrimaryKey(String primaryKey) throws DAOException {
        if (primaryKey == null) {
            throw new DAOException( " primaryKey è null " );
        }
        try (PreparedStatement stm = CON.prepareStatement(
                                "  SELECT MB.codF as codF ,  "                  +
                                        " P.nome as nome ,  "                   +
                                        " P.cognome as cognome ,  "             +
                                        " P.password as password ,  "           +
                                        " P.salt as salt ,  "                   +
                                        " P.sesso as sesso ,  "                 +
                                        " P.nascita as nascita ,  "             +
                                        " P.citta as citta ,  "                 +
                                        " P.email as email ,  "                 +
                                        " P.vive_id_SSP as vive_id_SSP ,  "     +
                                        " MB.lavora_id_SSP as lavora_id_SSP  "  +
                                "  FROM MedicoBase AS MB , Persona AS P "       +
                                "  WHERE MB.codF = P.codF    AND  "             +
                                        " MB.codF = ? " )) {
            // imposto i valori
            stm.setString(1, primaryKey);
            
            try (ResultSet rs = stm.executeQuery()) {
                rs.next();
                MedicoBase medicoBase = new MedicoBase();
                medicoBase.setCodF              (rs.getString   ( "codF" ));
                medicoBase.setNome              (rs.getString   ( "nome" ));
                medicoBase.setCognome           (rs.getString   ( "cognome" ));
                medicoBase.setPassword          (rs.getString   ( "password" ));
                medicoBase.setSalt              (rs.getString   ( "salt" ));
                medicoBase.setSesso             (rs.getBoolean  ( "sesso" ));
                medicoBase.setNascita           (rs.getDate     ( "nascita" ));
                medicoBase.setCitta             (rs.getString   ( "citta" ));
                medicoBase.setEmail             (rs.getString   ( "email" ));
                medicoBase.setVive_id_SSP       (rs.getInt      ( "vive_id_SSP" ));
                medicoBase.setLavora_id_SSP     (rs.getInt      ( "lavora_id_SSP" ));
                        
                return medicoBase;
            }
        } catch (SQLException ex) {
            throw new DAOException( " Impossibile perendere la MedicoBase indicato dalla primary key " , ex);
        }
    }
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    @Override
    public List<MedicoBase> getAll() throws DAOException {
        List<MedicoBase> listaMedicoBase = new ArrayList<>();

        try (Statement stm = CON.createStatement()) {
            try (ResultSet rs = stm.executeQuery(
                                     "  SELECT MB.codF as codF,  "                  +
                                             " P.nome as nome,  "                   +
                                             " P.cognome as cognome,  "             +
                                             " P.password as password,  "           +
                                             " P.salt as salt,  "                   +
                                             " P.sesso as sesso,  "                 +
                                             " P.nascita as nascita,  "             +
                                             " P.citta as citta,  "                 +
                                             " P.email as email,  "                 +
                                             " P.vive_id_SSP as vive_id_SSP,  "     +
                                             " MB.lavora_id_SSP as lavora_id_SSP  " +
                                     "  FROM MedicoBase AS MB, Persona AS P  "      +
                                     "  WHERE MB.codF = P.codF  "                   )){
                
                while (rs.next()) {
                    MedicoBase medicoBase = new MedicoBase();
                    
                    medicoBase.setCodF              (rs.getString   ( "codF" ));
                    medicoBase.setNome              (rs.getString   ( "nome" ));
                    medicoBase.setCognome           (rs.getString   ( "cognome" ));
                    medicoBase.setPassword          (rs.getString   ( "password" ));
                    medicoBase.setSalt              (rs.getString   ( "salt" ));
                    medicoBase.setSesso             (rs.getBoolean  ( "sesso" ));
                    medicoBase.setNascita           (rs.getDate     ( "nascita" ));
                    medicoBase.setCitta             (rs.getString   ( "citta" ));
                    medicoBase.setEmail             (rs.getString   ( "email" ));
                    medicoBase.setVive_id_SSP       (rs.getInt      ( "vive_id_SSP" ));
                    medicoBase.setLavora_id_SSP     (rs.getInt      ( "lavora_id_SSP" ));
                    
                    listaMedicoBase.add(medicoBase);
                }
            }
        } catch (SQLException ex) {
            throw new DAOException( " Imposibile estrarre tutta la lista di MedicoBase  " , ex);
        }

        return listaMedicoBase;
    }
/*============================================================================*/
   
/*============================================================================*/    
/*                      METODI QUERY                                          */
/*============================================================================*/
//------------------------------------------------------------------------------
//                      INSERT
//------------------------------------------------------------------------------
    @Override
    public String insert(MedicoBase medicoBase) throws DAOException {
        if (medicoBase == null) {
            throw new DAOException( " La medicoBase è null " );
        }
        try (PreparedStatement ps = CON.prepareStatement(
                     " INSERT INTO Persona "  +
                                                 " ( codF  "            +    "  ,  "    +
                                                 "  nome  "             +    "  ,  "    +
                                                 "  cognome "           +    "  ,  "    +
                                                 "  password "          +    "  ,  "    +
                                                 "  salt "              +    "  ,  "    +
                                                 "  sesso "             +    "  ,  "    +
                                                 "  nascita "           +    "  ,  "    +
                                                 "  citta "             +    "  ,  "    +
                                                 "  email "             +    "  ,  "    +
                                                 "  vive_id_SSP "       +    "  )  "    +  
                     " VALUES (?,?,?,?,?,?,?,?,?,?);  "                             +
                     " INSERT INTO medicoBase "  +
                                                 " ( codF  "            +    "  ,  "    +
                                                 "  lavora_id_SSP "     +    "  )  "    +  
                     " VALUES (?,?); " 
                            +  "  " , Statement.RETURN_GENERATED_KEYS)) {
            
            // imposto i valori persona
            ps.setString    (1,  medicoBase.getCodF());
            ps.setString    (2,  medicoBase.getNome());
            ps.setString    (3,  medicoBase.getCognome());
            ps.setString    (4,  medicoBase.getPassword());
            ps.setString    (5,  medicoBase.getSalt());
            ps.setBoolean   (6,  medicoBase.getSesso());
            ps.setDate      (7,  medicoBase.getNascita());
            ps.setString    (8,  medicoBase.getCitta());
            ps.setString    (9,  medicoBase.getEmail());
            ps.setInt       (10, medicoBase.getVive_id_SSP());
            // imposto i valori persona
            ps.setString    (11, medicoBase.getCodF());
            ps.setInt       (12, medicoBase.getLavora_id_SSP());
            
            ps.executeUpdate();
            
            return medicoBase.getCodF();
        } catch (SQLException ex) {
            throw new DAOException( " Impossibile aggiungere un nuovo record di medicoBase " , ex);
        }
    }
//------------------------------------------------------------------------------
//                      UPDATE
//------------------------------------------------------------------------------
    @Override
    public void update(MedicoBase medicoBase) throws DAOException {
        if (medicoBase == null) {
            throw new DAOException( " La medicoBase è null " );
        }
        try (PreparedStatement ps = CON.prepareStatement(
                     " UPDATE Persona "  +
                     " SET  "  + "  codF = ?  "             +     "  ,  "    +
                                 "  nome = ?  "             +     "  ,  "    +
                                 "  cognome = ?  "          +     "  ,  "    +
                                 "  password = ?  "         +     "  ,  "    +
                                 "  salt = ?  "             +     "  ,  "    +
                                 "  sesso = ?  "            +     "  ,  "    +
                                 "  nascita = ?  "          +     "  ,  "    +
                                 "  citta = ?  "            +     "  ,  "    +
                                 "  email = ?  "            +     "  ,  "    +
                                 "  vive_id_SSP = ?  "      +     
                     " WHERE codF = ?  "                    +     "  ;  "    +
                    
                     " UPDATE MedicoBase "  +
                     " SET  "   +    "  codF = ?  "             +     "  ,  "    +
                                 "  lavora_id_SSP = ?  "      +     
                     " WHERE codF = ? ; " , Statement.RETURN_GENERATED_KEYS)) {
            
            // imposto i valori Persona
            ps.setString    (1,  medicoBase.getCodF());
            
            ps.setString    (2,  medicoBase.getNome());
            ps.setString    (3,  medicoBase.getCognome());
            ps.setString    (4,  medicoBase.getPassword());
            ps.setString    (5,  medicoBase.getSalt());
            ps.setBoolean   (6,  medicoBase.getSesso());
            ps.setDate      (7,  medicoBase.getNascita());
            ps.setString    (8,  medicoBase.getCitta());
            ps.setString    (9,  medicoBase.getEmail());
            ps.setInt       (10, medicoBase.getVive_id_SSP());
            //WHERE
            ps.setString    (11, medicoBase.getCodF());
            
            // imposto i valori Medico Base
            ps.setString    (12, medicoBase.getCodF());
            ps.setInt       (13, medicoBase.getLavora_id_SSP());
            //WHERE
            ps.setString    (14, medicoBase.getCodF());
            
            ps.executeUpdate();
            
            
        } catch (SQLException ex) {
            throw new DAOException( " Impossibile aggiornare il record di MedicoBase " , ex);
        }
    }
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    @Override
    public void update_Password(String id, String Password) throws DAOException {
        if (id == null || Password == null ) {
            throw new DAOException( " id o Password è null " );
        }
         
         /*
            PRE E POST CONDIZIONI SONO PRESENTE NELLA TABELLA DEL MEDICO DI BASE
         */
        try (PreparedStatement ps = CON.prepareStatement(
                     " UPDATE Persona  "  +
                     " SET  password = ?  "         +  
                     " WHERE codF = ?  " , Statement.RETURN_GENERATED_KEYS)) {
            
            // imposto i valori
            ps.setString(1, Password);
            //WHERE
            ps.setString(2, id);
            
            ps.executeUpdate();
            
        } catch (SQLException ex) {
            throw new DAOException( " Impossibile aggiornare il record di MedicoBase " , ex);
        }
    }
//------------------------------------------------------------------------------
//                      SELECT
//------------------------------------------------------------------------------
    @Override
    public List<MedicoBase> getBySSProvincialeId_Lavora(Integer id) throws DAOException {
        if (id == null ) {
            throw new DAOException( " id  è null " );
        }
        List<MedicoBase> listaMedicoBase = new ArrayList<>();

        try (PreparedStatement stm = CON.prepareStatement( 
                                "  SELECT MB.codF as codF,  "                   +
                                         " P.nome as nome,  "                   +
                                         " P.cognome as cognome,  "             +
                                         " P.password as password,  "           +
                                         " P.salt as salt,  "                   +
                                         " P.sesso as sesso,  "                 +
                                         " P.nascita as nascita,  "             +
                                         " P.citta as citta,  "                 +
                                         " P.email as email,  "                 +
                                         " P.vive_id_SSP as vive_id_SSP,  "     +
                                         " MB.lavora_id_SSP as lavora_id_SSP  " +
                             "  FROM Persona as P, MedicoBase as MB  "          +
                             "  WHERE MB.lavora_id_SSP =  ?  AND  "             + 
                                    "  MB.codF = P.codF "  )){
                stm.setInt(1, id);
            try (ResultSet rs = stm.executeQuery()){
                                
                
                while (rs.next()) {
                    MedicoBase medicoBase = new MedicoBase();
                    
                    medicoBase.setCodF              (rs.getString   ( "codF" ));
                    medicoBase.setNome              (rs.getString   ( "nome" ));
                    medicoBase.setCognome           (rs.getString   ( "cognome" ));
                    medicoBase.setPassword          (rs.getString   ( "password" ));
                    medicoBase.setSalt              (rs.getString   ( "salt" ));
                    medicoBase.setSesso             (rs.getBoolean  ( "sesso" ));
                    medicoBase.setNascita           (rs.getDate     ( "nascita" ));
                    medicoBase.setCitta             (rs.getString   ( "citta" ));
                    medicoBase.setEmail             (rs.getString   ( "email" ));
                    medicoBase.setVive_id_SSP       (rs.getInt      ( "vive_id_SSP" ));
                    medicoBase.setLavora_id_SSP     (rs.getInt      ( "lavora_id_SSP" ));
                    
                    listaMedicoBase.add(medicoBase);
                }
            }
        } catch (SQLException ex) {
            throw new DAOException( " Imposibile estrarre la lista di MedicoBase  " , ex);
        }

        return listaMedicoBase;
    }
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    @Override
    public List<MedicoBase> getBySSProvincialeId_Vive(Integer id) throws DAOException {
        if (id == null ) {
            throw new DAOException( " id  è null " );
        }
        List<MedicoBase> listaMedicoBase = new ArrayList<>();

        try (PreparedStatement stm = CON.prepareStatement(
                            "  SELECT MB.codF as codF,  "                       +
                                    " P.nome as nome,  "                        +
                                    " P.cognome as cognome,  "                  +
                                    " P.password as password,  "                +
                                    " P.salt as salt,  "                        +
                                    " P.sesso as sesso,  "                      +
                                    " P.nascita as nascita,  "                  +
                                    " P.citta as citta,  "                      +
                                    " P.email as email,  "                      +
                                    " P.vive_id_SSP as vive_id_SSP,  "          +
                                    " MB.lavora_id_SSP as lavora_id_SSP  "      +
                            "  FROM Persona as P, MedicoBase as MB  "           +    
                            "  WHERE P.vive_id_SSP =  ?  AND  "                 + 
                                  "  MB.codF = P.codF " )) {
            stm.setInt(1, id);
            try (ResultSet rs = stm.executeQuery()){
                
                while (rs.next()) {
                    MedicoBase medicoBase = new MedicoBase();
                    
                    medicoBase.setCodF              (rs.getString   ( "codF" ));
                    medicoBase.setNome              (rs.getString   ( "nome" ));
                    medicoBase.setCognome           (rs.getString   ( "cognome" ));
                    medicoBase.setPassword          (rs.getString   ( "password" ));
                    medicoBase.setSalt              (rs.getString   ( "salt" ));
                    medicoBase.setSesso             (rs.getBoolean  ( "sesso" ));
                    medicoBase.setNascita           (rs.getDate     ( "nascita" ));
                    medicoBase.setCitta             (rs.getString   ( "citta" ));
                    medicoBase.setEmail             (rs.getString   ( "email" ));
                    medicoBase.setVive_id_SSP       (rs.getInt      ( "vive_id_SSP" ));
                    medicoBase.setLavora_id_SSP     (rs.getInt      ( "lavora_id_SSP" ));
                    
                    listaMedicoBase.add(medicoBase);
                }
            }
        } catch (SQLException ex) {
            throw new DAOException( " Imposibile estrarre la lista di MedicoBase  " , ex);
        }

        return listaMedicoBase;
    }
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    @Override
    public MedicoBase getByMedicoBaseEmailAndPassword(String email, String password) throws DAOException {
        if (email == null || password == null ) {
            throw new DAOException( " email o password è null " );
        }
        try (PreparedStatement stm = CON.prepareStatement(
                            "  SELECT MB.codF as codF,  "                       +
                                   " P.nome as nome,  "                         +
                                   " P.cognome as cognome,  "                   +
                                   " P.password as password,  "                 +
                                   " P.salt as salt,  "                         +
                                   " P.sesso as sesso,  "                       +
                                   " P.nascita as nascita,  "                   +
                                   " P.citta as citta,  "                       +
                                   " P.email as email,  "                       +
                                   " P.vive_id_SSP as vive_id_SSP,  "           +
                                   " MB.lavora_id_SSP as lavora_id_SSP  "       +                  
                           "  FROM Persona as P, MedicoBase as MB  "            +
                           "  WHERE P.email = ?  "      +    "  AND  "          +   
                                 "  P.password = ?  "   +    "  AND  "          +
                                 "  MB.codF = P.codF "                          +  
                                 "    " )) {
            // imposto i valori
            stm.setString(1, email);
            stm.setString(2, password);
            
            try (ResultSet rs = stm.executeQuery()) {
                rs.next();
                MedicoBase medicoBase = new MedicoBase();
                    
                medicoBase.setCodF              (rs.getString   ( "codF" ));
                medicoBase.setNome              (rs.getString   ( "nome" ));
                medicoBase.setCognome           (rs.getString   ( "cognome" ));
                medicoBase.setPassword          (rs.getString   ( "password" ));
                medicoBase.setSalt              (rs.getString   ( "salt" ));
                medicoBase.setSesso             (rs.getBoolean  ( "sesso" ));
                medicoBase.setNascita           (rs.getDate     ( "nascita" ));
                medicoBase.setCitta             (rs.getString   ( "citta" ));
                medicoBase.setEmail             (rs.getString   ( "email" ));
                medicoBase.setVive_id_SSP       (rs.getInt      ( "vive_id_SSP" ));
                medicoBase.setLavora_id_SSP     (rs.getInt      ( "lavora_id_SSP" ));
                    
                    
                        
                return medicoBase;
            }
        } catch (SQLException ex) {
            throw new DAOException( "Impossibile perendere la MedicoBase indicato dall'email e password " , ex);
        }
    }
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    @Override
    public String getSaltMedicoBaseByEmail(String email) throws DAOException {
        if (email == null ) {
            throw new DAOException( " email è null. " );
        }
        try (PreparedStatement stm = CON.prepareStatement(
                                    "  SELECT  P.salt as salt  "                +
                                     "  FROM Persona as P, MedicoBase as MB  "  +
                                     "  WHERE P.email = ?  "   + "  AND  "      +  
                                           "  MB.codF = P.codF "                )) {
            // imposto i valori 
            stm.setString(1, email);
            
            try (ResultSet rs = stm.executeQuery()) {
                rs.next();
                String salt;
                salt= rs.getString( "salt" );
                        
                return salt;
            }
        } catch (SQLException ex) {
            throw new DAOException( "Impossibile perendere il salt MedicoBase indicato dall'email" , ex);
        }
    }
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    @Override
    public MedicoBase getByMedicoBaseCodFAndPassword(String codF, String password) throws DAOException {
        if (codF == null || password == null ) {
            throw new DAOException( " codice ficale o password è null " );
        }
        try (PreparedStatement stm = CON.prepareStatement(
                            "  SELECT MB.codF as codF,  "                       + 
                                   " P.nome as nome,  "                         +
                                   " P.cognome as cognome,  "                   +
                                   " P.password as password,  "                 +
                                   " P.salt as salt,  "                         +
                                   " P.sesso as sesso,  "                       +
                                   " P.nascita as nascita,  "                   +
                                   " P.citta as citta,  "                       +
                                   " P.email as email,  "                       +
                                   " P.vive_id_SSP as vive_id_SSP,  "           +
                                   " MB.lavora_id_SSP as lavora_id_SSP  "       +                  
                           "  FROM Persona as P, MedicoBase as MB  "            +
                           "  WHERE P.codF = ?  "       +    "  AND  "          +   
                                 "  P.password = ?  "   +    "  AND  "          +
                                 "  MB.codF = P.codF "                          +  
                                 "    " )) {
            // imposto i valori
            stm.setString(1, codF);
            stm.setString(2, password);
            
            try (ResultSet rs = stm.executeQuery()) {
                rs.next();
                MedicoBase medicoBase = new MedicoBase();
                medicoBase.setCodF             (rs.getString   ( "codF" ));
                medicoBase.setNome             (rs.getString   ( "nome" ));
                medicoBase.setCognome          (rs.getString   ( "cognome" ));
                medicoBase.setPassword         (rs.getString   ( "password" ));
                medicoBase.setSalt             (rs.getString   ( "salt" ));
                medicoBase.setSesso            (rs.getBoolean  ( "sesso" ));
                medicoBase.setNascita          (rs.getDate     ( "nascita" ));
                medicoBase.setCitta            (rs.getString   ( "citta" ));
                medicoBase.setEmail            (rs.getString   ( "email" ));
                medicoBase.setVive_id_SSP      (rs.getInt      ( "vive_id_SSP" ));
                medicoBase.setLavora_id_SSP    (rs.getInt      ( "lavora_id_SSP" ));
                return medicoBase;
            }
        } catch (SQLException ex) {
            throw new DAOException( " Impossibile perendere il MedicoBase indicato dal codice ficale e password " , ex);
        }
    }
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    @Override
    public String getSaltMedicoBaseByCodF(String codF) throws DAOException {
        if (codF == null ) {
            throw new DAOException( " Codice fiscale è null " );
        }
        try (PreparedStatement stm = CON.prepareStatement(
                                     "  SELECT P.salt  "                        +
                                     "  FROM Persona AS P, MedicoBase as MB "   +
                                     "  WHERE P.codF = ?  AND  "                +
                                           "  MB.codF = P.codF "                )) {
            // imposto i valori 
            stm.setString(1, codF);
            
            try (ResultSet rs = stm.executeQuery()) {
                rs.next();
                String salt = new String();
                salt= rs.getString( "salt" );
                        
                return salt;
            }
        } catch (SQLException ex) {
            throw new DAOException( " Impossibile perendere la MedicoBase indicato dal codice fiscale " , ex);
        }
    }
/*============================================================================*/
    @Override
    public MedicoBase getByMedicoBaseCodF(String codF) throws DAOException {
        if (codF == null ) {
            throw new DAOException( " codice ficale è null " );
        }
        try (PreparedStatement stm = CON.prepareStatement(
                                "  SELECT MB.codF as codF,  "                   +
                                       " P.nome as nome,  "                     +
                                       " P.cognome as cognome,  "               +
                                       " P.email as email  "                    +                 
                               "  FROM Persona as P, MedicoBase as MB  "        +
                               "  WHERE P.codF = ?   AND  "                     +  
                                     "  MB.codF = P.codF ")) {
            // imposto i valori
            stm.setString(1, codF);
            
            try (ResultSet rs = stm.executeQuery()) {
                rs.next();
                MedicoBase medicoBase = new MedicoBase();
                medicoBase.setCodF             (rs.getString   ( "codF" ));
                medicoBase.setNome             (rs.getString   ( "nome" ));
                medicoBase.setCognome          (rs.getString   ( "cognome" ));
                medicoBase.setEmail            (rs.getString   ( "email" ));
                medicoBase.setPassword         (null);
                medicoBase.setSalt             (null);
                medicoBase.setSesso            (null);
                medicoBase.setNascita          (null);
                medicoBase.setCitta            (null);
                medicoBase.setVive_id_SSP      (null);
                medicoBase.setLavora_id_SSP    (null);
                return medicoBase;
            }
        } catch (SQLException ex) {
            throw new DAOException( " Impossibile perendere il MedicoBase indicato dal codice ficale e password " , ex);
        }
    }
/*============================================================================*/
/*============================================================================*/    
/*                         METODO AGGIUNTIVI                                  */
/*============================================================================*/
    public Integer getNumeroNuoviPazientiByMedicoBaseCodF(String CodF)throws DAOException{
        if (CodF == null ) {
            throw new DAOException( " CodF è null. " );
        }
        
        try (PreparedStatement stm = CON.prepareStatement(
                                    "  SELECT  Count(*) AS num "                +
                                    "  FROM CartellaClinicaPerMB as CC "        +
                                    "  WHERE CC.dataFine IS NULL  AND  "        +  
                                           " CC.vistoMedicoBase = False  AND  " +  
                                           " CC.id_MedicoBase = ? "                )) {
            // imposto i valori 
            stm.setString(1, CodF);
            
            try (ResultSet rs = stm.executeQuery()) {
                rs.next();
                Integer num;
                num= rs.getInt("num" );
                        
                return num;
            }
        } catch (SQLException ex) {
            throw new DAOException( "Impossibile perendere il salt MedicoBase indicato dall'email" , ex);
        }
    }
    
}
