package it.tn.unitn.pw.progetto.SSN.Genera.PDF;
//import per i QR
import be.quodlibet.boxable.BaseTable;
import be.quodlibet.boxable.Row;
import be.quodlibet.boxable.utils.PDStreamUtils;

import java.io.ByteArrayOutputStream;
import java.io.File;
import net.glxn.qrgen.QRCode;
import net.glxn.qrgen.image.ImageType; 

//import per i pdf
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.http.HttpServletResponse;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.graphics.image.PDImageXObject;
import org.apache.pdfbox.pdmodel.font.PDType1Font;

import it.tn.unitn.pw.progetto.SSN.Genera.PDF.Ticket;
import it.tn.unitn.pw.progetto.SSN.persistence.entities.*;
import java.awt.Color;
import java.util.List;

//import CoseRicetta.java;
//Questa funzione prende in input una stringa con le info da mettere nel QR (tipo info varie del farmaco),
//e mette il QR risultante nel PDF. 
public class CreatePDF{
    

    public static void produciPDFRicetta(HttpServletResponse response,Persona persona,Ricetta ricetta,Farmaco farmaco,String nomeMB,String urlQR) throws IOException{
        try (PDDocument doc = new PDDocument()) {
            PDPage page = new PDPage();
            doc.addPage(page);
             File  qrImg = QRCode.from(urlQR).to(ImageType.JPG).file();
            try (PDPageContentStream contentStream = new PDPageContentStream(doc, page)) {
                
                
                
                PDImageXObject qr_code = PDImageXObject.createFromFileByContent(qrImg,doc);
                //Per inserire l'immagine si usa draImage()
                contentStream.drawImage(qr_code, 250, 50);//i 125x125 è la dim dell'immagine.
                //Serve per indicare l'inizio della procedura per aggiungere del testo
                contentStream.beginText();
                    //Setta il font delle scritte. Da fare sempre prima di showText
                    contentStream.setFont(PDType1Font.TIMES_ROMAN, 23);
                    //Serve per posizionare le scritte in modo decente.
                    contentStream.newLineAtOffset(25, 700);
                    //Aggiunge effettivamente il testo.
                    contentStream.showText("Ricetta Da Ritirare entro il "+ricetta.getScadenza().toString());
                contentStream.endText();
                
                contentStream.beginText();
                    //Setta il font delle scritte. Da fare sempre prima di showText
                    contentStream.setFont(PDType1Font.TIMES_ROMAN, 16);
                    contentStream.newLineAtOffset(25, 650);
                    contentStream.showText("Pescritta dal "+nomeMB+" il giorno "+ricetta.getDataPrescrizione().toString());
                    contentStream.newLineAtOffset(0, -25);
                    
                    contentStream.showText("Pescritta a: "+persona.getNome()+" "+persona.getCognome());
                    contentStream.newLineAtOffset(0, -20);
                    contentStream.showText("Codice fiscale: "+persona.getCodF());
                    contentStream.newLineAtOffset(0, -25);
                    
                    contentStream.showText("Riferita al farmaco: "+farmaco.getNome());
                    contentStream.newLineAtOffset(0, -20);
                    contentStream.showText("Quantità: "+ricetta.getQuantita());
                    contentStream.newLineAtOffset(0, -20);
                    contentStream.showText("Descrizione del farmaco: ");
                    contentStream.newLineAtOffset(25, -16);
                    contentStream.showText(farmaco.getDescrizione());
                
                contentStream.endText();
            }

           
            response.setContentType("application/pdf");
            response.setHeader("Content-disposition", "inline; filename='Ricetta.pdf'");
           
            doc.save(response.getOutputStream());
        }
    }
    
    public static void produciPDFReportPagamenti(HttpServletResponse response, List<Ticket> listaTickets,Persona persona) throws IOException{
        try (PDDocument doc = new PDDocument()) {
            PDPage page = new PDPage();
            doc.addPage(page);

            try (PDPageContentStream contents = new PDPageContentStream(doc, page)) {
                PDStreamUtils.write(
                        contents,
                        "Resoconto di tutte le spese mediche",
                        PDType1Font.HELVETICA_BOLD,
                        26,
                        80,
                        700,
                        Color.BLUE);
                PDStreamUtils.write(
                        contents,
                        "La seguente tabella mostra la lista di tutte le spese fatte da \"" + persona.getNome()+ " " + persona.getCognome()+ "\".",
                        PDType1Font.HELVETICA_BOLD,
                        14,
                        25,
                        670,
                        Color.black);

                float margin = 25;
                float yStartNewPage = page.getMediaBox().getHeight() - (2 * margin);
                float tableWidth = page.getMediaBox().getWidth() - (2 * margin);
                
                boolean drawContent = true;
                float yStart = yStartNewPage;
                float bottomMargin = 70;
                float yPosition = 650;
                
                BaseTable table = new BaseTable(yPosition, yStartNewPage, bottomMargin, tableWidth, margin, doc, page, true, drawContent);
                Row<PDPage> header = table.createRow(20);
                header.createCell(15, "Tipo");
                header.createCell(10, "Pescritto il");
                header.createCell(15, "Fatto/Ritirato il");
                header.createCell(50, "Descrizione");
                header.createCell(10, "Prezzo");
                table.addHeaderRow(header);
                
                for (Ticket elementoTicket : listaTickets) {
                    Row<PDPage> row = table.createRow(12);
                    row.createCell(elementoTicket.getTipo());
                    row.createCell(elementoTicket.getDataPescritto());
                    row.createCell(elementoTicket.getData());
                    row.createCell(elementoTicket.getPrescrizione());
                    row.createCell(elementoTicket.getCosto().toString());
                }
                
                table.draw();
            }


            response.setContentType("application/pdf");
            response.setHeader("Content-disposition", "inline; filename='Ricetta.pdf'");
            //response.setHeader("Content-disposition", "attachment; filename='shopping-lists.pdf'");
            doc.save(response.getOutputStream());
        }
    }
}