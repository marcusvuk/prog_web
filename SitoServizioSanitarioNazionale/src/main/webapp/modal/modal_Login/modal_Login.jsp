
<div id="LoginModal" class="modal fade" >
    <div class="modal-dialog modal-login modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Accedi al tuo account</h4>
                <button type="button"
                        class="close"
                        data-dismiss="modal"
                        aria-hidden="true">
                    &times;
                </button>    
            </div>  
            <div class="btn-group btn-group-toggle btn-toolbar" data-toggle="buttons">
                
                <label id="btn_CittadinoLogin" 
                       class="btn btn-light active" >
                    <input type="radio" nome="options" id="options1" autocomplete="off" checked>
                        Cittadino
                </label>
                
                <label id="btn_MedicoBaseLogin" 
                       class="btn btn-light">
                    <input type="radio" nome="options" id="options2" autocomplete="off">
                        Medico di Base
                </label>
                
                <label id="btn_MedicoSpecialistaLogin" 
                       class="btn btn-light">
                    <input type="radio" nome="options" id="options3" autocomplete="off">
                        Medico Specialista
                </label>
                
                <label id="btn_FarmaciaLogin" 
                       class="btn btn-light">
                    <input type="radio" nome="options" id="options4" autocomplete="off">
                        Farmacia
                </label>
                
                <label id="btn_SSProvincialeLogin" 
                       class="btn btn-light">
                    <input type="radio" nome="options" id="options5" autocomplete="off">
                        S.S. Provinciale
                </label>
                
            </div>
            

            <%-- Form per login Cittadino --%>
            <div id="CittadiniLogin" 
                 class="collapse show">
                <h4>Cittadino</h4>
                <form class="container-fluid" action="login.Cittadino.handler" method="POST">    
                    <input  type="hidden" 
                            id="login" 
                            name="login" 
                            value="Cittadino">
                    <%@include file="Form_Pers_MB_MS_Far.jsp" %>
                </form>
            </div>
                
            <%-- Form per login Medico di Base --%>
            <div id="MedicoBaseLogin" 
                 class="collapse">
                <h4>Medico di Base</h4>
                <form class="container-fluid" action="login.MBase.handler" method="POST"> 
                    <input  type="hidden" 
                            id="login" 
                            name="login" 
                            value="MBase">
                    <%@include file="Form_Pers_MB_MS_Far.jsp" %>
                </form>
            </div>

            <%-- Form per login Medico di Specialista --%>
            <div id="MedicoSpecialistaLogin"
                 class="collapse">
                <h4>Medico di Specialista</h4>
                <form class="container-fluid" action="login.MSpecialista.handler" method="POST">   
                    <input  type="hidden" 
                            id="login" 
                            name="login" 
                            value="MSpecialista">
                    <%@include file="Form_Pers_MB_MS_Far.jsp" %>
                </form>
            </div>
            
            <%-- Form per login Farmacia --%>
            <div id="FarmaciaLogin"
                 class="collapse">
                <h4>Farmacia</h4>
                <form class="container-fluid" action="login.Farmacia.handler" method="POST">   
                    <input  type="hidden" 
                            id="login" 
                            name="login" 
                            value="Farmacia">
                    <%@include file="Form_Pers_MB_MS_Far.jsp" %>
                </form>
            </div>
                
            <%-- Form per login SSProvinciale --%>
            <div id="SSProvincialeLogin"
                 class="collapse">
                <h4>SSProvinciale</h4>
                <form class="container-fluid" action="login.SSProvinciale.handler" method="POST">  
                    <input  type="hidden" 
                            id="login" 
                            name="login" 
                            value="SSProvinciale">
                    <%@include file="Form_SSProvinciale.jsp" %>
                </form>
            </div>
            
        </div>
    </div>
</div>