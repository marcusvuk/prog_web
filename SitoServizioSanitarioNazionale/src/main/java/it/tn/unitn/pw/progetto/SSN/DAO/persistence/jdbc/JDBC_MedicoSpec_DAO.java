/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.tn.unitn.pw.progetto.SSN.DAO.persistence.jdbc;

import java.sql.Date;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import it.tn.unitn.pw.progetto.SSN.DAO.common.persistence.dao.jdbc.JDBCDAO;
import it.tn.unitn.pw.progetto.SSN.DAO.common.exception.*;
import it.tn.unitn.pw.progetto.SSN.DAO.persistence.*;
import it.tn.unitn.pw.progetto.SSN.persistence.entities.*;
/**
 *
 * @author Brian
 */
public class JDBC_MedicoSpec_DAO extends JDBCDAO<MedicoSpec, String> implements MedicoSpec_DAO {
/*============================================================================*/    
/*                                      METODI COSTRUTTORI                    */
/*============================================================================*/
    public JDBC_MedicoSpec_DAO(Connection con) {
        super(con);
    }
/*============================================================================*/
    
/*============================================================================*/    
/*                      METODI GETTER DAO IMPLEMENTATION                      */
/*============================================================================*/
    @Override
    public Long getCount() throws DAOException {
        try (Statement stmt = CON.createStatement()) {
            ResultSet counter = stmt.executeQuery( 
                     "  SELECT COUNT(*) "               +
                     "  FROM MedicoSpec "     );
            if (counter.next()) {
                return counter.getLong(1);
            }
        } catch (SQLException ex) {
            throw new DAOException( " Immpossibile contare gli elementi di MedicoSpec  " , ex);
        }
        return 0L;
    }
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    @Override
    public MedicoSpec getByPrimaryKey(String primaryKey) throws DAOException {
       if (primaryKey == null) {
            throw new DAOException( " primaryKey è null " );
        }
        try (PreparedStatement stm = CON.prepareStatement(
                            "  SELECT MS.codF as codF,  "                       +
                                    " P.nome as nome,  "                        +
                                    " P.cognome as cognome,  "                  +
                                    " P.password as password,  "                +
                                    " P.salt as salt,  "                        +
                                    " P.sesso as sesso,  "                      +
                                    " P.nascita as nascita,  "                  +
                                    " P.citta as citta,  "                      +
                                    " P.email as email,  "                      +
                                    " P.vive_id_SSP as vive_id_SSP  "           +
                            "  FROM MedicoSpec AS MS, Persona AS P "            +
                            "  WHERE MS.codF = P.codF  "        +  "  AND  "    +
                                    " MS.codF = ? " )) {
            // imposto i valori
            stm.setString(1, primaryKey);
            
            try (ResultSet rs = stm.executeQuery()) {
                rs.next();
                MedicoSpec medicoSpec = new MedicoSpec();
                
                medicoSpec.setCodF              (rs.getString   ( "codF" ));
                medicoSpec.setNome              (rs.getString   ( "nome" ));
                medicoSpec.setCognome           (rs.getString   ( "cognome" ));
                medicoSpec.setPassword          (rs.getString   ( "password" ));
                medicoSpec.setSalt              (rs.getString   ( "salt" ));
                medicoSpec.setSesso             (rs.getBoolean  ( "sesso" ));
                medicoSpec.setNascita           (rs.getDate     ( "nascita" ));
                medicoSpec.setCitta             (rs.getString   ( "citta" ));
                medicoSpec.setEmail             (rs.getString   ( "email" ));
                medicoSpec.setVive_id_SSP       (rs.getInt      ( "vive_id_SSP" ));
                        
                return medicoSpec;
            }
        } catch (SQLException ex) {
            throw new DAOException( " Impossibile perendere la MedicoSpec indicato dalla primary key " , ex);
        }
    }
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    @Override
    public List<MedicoSpec> getAll() throws DAOException {
        List<MedicoSpec> listaMedicoSpec = new ArrayList<>();

        try (Statement stm = CON.createStatement()) {
            try (ResultSet rs = stm.executeQuery(
                                "  SELECT MS.codF as codF,  "                   +
                                        " P.nome as nome,  "                    +
                                        " P.cognome as cognome,  "              +
                                        " P.password as password,  "            +
                                        " P.salt as salt,  "                    +
                                        " P.sesso as sesso,  "                  +
                                        " P.nascita as nascita,  "              +
                                        " P.citta as citta,  "                  +
                                        " P.email as email,  "                  +
                                        " P.vive_id_SSP as vive_id_SSP  "       +
                                "  FROM MedicoSpec AS MS, Persona AS P  "       +
                                "  WHERE MS.codF = P.codF  "   )){
                
                while (rs.next()) {
                    MedicoSpec medicoSpec = new MedicoSpec();
                    
                    medicoSpec.setCodF              (rs.getString   ( "codF" ));
                    medicoSpec.setNome              (rs.getString   ( "nome" ));
                    medicoSpec.setCognome           (rs.getString   ( "cognome" ));
                    medicoSpec.setPassword          (rs.getString   ( "password" ));
                    medicoSpec.setSalt              (rs.getString   ( "salt" ));
                    medicoSpec.setSesso             (rs.getBoolean  ( "sesso" ));
                    medicoSpec.setNascita           (rs.getDate     ( "nascita" ));
                    medicoSpec.setCitta             (rs.getString   ( "citta" ));
                    medicoSpec.setEmail             (rs.getString   ( "email" ));
                    medicoSpec.setVive_id_SSP       (rs.getInt      ( "vive_id_SSP" ));
                    
                    listaMedicoSpec.add(medicoSpec);
                }
            }
        } catch (SQLException ex) {
            throw new DAOException( " Imposibile estrarre tutta la lista di MedicoSpec  " , ex);
        }

        return listaMedicoSpec;
    }
/*============================================================================*/
   
/*============================================================================*/    
/*                      METODI QUERY                                          */
/*============================================================================*/
//------------------------------------------------------------------------------
//                      INSERT
//------------------------------------------------------------------------------
    @Override
    public String insert(MedicoSpec medicoSpec) throws DAOException {
        if (medicoSpec == null) {
            throw new DAOException( " La medicoSpec è null " );
        }
        try (PreparedStatement ps = CON.prepareStatement(
                     " INSERT INTO Persona "  +
                            " ( codF  "            +    "  ,  "    +
                            "  nome  "             +    "  ,  "    +
                            "  cognome "           +    "  ,  "    +
                            "  password "          +    "  ,  "    +
                            "  salt "              +    "  ,  "    +
                            "  sesso "             +    "  ,  "    +
                            "  nascita "           +    "  ,  "    +
                            "  citta "             +    "  ,  "    +
                            "  email "             +    "  ,  "    +
                            "  vive_id_SSP "       +    "  )  "    +  
                     " VALUES (?,?,?,?,?,?,?,?,?,?);  "            +
                     " INSERT INTO MedicoSpec "  +
                            " ( codF  "            +    "  )  "    +  
                     " VALUES (?); " 
                            +  "  " , Statement.RETURN_GENERATED_KEYS)) {
            
            // imposto i valori persona
            ps.setString    (1,  medicoSpec.getCodF());
            ps.setString    (2,  medicoSpec.getNome());
            ps.setString    (3,  medicoSpec.getCognome());
            ps.setString    (4,  medicoSpec.getPassword());
            ps.setString    (5,  medicoSpec.getSalt());
            ps.setBoolean   (6,  medicoSpec.getSesso());
            ps.setDate      (7,  medicoSpec.getNascita());
            ps.setString    (8,  medicoSpec.getCitta());
            ps.setString    (9,  medicoSpec.getEmail());
            ps.setInt       (10, medicoSpec.getVive_id_SSP());
            // imposto i valori persona
            ps.setString    (11,  medicoSpec.getCodF());
            
            ps.executeUpdate();
            
            return medicoSpec.getCodF();
        } catch (SQLException ex) {
            throw new DAOException( " Impossibile aggiungere un nuovo record di medicoSpec " , ex);
        }
    }
//------------------------------------------------------------------------------
//                      UPDATE
//------------------------------------------------------------------------------
    @Override
    public void update(MedicoSpec medicoSpec) throws DAOException {
        if (medicoSpec == null) {
            throw new DAOException( " La medicoSpec è null " );
        }
        try (PreparedStatement ps = CON.prepareStatement(
                     " UPDATE Persona "  +
                     " SET "+    "  codF = ?  "             +     "  ,  "    +
                                 "  nome = ?  "             +     "  ,  "    +
                                 "  cognome = ?  "          +     "  ,  "    +
                                 "  password = ?  "         +     "  ,  "    +
                                 "  salt = ?  "             +     "  ,  "    +
                                 "  sesso = ?  "            +     "  ,  "    +
                                 "  nascita = ?  "          +     "  ,  "    +
                                 "  citta = ?  "            +     "  ,  "    +
                                 "  email = ?  "            +     "  ,  "    +
                                 "  vive_id_SSP = ?  "      +     
                     " WHERE codF = ?  "                    +     "  ;  "    +
                    
                     " UPDATE MedicoSpec "  +
                     " SET   codF = ?  "    +       
                     " WHERE codF = ? ; " , Statement.RETURN_GENERATED_KEYS)) {
            
            // imposto i valori Persona
            ps.setString    (1,  medicoSpec.getCodF());
            
            ps.setString    (2,  medicoSpec.getNome());
            ps.setString    (3,  medicoSpec.getCognome());
            ps.setString    (4,  medicoSpec.getPassword());
            ps.setString    (5,  medicoSpec.getSalt());
            ps.setBoolean   (6,  medicoSpec.getSesso());
            ps.setDate      (7,  medicoSpec.getNascita());
            ps.setString    (8,  medicoSpec.getCitta());
            ps.setString    (9,  medicoSpec.getEmail());
            ps.setInt       (10, medicoSpec.getVive_id_SSP());
            //WHERE
            ps.setString    (11, medicoSpec.getCodF());
            
            // imposto i valori Medico Base
            ps.setString    (12, medicoSpec.getCodF());
            //WHERE
            ps.setString    (13, medicoSpec.getCodF());
            
            ps.executeUpdate();
            
        } catch (SQLException ex) {
            throw new DAOException( " Impossibile aggiornare il record di MedicoSpec " , ex);
        }
    }
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    @Override
    public void update_Password(String id, String Password) throws DAOException {
        if (id == null || Password == null ) {
            throw new DAOException( " id o Password è null " );
        }
         
         /*
            PRE E POST CONDIZIONI SONO PRESENTE NELLA TABELLA DEL MEDICO DI BASE
         */
        try (PreparedStatement ps = CON.prepareStatement(
                     " UPDATE Persona  "  +
                     " SET  password = ?  "         +  
                     " WHERE codF = ?  " , Statement.RETURN_GENERATED_KEYS)) {
            
            // imposto i valori
            ps.setString(1, Password);
            //WHERE
            ps.setString(2, id);
            
            ps.executeUpdate();
            
        } catch (SQLException ex) {
            throw new DAOException( " Impossibile aggiornare il record di MedicoSpec " , ex);
        }
    }
//------------------------------------------------------------------------------
//                      SELECT
//------------------------------------------------------------------------------
    @Override
    public List<MedicoSpec> getBySSProvincialeId_Vive(Integer id) throws DAOException {
        if (id == null ) {
            throw new DAOException( " id  è null " );
        }
        List<MedicoSpec> listaMedicoSpec = new ArrayList<>();

        try (PreparedStatement stm = CON.prepareStatement(
                                "  SELECT MS.codF as codF,  "                   +
                                        " P.nome as nome,  "                    +
                                        " P.cognome as cognome,  "              +
                                        " P.password as password,  "            +
                                        " P.salt as salt,  "                    +
                                        " P.sesso as sesso,  "                  +
                                        " P.nascita as nascita,  "              +
                                        " P.citta as citta,  "                  +
                                        " P.email as email,  "                  +
                                        " P.vive_id_SSP as vive_id_SSP   "      +
                                "  FROM Persona as P, MedicoSpec as MS  "       +
                                "  WHERE P.vive_id_SSP =  ?  AND  "             + 
                                      "  MS.codF = P.codF "  )) {
            stm.setInt(1, id);
            try (ResultSet rs = stm.executeQuery()){
                
                while (rs.next()) {
                    MedicoSpec medicoSpec = new MedicoSpec();
                    
                    medicoSpec.setCodF              (rs.getString   ( "codF" ));
                    medicoSpec.setNome              (rs.getString   ( "nome" ));
                    medicoSpec.setCognome           (rs.getString   ( "cognome" ));
                    medicoSpec.setPassword          (rs.getString   ( "password" ));
                    medicoSpec.setSalt              (rs.getString   ( "salt" ));
                    medicoSpec.setSesso             (rs.getBoolean  ( "sesso" ));
                    medicoSpec.setNascita           (rs.getDate     ( "nascita" ));
                    medicoSpec.setCitta             (rs.getString   ( "citta" ));
                    medicoSpec.setEmail             (rs.getString   ( "email" ));
                    medicoSpec.setVive_id_SSP       (rs.getInt      ( "vive_id_SSP" ));
                    
                    listaMedicoSpec.add(medicoSpec);
                }
            }
        } catch (SQLException ex) {
            throw new DAOException( " Imposibile estrarre la lista di MedicoSpec  " , ex);
        }

        return listaMedicoSpec;
    }
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    @Override
    public MedicoSpec getByMedicoSpecEmailAndPassword(String email, String password) throws DAOException {
        if (email == null || password == null ) {
            throw new DAOException( " email o password è null " );
        }
        try (PreparedStatement stm = CON.prepareStatement(
                                "  SELECT MS.codF as codF,  "                   +
                                       " P.nome as nome,  "                     +
                                       " P.cognome as cognome,  "               +
                                       " P.password as password,  "             +
                                       " P.salt as salt,  "                     +
                                       " P.sesso as sesso,  "                   +
                                       " P.nascita as nascita,  "               +
                                       " P.citta as citta,  "                   +
                                       " P.email as email,  "                   +
                                       " P.vive_id_SSP as vive_id_SSP   "       +                
                               "  FROM Persona as P, MedicoSpec as MS  "        +
                               "  WHERE P.email = ?  "      +    "  AND  "      +   
                                     "  P.password = ?  "   +    "  AND  "      +
                                     "  MS.codF = P.codF "                      +  
                                     "    " )) {
            // imposto i valori
            stm.setString(1, email);
            stm.setString(2, password);
            
            try (ResultSet rs = stm.executeQuery()) {
                rs.next();
                MedicoSpec medicoSpec = new MedicoSpec();
                    
                medicoSpec.setCodF              (rs.getString   ( "codF" ));
                medicoSpec.setNome              (rs.getString   ( "nome" ));
                medicoSpec.setCognome           (rs.getString   ( "cognome" ));
                medicoSpec.setPassword          (rs.getString   ( "password" ));
                medicoSpec.setSalt              (rs.getString   ( "salt" ));
                medicoSpec.setSesso             (rs.getBoolean  ( "sesso" ));
                medicoSpec.setNascita           (rs.getDate     ( "nascita" ));
                medicoSpec.setCitta             (rs.getString   ( "citta" ));
                medicoSpec.setEmail             (rs.getString   ( "email" ));
                medicoSpec.setVive_id_SSP       (rs.getInt      ( "vive_id_SSP" ));
                        
                return medicoSpec;
            }
        } catch (SQLException ex) {
            throw new DAOException( " Impossibile perendere la MedicoSpec indicato dall'email e password " , ex);
        }
    }
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    @Override
    public String getSaltMedicoSpecByEmail(String email) throws DAOException {
        if (email == null ) {
            throw new DAOException( " email è null. " );
        }
        try (PreparedStatement stm = CON.prepareStatement(
                                    "  SELECT  P.salt as salt  "                +
                                     "  FROM Persona as P, MedicoSpec as MS  "  +
                                     "  WHERE P.email = ?   AND  "              +  
                                           "  MS.codF = P.codF "                )) {
            // imposto i valori 
            stm.setString(1, email);
            
            try (ResultSet rs = stm.executeQuery()) {
                rs.next();
                String salt;
                salt= rs.getString( "salt" );
                        
                return salt;
            }
        } catch (SQLException ex) {
            throw new DAOException( " Impossibile perendere il salt MedicoSpec indicato dall'email " , ex);
        }
    }
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    @Override
    public MedicoSpec getByMedicoSpecCodFAndPassword(String codF, String password) throws DAOException {
        if (codF == null || password == null ) {
            throw new DAOException( " codice ficale o password è null " );
        }
        try (PreparedStatement stm = CON.prepareStatement(
                            "  SELECT MS.codF as codF,  "                       +
                                   " P.nome as nome,  "                         +
                                   " P.cognome as cognome,  "                   +
                                   " P.password as password,  "                 +
                                   " P.salt as salt,  "                         +
                                   " P.sesso as sesso,  "                       +
                                   " P.nascita as nascita,  "                   +
                                   " P.citta as citta,  "                       +
                                   " P.email as email,  "                       +
                                   " P.vive_id_SSP as vive_id_SSP  "            +                 
                           "  FROM Persona as P, MedicoSpec as MS  "            +
                           "  WHERE P.codF = ?    AND  "                        +   
                                 "  P.password = ?    AND  "                    +
                                 "  MS.codF = P.codF "                          +   
                                 "    " )) {
            // imposto i valori
            stm.setString(1, codF);
            stm.setString(2, password);
            
            try (ResultSet rs = stm.executeQuery()) {
                rs.next();
                MedicoSpec medicoSpec = new MedicoSpec();
                medicoSpec.setCodF             (rs.getString   ( "codF" ));
                medicoSpec.setNome             (rs.getString   ( "nome" ));
                medicoSpec.setCognome          (rs.getString   ( "cognome" ));
                medicoSpec.setPassword         (rs.getString   ( "password" ));
                medicoSpec.setSalt             (rs.getString   ( "salt" ));
                medicoSpec.setSesso            (rs.getBoolean  ( "sesso" ));
                medicoSpec.setNascita          (rs.getDate     ( "nascita" ));
                medicoSpec.setCitta            (rs.getString   ( "citta" ));
                medicoSpec.setEmail            (rs.getString   ( "email" ));
                medicoSpec.setVive_id_SSP      (rs.getInt      ( "vive_id_SSP" ));
                        
                return medicoSpec;
            }
        } catch (SQLException ex) {
            throw new DAOException( " Impossibile perendere il MedicoSpec indicato dal codice ficale e password " , ex);
        }
    }
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    @Override
    public String getSaltMedicoSpecByCodF(String codF) throws DAOException {
        if (codF == null ) {
            throw new DAOException( " Codice fiscale è null " );
        }
        try (PreparedStatement stm = CON.prepareStatement(
                                     "  SELECT P.salt  "                        +
                                     "  FROM Persona AS P, MedicoSpec as MS "   +
                                     "  WHERE P.codF = ?  AND  "                +
                                           "  MS.codF = P.codF "                )) {
            // imposto i valori 
            stm.setString(1, codF);
            
            try (ResultSet rs = stm.executeQuery()) {
                rs.next();
                String salt = new String();
                salt= rs.getString( "salt" );
                        
                return salt;
            }
        } catch (SQLException ex) {
            throw new DAOException( " Impossibile perendere la MedicoSpec indicato dal codice fiscale " , ex);
        }
    }
/*============================================================================*/
}
