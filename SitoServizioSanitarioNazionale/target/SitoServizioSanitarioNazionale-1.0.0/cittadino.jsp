
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%@page import="java.util.List"%>
<%@page import="it.tn.unitn.pw.progetto.SSN.DAO.common.exception.*"%>
<%@page import="it.tn.unitn.pw.progetto.SSN.DAO.common.factories.jdbc.*"%>
<%@page import="it.tn.unitn.pw.progetto.SSN.DAO.common.factory.*"%>
<%@page import="it.tn.unitn.pw.progetto.SSN.DAO.common.persistence.dao.jdbc.*"%>
<%@page import="it.tn.unitn.pw.progetto.SSN.DAO.persistence.*"%>
<%@page import="it.tn.unitn.pw.progetto.SSN.persistence.entities.*"%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
        <title>
            SSN Utente
        </title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" crossorigin="anonymous">
    
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/css/select2.min.css">
  
        <!-- Cittadino -->
        <link rel="stylesheet" href="${pageContext.request.contextPath}/modal/modal_Login/css/floating-labels.css">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/modal/modal_Login/css/forms.css">
        <!-- Cittadino -->
        <link rel="icon" href="${pageContext.request.contextPath}/sito_IMG/icona.ico" type="image/x-icon" />
        <link href="${pageContext.request.contextPath}/sito_CSS/style.css" rel="stylesheet" />
        <link href="${pageContext.request.contextPath}/sito_CSS/style_MenuUtente.css" rel="stylesheet" />
    </head>
    <body onload="showEsitoModale()">
        <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/js/select2.min.js"></script>
        
        
    <!-- JAVASCRIP -->
        <%@include file="/pag_ElementiComunni/pag_MenuPrincipale.jsp" %>
        
        <%@include file="/modal/modal_CambiaFoto/modal_CambiaFoto.jsp" %> 
        <%@include file="/modal/modal_CambiaPassword/modal_CambiaPassword.jsp" %> 
        <%@include file="/modal/modal_CambiaMedicoBase/modal_CambiaMedicoBase.jsp" %> 
        <div class="container">
            <div>
                <div class="row">
                    <div class="col-12">
                        <h2>
                            <c:choose>
                                <c:when test="${c_Persona ne null}">
                                    Buongiorno<b> <c:out value="${c_Persona.cognome} ${c_Persona.nome}"/> </b>
                                </c:when>
                                <c:when test="${b_Persona ne null}">
                                    Buongiorno Dr.<b> <c:out value="${b_Persona.cognome} ${b_Persona.nome}"/> </b>
                                </c:when>
                                <c:when test="${s_Persona ne null}">
                                    Buongiorno Dr.<b> <c:out value="${s_Persona.cognome} ${s_Persona.nome}"/> </b>
                                </c:when>
                                <c:when test="${f_Farmacia ne null}">
                                    Buongiorno farmacia <b> <c:out value="${f_Farmacia.nome}"/> </b>
                                </c:when>
                                <c:when test="${p_ssProvinciale ne null}">
                                    Buongiorno Sistema Sanitario Provinciale di <b> <c:out value="${p_ssProvinciale.nome}"/> </b>
                                </c:when>
                            </c:choose>
                             
                        </h2>
                    </div>
                </div>
                <c:choose>
                    <c:when test="${(c_Persona ne null) || (b_Persona ne null) || (s_Persona ne null)}">
                        <%@include file="/pag_Cittadino/pag_Dati_Cit_MB_MS.jsp" %>
                    </c:when>
                    <c:when test="${f_Farmacia ne null}">
                        <%@include file="/pag_Cittadino/pag_Farmacia_SSP.jsp" %>
                    </c:when>
                    <c:when test="${p_ssProvinciale ne null}">
                        <%@include file="/pag_Cittadino/pag_Farmacia_SSP.jsp" %>
                    </c:when>
                </c:choose>
            </div>
        </div>
        <%@include file="pag_ElementiComunni/pag_Footer.jsp" %>
    </body>
</html>
