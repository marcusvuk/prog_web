$(function () {
    $("#EsamiVisiteTable").dataTable( {
        ajax: {
            url: '/SitoServizioSanitarioNazionale/services/VisitePaziente/'+
                    $('#MyIdPaziente').attr('value')+
                    '/PerMedicoSpec/'+$('#MyIdMedicoSpec').attr('value'),
            dataSrc: 'results'
        },
        columns: [
            { results: 0,//results: Corrispnde a data
              targets: 0, 
              render: function ( data, type, row, meta ) {
                    if(type === 'display'){
                        if(data === 'true'){
                            //Deve effetuare la visita
                            data ='<i class="fas fa-check-circle"  " style="font-size:36px"></i>';
                        }else{
                            //La visita è stata effetuata
                            data ='<i class="fas fa-cogs text-primary" style="font-size:36px"></i>';
                        }
                    }
                    return data;
                }
            },
            { results: 1 },
            { results: 2 },
            { results: 3 },
            { results: 4 },
            { results: 5,//results: Corrispnde a data
              targets: -1, 
              render: function ( data, type, row, meta ) {
                    if(type === 'display'){
                        data = '<a href="Visite?idVisita=' + encodeURIComponent(data) + '">'+
                               '<i class="fas fa-notes-medical" style="font-size:36px" ></i>'+
                               '</a>';
                    }
                    return data;
                }
            },
        ]
    });

});