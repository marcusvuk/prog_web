/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.tn.unitn.pw.progetto.SSN.DAO.persistence;

import java.util.List;
import java.sql.Date;
        
import it.tn.unitn.pw.progetto.SSN.persistence.entities.Ricetta;
import it.tn.unitn.pw.progetto.SSN.DAO.common.DAO;
import it.tn.unitn.pw.progetto.SSN.DAO.common.exception.DAOException;
/**
 *
 * @author Brian
 */
public interface Ricetta_DAO extends DAO<Ricetta, Integer>{
/*============================================================================*/    
/*                         METODO INSERT                                      */
/*============================================================================*/
    public Integer insert(Ricetta ricetta)throws DAOException;
/*============================================================================*/    
/*============================================================================*/    
/*                         METODO UPDATE                                      */
/*============================================================================*/
    public void update(Ricetta ricetta)throws DAOException;
    public void update_VistoPaziente(Integer id,Boolean visto)throws DAOException;
    public void update_Evalsa(Integer id,Date dataEvalsa,Double prezzo,String id_Farmacia)throws DAOException;
/*============================================================================*/
/*============================================================================*/    
/*                         METODO GET Da ID                                   */
/*============================================================================*/
    public List<Ricetta> getByPazienteId(String id)throws DAOException;
    public List<Ricetta> getByMedicoBaseId(String id)throws DAOException; 
/*============================================================================*/ 
/*============================================================================*/    
/*                         METODO GET Da ID solo Attivi non conclusi          */
/*============================================================================*/
    public List<Ricetta> getByPazienteId_Ririrate(String id)throws DAOException;
    public List<Ricetta> getByPazienteId_DaRirirate(String id)throws DAOException; 
    public List<Ricetta> getByPazienteId_Scadute(String id)throws DAOException;
    
    public List<Ricetta> getByMedicoBaseId_Ririrate(String id)throws DAOException;
    public List<Ricetta> getByMedicoBaseId_DaRirirate(String id)throws DAOException; 
    public List<Ricetta> getByMedicoBaseId_Scadute(String id)throws DAOException;
/*============================================================================*/   

}
