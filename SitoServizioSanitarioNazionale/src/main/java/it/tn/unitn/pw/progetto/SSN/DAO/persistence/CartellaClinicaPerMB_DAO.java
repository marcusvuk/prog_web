/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.tn.unitn.pw.progetto.SSN.DAO.persistence;

import java.util.List;
import java.sql.Date;
        
import it.tn.unitn.pw.progetto.SSN.persistence.entities.CartellaClinicaPerMB;
import it.tn.unitn.pw.progetto.SSN.DAO.common.DAO;
import it.tn.unitn.pw.progetto.SSN.DAO.common.exception.DAOException;
/**
 *
 * @author Brian
 */
public interface CartellaClinicaPerMB_DAO extends DAO<CartellaClinicaPerMB, Integer> {
/*============================================================================*/    
/*                         METODO INSERT                                      */
/*============================================================================*/
    public Integer insert(CartellaClinicaPerMB cartellaClinica)throws DAOException;
/*============================================================================*/    
/*============================================================================*/    
/*                         METODO UPDATE                                      */
/*============================================================================*/
    public void update(CartellaClinicaPerMB cartellaClinica)throws DAOException;
    public void update_FinePeriodo(Integer id,java.sql.Date data)throws DAOException;
    public void update_VistoDaMedicoBase(Integer id,Boolean visto)throws DAOException;
/*============================================================================*/
/*============================================================================*/    
/*                         METODO GET Da ID                                   */
/*============================================================================*/
    public List<CartellaClinicaPerMB> getByPazienteId(String id)throws DAOException;
    public List<CartellaClinicaPerMB> getByMedicoBaseId(String id)throws DAOException; 
/*============================================================================*/ 
/*============================================================================*/    
/*                         METODO GET Da ID solo Attivi non conclusi          */
/*============================================================================*/
    public CartellaClinicaPerMB getByPazienteId_Attivo(String id)throws DAOException;
    public List<CartellaClinicaPerMB> getByMedicoBaseId_Attivo(String id)throws DAOException; 
/*============================================================================*/   
}
