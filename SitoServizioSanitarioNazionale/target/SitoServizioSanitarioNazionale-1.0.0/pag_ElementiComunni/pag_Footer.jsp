<footer>
    <div class="container-fluid padding">
        <div class="row text-center">
            <div class="col-md-4">
                <hr class="light" />
                <h5>
                    Contattaci
                </h5>
                <hr class="light" />
                <a href="${pageContext.request.contextPath}/scriviEmail.jsp">
                    <p>
                        E-mail
                    </p>
                </a>
            </div>
            <div class="col-md-4">
                <hr class="light" />
                <h5>
                    Cookies e Privacy
                </h5>
                <hr class="light"/>
                <a href="${pageContext.request.contextPath}/cookies.jsp">
                    <p>
                        Cookies
                    </p>
                </a>
                <a href="${pageContext.request.contextPath}/privacy.jsp">
                    <p>
                        Informativa sulla Privacy
                    </p>
                </a>
            </div>
            <div class="col-md-4">
                <hr class="light" />
                <h5>
                    Le nostre sedi
                </h5>
                <hr class="light" />
                <a href="">
                    <p>
                        Sede del Ministro Lungotevere Ripa, 1 00153 - Roma
                    </p>
                </a>
                <a href="">
                    <p>
                        Sede Centrale Viale Giorgio Ribotta, 5 00144 - Roma- Uffici
                        periferici territoriali
                    </p>
                </a>
            </div>
        </div>
    </div>
</footer>