<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div class="row container-fluid ">
    <div class="col-md-5">
        <h3><b>Foto profilo attuale:</b></h3>
        <c:choose>
            <c:when test="${(c_Persona ne null)  && (c_FotoAttiva ne null)}">
                <img class="img-thumbnail rounded w-100" 
                     src="${pageContext.request.contextPath}/DB_FOTO${c_FotoAttiva.path}${c_FotoAttiva.nome}"
                     onerror ="imgError(this);" >
            </c:when>
            <c:when test="${(b_Persona ne null)  && (b_FotoAttiva ne null)}">
                <img class="img-thumbnail rounded w-100" 
                     src="${pageContext.request.contextPath}/DB_FOTO${b_FotoAttiva.path}${b_FotoAttiva.nome}"
                     onerror ="imgError(this);" >
            </c:when>
            <c:when test="${(s_Persona ne null)  && (s_FotoAttiva ne null)}">
                <img class="img-thumbnail rounded w-100" 
                     src="${pageContext.request.contextPath}/DB_FOTO${s_FotoAttiva.path}${s_FotoAttiva.nome}"
                     onerror ="imgError(this);" >
            </c:when>
            <c:otherwise>
                <img class="img-thumbnail rounded w-100" 
                     src="${pageContext.request.contextPath}/sito_IMG/blank-profile-picture.png"
                     onerror ="imgError(this);"> 
            </c:otherwise>
        </c:choose>

        <a  class="container-fluid
                btn btn-primary"
            href="#"
            data-toggle="modal" 
            data-target="#CambiaFotoModal">
               Modifica Foto <i class="fas fa-edit"></i> 
        </a>
    </div>
    <div class="col-md-7">
        <div class="row container-fluid">
            <div class="col-md-4">
                <h5>
                    <b>
                    Nome:
                    </b>
                </h5>
            </div>
            <div class="col-md-8">
                <h5>
                    <c:choose>
                        <c:when test="${c_Persona ne null}">
                            <c:out value="${c_Persona.nome}"/>
                        </c:when>
                        <c:when test="${b_Persona ne null}">
                            <c:out value="${b_Persona.nome}"/>
                        </c:when>
                        <c:when test="${s_Persona ne null}">
                            <c:out value="${s_Persona.nome}"/>
                        </c:when>
                    </c:choose>
                </h5>
            </div>
        </div>
        <div class="row container-fluid">
            <div class="col-md-4">
                <h5>
                    <b>
                    Cognome: 
                    </b>
                </h5>
            </div>
            <div class="col-md-8">
                <h5>
                    <c:choose>
                        <c:when test="${c_Persona ne null}">
                            <c:out value="${c_Persona.cognome}"/> 
                        </c:when>
                        <c:when test="${b_Persona ne null}">
                            <c:out value="${b_Persona.cognome}"/>
                        </c:when>
                        <c:when test="${s_Persona ne null}">
                            <c:out value="${s_Persona.cognome}"/>
                        </c:when>
                    </c:choose>
                </h5>
            </div>
        </div>
        <div class="row container-fluid">
            <div class="col-md-4">
                <h5>
                    <b>
                    Nato il:
                    </b>
                </h5>
            </div>
            <div class="col-md-8">
                <h5>
                    <c:choose>
                        <c:when test="${c_Persona ne null}">
                            <c:out value="${c_Persona.nascita}"/> 
                        </c:when>
                        <c:when test="${b_Persona ne null}">
                            <c:out value="${b_Persona.nascita}"/>
                        </c:when>
                        <c:when test="${s_Persona ne null}">
                            <c:out value="${s_Persona.nascita}"/>
                        </c:when>
                    </c:choose> 
                </h5>
            </div>
        </div>
        <div class="row container-fluid">
            <div class="col-md-4">
                <h5>
                    <b>
                    Codice fiscale: 
                    </b>
                </h5>
            </div>
            <div class="col-md-8">
                <h5>
                    <c:choose>
                        <c:when test="${c_Persona ne null}">
                            <c:out value="${c_Persona.codF}"/> 
                        </c:when>
                        <c:when test="${b_Persona ne null}">
                            <c:out value="${b_Persona.codF}"/>
                        </c:when>
                        <c:when test="${s_Persona ne null}">
                            <c:out value="${s_Persona.codF}"/>
                        </c:when>
                    </c:choose> 
                </h5>
            </div>
        </div>
        <div class="row container-fluid">
            <div class="col-md-4">
                <h5>
                    <b>
                    Residente a:
                    </b>
                </h5>
            </div>
            <div class="col-md-8">
                <h5>
                    <c:choose>
                        <c:when test="${c_Persona ne null}">
                            <c:out value="${c_Persona.citta}"/> 
                        </c:when>
                        <c:when test="${b_Persona ne null}">
                            <c:out value="${b_Persona.citta}"/>
                        </c:when>
                        <c:when test="${s_Persona ne null}">
                            <c:out value="${s_Persona.citta}"/>
                        </c:when>
                    </c:choose> 
                </h5>
            </div>
        </div>
        <div class="row container-fluid">
            <div class="col-md-4">
                <h5>
                    <b>
                    Email: 
                    </b>
                </h5>
            </div>
            <div class="col-md-8">
                <h5>
                    <c:choose>
                        <c:when test="${c_Persona ne null}">
                            <c:out value="${c_Persona.email}"/> 
                        </c:when>
                        <c:when test="${b_Persona ne null}">
                            <c:out value="${b_Persona.email}"/>
                        </c:when>
                        <c:when test="${s_Persona ne null}">
                            <c:out value="${s_Persona.email}"/>
                        </c:when>
                    </c:choose> 
                </h5>
            </div>
        </div>
        <div class="row container-fluid">
            <div class="col-md-4">
                <h5>
                    <b>
                    Abita in provincia di: 
                    </b>
                </h5>
            </div>
            <div class="col-md-8">
                <h5>
                    <c:choose>
                        <c:when test="${c_ViveProvincia ne null}">
                            <c:out value="${c_ViveProvincia.nome}"/> 
                        </c:when>
                        <c:when test="${b_ViveProvincia ne null}">
                            <c:out value="${b_ViveProvincia.nome}"/>
                        </c:when>
                        <c:when test="${s_ViveProvincia ne null}">
                            <c:out value="${s_ViveProvincia.nome}"/>
                        </c:when>
                    </c:choose> 
                </h5>
            </div>
        </div>
        
        <c:if test="${b_LavoraPerSSP ne null}">
            <div class="row container-fluid">
                <div class="col-md-4">
                    <h5>
                        <b>
                        Lavora per:  
                        </b>
                    </h5>
                </div>
                <div class="col-md-8">
                    <h5>
                        S.S.Provinciale di <c:out  value="${b_LavoraPerSSP.nome}" />
                    </h5>
                </div>
            </div>
        </c:if>
                        
                        
        <div class="row">
            <div class="col-md-4 mx-auto">
                <a  class="container-fluid
                        btn btn-primary"
                    href="#"
                    data-toggle="modal" 
                    data-target="#CambiaPassword">
                       Modifica Password <i class="fas fa-edit"></i> 
                </a>
            </div>
        </div>
    </div> 
</div>
<hr>
<%-- DATI MEDICO DI BASE   --%>             

<div class="row container-fluid ">
    <div class="col-md-5">
        <h3><b>Foto medico di base</b></h3>
        <c:choose>
            <c:when test="${c_Persona ne null}">
                <img class="img-thumbnail rounded w-100" 
                     src="${pageContext.request.contextPath}/DB_FOTO${c_fotoMyMedicoBase.path}${c_fotoMyMedicoBase.nome}"
                     onerror ="imgError(this);" >
            </c:when>
            <c:when test="${b_Persona ne null}">
                <img class="img-thumbnail rounded w-100" 
                     src="${pageContext.request.contextPath}/DB_FOTO${b_fotoMyMedicoBase.path}${b_fotoMyMedicoBase.nome}"
                     onerror ="imgError(this);" >
            </c:when>
            <c:when test="${s_Persona ne null}">
                <img class="img-thumbnail rounded w-100" 
                     src="${pageContext.request.contextPath}/DB_FOTO${s_fotoMyMedicoBase.path}${s_fotoMyMedicoBase.nome}"
                     onerror ="imgError(this);" >
            </c:when>
        </c:choose>
    </div>
    <div class="col-md-7">
        <h3><b>Medico di base</b></h3>
        <div class="row container-fluid">
            <div class="col-md-4">
                <h5>
                    <b>
                    Nome:
                    </b>
                </h5>
            </div>
            <div class="col-md-8">
                <h5>
                    <c:choose>
                        <c:when test="${c_Persona ne null}">
                            <c:out value="${c_MyMedicoBase.nome}"/> 
                        </c:when>
                        <c:when test="${b_Persona ne null}">
                            <c:out value="${b_MyMedicoBase.nome}"/>
                        </c:when>
                        <c:when test="${s_Persona ne null}">
                            <c:out value="${s_MyMedicoBase.nome}"/>
                        </c:when>
                    </c:choose> 
                </h5>
            </div>
        </div>
        <div class="row container-fluid">
            <div class="col-md-4">
                <h5>
                    <b>
                    Cognome: 
                    </b>
                </h5>
            </div>
            <div class="col-md-8">
                <h5>
                    <c:choose>
                        <c:when test="${c_Persona ne null}">
                            <c:out value="${c_MyMedicoBase.cognome}"/> 
                        </c:when>
                        <c:when test="${b_Persona ne null}">
                            <c:out value="${b_MyMedicoBase.cognome}"/>
                        </c:when>
                        <c:when test="${s_Persona ne null}">
                            <c:out value="${s_MyMedicoBase.cognome}"/>
                        </c:when>
                    </c:choose> 
                </h5>
            </div>
        </div>
        <div class="row container-fluid">
            <div class="col-md-4">
                <h5>
                    <b>
                    Email: 
                    </b>
                </h5>
            </div>
            <div class="col-md-8">
                <h5>
                    <c:choose>
                        <c:when test="${c_Persona ne null}">
                            <c:out value="${c_MyMedicoBase.email}"/>
                        </c:when>
                        <c:when test="${b_Persona ne null}">
                            <c:out value="${b_MyMedicoBase.email}"/>
                        </c:when>
                        <c:when test="${s_Persona ne null}">
                            <c:out value="${s_MyMedicoBase.email}"/>
                        </c:when>
                    </c:choose>  
                </h5>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4 mx-auto">
                <a  class="container-fluid
                        btn btn-primary"
                    href="#"
                    data-toggle="modal" 
                    data-target="#CambiaMedicoBase">
                    Modifica medico di base <i class="fas fa-edit"></i>
                </a>
            </div>
        </div>
    </div> 
</div>