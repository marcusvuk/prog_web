
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div name="CambiaFotoModal" id="CambiaFotoModal"  class="modal fade" >
    <div class="modal-dialog modal-login modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Cambia Foto</h4>
                <button type="button"
                        class="close"
                        data-dismiss="modal"
                        aria-hidden="true">
                    &times;
                </button>    
            </div> 
            
            <div class="modal-body">
                <div class="row container-fluid justify-content-center ">
                    <div class="col-md-3">
                        <h5>Foto utente attuale</h5>
                        <c:choose>
                            <c:when test="${c_FotoAttiva ne null}">
                                <img class="img-thumbnail rounded w-100 " 
                                     alt="Caricamento foto profilo"
                                     onerror ="imgError(this);"
                                     src="${pageContext.request.contextPath}/DB_FOTO${c_FotoAttiva.path}${c_FotoAttiva.nome}">
                            </c:when>
                            <c:when test="${b_FotoAttiva ne null}">
                                <img class="img-thumbnail rounded w-100 " 
                                     alt="Caricamento foto profilo"
                                     onerror ="imgError(this);"
                                     src="${pageContext.request.contextPath}/DB_FOTO${b_FotoAttiva.path}${b_FotoAttiva.nome}">
                            </c:when>
                            <c:when test="${s_FotoAttiva ne null}">
                                <img class="img-thumbnail rounded w-100 " 
                                     alt="Caricamento foto profilo"
                                     onerror ="imgError(this);"
                                     src="${pageContext.request.contextPath}/DB_FOTO${s_FotoAttiva.path}${s_FotoAttiva.nome}">
                            </c:when>
                            <c:otherwise>
                                <img class="img-thumbnail rounded " 
                                     src="${pageContext.request.contextPath}/sito_IMG/blank-profile-picture.png"> 
                            </c:otherwise>
                        </c:choose>
                    </div>
                    <div class="col-md-3"></div>
                    <div class="col-md-3">
                        <h5>Foto passate</h5>
                    <c:choose>
                      <c:when test="${(c_ListaFoto ne null) && (!(empty c_ListaFoto))}">
                        <div id="demo" class="carousel slide" data-ride="carousel">
                            <!-- The slideshow -->
                            <div class="carousel-inner">
                                <% int i1=0;%>
                                <c:forEach items="${c_ListaFoto}" var="item">
                                    <% if(i1==0){%>
                                    <div class="carousel-item active">
                                    <%}else{%>
                                    <div class="carousel-item">
                                    <%}
                                    i1++;%>
                                        <img class="img-thumbnail rounded w-100"
                                             onerror ="imgError(this);"
                                             src="${pageContext.request.contextPath}/DB_FOTO${item.path}${item.nome}">
                                    </div>
                                </c:forEach>
                                </div>
                                <!-- Left and right controls -->
                                <a class="carousel-control-prev" href="#demo" data-slide="prev">
                                    <span class="carousel-control-prev-icon"></span>
                                </a>
                                <a class="carousel-control-next" href="#demo" data-slide="next">
                                    <span class="carousel-control-next-icon"></span>
                                </a>
                            </div>
                        </div>
                      </c:when>
                      
                      <c:when test="${(b_ListaFoto ne null) && (!(empty b_ListaFoto))}">
                        <div id="demo" class="carousel slide" data-ride="carousel">
                            <!-- The slideshow -->
                            <div class="carousel-inner">
                                <% int i2=0;%>
                                <c:forEach items="${b_ListaFoto}" var="item">
                                    <% if(i2==0){%>
                                    <div class="carousel-item active">
                                    <%}else{%>
                                    <div class="carousel-item">
                                    <%}
                                    i2++;%>
                                        <img class="img-thumbnail rounded w-100"
                                             onerror ="imgError(this);"
                                             src="${pageContext.request.contextPath}/DB_FOTO${item.path}${item.nome}">
                                    </div>
                                </c:forEach>
                                </div>
                                <!-- Left and right controls -->
                                <a class="carousel-control-prev" href="#demo" data-slide="prev">
                                    <span class="carousel-control-prev-icon"></span>
                                </a>
                                <a class="carousel-control-next" href="#demo" data-slide="next">
                                    <span class="carousel-control-next-icon"></span>
                                </a>
                            </div>
                        </div>
                      </c:when>
                      
                      <c:when test="${(s_ListaFoto ne null) && (!(empty s_ListaFoto))}">
                        <div id="demo" class="carousel slide" data-ride="carousel">
                            <!-- The slideshow -->
                            <div class="carousel-inner">
                                <% int i3=0;%>
                                <c:forEach items="${s_ListaFoto}" var="item">
                                    <% if(i3==0){%>
                                    <div class="carousel-item active">
                                    <%}else{%>
                                    <div class="carousel-item">
                                    <%}
                                    i3++;%>
                                        <img class="img-thumbnail rounded w-100"
                                             onerror ="imgError(this);"
                                             src="${pageContext.request.contextPath}/DB_FOTO${item.path}${item.nome}">
                                    </div>
                                </c:forEach>
                                </div>
                                <!-- Left and right controls -->
                                <a class="carousel-control-prev" href="#demo" data-slide="prev">
                                    <span class="carousel-control-prev-icon"></span>
                                </a>
                                <a class="carousel-control-next" href="#demo" data-slide="next">
                                    <span class="carousel-control-next-icon"></span>
                                </a>
                            </div>
                        </div>
                      </c:when>
                     <c:otherwise>
                        <img class="img-thumbnail rounded " src="${pageContext.request.contextPath}/sito_IMG/blank-profile-picture.png"> 
                     </c:otherwise>
                    </c:choose>
                    </div>  
                </div>
                
                
                <c:choose>
                    <c:when test="${(c_Persona ne null)}">
                <form action="${pageContext.request.contextPath}/Cittadino/Cittadino.handler" method="POST" enctype="multipart/form-data">

                    </c:when>
                    <c:when test="${(b_Persona ne null)}">
                <form action="${pageContext.request.contextPath}/MedicoBase/MedicoBase.handler" method="POST" enctype="multipart/form-data">

                    </c:when>
                    <c:when test="${(s_Persona ne null)}">
                <form action="${pageContext.request.contextPath}/MedicoSpec/MedicoSpec.handler" method="POST" enctype="multipart/form-data">

                    </c:when>
                </c:choose>       
                    
                    
                <div class="container-fluid justify-content-center ">  
                    <div class="container">
                        <input  type="hidden" id="typeCambia" name="typeCambia" value="Foto">
                        <label for="file1Id">Carica un nuova foto:</label>  
                        <input class="form-control-file"
                               accept="image/x-png,image/jpeg" 
                               id="file1Id" name="file1" type="file" required="required"> 
                        <br> 
                    </div>
                    <div class=" container-fluid modal-footer">
                        <input  type="submit"
                            class="btn btn-primary pull-right"
                            accept=""value="Conferma"/>
                    </div>
                </div>
                </form>
            </div>
        </div>
    </div>
</div>