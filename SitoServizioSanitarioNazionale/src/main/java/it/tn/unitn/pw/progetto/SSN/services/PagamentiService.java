/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.tn.unitn.pw.progetto.SSN.services;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Produces;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;
import javax.ws.rs.core.MediaType;

import com.google.gson.Gson;
import it.tn.unitn.pw.progetto.SSN.DAO.common.exception.DAOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;

import it.tn.unitn.pw.progetto.SSN.DAO.persistence.*;
import it.tn.unitn.pw.progetto.SSN.DAO.common.exception.DAOFactoryException;
import it.tn.unitn.pw.progetto.SSN.DAO.common.factory.DAOFactory;
import it.tn.unitn.pw.progetto.SSN.persistence.entities.*;
import java.sql.SQLException;
import java.sql.Date;
import java.util.Iterator;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.logging.Logger;
import sun.tools.tree.ThisExpression;

/**
 * REST Web Service
 *
 * @author Brian
 */
@Path("Pagamenti")
public class PagamentiService {
    
    private static DAOFactory       daoFactory;

    private static VisitaMB_DAO                     visitaMB_DAO;
    private static VisitaMSEReport_DAO              visitaMSEReport_DAO;
    private static Farmaco_DAO                      farmaco_DAO;
    private static PrescrizioneEsameEReferto_DAO    prescrizioneEsame_DAO;
    private static Esame_DAO                        esame_DAO;
    private static Ricetta_DAO                      ricetta_DAO;
    private static MedicoSpec_DAO                   medicoSpec_DAO;
    
    
    
    static public void init(DAOFactory daoFactory1) {
        daoFactory =  daoFactory1;
    }
    
    @Context
    private UriInfo context;

    /**
     * Creates a new instance of PagamentiService
     */
    public PagamentiService() {
    }
//==============================================================================
//                                 PAZIENTI
//==============================================================================
    @GET
    @Path("{pazienteCodF}")
    public String getPagamentiPazienti(@PathParam("pazienteCodF") String pazienteCodF){
        List<String[]> results = new ArrayList();
        
        List<String[]> listaPrezzi = new ArrayList();

        try {
            visitaMB_DAO            = daoFactory.getDAO(VisitaMB_DAO.class);
            visitaMSEReport_DAO     = daoFactory.getDAO(VisitaMSEReport_DAO.class);
            ricetta_DAO             = daoFactory.getDAO(Ricetta_DAO.class);
            farmaco_DAO             = daoFactory.getDAO(Farmaco_DAO.class);
            prescrizioneEsame_DAO   = daoFactory.getDAO(PrescrizioneEsameEReferto_DAO.class);
            esame_DAO               = daoFactory.getDAO(Esame_DAO.class);
            
            //------------------------------------------------------------------
            //                      VISITE
            //------------------------------------------------------------------
            List<VisitaMB>        listaVisiteMB = visitaMB_DAO.getByPazienteId_Fatte(pazienteCodF);
            List<VisitaMSEReport> listaVisiteMS = visitaMSEReport_DAO.getByPazienteId_Fatte(pazienteCodF);
            
            List<Visita> listaTutteVisite= new LinkedList<Visita>();
            listaTutteVisite.addAll(listaVisiteMB);
            listaTutteVisite.addAll(listaVisiteMS);
            
            Iterator<Visita> visitaIterator = listaTutteVisite.iterator();
            while (visitaIterator.hasNext()) {
                Visita element = visitaIterator.next();
                String[] rigaRis= new String[7];
                
                String timpoVisiata="";
                if(element instanceof VisitaMB){
                    timpoVisiata = "<b>Visita medica:</b> \"";
                }else if(element instanceof VisitaMSEReport){
                    timpoVisiata = "<b>Visita specialistica:</b> \"";
                }
                //Aggunta visto dal paziente è fatto in posizione 0
                rigaRis[0] = element.getVistoPaziente().toString();
                //Aggunta Data di prescrizione in posizione 1
                rigaRis[1] = timpoVisiata + element.getPrescrizione() +" \"";
                //Aggunta Nome medico base in posizione 2
                rigaRis[2] = element.getDataPrescrizione().toString();
                //Aggunta Cognome medico base in posizione 3
                rigaRis[3] = element.getData().toString();
                //Aggunta Riassunto  in posizione 4
                if(element instanceof VisitaMB){
                    rigaRis[4] = "Gratuita";
                    rigaRis[5] = new Boolean(true).toString();
                }else if(element instanceof VisitaMSEReport){
                    rigaRis[4] = ((VisitaMSEReport)element).getTichet().toString()+" &euro;";
                    rigaRis[5] = ((VisitaMSEReport)element).getPagato().toString();
                }
                //Aggunta Pulsante  in posizione 5
                if(element instanceof VisitaMB){
                    rigaRis[6] = "V=VMB-"+element.getId().toString();
                }else if(element instanceof VisitaMSEReport){
                    rigaRis[6] = "V=VMS-"+element.getId().toString();
                }else{
                    rigaRis[6] = "V=ERR-"+element.getId().toString();
                }
                listaPrezzi.add(rigaRis);
            }
            //------------------------------------------------------------------
            //                      ESAMI
            //------------------------------------------------------------------
            HashMap<Integer,Esame> tipoEsameGiaCercati = new HashMap<>();
            List<PrescrizioneEsameEReferto>  listaEsami = prescrizioneEsame_DAO.getByPazienteId_Fatte(pazienteCodF);

            Iterator<PrescrizioneEsameEReferto> esameIterator = listaEsami.iterator();
            while (esameIterator.hasNext()) {
                PrescrizioneEsameEReferto element = esameIterator.next();
                String[] rigaRis= new String[7];
                
                Esame esame = tipoEsameGiaCercati.get(element.getId_Esame());
                if(esame  == null ){
                    
                    esame = esame_DAO.getByPrimaryKey(element.getId_Esame());
                    
                    tipoEsameGiaCercati.put(element.getId_Esame(), esame);
                }
                
                //Aggunta visto dal paziente è fatto in posizione 0
                rigaRis[0] = element.getVistoPaziente().toString();
                //Aggunta Data di prescrizione in posizione 1
                rigaRis[1] = "<b>Esame:</b> \"" + esame.getNome()+" \"";
                //Aggunta Nome medico base in posizione 2
                rigaRis[2] = element.getDataPrescrizione().toString();
                //Aggunta Cognome medico base in posizione 3
                rigaRis[3] = element.getData().toString();
                //Aggunta Riassunto  in posizione 4
                if(element.getRichiamo()){
                    rigaRis[4] = "Gratuita";
                    rigaRis[5] = new Boolean(true).toString();
                }else{
                    rigaRis[4] = element.getTichet().toString()+" &euro;";
                    rigaRis[5] = element.getPagato().toString();
                }
                //Aggunta Pulsante  in posizione 5
                rigaRis[6] = "E="+element.getId().toString();
                
                listaPrezzi.add(rigaRis);
            }
            //------------------------------------------------------------------
            //                      Ricette
            //------------------------------------------------------------------
            HashMap<Integer,Farmaco> farmacoGiaCercati = new HashMap<>();
            List<Ricetta>  listaRicetta = ricetta_DAO.getByPazienteId_Ririrate(pazienteCodF);

            Iterator<Ricetta> ricettaIterator = listaRicetta.iterator();
            while (ricettaIterator.hasNext()) {
                Ricetta element = ricettaIterator.next();
                String[] rigaRis= new String[7];
                
                Farmaco farmaco = farmacoGiaCercati.get(element.getId_Farmaco());
                if(farmaco  == null ){
                    farmaco = farmaco_DAO.getByPrimaryKey(element.getId_Farmaco());
                    
                    farmacoGiaCercati.put(element.getId_Farmaco(), farmaco);
                }
                
                //Aggunta visto dal paziente è fatto in posizione 0
                rigaRis[0] = element.getVistoPaziente().toString();
                //Aggunta Data di prescrizione in posizione 1
                rigaRis[1] = "<b>Ricetta:</b> \"" + farmaco.getNome()+" \"";
                //Aggunta Nome medico base in posizione 2
                rigaRis[2] = element.getDataPrescrizione().toString();
                //Aggunta Cognome medico base in posizione 3
                rigaRis[3] = element.getDataEvalsa().toString();
                //Aggunta Riassunto  in posizione 4
                rigaRis[4] = element.getPrezzoFarmacia().toString()+" &euro;";
                rigaRis[5] = new Boolean(true).toString();
                
                //Aggunta Pulsante  in posizione 5
                rigaRis[6] = "R="+element.getId().toString();
                
                listaPrezzi.add(rigaRis);
            }
        } catch (DAOFactoryException ex) {
            Logger.getLogger(getClass().getName()).severe(ex.toString());
        } catch (DAOException ex) {
            Logger.getLogger(getClass().getName()).severe(ex.toString());
        }catch(NumberFormatException ex){
             Logger.getLogger(getClass().getName()).severe(ex.toString());
        }

        results.addAll(listaPrezzi);

        Gson gson = new Gson();
        return gson.toJson(new Results(results.toArray(new String[0][6] ) ) );
    }
//------------------------------------------------------------------------------
//==============================================================================
//                                 PAZIENTI
//==============================================================================
    @GET
    @Path("{pazienteCodF}/PerMedicoSpec/{medicoSpecCodF}")
    public String getPagamentiPerMedicoBase(@PathParam("pazienteCodF") String pazienteCodF,@PathParam("medicoSpecCodF") String medicoSpecCodF){
        List<String[]> results = new ArrayList();
        
        List<String[]> listaPrezzi = new ArrayList();

        try {
            visitaMB_DAO            = daoFactory.getDAO(VisitaMB_DAO.class);
            visitaMSEReport_DAO     = daoFactory.getDAO(VisitaMSEReport_DAO.class);
            ricetta_DAO             = daoFactory.getDAO(Ricetta_DAO.class);
            farmaco_DAO             = daoFactory.getDAO(Farmaco_DAO.class);
            prescrizioneEsame_DAO   = daoFactory.getDAO(PrescrizioneEsameEReferto_DAO.class);
            esame_DAO               = daoFactory.getDAO(Esame_DAO.class);
            medicoSpec_DAO        = daoFactory.getDAO(MedicoSpec_DAO.class);
            
            //Controllo Se è un vero medico di base
            if(medicoSpec_DAO.getByPrimaryKey(medicoSpecCodF) != null){
            
            //------------------------------------------------------------------
            //                      VISITE
            //------------------------------------------------------------------
                //List<VisitaMB>        listaVisiteMB = visitaMB_DAO.getByPazienteId_Fatte(pazienteCodF);
                List<VisitaMSEReport> listaVisiteMS = visitaMSEReport_DAO.getByPazienteId_Fatte(pazienteCodF);

                List<Visita> listaTutteVisite= new LinkedList<Visita>();
                //listaTutteVisite.addAll(listaVisiteMB);
                listaTutteVisite.addAll(listaVisiteMS);

                Iterator<Visita> visitaIterator = listaTutteVisite.iterator();
                while (visitaIterator.hasNext()) {
                    Visita element = visitaIterator.next();
                    String[] rigaRis= new String[7];

                    String timpoVisiata="";
                    if(element instanceof VisitaMB){
                        timpoVisiata = "<b>Visita medica:</b> \"";
                    }else if(element instanceof VisitaMSEReport){
                        timpoVisiata = "<b>Visita specialistica:</b> \"";
                    }
                    //Aggunta visto dal paziente è fatto in posizione 0
                    if(element instanceof VisitaMB){
                        rigaRis[0] = new Boolean(true).toString();
                    }else if(element instanceof VisitaMSEReport){
                        rigaRis[0] = new Boolean(!(((VisitaMSEReport)element).getId_MedicoSpec().equals(medicoSpecCodF) &&
                                     ( !((VisitaMSEReport)element).getPagato()) )
                                     ).toString();
                    }
                    //Aggunta Data di prescrizione in posizione 1
                    rigaRis[1] = timpoVisiata + element.getPrescrizione() +" \"";
                    //Aggunta Nome medico base in posizione 2
                    rigaRis[2] = element.getDataPrescrizione().toString();
                    //Aggunta Cognome medico base in posizione 3
                    rigaRis[3] = element.getData().toString();
                    //Aggunta Riassunto  in posizione 4
                    if(element instanceof VisitaMB){
                        rigaRis[4] = "Gratuita";
                        rigaRis[5] = new Boolean(true).toString();
                    }else if(element instanceof VisitaMSEReport){
                        rigaRis[4] = ((VisitaMSEReport)element).getTichet().toString()+" &euro;";
                        rigaRis[5] = ((VisitaMSEReport)element).getPagato().toString();
                    }
                    //Aggunta Pulsante  in posizione 5
                    if(element instanceof VisitaMB){
                        rigaRis[6] = "V=VMB-"+element.getId().toString();
                    }else if(element instanceof VisitaMSEReport){
                        rigaRis[6] = "V=VMS-"+element.getId().toString();
                    }else{
                        rigaRis[6] = "V=ERR-"+element.getId().toString();
                    }
                    listaPrezzi.add(rigaRis);
                }
            //------------------------------------------------------------------
            //                      ESAMI
            //------------------------------------------------------------------
                HashMap<Integer,Esame> tipoEsameGiaCercati = new HashMap<>();
                List<PrescrizioneEsameEReferto>  listaEsami = prescrizioneEsame_DAO.getByPazienteId_Fatte(pazienteCodF);

                Iterator<PrescrizioneEsameEReferto> esameIterator = listaEsami.iterator();
                while (esameIterator.hasNext()) {
                    PrescrizioneEsameEReferto element = esameIterator.next();
                    String[] rigaRis= new String[7];

                    Esame esame = tipoEsameGiaCercati.get(element.getId_Esame());
                    if(esame  == null ){

                        esame = esame_DAO.getByPrimaryKey(element.getId_Esame());

                        tipoEsameGiaCercati.put(element.getId_Esame(), esame);
                    }

                    //Aggunta visto dal paziente è fatto in posizione 0
                    rigaRis[0] = new Boolean(! ((element.getId_MedicoSpec().equals(medicoSpecCodF)) && 
                                              (!element.getPagato()))
                                            ).toString();
                    //Aggunta Data di prescrizione in posizione 1
                    rigaRis[1] = "<b>Esame:</b> \"" + esame.getNome()+" \"";
                    //Aggunta Nome medico base in posizione 2
                    rigaRis[2] = element.getDataPrescrizione().toString();
                    //Aggunta Cognome medico base in posizione 3
                    rigaRis[3] = element.getData().toString();
                    //Aggunta Riassunto  in posizione 4
                    if(element.getRichiamo()){
                        rigaRis[4] = "Gratuita";
                        rigaRis[5] = new Boolean(true).toString();
                    }else{
                        rigaRis[4] = element.getTichet().toString()+" &euro;";
                        rigaRis[5] = element.getPagato().toString();
                    }
                    //Aggunta Pulsante  in posizione 5
                    rigaRis[6] = "E="+element.getId().toString();

                    listaPrezzi.add(rigaRis);
                }
            }
        } catch (DAOFactoryException ex) {
            Logger.getLogger(getClass().getName()).severe(ex.toString());
        } catch (DAOException ex) {
            Logger.getLogger(getClass().getName()).severe(ex.toString());
        }catch(NumberFormatException ex){
             Logger.getLogger(getClass().getName()).severe(ex.toString());
        }

        results.addAll(listaPrezzi);

        Gson gson = new Gson();
        return gson.toJson(new Results(results.toArray(new String[0][6] ) ) );
    }
//##############################################################################
    public static class Results implements Serializable {

        private String[][] results;

        public Results(String[][] results) {
            this.results = results;
        }

        public String[][] getResults() {
            return results;
        }

        public void setResults(String[][] results) {
            this.results = results;
        }
    }
}
