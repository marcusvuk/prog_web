var mes_codF = document.getElementById('msg_CodFCliente'); 
var inp_codF = document.getElementById('codF_Cliente');
     
var inp_nome    = document.getElementById('nomeCliente'); 
var inp_cognome = document.getElementById('cognomeCliente');

window.erroreOmonimia();      

function erroreOmonimia(){ 

    var queryString = window.location.search;
    var len = queryString.length;
    if( len>0 ){
        queryString= queryString.substring(1,len);
        
        var parametri = queryString.split("&");
        var numParam  = parametri.length;
        var nome="";
        var cognome="";
        for(let i=0; i< numParam; i++){
            var coppia=parametri[i].split("=");
            
            if(coppia[0]==='nome'){
                nome=coppia[1];
            }
            if(coppia[0]==='cognome'){
                cognome=coppia[1];
            }
        }
        if( (nome!=="") && (cognome!=="") ){
            inp_codF.setAttribute('class',inp_codF.getAttribute('class')+' is-invalid ');
            mes_codF.setAttribute('class',mes_codF.getAttribute('class')+' invalid-feedback ');
            
            inp_nome.setAttribute('value',nome);
            inp_cognome.setAttribute('value',cognome);
        }
    }
}


inp_codF.addEventListener("keyup",function (){ 
    inp_codF.setAttribute('class',' form-control form-control ');
    mes_codF.setAttribute('class',' collapse ');
});