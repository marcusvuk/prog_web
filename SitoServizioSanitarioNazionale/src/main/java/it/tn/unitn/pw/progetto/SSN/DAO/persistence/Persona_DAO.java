/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.tn.unitn.pw.progetto.SSN.DAO.persistence;

import java.util.List;
import java.sql.Date;
        
import it.tn.unitn.pw.progetto.SSN.persistence.entities.Persona;
import it.tn.unitn.pw.progetto.SSN.DAO.common.DAO;
import it.tn.unitn.pw.progetto.SSN.DAO.common.exception.DAOException;

/**
 *
 * @author Brian
 */
public interface Persona_DAO extends DAO<Persona, String>{
/*============================================================================*/    
/*                         METODO INSERT                                      */
/*============================================================================*/
    public String insert(Persona paziente)throws DAOException;
/*============================================================================*/    
/*============================================================================*/    
/*                         METODO UPDATE                                      */
/*============================================================================*/
    public void update(Persona paziente)throws DAOException;
    public void update_Password(String id,String Password )throws DAOException;
/*============================================================================*/
/*============================================================================*/    
/*                         METODO GET Da ID                                   */
/*============================================================================*/
    public List<Persona> getBySSProvincialeId_Vive(Integer id)throws DAOException;
    public List<Persona> getByNomeCognome(String nome,String cognome)throws DAOException;
/*============================================================================*/
/*============================================================================*/    
/*                         METODO GET Data ultima Prescrizione                */
/*============================================================================*/
    public Date getUltimaPrescrizione(String id)throws DAOException;
/*============================================================================*/
/*============================================================================*/    
/*                         METODO GET LOGIN                                  */
/*============================================================================*/
    public Persona getByPersonaEmailAndPassword(String email, String password)throws DAOException;
    public String getSaltPersonaByEmail(String email)throws DAOException;
    
    public Persona getByPersonaCodFAndPassword(String CodF, String password)throws DAOException;
    public String getSaltPersonaByCodF(String CodF)throws DAOException;
    
    
    public Persona getByPersonaEmail(String email)throws DAOException;
/*============================================================================*/
/*============================================================================*/    
/*                         METODO AGGIUNTIVI                                  */
/*============================================================================*/
    public Integer getNumeroNuoviReportEsameForPazienteByPersonaCodF(String codF)throws DAOException;
    public Integer getNumeroNuoviReportEsameForMedicoBaseByPersonaCodF(String codF)throws DAOException;
    
    public Integer getNumeroNuoviReportVisiteForPazienteByPersonaCodF(String codF)throws DAOException;
    public Integer getNumeroNuoviReportVisiteForMedicoBaseByPersonaCodF(String codF)throws DAOException;
    
    
    public Integer getNumeroNuoviRicetteByPersonaCodF(String codF)throws DAOException;
    public Integer getNumeroNuoviVisiteByPersonaCodF(String codF)throws DAOException;
    public Integer getNumeroNuoviEsamiByPersonaCodF(String codF)throws DAOException;
    
    public Integer getNumeroEsamiSenzaReportByPersonaCodF(String codF)throws DAOException;
    public Integer getNumeroVisiteSenzaReportByPersonaCodF(String codF)throws DAOException;
/*============================================================================*/
/*============================================================================*/    
/*                         METODO GER PERSONA Intervallo di età               */
/*============================================================================*/
    public List<Persona> getBySSProvincialeId_Vive_E_NatoNelIntervallo(Integer id,Date inizio , Date fine)throws DAOException;
/*============================================================================*/
}
