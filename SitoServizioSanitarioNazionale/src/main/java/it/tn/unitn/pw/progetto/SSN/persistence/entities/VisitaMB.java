/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.tn.unitn.pw.progetto.SSN.persistence.entities;

/**
 *
 * @author Brian
 */
public class VisitaMB extends Visita{
/*============================================================================*/
/*                                       ATTRIBUTI                            */
/*============================================================================*/
    private String     id_MedicoBase;
/*============================================================================*/
    
/*============================================================================*/    
/*                                      METODI COSTRUTTORI                    */
/*============================================================================*/
    public VisitaMB() {
        id_MedicoBase = null;
    }
/*============================================================================*/

/*============================================================================*/    
/*                                      METODI GETTER E SETTER                */
/*============================================================================*/
    /*--------------------------------------*/   
    public String getId_MedicoBase() {
        return id_MedicoBase;
    }

    public void setId_MedicoBase(String id_MedicoBase) {
        this.id_MedicoBase = id_MedicoBase;
    }
    /*--------------------------------------*/
/*============================================================================*/
}
