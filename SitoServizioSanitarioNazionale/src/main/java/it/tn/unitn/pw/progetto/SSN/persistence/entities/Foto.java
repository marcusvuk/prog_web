/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.tn.unitn.pw.progetto.SSN.persistence.entities;

import java.sql.Date;

/**
 *
 * @author Brian
 */
public class Foto {
/*============================================================================*/
/*                                       ATTRIBUTI                            */
/*============================================================================*/
    private Integer     id;
    private String      id_Persona;
    private Date        data;
    private String      path;
    private String      nome;
    private Boolean     scelta;
/*============================================================================*/
    
/*============================================================================*/    
/*                                      METODI COSTRUTTORI                    */
/*============================================================================*/
    public Foto() {
        id          = null;
        id_Persona  = null;
        data        = null;
        path        = null;
        nome        = null;
        scelta      = null;
    }
/*============================================================================*/

/*============================================================================*/    
/*                                      METODI GETTER E SETTER                */
/*============================================================================*/
    /*--------------------------------------*/   
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
    /*--------------------------------------*/
    public String getId_Persona() {
        return id_Persona;
    }
    
    public void setId_Persona(String id_Persona) {
        this.id_Persona = id_Persona;
    }
    /*--------------------------------------*/
    public Date getData() {
        return data;
    }
    
    public void setData(Date data) {
        this.data = data;
    }
    /*--------------------------------------*/
    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }
    /*--------------------------------------*/
    public String getNome() {
        return nome;
    }
    public void setNome(String nome) {
        this.nome = nome;
    }
    /*--------------------------------------*/
    public Boolean getScelta() {
        return scelta;
    }

    public void setScelta(Boolean scelta) {
        this.scelta = scelta;
    }
    /*--------------------------------------*/
/*============================================================================*/
}
