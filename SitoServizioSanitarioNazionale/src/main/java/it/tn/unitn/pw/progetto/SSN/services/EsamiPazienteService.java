/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.tn.unitn.pw.progetto.SSN.services;


import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Produces;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;
import javax.ws.rs.core.MediaType;

import com.google.gson.Gson;
import it.tn.unitn.pw.progetto.SSN.DAO.common.exception.DAOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;

import it.tn.unitn.pw.progetto.SSN.DAO.persistence.*;
import it.tn.unitn.pw.progetto.SSN.DAO.common.exception.DAOFactoryException;
import it.tn.unitn.pw.progetto.SSN.DAO.common.factory.DAOFactory;
import it.tn.unitn.pw.progetto.SSN.persistence.entities.*;
import java.sql.SQLException;
import java.sql.Date;
import java.util.Iterator;
import java.util.HashMap;
import java.util.logging.Logger;
import sun.tools.tree.ThisExpression;

/**
 * REST Web Service
 *
 * @author Brian
 */
@Path("EsamiPaziente")
public class EsamiPazienteService {
    
    private static DAOFactory       daoFactory;

    private static PrescrizioneEsameEReferto_DAO    esameEReferto_DAO;
    private static CartellaClinicaPerMB_DAO         cartellaClinica_DAO;
    private static MedicoBase_DAO                   medicoBase_DAO;
    private static MedicoSpec_DAO                   medicoSpec_DAO;
    private static Esame_DAO                        esame_DAO;
    
    static public void init(DAOFactory daoFactory1) {
        daoFactory =  daoFactory1;
    }
    
    @Context
    private UriInfo context;

    /**
     * Creates a new instance of EsamiPazienteService
     */
    public EsamiPazienteService() {
    }
//==============================================================================
//                                 PAZIENTI
//==============================================================================
//------------------------------------------------------------------------------
//                                 PER PAZIENTE  (fatti)
//------------------------------------------------------------------------------
    @GET
    @Path("Fatte/{pazienteCodF}")
    public String getPazientiFatti(@PathParam("pazienteCodF") String pazienteCodF){
        List<String[]> results = new ArrayList();
        
        List<String[]> listaEsamiDaFiltrare = new ArrayList();

        try {
            medicoBase_DAO        = daoFactory.getDAO(MedicoBase_DAO.class);
            cartellaClinica_DAO   = daoFactory.getDAO(CartellaClinicaPerMB_DAO.class);
            esameEReferto_DAO     = daoFactory.getDAO(PrescrizioneEsameEReferto_DAO.class);
            esame_DAO             = daoFactory.getDAO(Esame_DAO.class);
            
            List<PrescrizioneEsameEReferto> listaEsame = esameEReferto_DAO.getByPazienteId_Fatte(pazienteCodF);
            
            HashMap<Integer,MedicoBase> mediciGiaCercati = new HashMap<>();
            HashMap<Integer,Esame> tipoEsameGiaCercati = new HashMap<>();
            
            Iterator<PrescrizioneEsameEReferto> esameIterator = listaEsame.iterator();
            while (esameIterator.hasNext()) {
                String[] rigaRis= new String[6];
                
                PrescrizioneEsameEReferto element = esameIterator.next();
                
                MedicoBase medicoBase = mediciGiaCercati.get(element.getId_CartellaClinica());
                if(medicoBase  == null ){
                    
                    CartellaClinicaPerMB cartellaClinica = cartellaClinica_DAO.getByPrimaryKey( element.getId_CartellaClinica() );
                    medicoBase = medicoBase_DAO.getByPrimaryKey(cartellaClinica.getId_MedicoBase());
                    
                    mediciGiaCercati.put(element.getId_CartellaClinica(), medicoBase);
                }
                
                Esame esame = tipoEsameGiaCercati.get(element.getId_Esame());
                if(esame  == null ){
                    
                    esame = esame_DAO.getByPrimaryKey(element.getId_Esame());
                    
                    tipoEsameGiaCercati.put(element.getId_Esame(), esame);
                }
                
                //Aggunta Esame è fatto in posizione 0
                rigaRis[0] = element.getVistoReportPaziente().toString();
                //Aggunta Data di prescrizione in posizione 1
                rigaRis[1] = element.getDataPrescrizione().toString();
                //Aggunta Nome medico base in posizione 2
                rigaRis[2] = medicoBase.getNome();
                //Aggunta Cognome medico base in posizione 3
                rigaRis[3] = medicoBase.getCognome();
                //Aggunta Tipo di Esame  in posizione 4
                rigaRis[4] = esame.getNome();
                //Aggunta Tipo di Esame  in posizione 5
                rigaRis[5] = element.getId().toString();
                
                
                listaEsamiDaFiltrare.add(rigaRis);
            }
              
        } catch (DAOFactoryException ex) {
            Logger.getLogger(getClass().getName()).severe(ex.toString());
        } catch (DAOException ex) {
            Logger.getLogger(getClass().getName()).severe(ex.toString());
        }catch(NumberFormatException ex){
             Logger.getLogger(getClass().getName()).severe(ex.toString());
        }

        results.addAll(listaEsamiDaFiltrare);

        Gson gson = new Gson();
        return gson.toJson(new Results(results.toArray(new String[0][6] ) ) );
    }
//------------------------------------------------------------------------------
    
//------------------------------------------------------------------------------
//                                 PER PAZIENTE  (da fare)
//------------------------------------------------------------------------------
    @GET
    @Path("DaFare/{pazienteCodF}")
    public String getPazientiDaFare(@PathParam("pazienteCodF") String pazienteCodF){
        List<String[]> results = new ArrayList();
        
        List<String[]> listaEsamiDaFiltrare = new ArrayList();

        try {
            medicoBase_DAO        = daoFactory.getDAO(MedicoBase_DAO.class);
            cartellaClinica_DAO   = daoFactory.getDAO(CartellaClinicaPerMB_DAO.class);
            esameEReferto_DAO     = daoFactory.getDAO(PrescrizioneEsameEReferto_DAO.class);
            esame_DAO             = daoFactory.getDAO(Esame_DAO.class);
            
            List<PrescrizioneEsameEReferto> listaEsame = esameEReferto_DAO.getByPazienteId_DaFare(pazienteCodF);
            
            HashMap<Integer,MedicoBase> mediciGiaCercati = new HashMap<>();
            HashMap<Integer,Esame> tipoEsameGiaCercati = new HashMap<>();
            
            Iterator<PrescrizioneEsameEReferto> esameIterator = listaEsame.iterator();
            while (esameIterator.hasNext()) {
                String[] rigaRis= new String[6];
                
                PrescrizioneEsameEReferto element = esameIterator.next();
                
                MedicoBase medicoBase = mediciGiaCercati.get(element.getId_CartellaClinica());
                if(medicoBase  == null ){
                    
                    CartellaClinicaPerMB cartellaClinica = cartellaClinica_DAO.getByPrimaryKey( element.getId_CartellaClinica() );
                    medicoBase = medicoBase_DAO.getByPrimaryKey(cartellaClinica.getId_MedicoBase());
                    
                    mediciGiaCercati.put(element.getId_CartellaClinica(), medicoBase);
                }
                
                Esame esame = tipoEsameGiaCercati.get(element.getId_Esame());
                if(esame  == null ){
                    
                    esame = esame_DAO.getByPrimaryKey(element.getId_Esame());
                    
                    tipoEsameGiaCercati.put(element.getId_Esame(), esame);
                }
                
                //Aggunta Esame è da fare  in posizione 0
                rigaRis[0] = element.getVistoPaziente().toString();
                //Aggunta Data di prescrizione in posizione 1
                rigaRis[1] = element.getDataPrescrizione().toString();
                //Aggunta Nome medico base in posizione 2
                rigaRis[2] = medicoBase.getNome();
                //Aggunta Cognome medico base in posizione 3
                rigaRis[3] = medicoBase.getCognome();
                //Aggunta Tipo di Esame  in posizione 4
                rigaRis[4] = esame.getNome();
                //Aggunta Tipo di Esame  in posizione 5
                rigaRis[5] = element.getId().toString();
                
                
                listaEsamiDaFiltrare.add(rigaRis);
            }
              
        } catch (DAOFactoryException ex) {
            Logger.getLogger(getClass().getName()).severe(ex.toString());
        } catch (DAOException ex) {
            Logger.getLogger(getClass().getName()).severe(ex.toString());
        }catch(NumberFormatException ex){
             Logger.getLogger(getClass().getName()).severe(ex.toString());
        }

        results.addAll(listaEsamiDaFiltrare);

        Gson gson = new Gson();
        return gson.toJson(new Results(results.toArray(new String[0][6] ) ) );
    }
//------------------------------------------------------------------------------
    
//==============================================================================
//                                 MEDICO BASE
//==============================================================================    
//------------------------------------------------------------------------------
//                                 PER MEDICO BASE  (fatti)
//------------------------------------------------------------------------------
    @GET
    @Path("Fatte/{pazienteCodF}/PerMedicoBase/{medicoBaseCodF}")
    public String getPazientiFattiPerMedicoBase(@PathParam("pazienteCodF") String pazienteCodF,@PathParam("medicoBaseCodF") String medicoBaseCodF){
        List<String[]> results = new ArrayList();
        
        List<String[]> listaEsamiDaFiltrare = new ArrayList();

        try {
            medicoBase_DAO        = daoFactory.getDAO(MedicoBase_DAO.class);
            cartellaClinica_DAO   = daoFactory.getDAO(CartellaClinicaPerMB_DAO.class);
            esameEReferto_DAO     = daoFactory.getDAO(PrescrizioneEsameEReferto_DAO.class);
            esame_DAO             = daoFactory.getDAO(Esame_DAO.class);
            
            
            CartellaClinicaPerMB cartellaClinicaAttiva = cartellaClinica_DAO.getByPazienteId_Attivo(pazienteCodF);
            //Controllo se è il mio medico di base
            if(cartellaClinicaAttiva.getId_MedicoBase().equals(medicoBaseCodF)){
            
                List<PrescrizioneEsameEReferto> listaEsame = esameEReferto_DAO.getByPazienteId_Fatte(pazienteCodF);

                HashMap<Integer,MedicoBase> mediciGiaCercati = new HashMap<>();
                HashMap<Integer,Esame> tipoEsameGiaCercati = new HashMap<>();

                Iterator<PrescrizioneEsameEReferto> esameIterator = listaEsame.iterator();
                while (esameIterator.hasNext()) {
                    String[] rigaRis= new String[6];

                    PrescrizioneEsameEReferto element = esameIterator.next();

                    MedicoBase medicoBase = mediciGiaCercati.get(element.getId_CartellaClinica());
                    if(medicoBase  == null ){

                        CartellaClinicaPerMB cartellaClinica = cartellaClinica_DAO.getByPrimaryKey( element.getId_CartellaClinica() );
                        medicoBase = medicoBase_DAO.getByPrimaryKey(cartellaClinica.getId_MedicoBase());

                        mediciGiaCercati.put(element.getId_CartellaClinica(), medicoBase);
                    }

                    Esame esame = tipoEsameGiaCercati.get(element.getId_Esame());
                    if(esame  == null ){

                        esame = esame_DAO.getByPrimaryKey(element.getId_Esame());

                        tipoEsameGiaCercati.put(element.getId_Esame(), esame);
                    }

                    //Aggunta Identificatore Esame in posizione 0
                    rigaRis[0] = element.getVistoReportMedicoBase().toString();
                    //Aggunta Data di prescrizione in posizione 1
                    rigaRis[1] = element.getDataPrescrizione().toString();
                    //Aggunta Nome medico base in posizione 2
                    rigaRis[2] = medicoBase.getNome();
                    //Aggunta Cognome medico base in posizione 3
                    rigaRis[3] = medicoBase.getCognome();
                    //Aggunta Tipo di Esame  in posizione 4
                    rigaRis[4] = esame.getNome();
                    //Aggunta Tipo di Esame  in posizione 5
                    rigaRis[5] = element.getId().toString();


                    listaEsamiDaFiltrare.add(rigaRis);
                }
            }  
        } catch (DAOFactoryException ex) {
            Logger.getLogger(getClass().getName()).severe(ex.toString());
        } catch (DAOException ex) {
            Logger.getLogger(getClass().getName()).severe(ex.toString());
        }catch(NumberFormatException ex){
             Logger.getLogger(getClass().getName()).severe(ex.toString());
        }

        results.addAll(listaEsamiDaFiltrare);

        Gson gson = new Gson();
        return gson.toJson(new Results(results.toArray(new String[0][6] ) ) );
    }
//------------------------------------------------------------------------------
    
//------------------------------------------------------------------------------
//                                 PER MEDICO BASE  (da fare)
//------------------------------------------------------------------------------
    @GET
    @Path("DaFare/{pazienteCodF}/PerMedicoBase/{medicoBaseCodF}")
    public String getPazientiDaFarePerMedicoBase(@PathParam("pazienteCodF") String pazienteCodF,@PathParam("medicoBaseCodF") String medicoBaseCodF){
        List<String[]> results = new ArrayList();
        
        List<String[]> listaEsamiDaFiltrare = new ArrayList();

        try {
            medicoBase_DAO        = daoFactory.getDAO(MedicoBase_DAO.class);
            cartellaClinica_DAO   = daoFactory.getDAO(CartellaClinicaPerMB_DAO.class);
            esameEReferto_DAO     = daoFactory.getDAO(PrescrizioneEsameEReferto_DAO.class);
            esame_DAO             = daoFactory.getDAO(Esame_DAO.class);
            
              
            CartellaClinicaPerMB cartellaClinicaAttiva = cartellaClinica_DAO.getByPazienteId_Attivo(pazienteCodF);
            //Controllo se è il mio medico di base
            if(cartellaClinicaAttiva.getId_MedicoBase().equals(medicoBaseCodF)){
            

                List<PrescrizioneEsameEReferto> listaEsame = esameEReferto_DAO.getByPazienteId_DaFare(pazienteCodF);

                HashMap<Integer,MedicoBase> mediciGiaCercati = new HashMap<>();
                HashMap<Integer,Esame> tipoEsameGiaCercati = new HashMap<>();

                Iterator<PrescrizioneEsameEReferto> esameIterator = listaEsame.iterator();
                while (esameIterator.hasNext()) {
                    String[] rigaRis= new String[6];

                    PrescrizioneEsameEReferto element = esameIterator.next();

                    MedicoBase medicoBase = mediciGiaCercati.get(element.getId_CartellaClinica());
                    if(medicoBase  == null ){

                        CartellaClinicaPerMB cartellaClinica = cartellaClinica_DAO.getByPrimaryKey( element.getId_CartellaClinica() );
                        medicoBase = medicoBase_DAO.getByPrimaryKey(cartellaClinica.getId_MedicoBase());

                        mediciGiaCercati.put(element.getId_CartellaClinica(), medicoBase);
                    }

                    Esame esame = tipoEsameGiaCercati.get(element.getId_Esame());
                    if(esame  == null ){

                        esame = esame_DAO.getByPrimaryKey(element.getId_Esame());

                        tipoEsameGiaCercati.put(element.getId_Esame(), esame);
                    }

                    //Aggunta non serve posizione 0
                    rigaRis[0] = "";
                    //Aggunta Data di prescrizione in posizione 1
                    rigaRis[1] = element.getDataPrescrizione().toString();
                    //Aggunta Nome medico base in posizione 2
                    rigaRis[2] = medicoBase.getNome();
                    //Aggunta Cognome medico base in posizione 3
                    rigaRis[3] = medicoBase.getCognome();
                    //Aggunta Tipo di Esame  in posizione 4
                    rigaRis[4] = esame.getNome();
                    //Aggunta Tipo di Esame  in posizione 5
                    rigaRis[5] = element.getId().toString();
                   

                     listaEsamiDaFiltrare.add(rigaRis);
                }
            }
              
        } catch (DAOFactoryException ex) {
            Logger.getLogger(getClass().getName()).severe(ex.toString());
        } catch (DAOException ex) {
            Logger.getLogger(getClass().getName()).severe(ex.toString());
        }catch(NumberFormatException ex){
             Logger.getLogger(getClass().getName()).severe(ex.toString());
        }

        results.addAll(listaEsamiDaFiltrare);

        Gson gson = new Gson();
        return gson.toJson(new Results(results.toArray(new String[0][6] ) ) );
    }
//------------------------------------------------------------------------------

//==============================================================================
//                                 MEDICO SPECIALISTA
//==============================================================================     
//------------------------------------------------------------------------------
//                                 PER  MEDICO SPECIALISTA  
//------------------------------------------------------------------------------
    @GET
    @Path("{pazienteCodF}/PerMedicoSpec/{medicoSpecCodF}")
    public String getPazientiFattiPerMedicoSpec(@PathParam("pazienteCodF") String pazienteCodF,@PathParam("medicoSpecCodF") String medicoSpecCodF){
        List<String[]> results = new ArrayList();
        
        List<String[]> listaEsamiDaFiltrare = new ArrayList();

        try {
            medicoBase_DAO        = daoFactory.getDAO(MedicoBase_DAO.class);
            cartellaClinica_DAO   = daoFactory.getDAO(CartellaClinicaPerMB_DAO.class);
            esameEReferto_DAO     = daoFactory.getDAO(PrescrizioneEsameEReferto_DAO.class);
            esame_DAO             = daoFactory.getDAO(Esame_DAO.class);
            medicoSpec_DAO        = daoFactory.getDAO(MedicoSpec_DAO.class);
            
            //Controllo Se è un vero medico di base
            if(medicoSpec_DAO.getByPrimaryKey(medicoSpecCodF) != null){
            
                List<PrescrizioneEsameEReferto> listaEsame = esameEReferto_DAO.getByPazienteId(pazienteCodF);

                HashMap<Integer,MedicoBase> mediciGiaCercati = new HashMap<>();
                HashMap<Integer,Esame> tipoEsameGiaCercati = new HashMap<>();

                Iterator<PrescrizioneEsameEReferto> esameIterator = listaEsame.iterator();
                while (esameIterator.hasNext()) {
                    String[] rigaRis= new String[6];

                    PrescrizioneEsameEReferto element = esameIterator.next();

                    MedicoBase medicoBase = mediciGiaCercati.get(element.getId_CartellaClinica());
                    if(medicoBase  == null ){

                        CartellaClinicaPerMB cartellaClinica = cartellaClinica_DAO.getByPrimaryKey( element.getId_CartellaClinica() );
                        medicoBase = medicoBase_DAO.getByPrimaryKey(cartellaClinica.getId_MedicoBase());

                        mediciGiaCercati.put(element.getId_CartellaClinica(), medicoBase);
                    }

                    Esame esame = tipoEsameGiaCercati.get(element.getId_Esame());
                    if(esame  == null ){

                        esame = esame_DAO.getByPrimaryKey(element.getId_Esame());

                        tipoEsameGiaCercati.put(element.getId_Esame(), esame);
                    }

                    //Aggunta Esame Ha un repot in posizione 0
                    if(element.getData()!= null){
                        rigaRis[0] = "true";//Si
                    }else{
                        rigaRis[0] = "false";//NO
                    }
                    
                    //Aggunta Data di prescrizione in posizione 1
                    rigaRis[1] = element.getDataPrescrizione().toString();
                    //Aggunta Nome medico base in posizione 2
                    rigaRis[2] = medicoBase.getNome();
                    //Aggunta Cognome medico base in posizione 3
                    rigaRis[3] = medicoBase.getCognome();
                    //Aggunta Tipo di Esame  in posizione 4
                    rigaRis[4] = esame.getNome();
                    //Aggunta Tipo di Esame  in posizione 5
                    rigaRis[5] = element.getId().toString();


                    listaEsamiDaFiltrare.add(rigaRis);
                }
            }
              
        } catch (DAOFactoryException ex) {
            Logger.getLogger(getClass().getName()).severe(ex.toString());
        } catch (DAOException ex) {
            Logger.getLogger(getClass().getName()).severe(ex.toString());
        }catch(NumberFormatException ex){
             Logger.getLogger(getClass().getName()).severe(ex.toString());
        }

        results.addAll(listaEsamiDaFiltrare);

        Gson gson = new Gson();
        return gson.toJson(new Results(results.toArray(new String[0][6] ) ) );
    }
//------------------------------------------------------------------------------ 
//##############################################################################
    public static class Results implements Serializable {

        private String[][] results;

        public Results(String[][] results) {
            this.results = results;
        }

        public String[][] getResults() {
            return results;
        }

        public void setResults(String[][] results) {
            this.results = results;
        }
    }
}
