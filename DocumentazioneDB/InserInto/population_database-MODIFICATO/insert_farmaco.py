#Python 3.6.7
import re
output = open("popola_farmaci.sql", "w")
for farmaco in open("Farmaci.txt", "r"):#si bugga all'ultimo farmaco, ma tutti gli altri li fa senza nessun problema
    substringhe=re.search('(.+?), (.+?)\n', farmaco)
    if substringhe==None:
        output.close()
    else:
        farmaco=substringhe.group(1)
        descrizione = substringhe.group(2)
        output.write("INSERT INTO farmaco(nome,descrizione) VALUES ('"+farmaco+"','"+descrizione+"');\n")