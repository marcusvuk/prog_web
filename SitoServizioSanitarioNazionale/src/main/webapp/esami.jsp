
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%@page import="java.util.List"%>
<%@page import="it.tn.unitn.pw.progetto.SSN.DAO.common.exception.*"%>
<%@page import="it.tn.unitn.pw.progetto.SSN.DAO.common.factories.jdbc.*"%>
<%@page import="it.tn.unitn.pw.progetto.SSN.DAO.common.factory.*"%>
<%@page import="it.tn.unitn.pw.progetto.SSN.DAO.common.persistence.dao.jdbc.*"%>
<%@page import="it.tn.unitn.pw.progetto.SSN.DAO.persistence.*"%>
<%@page import="it.tn.unitn.pw.progetto.SSN.persistence.entities.*"%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
        <title>
            Esame
        </title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" crossorigin="anonymous">
    
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/css/select2.min.css">
  
        <!-- Cittadino -->
        <link rel="stylesheet" href="${pageContext.request.contextPath}/modal/modal_Login/css/floating-labels.css">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/modal/modal_Login/css/forms.css">
        <!-- Cittadino -->
        <link rel="icon" href="${pageContext.request.contextPath}/sito_IMG/icona.ico" type="image/x-icon" />
        <link href="${pageContext.request.contextPath}/sito_CSS/style.css" rel="stylesheet" />
        <link href="${pageContext.request.contextPath}/sito_CSS/style_MenuUtente.css" rel="stylesheet" />
    </head>
    <body onload="showEsitoModale()">
        <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/js/select2.min.js"></script>
        
        
    <!-- JAVASCRIP -->
        <%@include file="/pag_ElementiComunni/pag_MenuPrincipale.jsp" %>
        
        <%@include file="/modal/modal_ScriviReportEsame/modal_ScriviReportEsame.jsp" %> 
        <%@include file="/modal/modal_PagamentoEsame/modal_PagamentoEsame.jsp" %> 
        <div class="container">
            <div>
                <div class="row">
                    <div class="col-md-10">
                        <h2>
                            <c:choose>
                                <c:when test="${p_ssProvinciale eq null}">
                                    Esame di
                                    <c:choose>
                                        <c:when test="${c_Persona ne null}">
                                            <b> <c:out value="${c_Persona.cognome} ${c_Persona.nome}"/> </b>
                                        </c:when>
                                        <c:when test="${b_Paziente ne null}">
                                            <b> <c:out value="${b_Paziente.cognome} ${b_Paziente.nome}"/> </b>
                                        </c:when>
                                        <c:when test="${s_Paziente ne null}">
                                            <b> <c:out value="${s_Paziente.cognome} ${s_Paziente.nome}"/> </b>
                                        </c:when>
                                    </c:choose>
                                </c:when>
                                <c:otherwise>
                                    Richiamo
                                </c:otherwise>
                            </c:choose>
                        </h2>
                    </div>
                    <c:if test="${(s_Paziente ne null) && (c_b_s_Esame.data  eq null)}">
                        <div class="col-md-2">
                            <a href="#"
                               class="btn btn-primary pull-right float-right"
                               data-toggle="modal" 
                               data-target="#ScriviReportEsameModal">
                                <i class="fas fa-pencil-alt"></i> Compila Report
                            </a>
                        </div>
                    </c:if>
                    <c:if test="${(s_Paziente ne null) && (c_b_s_Esame.data  ne null) &&
                                  (c_b_s_Esame.pagato eq false) &&  (c_b_s_Esame.richiamo eq false)}">
                        <div class="col-md-2">
                            <a href="#"
                               class="btn btn-primary pull-right float-right"
                               data-toggle="modal" 
                               data-target="#PagamentoEsameModal">
                                <i class="fas fa-money-bill-wave"></i> Paga Esame
                            </a>
                        </div>
                    </c:if>
                </div>
                
                
                
                <c:if test="${p_ssProvinciale eq null}">
                    <div class="row">
                        <div class="col-5">
                            <h5>
                                <b>
                                Prescritto da:
                                </b>
                            </h5>
                        </div>
                        <div class="col-7">
                            <h5>
                                Dr. <c:out value="${c_b_s_PescrittoMB.cognome}"/> <c:out value="${c_b_s_PescrittoMB.nome}"/>
                            </h5>
                        </div>
                    </div>
                </c:if>        
                        
                        
                        
                <c:if test="${c_b_s_Esame.richiamo eq true}">
                    <div class="row">
                        <div class="col-md-5">
                            <h5>
                                <b>
                                Prescritto Come :
                                </b>
                            </h5>
                        </div>
                        <div class="col-md-7">
                            <h5>
                                Richiamo
                            </h5>
                        </div>
                    </div>
                </c:if>
                
                        
                        
                        
                <div class="row">
                    <div class="col-md-5">
                        <h5>
                            <b>
                            Prescritto il giorno:
                            </b>
                        </h5>
                    </div>
                    <div class="col-md-7">
                        <h5>
                            <c:out value="${c_b_s_Esame.dataPrescrizione}"/>
                        </h5>
                    </div>
                </div>
                 <div class="row">
                    <div class="col-md-5">
                        <h5>
                            <b>
                             Tipo esame:
                            </b>
                        </h5>
                    </div>
                    <div class="col-md-7">
                        <h5>
                            <c:out value="${c_b_s_TipoEsame.nome}"/>
                        </h5>
                    </div>
                </div>
                <br>   
                <hr>
                 <!-- -------- MEDICO SPECIALISTA -------- -->
                <c:if test="${c_b_s_Esame.data ne null}">   
                    <div class="row">
                        <div class="col-md-5">
                            <h5>
                                <b>
                                Il report fatto il giorno:
                                </b>
                            </h5>
                        </div>
                        <div class="col-md-7">
                            <h5>
                                <c:out value="${c_b_s_Esame.data}"/>
                            </h5>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-5">
                            <h5>
                                <b>
                                Dal medico specialista:
                                </b>
                            </h5>
                        </div>
                        <div class="col-md-7">
                            <h5>
                                Dr. <c:out value="${c_b_s_ReportFattoMS.cognome}"/> <c:out value="${c_b_s_ReportFattoMS.nome}"/>
                            </h5>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-5">
                            <h5>
                                <b>
                                Risultato:
                                </b>
                            </h5>
                        </div>
                        <div class="col-md-7">
                            <h5>
                                <c:out value="${c_b_s_Esame.risultato}"/>
                            </h5>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-5">
                            <h5>
                                <b>
                                Cura:
                                </b>
                            </h5>
                        </div>
                        <div class="col-md-7">
                            <h5>
                                <c:out value="${c_b_s_Esame.cura}"/>
                            </h5>
                        </div>
                    </div>
                    <c:if test="${c_b_s_Esame.richiamo ne true}">
                        <div class="row">
                            <div class="col-md-5">
                                <h5>
                                    <b>
                                    Tichet:
                                    </b>
                                </h5>
                            </div>
                            <div class="col-md-7">
                                <h5>
                                    <c:out value="${c_b_s_Esame.tichet}"/> &euro;
                                </h5>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-5">
                                <h5>
                                    <b>
                                    Stato pagamento:
                                    </b>
                                </h5>
                            </div>
                            <div class="col-md-7">
                                <h5>
                                    <c:choose>
                                        <c:when test="${c_b_s_Esame.pagato eq true}">
                                            Pagato
                                        </c:when>
                                        <c:otherwise>
                                            <b class="text-danger">Non pagato</b>
                                        </c:otherwise>
                                    </c:choose>
                                </h5>
                            </div>
                        </div>
                    </c:if>
                </c:if>
            </div>
        </div>
        <%@include file="pag_ElementiComunni/pag_Footer.jsp" %>
    </body>
</html>
