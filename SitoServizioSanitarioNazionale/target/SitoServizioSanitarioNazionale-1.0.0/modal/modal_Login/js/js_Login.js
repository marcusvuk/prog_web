$(function () {
    $("#autocomplete-provincia").select2({
        placeholder: "Scegli provincia",
        allowClear: true,
        ajax: {
            url: "/SitoServizioSanitarioNazionale/services/provincie",
            dataType: "json"
        }
    });
    $("#autocomplete-provincia").val(null).trigger("change");
    
    
    

    
    $("#btn_CittadinoLogin").click(function(){
    $("#MedicoBaseLogin"        ).collapse('hide');
    $("#MedicoSpecialistaLogin" ).collapse('hide');
    $("#FarmaciaLogin"          ).collapse('hide');
    $("#SSProvincialeLogin"     ).collapse('hide');
    $("#CittadiniLogin"         ).collapse('show');
  });
  $("#btn_MedicoBaseLogin").click(function(){
    $("#CittadiniLogin"         ).collapse('hide');
    $("#MedicoSpecialistaLogin" ).collapse('hide');
    $("#FarmaciaLogin"          ).collapse('hide');
    $("#SSProvincialeLogin"     ).collapse('hide');
    $("#MedicoBaseLogin"        ).collapse('show');
  });
  $("#btn_MedicoSpecialistaLogin").click(function(){
    $("#CittadiniLogin"         ).collapse('hide');
    $("#MedicoBaseLogin"        ).collapse('hide');
    $("#FarmaciaLogin"          ).collapse('hide');
    $("#SSProvincialeLogin"     ).collapse('hide');
    $("#MedicoSpecialistaLogin" ).collapse('show');
  });
  $("#btn_FarmaciaLogin").click(function(){
    $("#CittadiniLogin"         ).collapse('hide');
    $("#MedicoBaseLogin"        ).collapse('hide');
    $("#MedicoSpecialistaLogin" ).collapse('hide');
    $("#SSProvincialeLogin"     ).collapse('hide');
    $("#FarmaciaLogin"          ).collapse('show');
  });
  $("#btn_SSProvincialeLogin").click(function(){
    $("#CittadiniLogin"         ).collapse('hide');
    $("#MedicoBaseLogin"        ).collapse('hide');
    $("#MedicoSpecialistaLogin" ).collapse('hide');
    $("#FarmaciaLogin"          ).collapse('hide');
    $("#SSProvincialeLogin"     ).collapse('show');
  });
});
