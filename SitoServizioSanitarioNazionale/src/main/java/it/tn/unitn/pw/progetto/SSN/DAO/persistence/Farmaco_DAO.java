/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.tn.unitn.pw.progetto.SSN.DAO.persistence;

import java.util.List;

import it.tn.unitn.pw.progetto.SSN.persistence.entities.Farmaco;
import it.tn.unitn.pw.progetto.SSN.DAO.common.DAO;
import it.tn.unitn.pw.progetto.SSN.DAO.common.exception.DAOException;
/**
 *
 * @author Brian
 */
public interface Farmaco_DAO extends DAO<Farmaco, Integer>{
/*============================================================================*/    
/*                         METODO INSERT                                      */
/*============================================================================*/
    public Integer insert(Farmaco farmaco)throws DAOException;
/*============================================================================*/ 
}
