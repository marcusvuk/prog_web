#Python 3.6.7
#Ho settato il campo scelta a True perchè così viene messa di default utilizzata
import re
import random

giorno = ["01","02","03","04","05","06","07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31"]
mese = ["01","02","03","04","05","06","07", "08", "09", "10", "11", "12"]

output = open("popola_foto.sql", "w")
i=0
for codF in open("CodiciFiscali.txt","r"):
    codF=codF.replace('\n','')
    i=i+1
    #blocco per la creazione della data
    selezioneM = mese[random.randint(0,len(mese)-1)]
    if(selezioneM == "02"):
        selezioneG = giorno[random.randint(0,len(giorno)-4)]
    elif(selezioneM == "04" or selezioneM == "06" or selezioneM == "09" or selezioneM == "11"):
        selezioneG = giorno[random.randint(0,len(giorno)-2)]
    else:
        selezioneG = giorno[random.randint(0,len(giorno)-1)]
    data=str(random.randint(2017,2019))+"-"+selezioneM+"-"+selezioneG
    #fine del blocco
    #blocco per il path
    path="'/stylegan-master/foto_database/1m_faces_sample/'"#il nome della foto bisogna metterlo sequenziale
    #fine blocco
    #blocco per il nome
    nome=str(i)
    #fine blocco

    output.write("INSERT INTO foto(id_persona,data,path,nome,scelta) VALUES ('"+codF+"','"+data+"',"+path+",'"+nome+"', 'True');\n")