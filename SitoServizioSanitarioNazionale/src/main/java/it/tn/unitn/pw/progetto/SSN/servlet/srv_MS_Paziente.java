/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.tn.unitn.pw.progetto.SSN.servlet;

import it.tn.unitn.pw.progetto.SSN.DAO.common.exception.DAOException;
import it.tn.unitn.pw.progetto.SSN.DAO.common.exception.DAOFactoryException;
import it.tn.unitn.pw.progetto.SSN.DAO.common.factory.DAOFactory;
import it.tn.unitn.pw.progetto.SSN.DAO.persistence.Foto_DAO;
import it.tn.unitn.pw.progetto.SSN.DAO.persistence.Persona_DAO;
import it.tn.unitn.pw.progetto.SSN.persistence.entities.Persona;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Brian
 */
public class srv_MS_Paziente extends HttpServlet {
//------------------------------------------------------------------------------   
    private Persona_DAO         persona_Dao;
    private Foto_DAO            foto_DAO;
        
    private Persona             paziente;
    
    @Override
    public void init() throws ServletException {
        DAOFactory daoFactory = (DAOFactory) super.getServletContext().getAttribute("daoFactory");
        if (daoFactory == null) {
            throw new ServletException("Impossible to get dao factory for user storage system");
        }
        try {
            persona_Dao         = daoFactory.getDAO(Persona_DAO.class);
            
        } catch (DAOFactoryException ex) {
            throw new ServletException("Impossible to get dao factory for user storage system", ex);
        }
    }

   /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        String contextPath = getServletContext().getContextPath();
        if (!contextPath.endsWith("/")) {
            contextPath += "/";
        }
        
        
        String codF_Paziente = request.getParameter("idPaziente");
        if(codF_Paziente == null || codF_Paziente.equals("")){
            response.sendRedirect("CercaPazienti.jsp");
        }else{
            paziente= getPaziente(request,codF_Paziente);
            if(paziente != null){
                request.getSession().setAttribute("s_Paziente", paziente);
                response.sendRedirect(response.encodeRedirectURL(contextPath + "MedicoSpec/PazienteMS/CartellaClinica.jsp"));
            }else{
                response.sendRedirect("CercaPazienti.jsp");
            }
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
//####################################################################################
//####################################################################################
    
//==============================================================================
//          get Paziente
//==============================================================================
    private Persona getPaziente(HttpServletRequest request,String codF){
        Persona paziente=new Persona();
        try {
            /*Solo persona*/
            paziente = persona_Dao.getByPrimaryKey(codF);
            if( paziente == null){
                return null;
            }
        } catch (DAOException ex) {
            //TODO: log exception
            request.getServletContext().log("Impossibile recuperare la persona.", ex);
            paziente= null;
        }finally{
            return paziente;
        }
        
    }
}
