/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.tn.unitn.pw.progetto.SSN.services;

import java.util.Set;
import javax.ws.rs.core.Application;

/**
 *
 * @author Brian
 */
@javax.ws.rs.ApplicationPath("services")
public class ApplicationConfig extends Application {

    @Override
    public Set<Class<?>> getClasses() {
        Set<Class<?>> resources = new java.util.HashSet<>();
        addRestResourceClasses(resources);
        return resources;
    }

    /**
     * Do not modify addRestResourceClasses() method.
     * It is automatically populated with
     * all resources defined in the project.
     * If required, comment out calling this method in getClasses().
     */
    private void addRestResourceClasses(Set<Class<?>> resources) {
        resources.add(it.tn.unitn.pw.progetto.SSN.services.EsamiPazienteService.class);
        resources.add(it.tn.unitn.pw.progetto.SSN.services.FarmaciService.class);
        resources.add(it.tn.unitn.pw.progetto.SSN.services.MedicoBaseService.class);
        resources.add(it.tn.unitn.pw.progetto.SSN.services.PagamentiService.class);
        resources.add(it.tn.unitn.pw.progetto.SSN.services.PazientiService.class);
        resources.add(it.tn.unitn.pw.progetto.SSN.services.ProvincieService.class);
        resources.add(it.tn.unitn.pw.progetto.SSN.services.RicettaPazienteService.class);
        resources.add(it.tn.unitn.pw.progetto.SSN.services.TipoEsameService.class);
        resources.add(it.tn.unitn.pw.progetto.SSN.services.VisitePazienteService.class);
    }
    
}
