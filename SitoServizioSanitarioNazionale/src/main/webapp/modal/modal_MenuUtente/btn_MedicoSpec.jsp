
<a class="btn btn-light m-1 text-left"
   href="${pageContext.request.contextPath}/MedicoSpec/CercaPazienti.jsp">
    Cerca Paziente
</a>
<c:if test="${s_Paziente ne null}">
<br>
    <h5>
        Ultimo paziente cercato:
        <br>
        <b> 
            <c:out value="${s_Paziente.cognome}"/>
            <c:out value="${s_Paziente.nome}"/>      
        </b>
    </h5>
    <a class="btn btn-light m-1 text-left"
        href="${pageContext.request.contextPath}/MedicoSpec/PazienteMS/CartellaClinica.jsp">
        Dati Anagrafici
    </a>
    <a class="btn btn-light m-1 text-left"
        href="${pageContext.request.contextPath}/MedicoSpec/PazienteMS/listaEsami.jsp">
        Esami / Aggungi Referto
        <c:if test="${s_NumEsamiSenzaReport ne 0}">
            <span class="badge badge-primary badge-pill float-right"><c:out value="${s_NumEsamiSenzaReport}"/></span>
        </c:if>
    </a>
    <a class="btn btn-light m-1 text-left"
        href="${pageContext.request.contextPath}/MedicoSpec/PazienteMS/listaVisite.jsp">
        Visite
        <c:if test="${s_NumVisitaSenzaReport ne 0}">
            <span class="badge badge-primary badge-pill float-right"><c:out value="${s_NumVisitaSenzaReport}"/></span>
        </c:if>
    </a>
    <br>
    <hr>
    <a class="btn btn-light m-1 text-left"
        href="${pageContext.request.contextPath}/MedicoSpec/PazienteMS/erogaVisita.jsp">
        Eroga Vistita
    </a>
    <br>
    <hr>
    <a class="btn btn-light m-1 text-left"
        href="${pageContext.request.contextPath}/MedicoSpec/PazienteMS/listaPagamenti.jsp">
        Visite e Esami non Pagati
    </a>
</c:if>