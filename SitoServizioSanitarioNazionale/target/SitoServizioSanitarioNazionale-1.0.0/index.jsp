
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <title>Servizio Sanitario Nazionale</title>
        <!-- CSS -->  
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" crossorigin="anonymous">
    
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/css/select2.min.css">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/modal/modal_Login/css/floating-labels.css">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/modal/modal_Login/css/forms.css">
        
        
        <link rel="icon" href="${pageContext.request.contextPath}/sito_IMG/icona.ico" type="image/x-icon" />
        <link href="${pageContext.request.contextPath}/sito_CSS/style.css" rel="stylesheet" />
        <link href="${pageContext.request.contextPath}/sito_CSS/style_MenuUtente.css" rel="stylesheet" />
        
    </head>
    <body>
        <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/js/select2.min.js"></script>
        
        
    <!-- JAVASCRIP -->
        <%@include file="/pag_ElementiComunni/pag_MenuPrincipale.jsp" %>
    <!--- Image Slider -->
        <div id="slides" class="carousel slide carousel-fade" data-ride="carousel">
            <ul class="carousel-indicators">
                <li data-target="#slides" data-slide-to="0" class="active"></li>
                <li data-target="#slides" data-slide-to="1"></li>
                <li data-target="#slides" data-slide-to="2"></li>
            </ul>
            <div class="carousel-inner">
                <div class="carousel-item active">
                    <img src="sito_IMG/vecchi_felici.jpg" />
                    <div class="carousel-caption">
                        <h1 class="display-2">
                            Il tuo sevizio sanitario
                        </h1>
                        <h3>
                            Tieniti sempre aggiornato
                        </h3>
                        <button type="button" class="btn btn-primary btn-lg"
                                data-toggle="modal" 
                                data-target="#LoginModal">
                            Accedi al tuo profilo
                        </button>
                    </div>
                </div>
                <div class="carousel-item">
                    <img src="sito_IMG/dottore.jpg" />
                    <div class="carousel-caption">
                        <h1 class="display-2">
                            Stare sempre al meglio
                        </h1>
                        <h3>
                            Prenota la tua visita
                        </h3>
                        <button type="button" class="btn btn-primary btn-lg"
                                data-toggle="modal" 
                                data-target="#LoginModal">
                            Accedi al tuo profilo
                        </button>
                    </div>
                </div>
                <div class="carousel-item">
                    <img src="sito_IMG/kids-playing-with-play-doh.jpg" />
                    <div class="carousel-caption">
                        <h1 class="display-2">
                            Crescita    
                        </h1>
                        <h3>
                            Prenditi cura del tuo futuro
                        </h3>
                        <button type="button" class="btn btn-primary btn-lg"
                                data-toggle="modal" 
                                data-target="#LoginModal">
                            Accedi al tuo profilo
                        </button>
                    </div>
                </div>
            </div>
            <a  class="carousel-control-prev"
                href="#slides"
                role="button"
                data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a  class="carousel-control-next"
                href="#slides"
                role="button"
                data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
    <!--- Welcome Section -->
        <div class="container-fluid padding">
            <div class="row welcome text-center">
                <div class="col-12">
                    <h1 class="display-4">
                        Il tuo sevizio sanitario
                    </h1>
                </div>
                <hr/>
                <div class="col-12">
                    <p class="lead">
                        Promozione della salute, screening, vaccini, contrasto alle
                        dipendenze e le altre iniziative per ridurre epidemie, morti
                        evitabili e malattie croniche.
                    </p>
                </div>
            </div>
        </div>
    <!--- Three Column Section -->
        <div class="container-fluid padding">
            <div class="row text-center padding">
                <div class="col-xs-12 col-sm-6 col-md-4">
                    <i class="fas fa-hospital" aria-hidden="true"></i>
                    <h3>
                        Ospedali
                    </h3>
                    <p>
                        Controllo della salute
                    </p>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-4">
                    <i class="fa fa-medkit" aria-hidden="true"></i>
                    <h3>
                        Medicine
                    </h3>
                    <p>
                        Controlla i tuoi progressi
                    </p>
                </div>
                <div class="col-sm-12 col-md-4">
                    <i class="fa fa-user-md" aria-hidden="true"></i>
                    <h3>
                        Dottori
                    </h3>
                    <p>
                        Resta in contatto
                    </p>
                </div>
            </div>
            <hr class="my-4" />
        </div>
    <!--- Two Column Section -->
        <div class="container-fluid padding">
            <div class="row padding">
                <div class="col-lg-6">
                    <h2>
                        Come ci si iscrive al SSN?
                    </h2>
                    <p>
                        L’iscrizione è obbligatoria per i cittadini italiani e stranieri
                        residenti in Italia e in regola con il permesso di soggiorno. I
                        bambini nati in Italia devono essere iscritti al Servizio Sanitario
                        e acquisiscono il diritto all’assistenza. Per iscriversi occorre
                        recarsi negli uffici dell’Azienda Sanitaria della zona di residenza.
                    </p>
                    <p>
                        L’iscrizione al Servizio sanitario garantisce l’assistenza da parte
                        del medico di medicina generale o del pediatra di libera scelta, che
                        rappresentano il primo riferimento per la salute dei cittadini.
                        L'assistenza sanitaria è garantita a tutti i cittadini attraverso
                        una rete di servizi.
                    </p>
                    <br/>
                </div>
                <div class="col-lg-5 ">
                    <img src="sito_IMG/analisi_cervello.jpg" class="img-fluid rounded" />
                </div>
            </div>
        </div>
        <hr class="my-4" />
    <!--- Three Column Section -->
        <div class="container-fluid padding" id="pag_CHI_SIAMO">
            <div class="row padding">
                <div class="col-lg-6">
                    <h2>
                        Chi Siamo
                    </h2>
                    <p>
                        Conosci il Servizio sanitario nazionale? <br> Qui trovi i principi 
                        che lo ispirano, i Livelli essenziali di assistenza e  quindi 
                        i diritti dei cittadini in tema di salute e assistenza sanitaria, 
                        le funzioni degli enti e delle strutture che lo compongono.
                    </p>
                    <br/>
                </div>
                <div class="col-lg-5 ">
                    <img src="sito_IMG/dottore.jpg" class="img-fluid rounded" />
                </div>
            </div>
        </div>
        <hr class="my-4" />
    <!--- For Column Section -->
        <div class="container-fluid padding" id="pag_COSA_FACCIAMO">
            <div class="row padding">
                <div class="col-lg-6">
                    <h2>
                        Cosa Facciamo
                    </h2>
                    <p>
                        Il Servizio sanitario nazionale (SSN), istituito dalla legge n.833 del 1978, 
                        fornisce l'assistenza sanitaria a tutti i cittadini senza distinzioni di genere, 
                        residenza, età, reddito e lavoro e si basa sui seguenti principi fondamentali:
                    </p>
                    <ul>
                        <li>
                            responsabilità pubblica della tutela della salute;
                        </li>
                        <li>
                            universalità ed equità di accesso ai servizi sanitari;
                        </li>
                        <li>
                            globalità di copertura in base alle necessità assistenziali 
                            di ciascuno, secondo quanto previsto dai Livelli essenziali di assistenza;
                        </li>
                       <li>
                           finanziamento pubblico attraverso la fiscalità generale;
                       </li>
                        <li>
                           globalità di copertura in base alle necessità assistenziali di ciascuno,
                           secondo quanto previsto dai Livelli essenziali di assistenza;
                        </li>
                        <li>
                            "portabilità" dei diritti in tutto il territorio nazionale 
                            e reciprocità di assistenza con le altre regioni.
                        </li>
                    </ul>
                    <br/>
                </div>
                <div class="col-lg-5 ">
                    <img src="sito_IMG/kids-playing-with-play-doh.jpg" class="img-fluid rounded" />
                </div>
            </div>
        </div>
    <!--- Five Column Section -->
        <hr class="my-4" />
         <div class="container-fluid padding" id="pag_MODULI_SERVIZZI">
            <div class="row padding">
                <div class="col-lg-6">
                    <h2>
                        Moduli e Servizi
                    </h2>
                    <p>
                        Alcuni dei servizzi offerti sono:
                    </p>
                    <ul>
                        <li>
                            avere un medico di famiglia o pediatra;
                        </li>
                        <li>
                            ricovero ospedaliero gratuito presso gli ospedali pubblici e convenzionati;
                        </li>
                        <li>
                            assistenza farmaceutica;
                        </li>
                        <li>
                            visite mediche generali in ambulatorio;
                        </li>
                        <li>
                            visite mediche specialistiche;
                        </li>
                        <li>
                            visite mediche a domicilio;
                        </li>
                        <li>
                            vaccinazioni;
                        </li>
                        <li>
                            esami del sangue;
                        </li>
                        <li>
                            radiografie;
                        </li>
                        <li>
                            ecografie;
                        </li>
                        <li>
                            medicine;
                        </li>
                        <li>
                            assistenza riabilitativa e per protesi;
                        </li>
                        <li>
                            altre prestazioni previste nei livelli essenziali di assistenza
                        </li>
                    </ul>
                    <br/>
                </div>
                <div class="col-lg-5 ">
                    <img src="sito_IMG/vecchi_felici.jpg" class="img-fluid rounded" />
                </div>
            </div>
        </div>
        <hr class="my-4" />
    <!--- Connect -->
        <div class="container-fluid padding" id="pag_CONTATTI">
            <div class="row text-center padding">
                <div class="col-12">
                    <h2>
                        Resta aggiornato sulle novità
                    </h2>
                </div>
                <div class="col-12 social padding">
                    <a href="#"><i class="fab fa-facebook"></i></a>
                    <a href="#"><i class="fab fa-twitter"></i></a>
                    <a href="#"><i class="fab fa-google-plus-g"></i></a>
                    <a href="#"><i class="fab fa-youtube"></i></a>
                </div>
            </div>
        </div>
    <!--- Footer -->
        <%@include file="/pag_ElementiComunni/pag_Footer.jsp" %>
    </body>
</html>
