
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%@page import="java.util.List"%>
<%@page import="it.tn.unitn.pw.progetto.SSN.DAO.common.exception.*"%>
<%@page import="it.tn.unitn.pw.progetto.SSN.DAO.common.factories.jdbc.*"%>
<%@page import="it.tn.unitn.pw.progetto.SSN.DAO.common.factory.*"%>
<%@page import="it.tn.unitn.pw.progetto.SSN.DAO.common.persistence.dao.jdbc.*"%>
<%@page import="it.tn.unitn.pw.progetto.SSN.DAO.persistence.*"%>
<%@page import="it.tn.unitn.pw.progetto.SSN.persistence.entities.*"%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
        <title>
            Cookie 
        </title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" crossorigin="anonymous">
    
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/css/select2.min.css">
  
        <!-- Cittadino -->
        <link rel="stylesheet" href="${pageContext.request.contextPath}/modal/modal_Login/css/floating-labels.css">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/modal/modal_Login/css/forms.css">
        <!-- Cittadino -->
        <link rel="icon" href="${pageContext.request.contextPath}/sito_IMG/icona.ico" type="image/x-icon" />
        <link href="${pageContext.request.contextPath}/sito_CSS/style.css" rel="stylesheet" />
        <link href="${pageContext.request.contextPath}/sito_CSS/style_MenuUtente.css" rel="stylesheet" />
    </head>
    <body onload="showEsitoModale()">
        <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/js/select2.min.js"></script>
        <%-- <script src="modal_Login/js/js_Login.js"></script> --%>
    <!-- Navigation -->
        <%@include file="../pag_ElementiComunni/pag_MenuPrincipale.jsp" %>
        
        <%@include file="../modal/modal_CambiaFoto/modal_CambiaFoto.jsp" %> 
        <script src="${pageContext.request.contextPath}/pag_Cittadino/js/js_showoEsitoModale.js"></script>
        <div class="container">
            <div>
                <div class="row justify-content-center">
                    <div class="col-5">
                        <h2>
                            Cookie                              
                        </h2>
                    </div>
                </div>
                <div class="container-fluid padding" style="min-height:300px;">
                    <div class="row padding">
                        <div class="col-lg-6">
                            <p>
                                I cookie sono piccoli file di testo che i siti visitati inviano al 
                                terminale dell'utente, dove vengono memorizzati, per poi essere 
                                ritrasmessi agli stessi siti alla visita successiva.<br>
                            </p>
                            <br/>
                            <h4 class="text-center">
                                Il sito del Sistema Sanitario Nazionale utilizza:
                            </h4>
                            <p>
                                cookie tecnici di sessione il cui utilizzo non è strumentale alla 
                                raccolta di dati personali identificativi dell'utente, essendo 
                                limitato alla sola trasmissione di dati identificativi di sessione 
                                nella forma di numeri generati automaticamente dal server.<br>
                                I cookie di sessione non sono memorizzati in modo persistente 
                                sul dispositivo dell'utente e  vengono cancellati automaticamente 
                                alla chiusura del browser.
                            </p>
                            <br>
                        </div>
                        <div class="col-lg-5 ">
                            <picture>
                                <source media="(min-width: 992px)" srcset="sito_IMG/S_cookie.jpg">
                                <img src="sito_IMG/L_cookie.jpg" style="width:100%" class="img-fluid rounded img_SeguiArticolo" />
                            </picture>
                        </div>
                    </div>
                </div>
                <hr class="my-4" />
            </div>
        </div>
        <%@include file="../pag_ElementiComunni/pag_Footer.jsp" %>
    </body>
</html>
