
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%@page import="java.util.List"%>
<%@page import="it.tn.unitn.pw.progetto.SSN.DAO.common.exception.*"%>
<%@page import="it.tn.unitn.pw.progetto.SSN.DAO.common.factories.jdbc.*"%>
<%@page import="it.tn.unitn.pw.progetto.SSN.DAO.common.factory.*"%>
<%@page import="it.tn.unitn.pw.progetto.SSN.DAO.common.persistence.dao.jdbc.*"%>
<%@page import="it.tn.unitn.pw.progetto.SSN.DAO.persistence.*"%>
<%@page import="it.tn.unitn.pw.progetto.SSN.persistence.entities.*"%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
        <title>
            Scrivi E-mail
        </title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" crossorigin="anonymous">
    
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/css/select2.min.css">
  
        <!-- Cittadino -->
        <link rel="stylesheet" href="${pageContext.request.contextPath}/modal/modal_Login/css/floating-labels.css">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/modal/modal_Login/css/forms.css">
        <!-- Cittadino -->
        <link rel="icon" href="${pageContext.request.contextPath}/sito_IMG/icona.ico" type="image/x-icon" />
        <link href="${pageContext.request.contextPath}/sito_CSS/style.css" rel="stylesheet" />
        <link href="${pageContext.request.contextPath}/sito_CSS/style_MenuUtente.css" rel="stylesheet" />
        
        <!-- Tabelle -->
        <link rel="stylesheet" href="https://cdn.datatables.net/1.10.18/css/dataTables.bootstrap4.min.css">
    </head>
    <body >
        <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/js/select2.min.js"></script>
      <!-- Tabelle -->   
        <script src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script>
        <script src="https://cdn.datatables.net/1.10.18/js/dataTables.bootstrap4.min.js"></script>
    <!-- JAVASCRIP -->
        <%@include file="/pag_ElementiComunni/pag_MenuPrincipale.jsp" %>
        
        <div class="container">
            <div style="min-height:300px;">
                <div class="row">
                    <div class="col-12">
                        <h2>
                            Richiedi password.
                        </h2>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <p>
                            Una volta ricevuta la richiesta le invieremo alla email con cui si è registrato
                            la nuova password.
                        </p>
                    </div>
                </div>          
                <form action="RichiediPassword.handler" method="POST">
                    
                    <div class="form-group">
                        <label><b>Inserisci l'e-mail del profilo:</b></label>
                        <input id="email" 
                               name="email"
                               type="email"  
                               class="form-control" 
                               required="required" />
                    </div>
                    <div class="form-group">
                        <label><b>Inserisci il codice fiscale:</b> Se sei una farmacia la pIVA.</label>
                        <input id="codF_pIVA" 
                               name="codF_pIVA"
                               type="text"  
                               class="form-control" 
                               required="required" />
                    </div>
                    <div class="form-group">
                        <input  type="submit"
                                class="btn btn-primary pull-right"
                                value="Richiedi nuova password"/> 
                    </div>   
                </form>
            </div>
        </div>
        <%@include file="/pag_ElementiComunni/pag_Footer.jsp" %>
    </body>
</html>
