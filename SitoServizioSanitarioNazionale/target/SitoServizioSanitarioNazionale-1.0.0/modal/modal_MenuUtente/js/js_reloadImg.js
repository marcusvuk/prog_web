/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


function imgError(image) {
    if (!image.hasOwnProperty('retryCount')){
      image.retryCount = 0;
    }
    if (image.retryCount < 5){
      setTimeout(function (){
          image.src += '?' + +new Date;
          image.retryCount += 1;
     }, 1000);
    }else if (image.retryCount == 5){
        image.src="/SitoServizioSanitarioNazionale/sito_IMG/blank-profile-picture.png";
    }
  }