/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.tn.unitn.pw.progetto.SSN.persistence.entities;

import java.sql.Date;
/**
 *
 * @author Brian
 */
public class Ricetta {
/*============================================================================*/
/*                                       ATTRIBUTI                            */
/*============================================================================*/
    private Integer     id;
    private Date        scadenza;
    private Date        dataPrescrizione;
    private Date        dataEvalsa;
    private Integer     quantita;
    private Double      prezzoFarmacia;
    private Integer     id_CartellaClinica;
    private Integer     id_Farmaco;
    private String      id_Farmacia;
    private Boolean     vistoPaziente;
/*============================================================================*/
    
/*============================================================================*/    
/*                                      METODI COSTRUTTORI                    */
/*============================================================================*/
    public Ricetta() {
        id                      = null;
        scadenza                = null;
        dataPrescrizione        = null;
        dataEvalsa              = null;
        quantita                = null;
        prezzoFarmacia          = null;
        id_CartellaClinica      = null;
        id_Farmaco              = null;
        id_Farmacia             = null;
        vistoPaziente           = null;
    }
/*============================================================================*/

/*============================================================================*/    
/*                                      METODI GETTER E SETTER                */
/*============================================================================*/
    /*--------------------------------------*/    
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
    /*--------------------------------------*/
    public Date getScadenza() {
        return scadenza;
    }

    public void setScadenza(Date scadenza) {
        this.scadenza = scadenza;
    }
    /*--------------------------------------*/
    public Date getDataPrescrizione() {
        return dataPrescrizione;
    }

    public void setDataPrescrizione(Date dataPrescrizione) {
        this.dataPrescrizione = dataPrescrizione;
    }
    /*--------------------------------------*/
    public Integer getQuantita() {
        return quantita;
    }

    public void setQuantita(Integer quantita) {
        this.quantita = quantita;
    }
    /*--------------------------------------*/
    public Double getPrezzoFarmacia() {
        return prezzoFarmacia;
    }

    public void setPrezzoFarmacia(Double prezzoFarmacia) {
        this.prezzoFarmacia = prezzoFarmacia;
    }
    /*--------------------------------------*/
    public Integer getId_CartellaClinica() {
        return id_CartellaClinica;
    }

    public void setId_CartellaClinica(Integer id_CartellaClinica) {
        this.id_CartellaClinica = id_CartellaClinica;
    }
    /*--------------------------------------*/
    public Integer getId_Farmaco() {
        return id_Farmaco;
    }

    public void setId_Farmaco(Integer id_Farmaco) {
        this.id_Farmaco = id_Farmaco;
    }
    /*--------------------------------------*/
    public String getId_Farmacia() {
        return id_Farmacia;
    }

    public void setId_Farmacia(String id_Farmacia) {
        this.id_Farmacia = id_Farmacia;
    }
    /*--------------------------------------*/
    public Boolean getVistoPaziente() {
        return vistoPaziente;
    }

    public void setVistoPaziente(Boolean vistoPaziente) {
        this.vistoPaziente = vistoPaziente;
    }
    /*--------------------------------------*/
    public Date getDataEvalsa() {
        return dataEvalsa;
    }

    public void setDataEvalsa(Date dataEvalsa) {
        this.dataEvalsa = dataEvalsa;
    }
    /*--------------------------------------*/
/*============================================================================*/
}
