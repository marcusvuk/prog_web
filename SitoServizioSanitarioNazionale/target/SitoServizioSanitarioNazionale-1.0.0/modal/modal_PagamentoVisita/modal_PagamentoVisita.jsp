
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div name="PagamentoVisitaModal" id="PagamentoVisitaModal"  class="modal fade" >
    <div class="modal-dialog modal-login modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Effetua pagamento</h4>
                <button type="button"
                        class="close"
                        data-dismiss="modal"
                        aria-hidden="true">
                    &times;
                </button>    
            </div> 

            <form action="Visita.handler" method="POST">
                <input  type="hidden" id="TipoInserimetoVisita" name="TipoInserimetoVisita" value="EffetuaPagamento">

            <div class="modal-body">
                <div class="container-fluid justify-content-center "> 
                    <div class="container">
                        <div class="clearfix">
                            <h3>
                                <b> Visita:</b>
                                <br>
                                <h5>"<c:out value="${c_b_s_Visita.prescrizione}"/>"</h5>
                            </h3>
                        </div>
                    </div>
                    <br>
                    <hr>
                    <div class="container">
                        <h5>
                            <b>
                            <div class="form-check">
                                <input  type="checkbox" 
                                   id="pagatoVisite"
                                   name="pagatoVisite"/>
                               <label class="form-check-label" for="pagatoVisite">Pagato</label> 
                            </div>
                            </b>
                        </h5>
                    </div>
                </div>
            </div>
            <div class=" container-fluid modal-footer">
                <input  type="submit"
                        class="btn btn-primary pull-right"
                        accept=""value="Conferma"/>
            </div>
            </form>
        </div>
    </div>
</div>