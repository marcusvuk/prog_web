/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.tn.unitn.pw.progetto.SSN.filter;

import it.tn.unitn.pw.progetto.SSN.persistence.entities.Farmacia;
import it.tn.unitn.pw.progetto.SSN.persistence.entities.MedicoBase;
import it.tn.unitn.pw.progetto.SSN.persistence.entities.MedicoSpec;
import it.tn.unitn.pw.progetto.SSN.persistence.entities.Persona;
import it.tn.unitn.pw.progetto.SSN.persistence.entities.SSProvinciale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.logging.Logger;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.Cookie;

/**
 *
 * @author Brian
 */
public class AuthenticationCookie implements Filter {
    
    private static final boolean debug = true;

//------------------------------------------------------------------------------
    private static  HashMap<String, Object> authenticatedUsers=null;

    static public void myInit(HashMap<String, Object> authenticatedUsers1){
        if(authenticatedUsers == null){
            authenticatedUsers=authenticatedUsers1;
        }
    }
    private Object retrieveUsername(String jSessionId) {
        return authenticatedUsers.get(jSessionId);
    }
//------------------------------------------------------------------------------
    
    
    // The filter configuration object we are associated with.  If
    // this value is null, this filter instance is not currently
    // configured. 
    private FilterConfig filterConfig = null;
    
    public AuthenticationCookie() {
    }    

//------------------------------------------------------------------------------------------QUI IL FILTRO 
    private void doBeforeProcessing(ServletRequest request, ServletResponse response)
            throws IOException, ServletException {
        if (debug) {
            log("AuthenticationCookie:DoBeforeProcessing");
        }
        if(     ( ((HttpServletRequest) request).getSession().getAttribute("p_ssProvinciale") == null ) &&
                ( ((HttpServletRequest) request).getSession().getAttribute("f_Farmacia") == null ) &&
                ( ((HttpServletRequest) request).getSession().getAttribute("b_Persona") == null ) &&
                ( ((HttpServletRequest) request).getSession().getAttribute("s_Persona") == null ) &&
                ( ((HttpServletRequest) request).getSession().getAttribute("c_Persona") == null ) 
            ){
            Cookie[] allCookies  = ((HttpServletRequest) request).getCookies();
            if(allCookies != null){
                for(int i=0; i<allCookies.length; i++){
                    String nomeCookies= allCookies[i].getName();
                    if(nomeCookies.equals("jsessionid")){
                        String valueCookies = allCookies[i].getValue();
                        Object utente = retrieveUsername(valueCookies);

                        if(utente instanceof SSProvinciale){
                            ((HttpServletRequest) request).getSession().setAttribute("p_ssProvinciale", utente);
                        }else if(utente instanceof Farmacia){
                            ((HttpServletRequest) request).getSession().setAttribute("f_Farmacia", utente);
                        }else if(utente instanceof MedicoBase){
                            ((HttpServletRequest) request).getSession().setAttribute("b_Persona", utente);
                        }else if(utente instanceof MedicoSpec){
                            ((HttpServletRequest) request).getSession().setAttribute("s_Persona", utente);
                        }else if(utente instanceof Persona){
                            ((HttpServletRequest) request).getSession().setAttribute("c_Persona", utente);
                        }
                    }
                }
            }
        }
    }    
//------------------------------------------------------------------------------------------^ QUI IL FILTRO
    private void doAfterProcessing(ServletRequest request, ServletResponse response)
            throws IOException, ServletException {
        if (debug) {
            log("AuthenticationCookie:DoAfterProcessing");
        }
    }
    /**
     *
     * @param request The servlet request we are processing
     * @param response The servlet response we are creating
     * @param chain The filter chain we are processing
     *
     * @exception IOException if an input/output error occurs
     * @exception ServletException if a servlet error occurs
     */
    public void doFilter(ServletRequest request, ServletResponse response,
            FilterChain chain)
            throws IOException, ServletException {
        
        if (debug) {
            log("AuthenticationCookie:doFilter()");
        }
        
        doBeforeProcessing(request, response);
        
        Throwable problem = null;
        try {
            chain.doFilter(request, response);
        } catch (IOException | ServletException | RuntimeException ex) {
            // If an exception is thrown somewhere down the filter chain,
            // we still want to execute our after processing, and then
            // rethrow the problem after that.
            problem = ex;
            request.getServletContext().log("Impossible to propagate to the next filter", ex);
        }
        
        doAfterProcessing(request, response);

        // If there was a problem, we want to rethrow it if it is
        // a known type, otherwise log it.
        if (problem != null) {
            if (problem instanceof ServletException) {
                throw (ServletException) problem;
            }
            if (problem instanceof IOException) {
                throw (IOException) problem;
            }
            sendProcessingError(problem, response);
        }
    }

    /**
     * Return the filter configuration object for this filter.
     */
    public FilterConfig getFilterConfig() {
        return (this.filterConfig);
    }

    /**
     * Set the filter configuration object for this filter.
     *
     * @param filterConfig The filter configuration object
     */
    public void setFilterConfig(FilterConfig filterConfig) {
        this.filterConfig = filterConfig;
    }

    /**
     * Destroy method for this filter
     */
    public void destroy() {        
    }

    /**
     * Init method for this filter
     */
    public void init(FilterConfig filterConfig) {        
        this.filterConfig = filterConfig;
        if (filterConfig != null) {
            if (debug) {                
                log("AuthenticationCookie:Initializing filter");
            }
        }
    }

    /**
     * Return a String representation of this object.
     */
    @Override
    public String toString() {
        if (filterConfig == null) {
            return ("AuthenticationCookie()");
        }
        StringBuffer sb = new StringBuffer("AuthenticationCookie(");
        sb.append(filterConfig);
        sb.append(")");
        return (sb.toString());
    }
//-------------------------------------------------------------------------------------------EROR PAGE
    private void sendProcessingError(Throwable t, ServletResponse response) {
        String stackTrace = getStackTrace(t);        
        
        if (stackTrace != null && !stackTrace.equals("")) {
            try {
                response.setContentType("text/html");
                PrintStream ps = new PrintStream(response.getOutputStream());
                PrintWriter pw = new PrintWriter(ps);                
                pw.print("<html>\n<head>\n<title>Error</title>\n</head>\n<body>\n"); //NOI18N

                // PENDING! Localize this for next official release
                pw.print("<h1>The resource did not process correctly</h1>\n<pre>\n");                
                pw.print(stackTrace);                
                pw.print("</pre></body>\n</html>"); //NOI18N
                pw.close();
                ps.close();
                response.getOutputStream().close();
            } catch (Exception ex) {
            }
        } else {
            try {
                PrintStream ps = new PrintStream(response.getOutputStream());
                t.printStackTrace(ps);
                ps.close();
                response.getOutputStream().close();
            } catch (Exception ex) {
            }
        }
    }
    
    public static String getStackTrace(Throwable t) {
        String stackTrace = null;
        try {
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            t.printStackTrace(pw);
            pw.close();
            sw.close();
            stackTrace = sw.getBuffer().toString();
        } catch (Exception ex) {
        }
        return stackTrace;
    }
    
    public void log(String msg) {
        if (filterConfig != null) {
            filterConfig.getServletContext().log(msg);
        } else {
            Logger.getLogger(msg);
        }   
    }
}
