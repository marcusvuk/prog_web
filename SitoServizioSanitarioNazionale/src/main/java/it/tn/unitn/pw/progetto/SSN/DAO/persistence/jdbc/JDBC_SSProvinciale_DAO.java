/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.tn.unitn.pw.progetto.SSN.DAO.persistence.jdbc;

import java.sql.Date;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import it.tn.unitn.pw.progetto.SSN.DAO.common.persistence.dao.jdbc.JDBCDAO;
import it.tn.unitn.pw.progetto.SSN.DAO.common.exception.*;
import it.tn.unitn.pw.progetto.SSN.DAO.persistence.*;
import it.tn.unitn.pw.progetto.SSN.persistence.entities.*;
/**
 *
 * @author Brian
 */
public class JDBC_SSProvinciale_DAO extends JDBCDAO<SSProvinciale, Integer> implements SSProvinciale_DAO {
/*============================================================================*/    
/*                                      METODI COSTRUTTORI                    */
/*============================================================================*/
    public JDBC_SSProvinciale_DAO(Connection con) {
        super(con);
    }
/*============================================================================*/
    
/*============================================================================*/    
/*                      METODI GETTER DAO IMPLEMENTATION                      */
/*============================================================================*/
    @Override
    public Long getCount() throws DAOException {
        try (Statement stmt = CON.createStatement()) {
            ResultSet counter = stmt.executeQuery( 
                     "  SELECT COUNT(*) "               +
                     "  FROM SSProvinciale "     );
            if (counter.next()) {
                return counter.getLong(1);
            }
        } catch (SQLException ex) {
            throw new DAOException( " Immpossibile contare gli elementi di SSProvinciale  " , ex);
        }
        return 0L;
    }
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    @Override
    public SSProvinciale getByPrimaryKey(Integer primaryKey) throws DAOException {
        if (primaryKey == null) {
            throw new DAOException( " primaryKey è null " );
        }
        try (PreparedStatement stm = CON.prepareStatement(
                                     "  SELECT *  "            +
                                     "  FROM SSProvinciale  "  +
                                     "  WHERE id = ? "      )) {
            // imposto i valori
            stm.setInt(1, primaryKey);
            
            try (ResultSet rs = stm.executeQuery()) {
                rs.next();
                SSProvinciale SSP = new SSProvinciale();
                SSP.setId               (rs.getInt      ( "id" ));
                SSP.setNome             (rs.getString   ( "nome" ));
                SSP.setPassword         (rs.getString   ( "password" ));
                SSP.setSalt             (rs.getString   ( "salt" ));
                        
                return SSP;
            }
        } catch (SQLException ex) {
            throw new DAOException( " Impossibile perendere la SSProvinciale indicato dalla primary key " , ex);
        }
    }
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    @Override
    public List<SSProvinciale> getAll() throws DAOException {
        List<SSProvinciale> listaSSProvinciale = new ArrayList<>();

        try (Statement stm = CON.createStatement()) {
            try (ResultSet rs = stm.executeQuery(
                                 " SELECT *  "                 +
                                 " FROM SSProvinciale  "       )){
                
                while (rs.next()) {
                    SSProvinciale SSP = new SSProvinciale();
                    
                    SSP.setId               (rs.getInt      ( "id" ));
                    SSP.setNome             (rs.getString   ( "nome" ));
                    SSP.setPassword         (rs.getString   ( "password" ));
                    SSP.setSalt             (rs.getString   ( "salt" ));
                    
                    listaSSProvinciale.add(SSP);
                }
            }
        } catch (SQLException ex) {
            throw new DAOException( " Imposibile estrarre tutta la lista di CartellaClinicaPerMB  " , ex);
        }

        return listaSSProvinciale;
    }
/*============================================================================*/
   
/*============================================================================*/    
/*                      METODI QUERY                                          */
/*============================================================================*/
//------------------------------------------------------------------------------
//                      INSERT
//------------------------------------------------------------------------------
    @Override
    public Integer insert(SSProvinciale ssp) throws DAOException {
        if (ssp == null) {
            throw new DAOException( " La ssp è null " );
        }
        try (PreparedStatement ps = CON.prepareStatement(
                     " INSERT INTO SSProvinciale "  +
                                                 " ( id "        +    "  ,  "   +
                                                 "  nome "       +    "  ,  "    +
                                                 "  password "   +    "  ,  "    +
                                                 "  salt "       +    "  )  "    +  
                     " VALUES (?,?,?,?) " , Statement.RETURN_GENERATED_KEYS)) {
            
            // imposto i valori
            ps.setInt       (1, ssp.getId());
            ps.setString    (2, ssp.getNome());
            ps.setString    (3, ssp.getPassword());
            ps.setString    (4, ssp.getSalt());
      
            
            ps.executeUpdate();
            
            ResultSet rs = ps.getGeneratedKeys();
            if (rs.next()) {
                ssp.setId(rs.getInt(1));
            }
            
            return ssp.getId();
        } catch (SQLException ex) {
            throw new DAOException( " Impossibile aggiungere un nuovo record di SSProvinciale " , ex);
        }
    }
//------------------------------------------------------------------------------
//                      UPDATE
//------------------------------------------------------------------------------
    @Override
    public void update(SSProvinciale ssp) throws DAOException {
        if (ssp == null) {
            throw new DAOException( " La cartellaClinica è null " );
        }
        try (PreparedStatement ps = CON.prepareStatement(
                     " UPDATE SSProvinciale "  +
                     " SET          id = ?  "               +     "  ,  "    +
                                 "  nome = ?  "             +     "  ,  "    +
                                 "  password = ?  "         +     "  ,  "    +
                                 "  salt = ?  "             +     
                     " WHERE id = ?  " , Statement.RETURN_GENERATED_KEYS)) {
            
            // imposto i valori
            ps.setInt       (1, ssp.getId());
            
            ps.setString    (2, ssp.getNome());
            ps.setString    (3, ssp.getPassword());
            ps.setString    (4, ssp.getSalt());
           
            //WHERE
            ps.setInt       (5, ssp.getId());
            
            ps.executeUpdate();
            
            
        } catch (SQLException ex) {
            throw new DAOException( " Impossibile aggiornare il record di SSProvinciale " , ex);
        }
    }
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    @Override
    public void update_Password(Integer id, String Password) throws DAOException {
        if (id == null || Password == null ) {
            throw new DAOException( " id o Password è null " );
        }
        try (PreparedStatement ps = CON.prepareStatement(
                     " UPDATE SSProvinciale "  +
                     " SET  password = ?  "         +  
                     " WHERE id = ?  " , Statement.RETURN_GENERATED_KEYS)) {
            
            // imposto i valori
            ps.setString(1, Password);
            //WHERE
            ps.setInt(2, id);
            
            ps.executeUpdate();
            
        } catch (SQLException ex) {
            throw new DAOException( " Impossibile aggiornare il record di SSProvinciale " , ex);
        }
    }
//------------------------------------------------------------------------------
//                      SELECT
//------------------------------------------------------------------------------
    @Override
    public SSProvinciale getBySSProvincialeNomeAndPassword(String nome, String password) throws DAOException {
        if (nome == null || password == null) {
            throw new DAOException( " nome o password è null " );
        }
        try (PreparedStatement stm = CON.prepareStatement(
                                     "  SELECT *  "                +
                                     "  FROM SSProvinciale  "      +
                                     "  WHERE nome = ? AND  "      +    
                                           "  password = ?  "       )) {
            // imposto i valori 
            stm.setString(1, nome);
            stm.setString(2, password);
            
            try (ResultSet rs = stm.executeQuery()) {
                rs.next();
                SSProvinciale ssp = new SSProvinciale();
                ssp.setId               (rs.getInt      ( "id" ));
                ssp.setNome             (rs.getString   ( "nome" ));
                ssp.setPassword         (rs.getString   ( "password" ));
                ssp.setSalt             (rs.getString   ( "salt" ));
                        
                return ssp;
            }
        } catch (SQLException ex) {
            throw new DAOException( " Impossibile perendere la SSProvinciale indicato dalla nome e password " , ex);
        }
    }
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    @Override
    public String getSaltSSProvincialeByNome(String nome) throws DAOException {
        if (nome == null ) {
            throw new DAOException( " nome è null " );
        }
        try (PreparedStatement stm = CON.prepareStatement(
                                     "  SELECT salt  "             +
                                     "  FROM SSProvinciale  "      +
                                     "  WHERE nome = ?  "          )) {
            // imposto i valori 
            stm.setString(1, nome);
            
            try (ResultSet rs = stm.executeQuery()) {
                rs.next();
                String salt ;
                salt= rs.getString( "salt" );
                        
                return salt;
            }
        } catch (SQLException ex) {
            throw new DAOException( " Impossibile perendere la SSProvinciale indicato dalla nome " , ex);
        }
    }
/*============================================================================*/
}
