 /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.tn.unitn.pw.progetto.SSN.persistence.entities;

import java.sql.Date;
/**
 *
 * @author Brian
 */
public class CartellaClinicaPerMB {
/*============================================================================*/
/*                                       ATTRIBUTI                            */
/*============================================================================*/
    private Integer     id;
    private java.sql.Date        dataInizio;
    private java.sql.Date        dataFine;
    private String      id_MedicoBase;
    private String      id_Persona;
    private Boolean     vistoMedicoBase;
/*============================================================================*/

/*============================================================================*/    
/*                                      METODI COSTRUTTORI                    */
/*============================================================================*/
    public CartellaClinicaPerMB() {
        id                  = null;
        dataInizio          = null;
        dataFine            = null;
        id_MedicoBase       = null;
        id_Persona          = null;
        vistoMedicoBase     = null;
    }
/*============================================================================*/

/*============================================================================*/    
/*                                      METODI GETTER E SETTER                */
/*============================================================================*/
    /*--------------------------------------*/    
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
    /*--------------------------------------*/
    public java.sql.Date getDataInizio() {
        return dataInizio;
    }

    public void setDataInizio(java.sql.Date dataInizio) {
        this.dataInizio = dataInizio;
    }
    /*--------------------------------------*/
    public java.sql.Date getDataFine() {
        return dataFine;
    }

    public void setDataFine(java.sql.Date dataFine) {
        this.dataFine = dataFine;
    }
    /*--------------------------------------*/
    public String getId_MedicoBase() {
        return id_MedicoBase;
    }

    public void setId_MedicoBase(String id_MedicoBase) {
        this.id_MedicoBase = id_MedicoBase;
    }
    /*--------------------------------------*/
    public String getId_Persona() {
        return id_Persona;
    }
    public void setId_Persona(String id_Persona) {
        this.id_Persona = id_Persona;
    }   
    /*--------------------------------------*/
    public Boolean getVistoMedicoBase() {
        return vistoMedicoBase;
    }

    public void setVistoMedicoBase(Boolean vistoMedicoBase) {
        this.vistoMedicoBase = vistoMedicoBase;
    }
    /*--------------------------------------*/
/*============================================================================*/
}
