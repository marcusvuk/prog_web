$(function () {
    $("#idPaziente").select2({
        placeholder: "Scegli paziente",
        allowClear: true,
        ajax: {
            url: function (request) {
                return "/SitoServizioSanitarioNazionale/services/pazienti/MedicoBase_S2/"+$('#MyIdMedicoBase').attr('value')+"/"+ request.term;
            },
            dataType: "json"
        }
    });
    $("#idPaziente").val(null).trigger("change");
});
