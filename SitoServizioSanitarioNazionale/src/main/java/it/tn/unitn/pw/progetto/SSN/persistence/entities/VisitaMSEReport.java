/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.tn.unitn.pw.progetto.SSN.persistence.entities;

/**
 *
 * @author Brian
 */
public class VisitaMSEReport extends Visita{
/*============================================================================*/
/*                                       ATTRIBUTI                            */
/*============================================================================*/
    private Double      tichet;
    private Boolean     pagato;
    private String      report;
    private String      id_MedicoSpec;
    private Boolean     vistoReportMedicoBase;
/*============================================================================*/
    
/*============================================================================*/    
/*                                      METODI COSTRUTTORI                    */
/*============================================================================*/
    public VisitaMSEReport() {
        tichet                  = null;
        pagato                  = null;
        report                  = null;
        id_MedicoSpec           = null;
        vistoReportMedicoBase   = null;
    }
/*============================================================================*/

/*============================================================================*/    
/*                                      METODI GETTER E SETTER                */
/*============================================================================*/
    /*--------------------------------------*/   
    public Double getTichet() {
        return tichet;
    }

    public void setTichet(Double tichet) {
        this.tichet = tichet;
    }
    /*--------------------------------------*/
    public Boolean getPagato() {
        return pagato;
    }

    public void setPagato(Boolean pagato) {
        this.pagato = pagato;
    }
    /*--------------------------------------*/
    public String getReport() {
        return report;
    }

    public void setReport(String report) {
        this.report = report;
    }
    /*--------------------------------------*/
    public String getId_MedicoSpec() {
        return id_MedicoSpec;
    }

    public void setId_MedicoSpec(String id_MedicoSpec) {
        this.id_MedicoSpec = id_MedicoSpec;
    }
    /*--------------------------------------*/
    public Boolean getVistoReportMedicoBase() {
        return vistoReportMedicoBase;
    }

    public void setVistoReportMedicoBase(Boolean vistoReportMedicoBase) {
        this.vistoReportMedicoBase = vistoReportMedicoBase;
    }
    /*--------------------------------------*/
/*============================================================================*/ 
}
