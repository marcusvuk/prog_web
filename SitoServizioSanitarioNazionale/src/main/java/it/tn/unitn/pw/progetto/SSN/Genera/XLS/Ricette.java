/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.tn.unitn.pw.progetto.SSN.Genera.XLS;

import java.sql.Date;//sql

/**
 *
 * @author mw
 */
public class Ricette {
    int ricetta;
    Date data;
    String farmaco;
    String farmacia;
    String mb;
    String paziente;
    double ticket;

    public Ricette(int ricetta, Date data, String farmaco, String farmacia, String mb, String paziente, double ticket ){
        this.ricetta = ricetta;
        this.data = data;
        this.farmaco = farmaco;
        this.farmacia = farmacia;
        this.mb = mb;
        this.paziente = paziente;
        this.ticket = ticket;
    }
}
