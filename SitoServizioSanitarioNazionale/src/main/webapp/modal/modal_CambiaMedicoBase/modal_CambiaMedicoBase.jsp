
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<script src="${pageContext.request.contextPath}/modal/modal_CambiaMedicoBase/js/js_MedicoBase.js"></script>
<div name="CambiaMedicoBase" id="CambiaMedicoBase"  class="modal fade" >
    <div class="modal-dialog modal-login modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Cambia Medico di Base</h4>
                <button type="button"
                        class="close"
                        data-dismiss="modal"
                        aria-hidden="true">
                    &times;
                </button>    
            </div> 
            
            <c:choose>
                <c:when test="${(c_Persona ne null)}">
            <form action="${pageContext.request.contextPath}/Cittadino/Cittadino.handler" method="POST" enctype="multipart/form-data">
            
                </c:when>
                <c:when test="${(b_Persona ne null)}">
            <form action="${pageContext.request.contextPath}/MedicoBase/MedicoBase.handler" method="POST" enctype="multipart/form-data">
            
                </c:when>
                <c:when test="${(s_Persona ne null)}">
            <form action="${pageContext.request.contextPath}/MedicoSpec/MedicoSpec.handler" method="POST" enctype="multipart/form-data">
            
                </c:when>
            </c:choose>
            
            
            <div class="modal-body">
               <div class="container-fluid justify-content-center ">  
                    <div class="container">
                        <input  type="hidden" id="typeCambia" name="typeCambia" value="MedicoBase">
                        <div class="clearfix">
                            <label>Password: </label>
                        </div>
                        <input  id="password" 
                                name="password" 
                                type="password"
                                class="form-control"
                                required="required"/>
                    </div>
                </div>
                <hr>
                <div class="container-fluid justify-content-center ">  
                    <div class="container">
                        <div class="clearfix">
                            <label>Nuovo medico di base:</label>
                        </div>
                        <c:choose>
                            <c:when test="${(c_Persona ne null)}">
                                <input  type="hidden" 
                                        id="MyCodF" 
                                        name="MyCodF" 
                                        value="${c_Persona.codF}">
                            </c:when>
                            <c:when test="${(b_Persona ne null)}">
                                <input  type="hidden" 
                                        id="MyCodF" 
                                        name="MyCodF" 
                                        value="${b_Persona.codF}">
                            </c:when>
                            <c:when test="${(s_Persona ne null)}">
                                <input  type="hidden" 
                                        id="MyCodF" 
                                        name="MyCodF" 
                                        value="${s_Persona.codF}">
                            </c:when>
                        </c:choose>
                        <select id="autocomplete-medicoBase" 
                                name="autocomplete-medicoBase" 
                                style="width: 100%"
                                class="form-control  select2-allow-clear"
                                required="required">
                        </select>
                        
                    </div>
                </div>
            </div>
            <div class=" container-fluid modal-footer">
                <input  type="submit"
                        class="btn btn-primary pull-right"
                        accept=""value="Conferma"/>
            </div>
            </form>
        </div>
    </div>
</div>