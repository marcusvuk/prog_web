$(function () {
    $("#EsamiVisiteTable").dataTable( {
        ajax: {
            url: '/SitoServizioSanitarioNazionale/services/Pagamenti/'+
                    $('#MyIdPaziente').attr('value')+
                    '/PerMedicoSpec/'+$('#MyIdMedicoSpec').attr('value'),
            dataSrc: 'results'
        },
        columns: [
            { results: 0,//results: Corrispnde a data
              targets: 0, 
              render: function ( data, type, row, meta ) {
                    if(type === 'display'){
                        if(data === 'true'){
                            //Deve effetuare il pagamento
                            data ='<i class="fas fa-check-circle"  " style="font-size:36px"></i>';
                        }else{
                            //è stata effetuata il pagamento
                            data ='<i class="fas fa-cogs text-primary" style="font-size:36px"></i>';
                        }
                    }
                    return data;
                }
            },
            { results: 1 },
            { results: 2 },
            { results: 3 },
            { results: 4 },
            { results: 5 ,
              targets: -2, 
              render: function ( data, type, row, meta ) {
                    if(type === 'display'){
                        if(data === 'true'){
                            //Vechio utente
                            data ='Pagato';
                        }else{
                            //Nuovo utente
                            data ='<b class="text-danger">Non pagato</b>';
                        }
                    }
                    return data;
                }
            
            },
            { results: 6,//results: Corrispnde a data
              targets: -1, 
              render: function ( data, type, row, meta ) {
                    if(type === 'display'){
                        var arraiTipe = data.split("=");
                        if(arraiTipe[0] === 'E'){
                            data =  '<a href="Esami?idEsami=' + encodeURIComponent(arraiTipe[1]) + '">'+
                                    '<i class="fas fa-notes-medical text-primary" style="font-size:36px" ></i>'+
                                    '</a>';
                        }else if(arraiTipe[0] === 'V'){
                            data =  '<a href="Visite?idVisita=' + encodeURIComponent(arraiTipe[1]) + '">'+
                                    '<i class="fas fa-notes-medical text-success" style="font-size:36px" ></i>'+
                                    '</a>';
                        }else if(arraiTipe[0] === 'R'){
                            data =  '<a href="Ricette?idRicetta=' + encodeURIComponent(arraiTipe[1]) + '">'+
                                    '<i class="fas fa-prescription-bottle-alt text-warning" style="font-size:36px" ></i>'+
                                    '</a>';
                        }
                    }
                    return data;
                }
            }
        ]
    });

});