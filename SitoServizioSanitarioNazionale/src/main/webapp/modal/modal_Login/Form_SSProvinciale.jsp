
<script src="${pageContext.request.contextPath}/modal/modal_Login/js/js_Login.js"></script>
<div class="modal-body">
    <div class="form-group">
        <label>Nome provincia</label>
        <select id="autocomplete-provincia" 
                name="autocomplete-provincia" 
                style="width: 100%"
                class="form-control  select2-allow-clear" autofocus
                required="required">
        </select>
    </div>
    <div class="form-group">
        <div class="clearfix">
            <label>Password</label>
            <a href="${pageContext.request.contextPath}/richiediPassword.jsp" class="pull-right text-muted">
                <small>Non ricordi la password?</small>
            </a>
        </div>
        <input  id="password" 
                name="password" 
                type="password"
                class="form-control"
                required="required"/>
    </div>
</div>    
<div class="modal-footer">
        <label class="checkbox-inline pull-left">
            <input  id="ricordami" 
                    name="ricordami"
                    type="checkbox" /> Ricordami
       </label>
    <input  type="submit"
            class="btn btn-primary pull-right"
            accept=""value="Accedi"/>
</div>