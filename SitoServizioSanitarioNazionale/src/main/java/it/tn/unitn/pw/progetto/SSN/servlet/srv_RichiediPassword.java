/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.tn.unitn.pw.progetto.SSN.servlet;

import it.tn.unitn.pw.progetto.SSN.DAO.common.exception.DAOException;
import it.tn.unitn.pw.progetto.SSN.DAO.common.exception.DAOFactoryException;
import it.tn.unitn.pw.progetto.SSN.DAO.common.factory.DAOFactory;
import it.tn.unitn.pw.progetto.SSN.DAO.persistence.*;
import it.tn.unitn.pw.progetto.SSN.Genera.Email.Mail;
import it.tn.unitn.pw.progetto.SSN.persistence.entities.*;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Logger;
import java.util.Random;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import it.tn.unitn.pw.progetto.SSN.servlet.*;


/**
 *
 * @author Brian
 */
public class srv_RichiediPassword extends HttpServlet {
//------------------------------------------------------------------------------   
    private Persona_DAO     persona_Dao;
    private Farmacia_DAO    farmacia_Dao;
        
    
    @Override
    public void init() throws ServletException {
        DAOFactory daoFactory = (DAOFactory) super.getServletContext().getAttribute("daoFactory");
        if (daoFactory == null) {
            throw new ServletException("Impossible to get dao factory for user storage system");
        }
        try {
            persona_Dao         = daoFactory.getDAO(Persona_DAO.class);
            farmacia_Dao = daoFactory.getDAO(Farmacia_DAO.class);
        } catch (DAOFactoryException ex) {
            throw new ServletException("Impossible to get dao factory for user storage system", ex);
        }
    }
    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String contextPath = getServletContext().getContextPath();
        if (!contextPath.endsWith("/")) {
            contextPath += "/";
        }
        
        String email         = request.getParameter("email");
        String codF_pIVA     = request.getParameter("codF_pIVA");
        if((email == null) || (email.equals("")) || (codF_pIVA == null) || (codF_pIVA.equals("")) ){
            response.sendRedirect("richiediPassword.jsp");
        }else{
            Object utente = getUtente(email);
            if(utente != null){
                if(utente instanceof  Persona){
                    if( !(((Persona)utente).getCodF().equals(codF_pIVA)) ){
                        response.sendRedirect("richiediPassword.jsp");
                    }else{
                        if(cambiaPasswordEInviaEmail(utente) == null){
                            response.sendRedirect("richiediPassword.jsp");
                        }else{
                            response.sendRedirect(response.encodeRedirectURL(contextPath + "Index"));
                        }
                    }
                }else if(utente instanceof Farmacia){
                    if( !(((Farmacia)utente).getpIVA().equals(codF_pIVA)) ){
                        response.sendRedirect("richiediPassword.jsp");
                    }else{
                        if(cambiaPasswordEInviaEmail(utente) == null){
                            response.sendRedirect("richiediPassword.jsp");
                        }else{
                            response.sendRedirect(response.encodeRedirectURL(contextPath + "Index"));
                        }
                    }
                }else{
                    response.sendRedirect("richiediPassword.jsp");
                }
                 }else{
                response.sendRedirect("richiediPassword.jsp");
            }
        }
    }
                
    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
//==============================================================================
//                             Identifica Utente
//==============================================================================
    
    private Object getUtente(String email){
        Object utente= null;
        try {
            utente = persona_Dao.getByPersonaEmail(email);
        } catch (DAOException ex) {
            utente=null;
            Logger.getLogger(getClass().getName()).severe(ex.toString());
        }
        if(utente == null){
            try {
                utente = farmacia_Dao.getByFarmaciaEmail(email);
            } catch (DAOException ex) {
                utente=null;
                Logger.getLogger(getClass().getName()).severe(ex.toString());
            }
        }
        return utente;
    }
//==============================================================================
//==============================================================================
//                             Identifica Utente
//==============================================================================
    private Object cambiaPasswordEInviaEmail(Object utente){
        String password =randomGenerator();
        if(utente instanceof Persona){
            String oldHasedPassword=((Persona)utente).getPassword();
            utente= cambiaPasswordPesona(password,(Persona)utente);
            if( (utente!= null) && (!(oldHasedPassword.equals(((Persona)utente).getPassword())))){
                //INVIO EMEIL
                inviaEmailNuovaPassword(utente,password);
                //INVIO EMEIL
            }else{
                utente =null;
            }
        }else if(utente instanceof Farmacia){
            String oldHasedPassword=((Farmacia)utente).getPassword();
            utente= cambiaPasswordFarmacia(password,(Farmacia)utente);
            if( (utente!= null) && (!(oldHasedPassword.equals(((Farmacia)utente).getPassword())))){
                //INVIO EMEIL
                inviaEmailNuovaPassword(utente,password);
                //INVIO EMEIL
            }else{
                utente =null;
            }
        }else{
            utente=null;
        }
        return utente;
    }
//==============================================================================
    private String randomGenerator() {
        Random generator = new Random(System.currentTimeMillis());
        String password="";
        do{
            int num = generator.nextInt(10);
            password=password.concat(""+num+"");
        }while(password.length()!=50);
       
        return password;
    }
//==============================================================================
//                          INVIO EMAIL CONFERMA PRESCRIZIONE
//==============================================================================
     private void inviaEmailNuovaPassword(Object utente,String password){
        String nome= "";
        String email= "";
        if(utente instanceof Persona){
            nome  = ((Persona)utente).getCognome()+" "+((Persona)utente).getNome();
            email = ((Persona)utente).getEmail();
        }else if(utente instanceof Farmacia){
            nome  = "Farmacia "+((Farmacia)utente).getNome();
            email = ((Farmacia)utente).getEmail();
        }
        
        String messaggio;
        String oggetto;
        try{
            messaggio=  "Buongiorno "+nome+",\n"+
                        "Le inviamo questa e-mail per informarla che la sua richiesta di recupero email è "+
                        "stata effetuato.\n"+
                        "La nuova password è la seguente: \n\t"+password+"\n"+
                        "Le ricordiamo di accedere alla propria area privata sul sito e cambiare la password."+
                        "Per maggiori informazzioni consultare il sito web nella propria area privata, "+
                        "nella sezione \"esami fatti\".\n"+
                        "Cordiali saluti,\n"+
                        "Progetto Universitario Servizio Sanitario Nazionale";
           
            oggetto =    "Richiesta nuova password -- Progetto Universitario Servizio Sanitario Nazionale --";

            Mail.sendEmail(messaggio,email, oggetto);
        }catch(Exception e){
            Logger.getLogger(getClass().getName()).severe(e.toString());
        }
     }
//==============================================================================
    protected Farmacia cambiaPasswordFarmacia(String password, Farmacia farmacia){
        try {
            String passwordHashed=srv_Index.convertToHashPassword(password, farmacia.getSalt());
            farmacia_Dao.update_Password(farmacia.getpIVA(), passwordHashed);
            farmacia.setPassword(passwordHashed);
        } catch (DAOException ex) {
            Logger.getLogger(getClass().getName()).severe(ex.toString());
        }
        
        return farmacia;
    }
//==============================================================================
    protected Persona cambiaPasswordPesona(String password, Persona persona){
        try {
            String passwordHashed=srv_Index.convertToHashPassword(password, persona.getSalt());
            persona_Dao.update_Password(persona.getCodF(), passwordHashed);
            persona.setPassword(passwordHashed);
        } catch (DAOException ex) {
            Logger.getLogger(getClass().getName()).severe(ex.toString());
        }
        
        return persona;
    }
}
