
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div name="PagamentoEsameModal" id="PagamentoEsameModal"  class="modal fade" >
    <div class="modal-dialog modal-login modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Effetua pagamento</h4>
                <button type="button"
                        class="close"
                        data-dismiss="modal"
                        aria-hidden="true">
                    &times;
                </button>    
            </div> 

            <form action="Esami.handler" method="POST">
                <input  type="hidden" id="TipoInserimetoEsame"  name="TipoInserimetoEsame"  value="EffetuaPagamento">

            <div class="modal-body">
                <div class="container-fluid justify-content-center "> 
                    <div class="container">
                        <div class="clearfix">
                            <h3>
                            <b> Esame:</b>
                            <br>
                            <h5>"<c:out value="${c_b_s_TipoEsame.nome}"/>"</h5>
                            </h3>
                        </div>
                    </div>
                    <br>
                    <hr>
                    <div class="container">
                        <h5>
                            <b>
                            <div class="form-check">
                                <input  type="checkbox" 
                                        id="pagatoEsame"
                                        name="pagatoEsame"/>
                               <label class="form-check-label" for="pagatoEsame">Pagato</label> 
                            </div>
                            </b>
                        </h5>
                    </div>
                </div>
            </div>
            <div class=" container-fluid modal-footer">
                <input  type="submit"
                        class="btn btn-primary pull-right"
                        accept=""value="Conferma"/>
            </div>
            </form>
        </div>
    </div>
</div>