/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.tn.unitn.pw.progetto.SSN.DAO.persistence;

import java.util.List;
        
import it.tn.unitn.pw.progetto.SSN.persistence.entities.MedicoBase;
import it.tn.unitn.pw.progetto.SSN.DAO.common.DAO;
import it.tn.unitn.pw.progetto.SSN.DAO.common.exception.DAOException;
/**
 *
 * @author Brian
 */
public interface MedicoBase_DAO extends DAO<MedicoBase, String>{
/*============================================================================*/    
/*                         METODO INSERT                                      */
/*============================================================================*/
    public String insert(MedicoBase medicoBase)throws DAOException;
/*============================================================================*/    
/*============================================================================*/    
/*                         METODO UPDATE                                      */
/*============================================================================*/
    public void update(MedicoBase medicoBase)throws DAOException;
    public void update_Password(String id,String Password )throws DAOException;
/*============================================================================*/
/*============================================================================*/    
/*                         METODO GET Da ID                                   */
/*============================================================================*/
    public List<MedicoBase> getBySSProvincialeId_Lavora(Integer id)throws DAOException;
    public List<MedicoBase> getBySSProvincialeId_Vive(Integer id)throws DAOException;
/*============================================================================*/
/*============================================================================*/    
/*                         METODO GET LOGIN                                  */
/*============================================================================*/
    public MedicoBase getByMedicoBaseEmailAndPassword(String email, String password)throws DAOException;
    public String getSaltMedicoBaseByEmail(String email)throws DAOException;
    
    public MedicoBase getByMedicoBaseCodFAndPassword(String CodF, String password)throws DAOException;
    public String getSaltMedicoBaseByCodF(String CodF)throws DAOException;
    
    public MedicoBase getByMedicoBaseCodF(String CodF)throws DAOException;
/*============================================================================*/
/*============================================================================*/    
/*                         METODO AGGIUNTIVI                                  */
/*============================================================================*/
    public Integer getNumeroNuoviPazientiByMedicoBaseCodF(String CodF)throws DAOException;
/*============================================================================*/
}
