/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.tn.unitn.pw.progetto.SSN.servlet;

import java.io.IOException;
import javax.servlet.http.HttpServletRequest;

import it.tn.unitn.pw.progetto.SSN.Genera.PDF.CreatePDF;
import it.tn.unitn.pw.progetto.SSN.persistence.entities.*;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletResponse;

import it.tn.unitn.pw.progetto.SSN.Genera.PDF.Ticket;
import java.util.LinkedList;


import it.tn.unitn.pw.progetto.SSN.DAO.common.exception.DAOException;
import it.tn.unitn.pw.progetto.SSN.DAO.common.exception.DAOFactoryException;
import it.tn.unitn.pw.progetto.SSN.DAO.common.factory.DAOFactory;
import it.tn.unitn.pw.progetto.SSN.DAO.persistence.*;
import it.tn.unitn.pw.progetto.SSN.persistence.entities.*;
import java.util.HashMap;
import java.util.Iterator;
import java.util.logging.Logger;
/**
 *
 * @author Brian
 */
public class srv_PDF_ReportPagamenti extends HttpServlet {
//------------------------------------------------------------------------------   
    private VisitaMB_DAO                        visitaMB_DAO;
    private VisitaMSEReport_DAO                 visitaMSEReport_DAO;  
    private Ricetta_DAO                         ricetta_DAO;
    private Farmaco_DAO                         farmaco_DAO;
    private PrescrizioneEsameEReferto_DAO       prescrizioneEsame_DAO;
    private Esame_DAO                           esame_DAO;
    
    @Override
    public void init() throws ServletException {
        DAOFactory daoFactory = (DAOFactory) super.getServletContext().getAttribute("daoFactory");
        if (daoFactory == null) {
            throw new ServletException("Impossible to get dao factory for user storage system");
        }
        try {
            visitaMB_DAO            = daoFactory.getDAO(VisitaMB_DAO.class);
            visitaMSEReport_DAO     = daoFactory.getDAO(VisitaMSEReport_DAO.class);
            ricetta_DAO             = daoFactory.getDAO(Ricetta_DAO.class);
            farmaco_DAO             = daoFactory.getDAO(Farmaco_DAO.class);
            prescrizioneEsame_DAO   = daoFactory.getDAO(PrescrizioneEsameEReferto_DAO.class);
            esame_DAO               = daoFactory.getDAO(Esame_DAO.class);
            
        } catch (DAOFactoryException ex) {
            throw new ServletException("Impossible to get dao factory for user storage system", ex);
        }
    }
//------------------------------------------------------------------------------
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Persona persona  = (Persona) request.getSession(false).getAttribute("c_Persona");
        if((persona!=null)){
            
            List<Ticket> listaPagamenti=getPagamenti(persona);    
            try{
                CreatePDF.produciPDFReportPagamenti(response,listaPagamenti,persona);
            }catch(IOException e){
                String contextPath = getServletContext().getContextPath();
                if (!contextPath.endsWith("/")) {
                    contextPath += "/";
                }
                response.sendRedirect(response.encodeRedirectURL(contextPath + "Cittadino/Ricetta/ricetta.jsp"));
            }
        }else{
            String contextPath = getServletContext().getContextPath();
            if (!contextPath.endsWith("/")) {
                contextPath += "/";
            }
            response.sendRedirect(response.encodeRedirectURL(contextPath + "Cittadino/Ricetta/ricetta.jsp"));
        }
    }   

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
//==============================================================================
//                          Get Pagameti Effetuati
//==============================================================================
    private List<Ticket> getPagamenti(Persona persona){
        List<Ticket> listaTiket = new LinkedList<>();
        Double prezzoTotale = 0.0; 
        try {
            //------------------------------------------------------------------
            //                      VISITE
            //------------------------------------------------------------------
            List<VisitaMB>        listaVisiteMB = visitaMB_DAO.getByPazienteId_Fatte(persona.getCodF());
            List<VisitaMSEReport> listaVisiteMS = visitaMSEReport_DAO.getByPazienteId_Fatte(persona.getCodF());

            List<Visita> listaTutteVisite= new LinkedList<Visita>();
            listaTutteVisite.addAll(listaVisiteMB);
            listaTutteVisite.addAll(listaVisiteMS);
            
            Iterator<Visita> visitaIterator = listaTutteVisite.iterator();
            while (visitaIterator.hasNext()) {
                Visita element = visitaIterator.next();
                if(! ((element instanceof VisitaMSEReport ) && (!((VisitaMSEReport)element).getPagato() ))){
                    Ticket nuovoTicket= new Ticket();
                    
                    
                    // DATA PRESCRIZIONE VISITA 
                    nuovoTicket.setDataPescritto(element.getDataPrescrizione().toString());
                    // DATA VISITA EFFETUATA
                    if(element.getData()!=null){
                        nuovoTicket.setData(element.getData().toString());
                    }else{
                        nuovoTicket.setData("");
                    }
                    // PRESCRIZIONE
                    nuovoTicket.setPrescrizione(element.getPrescrizione());
                    // TIPO E PREZZO
                    if(element instanceof VisitaMB){
                        nuovoTicket.setTipo("Visita medica");
                        nuovoTicket.setCosto(0.0);
                    }else if(element instanceof VisitaMSEReport){
                        nuovoTicket.setTipo("Visita medica specialista");
                        nuovoTicket.setCosto(((VisitaMSEReport) element).getTichet());
                        prezzoTotale+= ((VisitaMSEReport) element).getTichet();
                    }

                    listaTiket.add(nuovoTicket);
                }
            }
            //------------------------------------------------------------------
            //                      ESAMI
            //------------------------------------------------------------------
            HashMap<Integer,Esame> tipoEsameGiaCercati = new HashMap<>();
            List<PrescrizioneEsameEReferto>  listaEsami = prescrizioneEsame_DAO.getByPazienteId_Fatte(persona.getCodF());

            Iterator<PrescrizioneEsameEReferto> esameIterator = listaEsami.iterator();
            while (esameIterator.hasNext()) {
                PrescrizioneEsameEReferto element = esameIterator.next();
                if(element.getPagato() || element.getRichiamo()){
                    Ticket nuovoTicket= new Ticket();

                    Esame esame = tipoEsameGiaCercati.get(element.getId_Esame());
                    if(esame  == null ){
                        esame = esame_DAO.getByPrimaryKey(element.getId_Esame());
                        tipoEsameGiaCercati.put(element.getId_Esame(), esame);
                    }

                    // DATA PRESCRIZIONE VISITA 
                    nuovoTicket.setDataPescritto(element.getDataPrescrizione().toString());
                    // DATA VISITA EFFETUATA
                    if(element.getData()!=null){
                        nuovoTicket.setData(element.getData().toString());
                    }else{
                        nuovoTicket.setData("");
                    }
                    // PRESCRIZIONE
                    nuovoTicket.setPrescrizione(esame.getNome());
                    // TIPO E PREZZO
                    if(element.getRichiamo()){
                        nuovoTicket.setTipo("Richiamo (screening)");
                        nuovoTicket.setCosto(0.0);
                    }else{
                        nuovoTicket.setTipo("Esame");
                        nuovoTicket.setCosto(element.getTichet());
                        prezzoTotale+= element.getTichet();
                    }
                   
                    
                    listaTiket.add(nuovoTicket);
                }
            }
            //------------------------------------------------------------------
            //                      Ricette
            //------------------------------------------------------------------
            HashMap<Integer,Farmaco> farmacoGiaCercati = new HashMap<>();
            List<Ricetta>  listaRicetta = ricetta_DAO.getByPazienteId_Ririrate(persona.getCodF());

            Iterator<Ricetta> ricettaIterator = listaRicetta.iterator();
            while (ricettaIterator.hasNext()) {
                Ricetta element = ricettaIterator.next();
                
                if(element.getId_Farmacia()!=null){
                    Ticket nuovoTicket= new Ticket();
                    
                    Farmaco farmaco = farmacoGiaCercati.get(element.getId_Farmaco());
                    if(farmaco  == null ){
                        farmaco = farmaco_DAO.getByPrimaryKey(element.getId_Farmaco());
                        farmacoGiaCercati.put(element.getId_Farmaco(), farmaco);
                    }
                    
                    // DATA PRESCRIZIONE VISITA 
                    nuovoTicket.setDataPescritto(element.getDataPrescrizione().toString());
                    // DATA VISITA EFFETUATA
                    if(element.getDataEvalsa()!=null){
                        nuovoTicket.setData(element.getDataEvalsa().toString());
                    }else{
                        nuovoTicket.setData("");
                    }
                    // PRESCRIZIONE
                    nuovoTicket.setPrescrizione(farmaco.getNome());
                    // TIPO E PREZZO
                    nuovoTicket.setTipo("Ricetta farmaceutica");
                    nuovoTicket.setCosto(element.getPrezzoFarmacia());
                    prezzoTotale+= element.getPrezzoFarmacia();
                    
                    listaTiket.add(nuovoTicket);
                }
            }
        }catch (DAOException ex) {
            Logger.getLogger(getClass().getName()).severe(ex.toString());
        }catch(NumberFormatException ex){
             Logger.getLogger(getClass().getName()).severe(ex.toString());
        }
        
        
        Ticket Riassunto= new Ticket();
        Riassunto.setCosto(prezzoTotale);
        Riassunto.setData("");
        Riassunto.setDataPescritto("");
        Riassunto.setPrescrizione("");
        Riassunto.setTipo("Totale");
        listaTiket.add(Riassunto);
        
        return listaTiket;
    }
}
