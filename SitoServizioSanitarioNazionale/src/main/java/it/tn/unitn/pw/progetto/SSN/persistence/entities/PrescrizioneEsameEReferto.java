/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.tn.unitn.pw.progetto.SSN.persistence.entities;

import java.sql.Date;

/**
 *
 * @author Brian
 */
public class PrescrizioneEsameEReferto {
/*============================================================================*/
/*                                       ATTRIBUTI                            */
/*============================================================================*/
    private Integer     id;
    private Date        data;
    private Date        dataPrescrizione;
    private String      risultato;
    private Double      tichet;
    private Boolean     pagato;
    private String      cura;
    private Boolean     richiamo;
    private Integer     id_CartellaClinica;
    private String     id_MedicoSpec;
    private Integer     id_Esame;
    private Boolean     vistoPaziente;
    private Boolean     vistoReportPaziente;
    private Boolean     vistoReportMedicoBase;
/*============================================================================*/
    
/*============================================================================*/    
/*                                      METODI COSTRUTTORI                    */
/*============================================================================*/
    public PrescrizioneEsameEReferto() {
        id                      = null;
        data                    = null;
        dataPrescrizione        = null;
        risultato               = null;
        tichet                  = null;
        pagato                  = null;  
        cura                    = null;
        richiamo                = null;
        id_CartellaClinica      = null;
        id_MedicoSpec           = null;
        id_Esame                = null;
        vistoPaziente           = null;
        vistoReportPaziente     = null;
        vistoReportMedicoBase   = null;
    }
/*============================================================================*/

/*============================================================================*/    
/*                                      METODI GETTER E SETTER                */
/*============================================================================*/
    /*--------------------------------------*/   
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
    /*--------------------------------------*/
    public Date getData() {
        return data;
    }
    
    public void setData(Date data) {
        this.data = data;
    }
    /*--------------------------------------*/
    public Date getDataPrescrizione() {
        return dataPrescrizione;
    }
    
    public void setDataPrescrizione(Date dataPrescrizione) {
        this.dataPrescrizione = dataPrescrizione;
    }
    /*--------------------------------------*/
    public String getRisultato() {
        return risultato;
    }

    public void setRisultato(String risultato) {
        this.risultato = risultato;
    }
    /*--------------------------------------*/
    public Double getTichet() {
        return tichet;
    }

    public void setTichet(Double tichet) {
        this.tichet = tichet;
    }
    /*--------------------------------------*/
    public Boolean getPagato() {
        return pagato;
    }

    public void setPagato(Boolean pagato) {
        this.pagato = pagato;
    }
    /*--------------------------------------*/
    public String getCura() {
        return cura;
    }

    public void setCura(String cura) {
        this.cura = cura;
    }
    /*--------------------------------------*/
    public Boolean getRichiamo() {
        return richiamo;
    }

    public void setRichiamo(Boolean richiamo) {
        this.richiamo = richiamo;
    }
    /*--------------------------------------*/
    public Integer getId_CartellaClinica() {
        return id_CartellaClinica;
    }

    public void setId_CartellaClinica(Integer id_CartellaClinica) {
        this.id_CartellaClinica = id_CartellaClinica;
    }
    /*--------------------------------------*/
    public String getId_MedicoSpec() {
        return id_MedicoSpec;
    }

    public void setId_MedicoSpec(String id_MedicoSpec) {
        this.id_MedicoSpec = id_MedicoSpec;
    }
    /*--------------------------------------*/
    public Integer getId_Esame() {
        return id_Esame;
    }
    
    public void setId_Esame(Integer id_Esame) {
        this.id_Esame = id_Esame;
    }
    /*--------------------------------------*/
    public Boolean getVistoPaziente() {
        return vistoPaziente;
    }

    public void setVistoPaziente(Boolean vistoPaziente) {
        this.vistoPaziente = vistoPaziente;
    }
    /*--------------------------------------*/
    public Boolean getVistoReportPaziente() {
        return vistoReportPaziente;
    }

    public void setVistoReportPaziente(Boolean vistoReportPaziente) {
        this.vistoReportPaziente = vistoReportPaziente;
    }
    /*--------------------------------------*/
    public Boolean getVistoReportMedicoBase() {
        return vistoReportMedicoBase;
    }

    public void setVistoReportMedicoBase(Boolean vistoReportMedicoBase) {
        this.vistoReportMedicoBase = vistoReportMedicoBase;
    }
    /*--------------------------------------*/
/*============================================================================*/
}
