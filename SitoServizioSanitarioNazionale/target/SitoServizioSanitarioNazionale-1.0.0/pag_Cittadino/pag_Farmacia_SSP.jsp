<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div class="row container-fluid ">
    <div class="col-md-5">
         <img class="img-thumbnail rounded w-100" 
                     src="${pageContext.request.contextPath}/sito_IMG/Farmacia.png"
                     onerror ="imgError(this);" >
    </div>
    <div class="col-md-7">
        <div class="row container-fluid">
            <div class="col-md-4">
                <h5>
                    <b>
                    Nome:
                    </b>
                </h5>
            </div>
            <div class="col-md-8">
                <h5>
                    <c:choose>
                        <c:when test="${f_Farmacia ne null}">
                            <c:out value="${f_Farmacia.nome}"/>
                        </c:when>
                        <c:when test="${p_ssProvinciale ne null}">
                            <c:out value="${p_ssProvinciale.nome}"/>
                        </c:when>
                    </c:choose>
                </h5>
            </div>
        </div>
        <c:if test="${f_Farmacia ne null}">
        <div class="row container-fluid">
            <div class="col-md-4">
                <h5>
                    <b>
                    Partita IVA: 
                    </b>
                </h5>
            </div>
            <div class="col-md-8">
                <h5>
                    <c:out value="${f_Farmacia.pIVA}"/>  
                </h5>
            </div>
        </div>
        <div class="row container-fluid">
            <div class="col-md-4">
                <h5>
                    <b>
                    Email: 
                    </b>
                </h5>
            </div>
            <div class="col-md-8">
                <h5>
                    <c:out value="${f_Farmacia.email}"/>
                </h5>
            </div>
        </div>
        </c:if>
                        
                        
        <div class="row">
            <div class="col-md-4 mx-auto">
                <a  class="container-fluid
                        btn btn-primary"
                    href="#"
                    data-toggle="modal" 
                    data-target="#CambiaPassword">
                       Modifica Password <i class="fas fa-edit"></i> 
                </a>
            </div>
        </div>
    </div> 
</div>