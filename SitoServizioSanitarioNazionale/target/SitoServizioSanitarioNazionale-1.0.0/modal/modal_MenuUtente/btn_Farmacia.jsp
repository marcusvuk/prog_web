<a class="btn btn-light m-1 text-left"
    href="${pageContext.request.contextPath}/Farmacia/cercaPazienti.jsp">
    Cerca cliente
</a>
<br>
<c:if test="${f_Cliente ne null}">
<br>
    <h5>
        Ultimo cliente cercato:
        <br>
        <b> 
            <c:out value="${f_Cliente.cognome}"/>
            <c:out value="${f_Cliente.nome}"/>      
        </b>
    </h5>
    <a class="btn btn-light m-1 text-left"
        href="${pageContext.request.contextPath}/Farmacia/Cliente/listaRicette.jsp">
        Lista Ricette
    </a>
</c:if>