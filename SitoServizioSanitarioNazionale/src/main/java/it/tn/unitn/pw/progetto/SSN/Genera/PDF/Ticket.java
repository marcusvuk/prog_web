
package it.tn.unitn.pw.progetto.SSN.Genera.PDF;

public class Ticket{
    private String tipo;
    private String dataPescritto;
    private String data;
    private String prescrizione;
    private Double costo;
    /**
     * Costruttore di un oggetto Ticket che rappresenta una linea
     * della tabella da mostrare sul sito/PDF
     * 
     * @param tipo_ticket tipo del ticket. Può essere 'Visita di Base', 'Visita Specialistica', 'Ricetta' oppure 'Esame'
     * @param data_prescrizione data in cui è stata fatta la prescrizione
     * @param prescrizione prescrizione della visita/ricetta/esame.
     * @param costo costo del ticket
     */
    public Ticket(String tipo_ticket, String data_prescrizione,String data, String prescrizione, Double costo){
        this.tipo=tipo_ticket;
        this.dataPescritto=data_prescrizione;
        this.data=data;
        this.prescrizione=prescrizione;
        this.costo = costo;
    }

    public Ticket() {
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getDataPescritto() {
        return dataPescritto;
    }

    public void setDataPescritto(String dataPescritto) {
        this.dataPescritto = dataPescritto;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getPrescrizione() {
        return prescrizione;
    }

    public void setPrescrizione(String prescrizione) {
        this.prescrizione = prescrizione;
    }

    public Double getCosto() {
        return costo;
    }

    public void setCosto(Double costo) {
        this.costo = costo;
    }

    
}