$(function () {
    $("#autocomplete-medicoBase").select2({
        placeholder: "Scegli Medico di Base",
        allowClear: true,
        ajax: {
            url: function (request) {
                return "/SitoServizioSanitarioNazionale/services/MedicoBase/ForCittadino/"+$('#MyCodF').attr('value')+"/"+ request.term;
            },
            dataType: "json"
        }
    });
    $("#autocomplete-provincia").val(null).trigger("change");
  
});
