/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.tn.unitn.pw.progetto.SSN.servlet;

import it.tn.unitn.pw.progetto.SSN.DAO.common.exception.DAOException;
import it.tn.unitn.pw.progetto.SSN.DAO.common.exception.DAOFactoryException;
import it.tn.unitn.pw.progetto.SSN.DAO.common.factory.DAOFactory;
import it.tn.unitn.pw.progetto.SSN.DAO.persistence.*;
import it.tn.unitn.pw.progetto.SSN.persistence.entities.*;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.LinkedList;

/**
 *
 * @author Brian
 */
public class srv_Far_Cliente extends HttpServlet {
//------------------------------------------------------------------------------   
    private Persona_DAO                 persona_Dao;
        
    private Persona             paziente;
    
    @Override
    public void init() throws ServletException {
        DAOFactory daoFactory = (DAOFactory) super.getServletContext().getAttribute("daoFactory");
        if (daoFactory == null) {
            throw new ServletException("Impossible to get dao factory for user storage system");
        }
        try {
            persona_Dao         = daoFactory.getDAO(Persona_DAO.class);
        } catch (DAOFactoryException ex) {
            throw new ServletException("Impossible to get dao factory for user storage system", ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        String contextPath = getServletContext().getContextPath();
        if (!contextPath.endsWith("/")) {
            contextPath += "/";
        }
        
        Farmacia farmacia = (Farmacia) request.getSession().getAttribute("f_Farmacia");
        if(farmacia == null){
            response.sendRedirect("cercaPazienti.jsp");
        }
        
        
        String nomeCliente = request.getParameter("nomeCliente");
        String cognomeCliente = request.getParameter("cognomeCliente");
        String codF_Cliente = request.getParameter("codF_Cliente");
       
        if( ((nomeCliente == null)    || (nomeCliente.equals("")))      || 
            ((cognomeCliente == null) || (cognomeCliente.equals("")))   ){
            response.sendRedirect("cercaPazienti.jsp");
        }else{
            List<Persona> listaCliente = cercaClienti(request,nomeCliente,cognomeCliente,codF_Cliente);
            if(listaCliente.size()== 0){
                response.sendRedirect("cercaPazienti.jsp");
            }else  if(listaCliente.size()== 1){//Correto
                Persona cliente = listaCliente.get(0);
                request.getSession().setAttribute("f_Cliente", cliente);
                response.sendRedirect(response.encodeRedirectURL(contextPath + "Farmacia/Cliente/listaRicette.jsp"));
            }else{
                //OMONIMIA
                response.sendRedirect("cercaPazienti.jsp?nome="+nomeCliente+"&cognome="+cognomeCliente);
            }
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
//####################################################################################
//####################################################################################
    
//==============================================================================
//                          GET CLIENTE
//==============================================================================
    private List<Persona> cercaClienti(HttpServletRequest request,String nomeCliente,String cognomeCliente,String codF_Cliente)
                                                                throws ServletException, IOException {
        List<Persona> listaCliente = new LinkedList<Persona>();
        if((codF_Cliente == null)    || (codF_Cliente.equals(""))){
            listaCliente = getListaClienteByNameCognome(request,nomeCliente,cognomeCliente);
        }else{
            Persona cliente = getCliente(request,codF_Cliente);
            if( (cliente!= null) && 
                (cliente.getNome().equals(nomeCliente)) && 
                (cliente.getCognome().equals(cognomeCliente))){
                
                listaCliente.add(cliente);
            }
            
        }
        return listaCliente;
    }
//==============================================================================
//                          GET CLIENTE
//============================================================================== 
    private Persona getCliente(HttpServletRequest request,String codF){
        Persona cliente=null;
        try {
            cliente = persona_Dao.getByPrimaryKey(codF);
            
        } catch (DAOException ex) {
            request.getServletContext().log("Impossibile recuperare la persona.", ex);
            cliente= null;
        }
        return cliente;
    }
//==============================================================================
//                          GET CLIENTE nome e cognome
//============================================================================== 
    private List<Persona> getListaClienteByNameCognome(HttpServletRequest request,String nome,String cognome){
        List<Persona> listaCliente=null;
        try {
            listaCliente = persona_Dao.getByNomeCognome(nome, cognome);
            
        } catch (DAOException ex) {
            request.getServletContext().log("Impossibile recuperare la persona.", ex);
            listaCliente= null;
        }
        return listaCliente;
    }
}
