/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.tn.unitn.pw.progetto.SSN.servlet;

import java.io.IOException;
import javax.servlet.http.HttpServletRequest;

import it.tn.unitn.pw.progetto.SSN.Genera.PDF.CreatePDF;
import it.tn.unitn.pw.progetto.SSN.persistence.entities.*;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletResponse;

import java.net.InetAddress;
import java.net.Socket;
/**
 *
 * @author Brian
 */
public class srv_PDF_Ricette extends HttpServlet {
    
    //-----------------------------------------------------------------------------
    private    String IP_LOCALE="192.168.0.103";
    //-----------------------------------------------------------------------------
    
     /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Persona persona  = (Persona) request.getSession(false).getAttribute("c_Persona");
        Ricetta ricetta  = (Ricetta) request.getSession(false).getAttribute("c_b_s_f_Ricetta");
        Farmaco farmaco  = (Farmaco) request.getSession(false).getAttribute("c_b_s_f_Farmaco");
        MedicoBase mdicoBase  = (MedicoBase) request.getSession(false).getAttribute("c_b_s_f_MedicoBase");
        if((ricetta!=null)&&(farmaco!=null)){
            
            String serverName = getServletContext().getContextPath();
            String url=IP_LOCALE+":8080"+serverName+"/QR_Ricetta?idRicetta="+ricetta.getId()+
                                                                "&personaCodF="+persona.getCodF()+
                                                                "&dataScadenza="+ricetta.getScadenza();
            
            String nomeCognomeMB="Dr. "+mdicoBase.getCognome()+" "+mdicoBase.getNome();
            try{
                
                CreatePDF.produciPDFRicetta(response,persona,ricetta,farmaco,nomeCognomeMB,url);
            }catch(IOException e){
                String contextPath = getServletContext().getContextPath();
                if (!contextPath.endsWith("/")) {
                    contextPath += "/";
                }
                response.sendRedirect(response.encodeRedirectURL(contextPath + "Cittadino/Ricetta/ricetta.jsp"));
            }
        }else{
            String contextPath = getServletContext().getContextPath();
            if (!contextPath.endsWith("/")) {
                contextPath += "/";
            }
            response.sendRedirect(response.encodeRedirectURL(contextPath + "Cittadino/Ricetta/ricetta.jsp"));
        }
    }    
        
    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
