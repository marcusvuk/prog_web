/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.tn.unitn.pw.progetto.SSN.services;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Produces;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;
import javax.ws.rs.core.MediaType;

import com.google.gson.Gson;
import it.tn.unitn.pw.progetto.SSN.DAO.common.exception.DAOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;

import it.tn.unitn.pw.progetto.SSN.DAO.persistence.*;
import it.tn.unitn.pw.progetto.SSN.DAO.common.exception.DAOFactoryException;
import it.tn.unitn.pw.progetto.SSN.DAO.common.factory.DAOFactory;
import it.tn.unitn.pw.progetto.SSN.persistence.entities.*;
import java.sql.SQLException;
import java.sql.Date;
import java.util.Iterator;
import java.util.logging.Logger;
import sun.tools.tree.ThisExpression;

/**
 * REST Web Service
 *
 * @author Brian
 */
@Path("pazienti")
public class PazientiService {
    
    
    private static DAOFactory       daoFactory;
    
    private static Persona_DAO                  persona_DAO;
    private static MedicoSpec_DAO               medicoSpec_DAO;
    private static Foto_DAO                     foto_DAO;
    private static CartellaClinicaPerMB_DAO      cartellaClinica_DAO;
    
    static public void init(DAOFactory daoFactory1) {
        daoFactory =  daoFactory1;
    }
    
    @Context
    private UriInfo context;

    /**
     * Creates a new instance of PazientiService
     */
    public PazientiService() {
    }
//------------------------------------------------------------------------------
//                          DATA TABLE
//------------------------------------------------------------------------------
    @GET
    @Path("ForMedicoSpec/{medicoSpecCodF}")
    public String getMedicoSpec(@PathParam("medicoSpecCodF") String medicoSpecCodF){

        List<String[]> results = new ArrayList();
        List<String[]> listaPazientiDaFiltrare = new ArrayList();
        
        try {
            persona_DAO     = daoFactory.getDAO(Persona_DAO.class);
            foto_DAO        = daoFactory.getDAO(Foto_DAO.class);
            medicoSpec_DAO  = daoFactory.getDAO(MedicoSpec_DAO.class);
            
            if(medicoSpec_DAO.getByPrimaryKey(medicoSpecCodF)!= null){
            
                List<Persona> listaPersona = persona_DAO.getAll();
                for(int i=0; i<listaPersona.size(); i++){
                    String[] rigaRis= new String[7];

                    //Aggunta foto in posizione 0
                    Foto fotoAttiva =null;
                    try{
                        fotoAttiva = foto_DAO.getByPazienteId_Attivo( listaPersona.get(i).getCodF() );
                    }catch(Exception e){
                        fotoAttiva=null;
                    }
                    if(fotoAttiva!= null){
                        rigaRis[0]=(fotoAttiva.getPath()+fotoAttiva.getNome());
                    }else{
                        rigaRis[0]="";
                    }
                    //Aggunta dati persona in posizione  1
                    rigaRis[1]=(listaPersona.get(i).getNome());
                    //Aggunta dati persona in posizione  2
                    rigaRis[2]=(listaPersona.get(i).getCognome());
                    //Aggunta dati persona in posizione  3
                    rigaRis[3]=(listaPersona.get(i).getNascita().toString());
                    //Aggunta dati persona in posizione  4
                    rigaRis[4]=(listaPersona.get(i).getCodF());
                    //Aggunta dati persona in posizione  5
                    Date ultimaModifica= persona_DAO.getUltimaPrescrizione(rigaRis[4]);
                    if(ultimaModifica!=null){
                        rigaRis[5]=ultimaModifica.toString();
                    }else{
                        rigaRis[5]=listaPersona.get(i).getNascita().toString();
                    }

                    //Aggunta dati persona in posizione  6
                    rigaRis[6]=rigaRis[4];

                    listaPazientiDaFiltrare.add(rigaRis);
                }
            }
            
        } catch (DAOFactoryException ex) {
            Logger.getLogger(getClass().getName()).severe(ex.toString());
        } catch (DAOException ex) {
            Logger.getLogger(getClass().getName()).severe(ex.toString());
        }catch(NumberFormatException ex){
             Logger.getLogger(getClass().getName()).severe(ex.toString());
        }
        
        
        results.addAll(listaPazientiDaFiltrare);
        

        Gson gson = new Gson();
        return gson.toJson(new Results(results.toArray(new String[0][7] ) ) );
    }
//------------------------------------------------------------------------------
//                          DATA TABLE
//------------------------------------------------------------------------------
    @GET
    @Path("MedicoBase/{medicoBaseCodF}")
    public String getMedicoBase(@PathParam("medicoBaseCodF") String medicoBaseCodF){
        
        List<String[]> results = new ArrayList();
        List<String[]> listaPazientiDaFiltrare = new ArrayList();
        
        try {
            persona_DAO             = daoFactory.getDAO(Persona_DAO.class);
            foto_DAO                = daoFactory.getDAO(Foto_DAO.class);
            cartellaClinica_DAO     = daoFactory.getDAO(CartellaClinicaPerMB_DAO.class);
            
            List<CartellaClinicaPerMB> listaCartellaClinica = cartellaClinica_DAO.getByMedicoBaseId_Attivo(medicoBaseCodF);
            Iterator<CartellaClinicaPerMB> iterCartellaClinica= listaCartellaClinica.iterator();
            while(iterCartellaClinica.hasNext()){
                String[] rigaRis= new String[8];
                
                CartellaClinicaPerMB catellaClinica= iterCartellaClinica.next();
                Persona paziente = persona_DAO.getByPrimaryKey(catellaClinica.getId_Persona());
                Foto fotoAttiva =null;
                try{
                    fotoAttiva = foto_DAO.getByPazienteId_Attivo( paziente.getCodF() );
                }catch(Exception e){
                    fotoAttiva=null;
                }
                
                //Aggunta foto in posizione 0
                if(fotoAttiva!= null){
                    rigaRis[0]=(fotoAttiva.getPath()+fotoAttiva.getNome());
                }else{
                    rigaRis[0]="";
                }
                //Aggunta visto dal medico in posinzione 1
                rigaRis[1] = catellaClinica.getVistoMedicoBase().toString();
                //Aggunta dati persona in posizione  2
                rigaRis[2]=(paziente.getNome());
                //Aggunta dati persona in posizione  3
                rigaRis[3]=(paziente.getCognome());
                //Aggunta dati persona in posizione  4
                rigaRis[4]=(paziente.getNascita().toString());
                //Aggunta dati persona in posizione  5
                rigaRis[5]=(paziente.getCodF());
                //Aggunta dati persona in posizione  6
                Date ultimaModifica= persona_DAO.getUltimaPrescrizione(paziente.getCodF());
                if(ultimaModifica!=null){
                    rigaRis[6]=ultimaModifica.toString();
                }else{
                    rigaRis[6]=paziente.getNascita().toString();
                }
                
                //Aggunta dati persona in posizione  7
                rigaRis[7]=paziente.getCodF();
                
                listaPazientiDaFiltrare.add(rigaRis);
            }
        } catch (DAOFactoryException ex) {
            Logger.getLogger(getClass().getName()).severe(ex.toString());
        } catch (DAOException ex) {
            Logger.getLogger(getClass().getName()).severe(ex.toString());
        }catch(NumberFormatException ex){
             Logger.getLogger(getClass().getName()).severe(ex.toString());
        }

        results.addAll(listaPazientiDaFiltrare);
        
        Gson gson = new Gson();
        return gson.toJson(new Results(results.toArray(new String[0][8] ) ) );
    }
//------------------------------------------------------------------------------
//                              SELECT 2
//------------------------------------------------------------------------------
    @GET
    @Path("MedicoBase_S2/{medicoBaseCodF}/{term}")
    public String getMedicoBaseSelect2(@PathParam("medicoBaseCodF") String medicoBaseCodF,@PathParam("term") String term){
        List<Paziente> listaPazienteForJson = new ArrayList<>();
        
        List<Paziente> results = new ArrayList<>();
        
        try {
            persona_DAO             = daoFactory.getDAO(Persona_DAO.class);
            cartellaClinica_DAO     = daoFactory.getDAO(CartellaClinicaPerMB_DAO.class);
            
            List<CartellaClinicaPerMB> listaCartellaClinica = cartellaClinica_DAO.getByMedicoBaseId_Attivo(medicoBaseCodF);
            Iterator<CartellaClinicaPerMB> iterCartellaClinica= listaCartellaClinica.iterator();
            while(iterCartellaClinica.hasNext()){
                CartellaClinicaPerMB catellaClinica= iterCartellaClinica.next();
                Persona paziente = persona_DAO.getByPrimaryKey(catellaClinica.getId_Persona());
                
                listaPazienteForJson.add(
                        new Paziente( paziente.getCodF(), paziente.getNome()+" "+paziente.getCognome()+
                                    " - "+paziente.getCodF())
                                    );
                
            }
            
        } catch (DAOFactoryException ex) {
            Logger.getLogger(getClass().getName()).severe(ex.toString());
        } catch (DAOException ex) {
            Logger.getLogger(getClass().getName()).severe(ex.toString());
        }catch(NumberFormatException ex){
             Logger.getLogger(getClass().getName()).severe(ex.toString());
        }
        
        
        if ((term == null) || "undefined".equals(term)) {
            results.addAll(listaPazienteForJson);
        } else {
            Iterator<Paziente> provincieIterator = listaPazienteForJson.iterator();
            while (provincieIterator.hasNext()) {
                Paziente element = provincieIterator.next();
                if(element.getText().toLowerCase().contains(term.toLowerCase())) {
                    results.add(element);
                }
            }
        }
        Gson gson = new Gson();
        return gson.toJson(new ResultsS2(results.toArray(new Paziente[0])));
    }
//------------------------------------------------------------------------------
//                              SELECT 2
//------------------------------------------------------------------------------
    @GET
    @Path("MedicoSpec_S2/{medicoSpecCodF}/{term}")
    public String getMedicoSpecSelect2(@PathParam("medicoSpecCodF") String medicoSpecCodF,@PathParam("term") String term){
        List<Paziente> listaPazienteForJson = new ArrayList<>();
        
        List<Paziente> results = new ArrayList<>();
 
        try {
            persona_DAO             = daoFactory.getDAO(Persona_DAO.class);
            medicoSpec_DAO          = daoFactory.getDAO(MedicoSpec_DAO.class);
            
            if(medicoSpec_DAO.getByPrimaryKey(medicoSpecCodF)!= null){
            
                List<Persona> listaPesona = persona_DAO.getAll();
                Iterator<Persona> iterPesona= listaPesona.iterator();
                while(iterPesona.hasNext()){
                    Persona paziente= iterPesona.next();

                    listaPazienteForJson.add(
                            new Paziente( paziente.getCodF(), paziente.getNome()+" "+paziente.getCognome()+
                                    " - "+paziente.getCodF())
                                        );

                }
            }
        } catch (DAOFactoryException ex) {
            Logger.getLogger(getClass().getName()).severe(ex.toString());
        } catch (DAOException ex) {
            Logger.getLogger(getClass().getName()).severe(ex.toString());
        }catch(NumberFormatException ex){
             Logger.getLogger(getClass().getName()).severe(ex.toString());
        }
        
        
        if ((term == null) || "undefined".equals(term)) {
            results.addAll(listaPazienteForJson);
        } else {
            Iterator<Paziente> provincieIterator = listaPazienteForJson.iterator();
            while (provincieIterator.hasNext()) {
                Paziente element = provincieIterator.next();
                if(element.getText().toLowerCase().contains(term.toLowerCase())) {
                    results.add(element);
                }
            }
        }
        Gson gson = new Gson();
        return gson.toJson(new ResultsS2(results.toArray(new Paziente[0])));
    }
//##############################################################################
//                          DATA TABLE
//##############################################################################
    public static class Results implements Serializable {

        private String[][] results;

        public Results(String[][] results) {
            this.results = results;
        }

        public String[][] getResults() {
            return results;
        }

        public void setResults(String[][] results) {
            this.results = results;
        }
    }
//##############################################################################    
//                          SELECT 2
//##############################################################################
    public static class Paziente implements Serializable {

        private String id;
        private String text;

        public Paziente(String id, String text) {
            this.id = id;
            this.text = text;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getText() {
            return text;
        }

        public void setText(String text) {
            this.text = text;
        }
    }
//##############################################################################
    public static class ResultsS2 implements Serializable {

        private Paziente[] results;

        public ResultsS2(Paziente[] results) {
            this.results = results;
        }

        public Paziente[] getResults() {
            return results;
        }

        public void setResults(Paziente[] results) {
            this.results = results;
        }
    }
}
