/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.tn.unitn.pw.progetto.SSN.DAO.persistence.jdbc;

import java.sql.Date;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import it.tn.unitn.pw.progetto.SSN.DAO.common.persistence.dao.jdbc.JDBCDAO;
import it.tn.unitn.pw.progetto.SSN.DAO.common.exception.*;
import it.tn.unitn.pw.progetto.SSN.DAO.persistence.*;
import it.tn.unitn.pw.progetto.SSN.persistence.entities.*;
/**
 *
 * @author Brian
 */
public class JDBC_Foto_DAO  extends JDBCDAO<Foto, Integer> implements Foto_DAO {
/*============================================================================*/    
/*                                      METODI COSTRUTTORI                    */
/*============================================================================*/
    public JDBC_Foto_DAO(Connection con) {
        super(con);
    }
/*============================================================================*/
        
/*============================================================================*/    
/*                      METODI GETTER DAO IMPLEMENTATION                      */
/*============================================================================*/
    @Override
    public Long getCount() throws DAOException {
        try (Statement stmt = CON.createStatement()) {
            ResultSet counter = stmt.executeQuery( 
                     "  SELECT COUNT(*) "               +
                     "  FROM Foto "     );
            if (counter.next()) {
                return counter.getLong(1);
            }
        } catch (SQLException ex) {
            throw new DAOException( " Immpossibile contare gli elementi di Foto  " , ex);
        }
        return 0L;
    }
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    @Override
    public Foto getByPrimaryKey(Integer primaryKey) throws DAOException {
        if (primaryKey == null) {
            throw new DAOException( " primaryKey è null " );
        }
        try (PreparedStatement stm = CON.prepareStatement(
                                     "  SELECT *  "                      +
                                     "  FROM Foto  "     +
                                     "  WHERE id = ? "                   )) {
            // imposto i valori
            stm.setInt(1, primaryKey);
            
            try (ResultSet rs = stm.executeQuery()) {
                rs.next();
                Foto foto = new Foto();
                foto.setId               (rs.getInt      ( "id" ));
                foto.setId_Persona       (rs.getString   ( "id_Persona" ));
                foto.setData             (rs.getDate     ( "data" ));
                foto.setPath             (rs.getString   ( "path" ));
                foto.setNome             (rs.getString   ( "nome" ));
                foto.setScelta           (rs.getBoolean  ( "scelta" ));
                        
                return foto;
            }
        } catch (SQLException ex) {
            throw new DAOException( " Impossibile perendere la Foto indicato dalla primary key " , ex);
        }
    }
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    @Override
    public List<Foto> getAll() throws DAOException {
        List<Foto> listaFoto = new ArrayList<>();

        try (Statement stm = CON.createStatement()) {
            try (ResultSet rs = stm.executeQuery(
                                 " SELECT *  "         +
                                 " FROM Foto  "        )){
                
                while (rs.next()) {
                    Foto foto = new Foto();
                    
                    foto.setId               (rs.getInt      ( "id" ));
                    foto.setId_Persona       (rs.getString   ( "id_Persona" ));
                    foto.setData             (rs.getDate     ( "data" ));
                    foto.setPath             (rs.getString   ( "path" ));
                    foto.setNome             (rs.getString   ( "nome" ));
                    foto.setScelta           (rs.getBoolean  ( "scelta" ));
                    
                    listaFoto.add(foto);
                }
            }
        } catch (SQLException ex) {
            throw new DAOException( " Imposibile estrarre tutta la lista di Foto  " , ex);
        }

        return listaFoto;
    }
/*============================================================================*/
   
/*============================================================================*/    
/*                      METODI QUERY                                          */
/*============================================================================*/
//------------------------------------------------------------------------------
//                      INSERT
//------------------------------------------------------------------------------
    @Override
    public Integer insert(Foto foto) throws DAOException {
        if (foto == null) {
            throw new DAOException( " La foto è null " );
        }
        try (PreparedStatement ps = CON.prepareStatement(
                     " INSERT INTO Foto "  +
                                 " ( id_Persona "       +    "  ,  "    +
                                 "  data "              +    "  ,  "    +
                                 "  path "              +    "  ,  "    +
                                 "  nome "              +    "  ,  "    +
                                 "  scelta "            +    "  )  "    +  
                     " VALUES (?,?,?,?,?) " , Statement.RETURN_GENERATED_KEYS)) {
            
            // imposto i valori
            ps.setString    (1, foto.getId_Persona());
            ps.setDate      (2, foto.getData());
            ps.setString    (3, foto.getPath());
            ps.setString    (4, foto.getNome());
            ps.setBoolean   (5, foto.getScelta());
            
            ps.executeUpdate();
            
            ResultSet rs = ps.getGeneratedKeys();
            if (rs.next()) {
                foto.setId(rs.getInt(1));
            }
            
            return foto.getId();
        } catch (SQLException ex) {
            throw new DAOException( " Impossibile aggiungere un nuovo record di CartellaClinicaPerMB " , ex);
        }
    }
//------------------------------------------------------------------------------
//                      UPDATE
//------------------------------------------------------------------------------
    @Override
    public void update(Foto foto) throws DAOException {
        if (foto == null) {
            throw new DAOException( " La foto è null " );
        }
        try (PreparedStatement ps = CON.prepareStatement(
                     " UPDATE Foto "  +
                     " SET id = ?  "           +     "  ,  "    +
                        "  id_Persona = ?  "   +     "  ,  "    +
                        "  data = ?  "         +     "  ,  "    +
                        "  path = ?  "         +     "  ,  "    +
                        "  nome = ?  "         +     "  ,  "    +
                        "  scelta = ?  "       +     
                     " WHERE id = ?  " , Statement.RETURN_GENERATED_KEYS)) {
            
            // imposto i valori
            ps.setInt       (1, foto.getId());
            
            ps.setString    (2, foto.getId_Persona());
            ps.setDate      (3, foto.getData());
            ps.setString    (4, foto.getPath());
            ps.setString    (5, foto.getNome());
            ps.setBoolean   (6, foto.getScelta());
            //WHERE
            ps.setInt       (7, foto.getId());
            
            ps.executeUpdate();
            
            
        } catch (SQLException ex) {
            throw new DAOException( " Impossibile aggiornare il record di Foto " , ex);
        }
    }
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    @Override
    public void update_Scelta(Integer id,String id_persona, Boolean scelta) throws DAOException {
        if (id == null || scelta == null) {
            throw new DAOException( " id o visto è null " );
        }
        try (PreparedStatement ps = CON.prepareStatement(
                     " UPDATE Foto "        +
                     " SET   scelta = ?  "  +
                     " WHERE id = ?  AND id_persona = ? " , Statement.RETURN_GENERATED_KEYS)) {
            
            // imposto i valori
            ps.setBoolean   (1, scelta);
            //WHERE
            ps.setInt       (2, id);
            ps.setString    (3, id_persona);
            
            ps.executeUpdate();
            
        } catch (SQLException ex) {
            throw new DAOException( " Impossibile aggiornare il record di Foto " , ex);
        }
    }
//------------------------------------------------------------------------------
//                      SELECT
//------------------------------------------------------------------------------
    @Override
    public List<Foto> getByPazienteId(String id) throws DAOException {
        if (id == null ) {
            throw new DAOException( " id  è null " );
        }
        List<Foto> listaFoto = new ArrayList<>();

        try (PreparedStatement stm = CON.prepareStatement(
                                "  SELECT *  "                     +
                                 "  FROM Foto  "                    +
                                 "  WHERE id_Persona = ? ")) {
            
            stm.setString(1, id);
            try (ResultSet rs = stm.executeQuery()){
               
                while (rs.next()) {
                    Foto foto = new Foto();
                    
                    foto.setId               (rs.getInt      ( "id" ));
                    foto.setId_Persona       (rs.getString   ( "id_Persona" ));
                    foto.setData             (rs.getDate     ( "data" ));
                    foto.setPath             (rs.getString   ( "path" ));
                    foto.setNome             (rs.getString   ( "nome" ));
                    foto.setScelta           (rs.getBoolean  ( "scelta" ));
                    
                    listaFoto.add(foto);
                }
            }
        } catch (SQLException ex) {
            throw new DAOException( " Imposibile estrarre la lista di Foto  " , ex);
        }

        return listaFoto;
    }
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    @Override
    public Foto getByPazienteId_Attivo(String id) throws DAOException {
        if (id == null) {
            throw new DAOException( " id è null " );
        }
        try (PreparedStatement stm = CON.prepareStatement(
                                     "  SELECT *  "      +
                                     "  FROM Foto  "     +
                                     "  WHERE id_Persona = ?  AND  scelta = TRUE "  )) {
            // imposto i valori
            stm.setString(1, id);
            
            try (ResultSet rs = stm.executeQuery()) {
                rs.next();
                Foto foto = new Foto();
                foto.setId               (rs.getInt      ( "id" ));
                foto.setId_Persona       (rs.getString   ( "id_Persona" ));
                foto.setData             (rs.getDate     ( "data" ));
                foto.setPath             (rs.getString   ( "path" ));
                foto.setNome             (rs.getString   ( "nome" ));
                foto.setScelta           (rs.getBoolean  ( "scelta" ));
                        
                return foto;
            }
        } catch (SQLException ex) {
            throw new DAOException( " Impossibile perendere la Foto indicato  " +
                                 " dalla chiave esterna che è anchè attivo " , ex);
        }
    }
/*============================================================================*/
}
