$(function () {
    $("#idTipoEsame").select2({
        placeholder: "Scegli tipo esame",
        allowClear: true,
        ajax: {
            url: function (request) {
                return "/SitoServizioSanitarioNazionale/services/tipoEsame/SSP/"+$('#MyIdSSP').attr('value')+"/"+ request.term;
            },
            dataType: "json"
        }
    });
    $("#idTipoEsame").val(null).trigger("change");
});
