/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.tn.unitn.pw.progetto.SSN.DAO.persistence;

import java.util.List;
        
import it.tn.unitn.pw.progetto.SSN.persistence.entities.SSProvinciale;
import it.tn.unitn.pw.progetto.SSN.DAO.common.DAO;
import it.tn.unitn.pw.progetto.SSN.DAO.common.exception.DAOException;
/**
 *
 * @author Brian
 */
public interface SSProvinciale_DAO extends DAO<SSProvinciale, Integer>{
/*============================================================================*/    
/*                         METODO INSERT                                      */
/*============================================================================*/
    public Integer insert(SSProvinciale ssp)throws DAOException;
/*============================================================================*/    
/*============================================================================*/    
/*                         METODO UPDATE                                      */
/*============================================================================*/
    public void update(SSProvinciale ssp)throws DAOException;
    public void update_Password(Integer id,String Password )throws DAOException;
/*============================================================================*/
/*============================================================================*/    
/*                         METODO GET LOGIN                                  */
/*============================================================================*/
    public SSProvinciale getBySSProvincialeNomeAndPassword(String nome, String password)throws DAOException;
    public String getSaltSSProvincialeByNome(String nome)throws DAOException;
/*============================================================================*/
}
